<?php
include_once('core/helpers/FDebug.php');
define('YII_DEBUG',true);
/**
 * Входная точка приложения
 */
/**
 * подключаем основные файлы движка
 */
include('core/include.php');

/**
 * инициализация конфига
 */
$test = FConfig::getInstance();

/**
 * инициализация конфига и фреймверка
 */
FCore::getInstance()->initFramework();
/**
 * подключение БД
 */
FCore::getInstance()->initDatabase(FConfig::$dbName, FConfig::$dbUser, FConfig::$dbPass);
/**
 * запуск движка на выполнение
 */
//Yii::createWebApplication(FConfig::$yiiOptions)->run();
FCore::getInstance()->run();