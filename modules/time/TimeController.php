<?php

/**
 * Class TimeController
 *
 * @property Time $model
 */
class TimeController extends FController {

    public function checkFriendlyUrl($url)
    {
        return false;
    }

    public function getMetaData()
    {
        return null;
    }

    public function getTemplate($activePage)
    {
        return 'default';
    }

    public function actionTimer()
    {
        return $this->render('timer', array(
            'render_type'=>self::TEMPLATE_EXTENSION_SMARTY,
            'timer'=>$this->model->findByAttributes(array('timeEnd'=>null, 'time'=>null, 'userId'=>FCore::app()->user->id)),
        ));
    }

    public function actionStartTimer($taskId)
    {
        if($this->model->startTimer($taskId))
        {
            $this->redirect(FCore::app()->request->urlReferrer);
        }
        else
        {
            throw new Exception('Ошибка запуска таймера');
        }
    }

    public function actionDelete($id)
    {
        if($this->model->findByPk($id)->delete())
        {
            $this->redirect(FCore::app()->request->urlReferrer);
        }
    }

    public function actionStopTimer($id)
    {
        if($this->model->findByPk($id)->stopTimer())
        {
            $this->redirect(FCore::app()->request->urlReferrer);
        }
        else
        {
            throw new Exception('Ошибка остановки таймера');
        }
    }

    public function actionList($from, $to, $user)
    {
        return $this->render('list', array(
            'render_type'=>self::TEMPLATE_EXTENSION_SMARTY,
            'items'=>$this->model->getAll(array(
                'criteria'=>array(
                    'condition'=>'t.timeStart>'.$from.' AND t.timeEnd < '.$to.' AND userId='.$user,
                    'with'=>array('task'),
                )
            ))
        ));
    }


    public function actionAdd($Time = null)
    {
        echo 'time::add';
        FDebug::printTimer();
        $model = $this->model->loadModel($Time);
        FDebug::printTimer();
        return $this->render('add', array(
            'render_type'=>self::TEMPLATE_EXTENSION_SMARTY,
            'model'=>$model,
        ));
    }
} 