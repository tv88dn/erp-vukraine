<ul class="list-group"">
    {foreach from=$items->data item=row}
        <li class="list-group-item" onclick="$(this).toggleClass('active')">
            <div class="time-head">
                <div class="dropdown right time-control">
                    <a class="glyphicon glyphicon-th  text-info" id="timeItemMenu" aria-expanded="true" data-toggle="dropdown"></a>
                    <ul class="dropdown-menu" aria-labelledby="timeItemMenu">
                        <li><a href="/time/delete?id={$row->id}">Удалить</a></li>
                    </ul>
                </div>
                <i class="glyphicon glyphicon-time"></i> <b>{$row->time}</b>
                <i>{$row->task->title}</i>
            </div>
            {$row->comment}

        </li>
    {/foreach}
</ul>