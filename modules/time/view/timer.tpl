<div id="timerControl" class="left timer-control dropdown">
    {if $timer}
        <input type="hidden" id="timeDiff" value="{$timer->timerValue}"/>
        <a href="/time/stopTimer?id={$timer->id}" class="glyphicon glyphicon-stop btn"></a>
    {else}
        <a href="#" da class="glyphicon glyphicon-play btn" id="taskSelect" aria-expanded="true" data-toggle="dropdown"></a>
    {/if}
    <span title="" id="timerScreen" class="timer-screen btn"></span>
    <ul class="dropdown-menu" aria-labelledby="taskSelect">
        {foreach from=$this->model->userOpenTasks item=task}
            <li class="dropdown sub-tasks-select">
                <a href="/time/startTimer?taskId={$task->id}" >{$task->title}</a>
                {if $task->subTasks}
                    <ul class="dropdown-menu">
                        {foreach from=$task->subTasks item=subTask}
                            <li><a href="/time/startTimer?taskId={$subTask->id}">{$subTask->title}</a></li>
                        {/foreach}
                    </ul>
                {/if}


            </li>


        {/foreach}
    </ul>
</div>