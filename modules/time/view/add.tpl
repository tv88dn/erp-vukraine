<div class="col-lg-10 col-lg-offset-1">
    <form action="/time/add" method="post" class="form-horizontal">
        {if $model->hasErrors()}
            {foreach from=$model->getErrors() item=error}
                {for $var=0 to count($error)-1}
                    <div class="alert alert-error">
                        <a class="close" data-dismiss="alert" href="#">×</a>{$error[$var]}
                    </div>
                {/for}
            {/foreach}
        {/if}
        {if $model->id}
            {FBootStrap::activeHiddenField($model, 'id')}
        {/if}
        {FBootStrap::activeHiddenField($model, 'taskId')}
        {FBootStrap::activeHiddenField($model, 'userId')}
        {FBootStrap::activeHiddenField($model, 'timeStart')}
        {FBootStrap::activeHiddenField($model, 'timeEnd')}
        <div class="form-group warp">
            {FBootStrap::activeTimeField($model, 'time')}
        </div>
        <div class="form-group warp">
            {FBootStrap::activeDropDownList($model, 'taskId', FBootStrap::listData($model->userOpenTasks, 'id', 'title'), ['class'=>'form-control'])}
        </div>
        <div class="form-group warp">
            {FBootStrap::activeEditField($model, 'comment', ['class'=>'form-control'])}
        </div>
        <div class="col-lg-12 text-center">
            {if $model->id}
                {FBootStrap::btnDangerLink('Удалить', "/time/delete?id=`$model->id`")}
            {/if}
            {FBootStrap::btnSuccessSubmit('Сохранить')}
        </div>
    </form>
</div>

