<?php
/**
 * Created by PhpStorm.
 * User: tv88d_000
 * Date: 15.11.2014
 * Time: 15:14
 */

class Time extends BaseTimeSections {

    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    public function beforeValidate()
    {
        if(!$this->userId)
        {
            $this->userId = FCore::app()->user->getState('id');
        }
        if(!$this->timeStart)
        {
            $this->timeStart = time();
        }
        if($this->time && !$this->timeEnd)
        {
            $this->timeEnd = $this->timeStart;
        }
        return parent::beforeValidate();
    }

    public function toIntervalString()
    {
        $string = explode(':', $this->time);
        return DateInterval::createFromDateString($string[0].' hourse '.$string[1].' minutes');
    }

    public static function addTimeSection($taskId, $time)
    {
        $model = new Time();
        $model->taskId = $taskId;
        $model->userId = FCore::app()->user->id;
        $model->time = $time;
        return $model->save();
    }

    public static function getTaskTime($taskId)
    {
        $timeItems = Time::model()->findAllByAttributes(array('taskId'=>$taskId, 'userId'=>FCore::app()->user->id));
        $time = new DateTime();
        $base = new DateTime();
        for($i = 0; $i < count($timeItems); $i++)
        {
            if($timeItems[$i]->time)
            {
                $time->add($timeItems[$i]->toIntervalString());
            }

        }
        return $time->diff($base)->format('%H:%I');
    }

    public function getUserOpenTasks()
    {
        if(FCore::app()->user->getState('id'))
        {
            return FCore::getInstance()->getControllerByName('TaskController')->model->getAll(array(
                'criteria'=>array(
                    'condition'=>'t.responsibleId='.FCore::app()->user->id,
                    'with'=>array(
                        'detailStatus'=>array(
                            'condition'=>'detailStatus.data="view" OR detailStatus.data="add" OR detailStatus.data="execute"'
                        ),
                        'subTasks',
                    )
                ),
                'pagination'=>1000,
            ))->getData();
        }

    }

    public function getTimerValue()
    {
        return time() - $this->timeStart;
    }

    public function startTimer($taskId)
    {
        $activeTimer = $this->findByAttributes(array('timeEnd'=>null, 'time'=>null));
        if($activeTimer)
        {
            $activeTimer->stopTimer();
        }
        $model = new Time();
        $model->taskId = $taskId;
        return $model->save();
    }

    public function stopTimer()
    {
        $this->timeEnd = time();
        $this->time = DateTime::createFromFormat('U', $this->timeStart)->
            diff(DateTime::createFromFormat('U', $this->timeEnd))->
            format('%H:%I');
        return $this->save();
    }

    public function relations()
    {
        return array(
            'task'=>array(self::HAS_ONE, 'Task', array('id'=>'taskId')),
            'user'=>array(self::HAS_ONE, 'User', array('id'=>'userId')),
        );
    }
} 