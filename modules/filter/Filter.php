<?php
/**
 * Created by PhpStorm.
 * User: tv88d_000
 * Date: 15.09.14
 * Time: 23:04
 */

class Filter extends FilterBase
{
    public $attachModel;
    public $filter;
    public $sort;
    protected $_module;
    protected $_action;

    public function relations()
    {
        return array(
            'fields'=>array(self::HAS_MANY, 'FilterFields', array('filterId'=>'id')),
        );
    }

    public function attachFilter($module, $action)
    {
        $this->_module = $module;
        $this->_action = $action;
        $this->filter = $this->with('fields')->findByAttributes(array('module'=>$module, 'action'=>$action));
        return $this->filter;
    }

    public function attachSort($module, $action)
    {
        $this->sort = call_user_func_array(array($module, 'model'), array());
        return $this->sort;
    }

    public static function getFilterFieldValueFromRequest($module, $action, $name)
    {
        $filter = FCore::getInstance()->request->getParam('Filter');
        if (isset($filter[$module][$name])) {
            return $filter[$module][$name];
        }
        return null;
    }

    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    protected static function addRelation($model, $attribute, $name)
    {
        $parent = DetailsParent::model()->with('type')->find(array('condition'=>'t.module="'.get_class($model).'" AND type.fieldName = "'.$attribute.'"'));
        if($parent)
        {
            $model->getMetaData()->addRelation($name, array(
                self::HAS_ONE,
                'Details',
                array('moduleId'=>'id'),
                'condition'=>$name.'.parentId='.$parent->id,
                'together'=>true,
            ));
            return true;
        }
        else return false;
    }

    protected static function processBaseProperty($model, $filter)
    {
        $criteria = new CDbCriteria();
        foreach($filter as $attribute=>$value)
        {
            $model->validateDetail($attribute);
            $value = $model->$attribute;
            if(isset($model->attributes[$attribute]))
            {
                $criteria->addSearchCondition($attribute, $value);
            }
            else
            {

                if(self::addRelation($model, $attribute, $attribute.'Details'))
                {
                    $criteria->with[] = $attribute.'Details';
                    if(is_array($value) && isset($value['from']) && isset($value['to']))
                    {
                        $criteria->addCondition($attribute.'Details.data>"'.$value['from'].'"');
                        $criteria->addCondition($attribute.'Details.data<"'.$value['to'].'"');
                    }
                    else
                    {
                        $criteria->compare($attribute.'Details.data', $value);
                    }
                }
            }
        }
        return $criteria;
    }

    public static function processFilter($model)
    {
        $filter = FCore::getInstance()->request->getParam('Filter');
        if($filter)
        {
            if(isset($filter[get_class($model)]))
            {
                $criteria = new CDbCriteria();
                foreach($filter as $modelName=>$value)
                {
                    if(get_class($model) == $modelName)
                    {
                        $criteria->mergeWith(self::processBaseProperty($model, $value));
                    }
                }
                return $criteria;
            }
            else
            {
                return new CDbCriteria();
            }
        }
        else return new CDbCriteria();
    }

    public static function processSort($model)
    {
        $modelRequestFields = FCore::getInstance()->request->getParam(get_class($model));
        $criteria = new CDbCriteria();
        if(isset($modelRequestFields['sort']))
        {
            $model->sort = $modelRequestFields['sort'];
            list($attribute, $order) = explode('.', $modelRequestFields['sort']);

            if(isset($model->attributes[$attribute]))
            {
                $criteria->order = $attribute.' '.$order;
            }
            else
            {
                if(self::addRelation($model, $attribute, $attribute.'SortDetails'))
                {
                    $criteria->with[] = $attribute.'SortDetails';
                    $criteria->order = $attribute.'SortDetails.data '.$order;
                }
            }
        }
        return $criteria;
    }


    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'name' => 'Название фильтра',
            'module' => 'Модуль',
            'action' => 'Действие (action)',
        );
    }

    public function beforeDelete()
    {
        $res = parent::beforeDelete();
        if($res)
        {
            return FilterFields::model()->deleteAll('filterId='.$this->id);
        }
        else
        {
            return $res;
        }

    }

    public function getExternalModuleAttributes($model)
    {
        $baseModelName = ucfirst($model);
        $baseModel = new $baseModelName;
        $moduleAttributes = array();
        foreach($baseModel->getAllAttributes() as $name=>$val)
        {
            $moduleAttributes[$name] = $name;
        }
        return $moduleAttributes;
    }



} 