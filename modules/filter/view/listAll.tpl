<div class="panel panel-primary warp">
    <div class="panel-heading">
        <h1>Фильтры</h1>
    </div>
    <div class="panel-body">
        <div class="col-lg-6">
            <table class="table table-striped table-bordered table-hover">
                <tr>
                    {foreach from=$items->model->getAllAttributeLabels() item=label}
                        <th>{$label}</th>
                    {/foreach}
                    <th></th>
                </tr>
                {foreach from=$items->getData() item=row}
                    <tr class="active">
                        {foreach from=$row->getAllAttributes() item=attribute}
                            {if is_array($attribute)}
                                <td>{implode(', ', $attribute)}</td>
                            {else}
                                <td>{$attribute}</td>
                            {/if}
                        {/foreach}
                        <td class="control-group">
                            {FBootStrap::linkGroup(
                            [
                            ['url'=>"/filter/edit?id=`$row->id`", 'text'=>FBootStrap::icon('edit')],
                            ['url'=>"/filter/delete?id=`$row->id`", 'text'=>FBootStrap::icon('remove')]
                            ]
                            )}
                        </td>
                    </tr>
                {/foreach}

            </table>
        </div>
        {$add}
    </div>
</div>