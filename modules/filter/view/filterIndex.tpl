<h3>Фильтр</h3>
<div class="filter-block">
    <form class="form-horizontal" method="get">
        {for $i = 0 to count($filter->fields)-1}
            <div class="col-lg-3">
                {FBootStrap::formGroup(
                $filter->fields[$i]->getFieldModel(),
                $filter->fields[$i]->attribute,
                $filter->fields[$i]->type,
                $filter->fields[$i]->getUniqueFields(),
                [
                'control'=>[
                'name'=>$filter->fields[$i]->getRequestName(),
                'width'=>8
                ],
                'label'=>[
                'width'=>4
                ]
                ]
                )}
            </div>
        {/for}
        <div class="col-lg-6 col-lg-offset-1 add-company-btn-group">
            {FBootStrap::btnPrimarySubmit('Искать')}
        </div>
        <div class="clearfix"></div>
    </form>
</div>