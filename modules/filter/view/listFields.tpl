<div class="panel panel-info warp">
    <div class="panel-heading">
        <h1>Поля фильтра</h1>
    </div>
    <div class="panel-body">
        <div class="col-lg-12">
            <table class="table table-striped table-bordered table-hover">
                <tr>
                    {foreach from=$items->model->getAllAttributeLabels() item=label}
                        <th>{$label}</th>
                    {/foreach}
                    <th></th>
                </tr>
                {foreach from=$items->getData() item=row}
                    <tr class="active">
                        {foreach from=$row->getAllAttributes() item=attribute}
                            {if is_array($attribute)}
                                <td>{implode(', ', $attribute)}</td>
                            {else}
                                <td>{$attribute}</td>
                            {/if}
                        {/foreach}
                        <td class="control-group">
                            {FBootStrap::linkGroup(
                            [
                            ['url'=>"/filter/editField?id=`$row->id`", 'text'=>FBootStrap::icon('edit')],
                            ['url'=>"/filter/deleteField?id=`$row->id`", 'text'=>FBootStrap::icon('remove')]
                            ]
                            )}
                        </td>
                    </tr>
                {/foreach}

            </table>
        </div>
    </div>
</div>