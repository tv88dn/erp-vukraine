<div class="col-lg-10">
    <div class="panel panel-info warp">
        <div class="panel-heading">
            {if $model->id}
                <h1>Редактировать поле фильтр #{$model->id}</h1>
            {else}
                <h1>Добавить Фильтр</h1>
            {/if}
        </div>
        <div class="panel-body">
            <form class="form-horizontal" action="/filter/addField" method="post">
                {if $model->hasErrors()}
                    {foreach from=$model->getErrors() item=error}
                        {for $var=0 to count($error)-1}
                            <div class="alert alert-error">
                                <a class="close" data-dismiss="alert" href="#">×</a>{$error[$var]}
                            </div>
                        {/for}
                    {/foreach}
                {/if}
                {FBootStrap::formGroup($model, 'id', 'activeHiddenField')}
                {FBootStrap::formGroup($model, 'filterId', 'activeHiddenField')}
                {FBootStrap::formGroup($model, 'model', 'activeDropDownList', $models)}
                {FBootStrap::formGroup($model, 'attribute', 'activeDropDownList', $attributes)}
                {FBootStrap::formGroup($model, 'type', 'activeDropDownList', $types)}
                <div class="col-lg-9 col-lg-offset-2">
                    {if $model->id}
                        {FBootStrap::btnDangerLink('Удалить', "/details/deleteType?id=`$model->id`")}
                    {else}
                        {FBootStrap::btnDefaultLink('Отмена', "/details/editType?id=`$model->id`")}
                    {/if}
                    {FBootStrap::btnPrimarySubmit('Сохранить')}
                </div>
            </form>
        </div>
    </div>
</div>