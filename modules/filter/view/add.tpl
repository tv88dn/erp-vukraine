<div class="col-lg-6">
    <div class="panel panel-primary warp">
        <div class="panel-heading">
            {if $model->id}
                <h1>Редактировать Фильтр #{$model->id} ({$model->name})</h1>
            {else}
                <h1>Добавить Фильтр</h1>
            {/if}
        </div>
        <div class="panel-body">
            <form class="form-horizontal" action="/filter/add" method="post">
                {if $model->hasErrors()}
                    {foreach from=$model->getErrors() item=error}
                        {for $var=0 to count($error)-1}
                            <div class="alert alert-error">
                                <a class="close" data-dismiss="alert" href="#">×</a>{$error[$var]}
                            </div>
                        {/for}
                    {/foreach}
                {/if}
                {FBootStrap::formGroup($model, 'id', 'activeHiddenField')}
                {FBootStrap::formGroup($model, 'name', 'activeTextField')}
                {FBootStrap::formGroup($model, 'module', 'activeDropDownList', $modules)}
                {FBootStrap::formGroup($model, 'action', 'activeDropDownList', $actions)}
                <div class="col-lg-9 col-lg-offset-2">
                    {if $model->id}
                        {FBootStrap::btnDangerLink('Удалить', "/details/deleteType?id=`$model->id`")}
                    {else}
                        {FBootStrap::btnDefaultLink('Отмена', "/details/editType?id=`$model->id`")}
                    {/if}
                    {FBootStrap::btnPrimarySubmit('Сохранить')}
                </div>
            </form>
        </div>
    </div>
    {if isset($listFields)}
        {$listFields}
    {/if}
</div>
<div class="col-lg-6">
    {if isset($addFields)}
        {$addFields}
    {/if}
</div>
