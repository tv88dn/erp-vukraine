<?php
/**
 * Created by PhpStorm.
 * User: tv88d_000
 * Date: 16.09.14
 * Time: 2:08
 */

class FilterFields extends BaseFilterFields
{
    protected  $fieldModel;

    public $uniqueDetails;

    public $value;

    public function getFieldModel()
    {
        if(!$this->fieldModel)
        {
            $this->fieldModel = new $this->model;
            $this->value = FCore::getInstance()->request->getParam('Filter');

            $this->value = (isset($this->value[$this->model][$this->attribute])) ? $this->value[$this->model][$this->attribute] : '';
        }
        return $this->fieldModel;
    }

    public function getRequestName()
    {
        return 'Filter['.get_class($this->getFieldModel()).']['.$this->attribute.']';
    }

    public function getUniqueFields()
    {
        if($this->type == 'activeTextField')
        {
            return array();
        }

        $criteria = new CDbCriteria();
        $criteria->with['type'] = array('condition'=>'fieldName="'.$this->attribute.'"');
        $criteria->addCondition('module="'.$this->model.'"');


        $detailModel = DetailsParent::model()->find($criteria);

        $criteria = new CDbCriteria();
        $criteria->condition = 'parentID='.$detailModel->id;
        $criteria->group = 'data';
        $unique = Details::model()->findAll($criteria);
        $prepareArray = array();
        for($i = 0; $i < count($unique); $i++)
        {
            $prepareArray[$unique[$i]->data] = $unique[$i]->data;
        }
        return $prepareArray;
    }


    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

} 