<?php

/**
 * Class FilterController
 *
 * @property Filter $model
 */
class FilterController extends FController implements FComponentInterface
{
    public function checkFriendlyUrl($url)
    {
        return false;
    }

    public function getMetaData()
    {
        return null;
    }

    public function getTemplate($activePage)
    {
        return 'default';
    }

    public function actionControl()
    {
        return '<li class="dropdown"><a href="/filter/listAll">Фильтры</a></li>';
    }

    public function actionEdit($id)
    {
        $model = $this->model->findByPk($id);
        $modules = FCore::getInstance()->getModules();
        return $this->render('add', array(
            'render_type'=>self::TEMPLATE_EXTENSION_SMARTY,
            'model'=>$model,
            'modules'=>$modules,
            'actions'=>FCore::getInstance()->getModuleActions(($model->module) ? $model->module : reset($modules)),
            'addFields'=>$this->actionAddField($model->id),
            'listFields'=>$this->actionListFields($model->id),
        ));
    }

    public function actionAdd($Filter = null)
    {
        $model = $this->model->loadModel($Filter);
        $modules = FCore::getInstance()->getModules();
        return $this->render('add', array(
            'render_type'=>self::TEMPLATE_EXTENSION_SMARTY,
            'model'=>$model,
            'modules'=>$modules,
            'actions'=>FCore::getInstance()->getModuleActions(($model->module) ? $model->module : reset($modules)),
        ));
    }

    public function actionGetModuleActions($moduleName)
    {
        return json_encode(FCore::getInstance()->getModuleActions($moduleName));
    }

    public function actionListAll()
    {
        return $this->render('listAll', array(
            'render_type'=>self::TEMPLATE_EXTENSION_SMARTY,
            'items'=>$this->model->getAll(),
            'add'=>$this->actionAdd(),
        ));
    }

    public function actionAddField($filterId = null, $FilterFields = null)
    {
        $model = FilterFields::model()->loadModel($FilterFields);
        if($filterId)
        {
            $model->filterId = $filterId;
        }
        $filter = $this->model->findByPk($model->filterId);
        $models = FCore::getInstance()->getModuleModels($filter->module);
        return $this->render('addField', array(
            'render_type'=>self::TEMPLATE_EXTENSION_SMARTY,
            'model'=>$model,
            'types'=>FBootStrap::getTypes(),
            'models'=>$models,
            'attributes' => $filter->getExternalModuleAttributes(reset($models)),
        ));
    }

    public function actionEditField($id)
    {
        $model = FilterFields::model()->findByPk($id);
        return $this->actionAddField(null, $model->attributes);
    }

    public function actionListFields($filterId)
    {
        return $this->render('listFields', array(
            'render_type'=>self::TEMPLATE_EXTENSION_SMARTY,
            'items'=>FilterFields::model()->getAll(array('criteria'=>array('condition'=>'filterId='.$filterId))),
        ));
    }

    public function actionDeleteField($id)
    {
        $field = FilterFields::model()->findByPk($id);
        $parentId = $field->filterId;
        if($field->delete())
        {
            $this->redirect('/filter/edit?id='.$parentId);
        }
    }

    public function actionAttachFilter($module, $action)
    {
        return $this->render('filter', array(
            'render_type'=>self::TEMPLATE_EXTENSION_SMARTY,
            'filter'=>$this->model->attachFilter($module, $action),
        ));
    }

    public function actionAttachSort($module, $action)
    {
        return $this->render('filter', array(
            'render_type'=>self::TEMPLATE_EXTENSION_SMARTY,
            'sort'=>$this->model->attachSort($module, $action),
        ));
    }

} 