<?php

/**
 * Class Activity
 *
 * Базовый класс модели активностей.
 * Служит для сохранения сущности события.
 */
class Activity extends ActivityBase {

    /**
     * @param string $className
     * @return Activity mixed
     *
     * Перегрузка метода модели для корректного использования класса.
     */
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    public function relations()
    {
        return array(
            'task'=>array(self::HAS_ONE, 'Task', array('id'=>'targetId')),
        );
    }

    public static function getScenarioType($scenario)
    {
        switch($scenario)
        {
            case 'insert': return 'добавил';
            case 'update': return 'изменил';
            case 'delete': return 'удалил';
        }
        return '';
    }

    /**
     * @param FActiveRecord $model
     *
     * Добавление действия в активность
     * @param $text
     * @return bool
     */
    public function addActivity(FActiveRecord $model, $text)
    {
        $activity = new Activity();
        $activity->userId = FCore::app()->user->id;
        $activity->targetId = $model->id;
        $activity->module = get_class($model);
        $activity->setAttribute('scenario', $model->getScenario());
        $activity->text = $text;
        return $activity->save();
    }

} 