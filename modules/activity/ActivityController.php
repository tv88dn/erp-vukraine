<?php

/**
 * Class ActivityController
 *
 * @property Activity $model
 *
 * Класс предназначен для информирования пользователей об активности в сферах их интересов
 */
class ActivityController extends FController {

    /**
     * Функция возвращает набор правил, для вывода activity
     *
     * @return array
     */
    public function activityShowRule()
    {
        return array(
            'task'=>'task.responseId='.FCore::app()->user->id,
        );
    }

    public function actionList()
    {
        $criteria = new CDbCriteria();
        foreach($this->activityShowRule() as $relation=>$condition)
        {
            $criteria->with[] = array($relation=>$condition);
        }

        return $this->render('list',array(
            'render_type'=>self::TEMPLATE_EXTENSION_SMARTY,
            'items'=>$this->model->getAll(array('criteria'=>$criteria))
        ));
    }

    /**
     * @param FActiveRecord $model
     *
     * Добавление действия в активность
     */
    public function addActivity(FActiveRecord $model)
    {
        if($model instanceof FActivityInterface)
        {
            $rules = $model->activityRule();
            if(isset($rules[$model->dbAction]))
            {
                $rule = $rules[$model->dbAction];
                $res = $this->model->addActivity(
                    $model,
                    $this->render($rule['view'],
                        array('render_type'=>self::TEMPLATE_EXTENSION_SMARTY,
                            'model'=>$model,
                            'rule'=>$rule,
                        )
                    )
                );
                if(!$res)
                {
                    var_dump($this->model->getErrors());
                    die();
                }
            }
        }


    }

} 