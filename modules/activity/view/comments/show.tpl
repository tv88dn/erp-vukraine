<div class="activity-item {$rule['type']}">
    <div class="activity-title"><span>пользователь</span>
        <img class="contact-avatar" src="/{User::$pathToAvatar}{FCore::app()->user->getState('userModel')->avatar}" alt="Автор задачи"/>
        <span>{FCore::app()->user->getState('userModel')->name} {$rule['text']} комментарий к задаче</span>
    </div>
    <div class="activity-body"><a href="/task/view?id={$model->target->id}"> {$model->target->title}</a></div>
    <div class="clearfix"></div>
</div>