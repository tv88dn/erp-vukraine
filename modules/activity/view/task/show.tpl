<div class="activity-item {$rule['type']}">
    <div class="activity-title"><span>пользователь</span>
        <img class="contact-avatar" src="/{User::$pathToAvatar}{FCore::app()->user->getState('userModel')}" alt="Автор задачи"/>
        <span>{FCore::app()->user->getState('userModel')->name} {$rule['text']} задачу</span>
    </div>
    {if $model->dbAction == 'delete'}
        <div class="activity-body">{$model->title}</div>
    {else}
        <div class="activity-body"><a href="/task/view?id={$model->id}"> {$model->title}</a></div>
    {/if}
    <div class="activity-status"><span class="label label-default">{$model->getFromDictionary('status')}</span></div>
    <div class="clearfix"></div>
</div>