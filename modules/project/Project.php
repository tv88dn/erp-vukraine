<?php
/**
 * Created by PhpStorm.
 * User: tv88d_000
 * Date: 27.09.2014
 * Time: 0:53
 */

class Project extends ProjectBase
{
    private $worker, $projectManager;
    /**
     * Статический метод, возвращающий єкземпляр модели
     * @param string $className имя класса модели
     * @return ProjectBase єкземпляр класса модели
     */
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    public function relations()
    {
        return array(
            'company' => array(self::HAS_ONE, 'Company', array('id'=>'companyId')),
        );
    }

    /**
     * @return array массив пользовательских имен полей
     */
    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'companyId' => 'проект для компании',
            'companyName' => 'Название компании',
            'title' => 'Название проекта',
        );
    }

    public function setFileAttributes($File)
    {
        $File['targetId'] = $this->id;
        $File['moduleName'] = get_class($this);
        $File['projectId'] = $this->id;
        return $File;
    }

    /**
     * Получает модель компании, связанной с проектом
     *
     * @return Company
     */
    public function getCompany()
    {
        return Company::model()->findByPk($this->companyId);
    }

    /**
     * Возвращает название компании
     *
     * @return string
     */
    public function getCompanyName()
    {
        return ($company = $this->getCompany()) ? $company->name : '';
    }

    public function getProgrammers()
    {
        $managers = User::model()->getAll(array('criteria'=>array('with'=>array('userContact'))))->getData();
        $managersList = array();
        for($i = 0; $i < count($managers); $i++)
        {
            //var_dump();
            if($managers[$i]->group == 'programmers')
            {
                $managersList[$managers[$i]->id] = $managers[$i]->name;
            }
        }
        return $managersList;
    }

    public function getManagers()
    {
        $managers = User::model()->getAll(array('criteria'=>array('with'=>array('userContact'))))->getData();
        $managersList = array();
        for($i = 0; $i < count($managers); $i++)
        {
            //var_dump();
            if($managers[$i]->group == 'admins' || $managers[$i]->group == 'managers')
            {
                $managersList[$managers[$i]->id] = $managers[$i]->name;
            }
        }
        return $managersList;
    }

    public function getTimeRemain()
    {
        if($this->dateStart && $this->dateEnd)
        {
            $start = new DateTime();
            $end = new DateTime();
            return date_diff($start->setTimestamp(strtotime($this->dateEnd)), $end->setTimestamp(time()))->days;
        }
        else
        {
            return '';
        }
    }

    public function getWorker()
    {
        if($this->defaultWorker)
        {
            if(!$this->worker)
            {
                $this->worker = User::model()->findByPk($this->defaultWorker);
            }
            return $this->worker;
        }
        else
        {
            return '';
        }
    }

    public function getProjectManager()
    {
        if($this->manager)
        {
            if(!$this->projectManager)
            {
                $this->projectManager = User::model()->findByPk($this->manager);
            }
            return $this->projectManager;
        }
        else
        {
            return '';
        }
    }


} 