<div class="panel panel-primary warp">
    <div class="panel-heading">
        <h1 class="left">{$model->title}</h1>
        <div class="clearfix"></div>
        (<i class="text-info"><i class="glyphicon glyphicon-cog"></i>{$model->getFromDictionary('type')}</i>)
        <div class="text-muted right">
            <span>Запущен для компании</span>
            <a class="text-info" href="/company/view?id={$model->companyId}">{$model->company->name}</a>
        </div>
        <div class="clearfix warp"></div>
    </div>
    <div class="panel-body">
        <div class="col-lg-5">
            <div class="well col-lg-12">
                <b>Описание: </b>
                <p>{$model->description}</p>
            </div>
            <legend><i class="glyphicon glyphicon-calendar warp"></i>Сроки</legend>
            <div class="col-lg-6">
                <i class="glyphicon glyphicon-time text-success"></i>
                <span><span class="text-muted">Старт:</span> {$model->dateStart}</span>
            </div>
            <div class="col-lg-6">
                <i class="glyphicon glyphicon-time text-danger"></i>
                <span><span class="text-muted">Финиш:</span> {$model->dateEnd}</span>
                <div class="text-danger">(Осталось дней: {$model->timeRemain})</div>
            </div>
            <div class="clearfix"></div>
            <div class="text-muted">Ход выполнения</div>
            <div class="progress progress-striped active">
                <div class="progress-bar progress-bar-success" style="width: 75%"></div>
            </div>
            <div class="right warp project-progress-info">
                7 из 10 задач
            </div>
            <div class="clearfix"></div>
            <legend><i class="glyphicon glyphicon-user warp"></i>Команда</legend>
            <div class="col-lg-6">
                <img class="contact-avatar left" src="/{User::$pathToAvatar}{$model->projectManager->avatar}" alt="Программист"/>
                <span>{$model->projectManager->name}</span>
                <i>менеджер</i>
            </div>
            <div class="col-lg-6">
                <img class="contact-avatar left" src="/{User::$pathToAvatar}{$model->worker->avatar}" alt="Программист"/>
                <span>{$model->worker->name}</span>
                <i>программист</i>
                <div>
                    <i class="glyphicon glyphicon-envelope text-info"></i>
                    <a class="text-info" href="mailto:{$model->worker->mail}"><b>{$model->worker->mail}</b></a>
                </div>
            </div>
            <div class="clearfix warp"></div>
            <br/>
            <legend><i class="glyphicon glyphicon-usd warp"></i>Финансы</legend>
            <div class="project-finance">
                <div>
                    <span class="text-muted">Бюджет:</span>
                    <b>{$model->budget} грн</b>
                </div>
                <div>
                    <span class="text-muted">Оплата часа работы программиста:</span>
                    <b>{$model->pay} грн</b>
                </div>
            </div>
            <legend><i class="glyphicon glyphicon-minus-sign warp"></i>Затраты</legend>
        </div>
        <div class="col-lg-7">
            <ul class="nav nav-tabs">
                <li class="active">
                    <a href="#files" data-toggle="tab">Файлы</a>
                </li>
                <li>
                    <a href="#appeals" data-toggle="tab">Обращения</a>
                </li>
                <li>
                    <a href="#tasks" data-toggle="tab">Задачи</a>
                </li>
                <li>
                    <a href="#payments" data-toggle="tab">Оплаты</a>
                </li>
            </ul>
            <div class="tab-content well">
                <div class="tab-pane active" id="files">
                    <a href="/file/add?companyId={$model->id}" class="btn btn-info">
                        <i class="glyphicon glyphicon-file"></i>
                        Новый файл
                    </a>
                    {if isset($listFiles)}
                        <div class="clearfix warp"></div>
                        {$listFiles}
                    {/if}
                    <div class="clearfix"></div>
                </div>
                <div class="tab-pane fade" id="contacts">
                    <a href="/contact/add?companyId={$model->id}" class="btn btn-info">
                        <i class="glyphicon glyphicon-user"></i>
                        Новый контакт
                    </a>
                    {if isset($listContacts)}
                        <div class="clearfix"></div>
                        {$listContacts}
                    {/if}
                    <div class="clearfix"></div>
                </div>
                <div class="tab-pane fade" id="appeals">
                    <a class="btn btn-success left warp" href="/appeal/add?companyId={$model->companyId}&projectId={$model->id}">
                        <div class="glyphicon glyphicon-transfer"></div>
                        Добавить обращение
                    </a>
                    {if isset($listAppeals)}
                        <div class="clearfix"></div>
                        {$listAppeals}

                    {/if}
                    <div class="clearfix"></div>
                </div>
                <div class="tab-pane fade" id="tasks">
                    <a class="btn btn-success left warp" href="/task/add?companyId={$model->companyId}&projectId={$model->id}">
                        <div class="glyphicon glyphicon-tasks"></div>
                        Добавить задачу
                    </a>
                    {if isset($listTasks)}
                        <div class="clearfix"></div>
                        {$listTasks}
                    {/if}
                    <div class="clearfix"></div>
                </div>
                <div class="tab-pane fade" id="payments">
                    <a class="btn btn-success left warp" href="/payment/add?companyId={$model->companyId}&projectId={$model->id}">
                        <div class="glyphicon glyphicon-usd"></div>
                        Добавить оплату
                    </a>
                    <div class="clearfix"></div>
                    {if isset($listPayments)}
                        {$listPayments}
                    {/if}
                </div>
            </div>
        </div>

        <div class="clerafix"></div>
    </div>
</div>

