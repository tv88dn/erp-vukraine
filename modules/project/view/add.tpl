<div class="col-lg-12">
    <div class="panel panel-info warp">
        <div class="panel-heading">
            <h1>Проект</h1>
        </div>
        <div class="panel-body">
            <form class="form-horizontal" action="/project/add?companyId={$model->companyId}" enctype="multipart/form-data" method="post">
                {if $model->hasErrors()}
                    {foreach from=$model->getErrors() item=error}
                        {for $var=0 to count($error)-1}
                            <div class="alert alert-error">
                                <a class="close" data-dismiss="alert" href="#">×</a>{$error[$var]}
                            </div>
                        {/for}
                    {/foreach}
                {/if}

                {FBootStrap::activeHiddenField($model, 'id')}
                {FBootStrap::activeHiddenField($model, 'companyId')}
                {FBootStrap::formGroup($model, 'companyName', 'activeTextField', [], [
                'control'=>['width'=>5, 'disabled'=>'disabled'],
                'label'=>['width'=>2]
                ])}

                {FBootStrap::formGroup($model, 'title', 'activeTextField', [], [
                'control'=>['width'=>8],
                'label'=>['width'=>2]
                ])}

                <div class="warp">
                    {FBootStrap::formGroup($model, 'type', 'activeDropDownList', $model->getDictionaryRows('type'), [
                    'control'=>['width'=>3],
                    'label'=>['width'=>2]
                    ])}
                </div>



                <div class="well warp">
                    <div class="col-lg-6 project-additional-block">
                        <legend><i class="glyphicon glyphicon-calendar"></i> Сроки</legend>
                        <div class="col-lg-6">
                            {FBootStrap::formGroup($model, 'dateStart', $model->getHtmlType('dateStart'), $model->getDictionaryRows('dateStart'), [
                            'control'=>['width'=>8],
                            'label'=>['width'=>5],
                            'addon'=>[
                            'left'=>'<span class="input-group-addon glyphicon glyphicon-dashboard text-success"></span>'
                            ]
                            ])}
                        </div>
                        <div class="col-lg-6">
                            {FBootStrap::formGroup($model, 'dateEnd', $model->getHtmlType('dateEnd'), $model->getDictionaryRows('dateEnd'), [
                            'control'=>['width'=>8],
                            'label'=>['width'=>5],
                            'addon'=>[
                            'left'=>'<span class="input-group-addon glyphicon glyphicon-dashboard text-danger"></span>'
                            ]
                            ])}
                        </div>
                    </div>

                    <div class="col-lg-6 project-additional-block">
                        <legend><i class="glyphicon glyphicon-usd"></i> Финансы</legend>
                        {FBootStrap::formGroup($model, 'budget', $model->getHtmlType('budget'), $model->getDictionaryRows('budget'), [
                        'control'=>['width'=>4, 'class'=>'money-input'],
                        'label'=>['width'=>3],
                            'addon'=>[
                                'right'=>'<span class="input-group-addon">.00</span>'
                            ]
                        ])}
                        {FBootStrap::formGroup($model, 'pay', $model->getHtmlType('pay'), $model->getDictionaryRows('pay'), [
                        'control'=>['width'=>4, 'class'=>'money-input'],
                        'label'=>['width'=>3],
                        'addon'=>[
                        'right'=>'<span class="input-group-addon">.00</span>'
                        ]
                        ])}
                    </div>
                    <div class="clearfix"></div>
                </div>
                    {FBootStrap::formGroup($model, 'description', $model->getHtmlType('description'), $model->getDictionaryRows('description'), [
                    'control'=>['width'=>10],
                    'label'=>['width'=>2]
                    ])}
                <div class="well warp">
                    {if $model->id}
                        <div class="col-lg-6 project-additional-block">
                            <legend><i class="glyphicon glyphicon-file"></i> Файлы</legend>
                            {if isset($listFiles)}
                                {$listFiles}
                            {/if}
                            {$addFile}
                        </div>
                    {/if}


                    <div class="col-lg-6 project-additional-block">
                        <legend><i class="glyphicon glyphicon-user"></i> Команда</legend>
                        {FBootStrap::formGroup($model, 'manager', $model->getHtmlType('manager'), $model->getManagers(), [
                        'control'=>['width'=>4],
                        'label'=>['width'=>6]
                        ])}
                        {FBootStrap::formGroup($model, 'defaultWorker', $model->getHtmlType('defaultWorker'), $model->getProgrammers(), [
                        'control'=>['width'=>4],
                        'label'=>['width'=>6]
                        ])}
                    </div>

                    <div class="clearfix"></div>
                </div>

                <div class="form-group warp text-center">
                    {if $model->id}
                        {FBootStrap::btnDangerLink('Удалить', "/appeal/delete?id=`$model->id`")}
                    {else}
                        {FBootStrap::btnDefaultLink('Отмена', "/company/view?id=`$model->companyId`")}
                    {/if}
                    {FBootStrap::btnPrimarySubmit('Сохранить')}
                </div>
            </form>
        </div>
    </div>