<ul class="list-group warp col-lg-12">
    {foreach from=$items->getData() item=row}
        <li class="list-group-item panel panel-default">

            <a class="project-edit" href="/project/edit?id={$row->id}">
                <div class="glyphicon glyphicon-edit right"></div>
            </a>
            <div class="project-title panel-heading"><a href="/project/view?id={$row->id}">{$row->title}</a></div>
            <div class="panel-body">
                <div class="col-lg-6">
                    <div class="col-lg-6">
                        <i class="glyphicon glyphicon-time text-success"></i>
                        <span><span class="text-muted">Старт:</span> {$row->dateStart}</span>
                    </div>
                    <div class="col-lg-6">
                        <i class="glyphicon glyphicon-time text-danger"></i>
                        <span><span class="text-muted">Финиш:</span> {$row->dateEnd}</span>
                        <div class="text-danger">(Осталось дней: {$row->timeRemain})</div>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="text-muted">Ход выполнения</div>
                    <div class="progress progress-striped active">
                        <div class="progress-bar progress-bar-success" style="width: 75%"></div>

                    </div>
                    <div class="right warp project-progress-info">
                        7 из 10 задач
                    </div>
                </div>

                <div class="clearfix"></div>
                <div class="col-lg-6">
                    <img class="contact-avatar left" src="/{User::$pathToAvatar}{$row->projectManager->avatar}" alt="Программист"/>
                    <span>{$row->projectManager->name}</span>
                    <i>менеджер</i>
                </div>
                <div class="col-lg-6">
                    <img class="contact-avatar left" src="/{User::$pathToAvatar}{$row->worker->avatar}" alt="Программист"/>
                    <span>{$row->worker->name}</span>
                    <i>программист</i>
                    <div>
                        <i class="glyphicon glyphicon-envelope text-info"></i>
                        <a class="text-info" href="mailto:{$row->worker->mail}"><b>{$row->worker->mail}</b></a>
                    </div>
                </div>
                <div class="clearfix"></div>
            </div>

        </li>
    {/foreach}
</ul>