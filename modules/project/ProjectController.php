<?php

/**
 * Class ProjectController
 *
 * @property Project $model
 */
class ProjectController extends FController implements FComponentInterface {
    /**
     * @param string $url
     * @return bool
     */
    public function checkFriendlyUrl($url)
    {
        return false;
    }

    /**
     * @param object $activePage
     * @return string
     */
    public function getTemplate($activePage)
    {
        return 'default';
    }

    /**
     * @return array
     */
    public function getMetaData()
    {
        $metaData = array(
            'title' => 'Не задано',
            'description' => 'Не задано',
            'keywords' => 'Не задано',
            'h1' => 'Не задано',
            'robots' => 'Не задано',
        );
        if(isset($this->action))
        {
            if($this->action->getId() == 'add')
            {
                $metaData['needCache'] = true;
            }
        }
        return $metaData;
    }

    public function actionAdd($companyId = null, $Project = null, $File = null)
    {
        $model = $this->model->loadModel($Project);
        if(!$model->companyId)
        {
            $model->companyId = $companyId;
        }
        if($File)
        {
            $File = $model->setFileAttributes($File);
        }
        return $this->render('add', array(
            'render_type'=>self::TEMPLATE_EXTENSION_SMARTY,
            'model'=>$model,
            'addFile'=>FCore::getInstance()->getControllerByName('FileController')->actionAdd($File),
        ));
    }

    public function actionView($id)
    {
        $model = $this->model->findByPk($id);
        return $this->render('view', array(
            'render_type'=>self::TEMPLATE_EXTENSION_SMARTY,
            'model'=>$model,
            'listFiles'=>FCore::getInstance()->getControllerByName('FileController')->actionIndex(
                array('targetId'=>$model->id)
            ),
            'listAppeals'=>FCore::getInstance()->getControllerByName('AppealController')->actionList($model->companyId, $model->id),
            'listTasks'=>FCore::getInstance()->getControllerByName('TaskController')->actionList($model->companyId, $model->id),
            'listPayments'=>FCore::getInstance()->getControllerByName('PaymentController')->actionList($model->companyId, $model->id),
        ));
    }

    public function actionEdit($id)
    {
        $model = $this->model->findByPk($id);
        return $this->render('add', array(
            'render_type'=>self::TEMPLATE_EXTENSION_SMARTY,
            'model'=>$model,
            'listFiles'=>FCore::getInstance()->getControllerByName('FileController')->actionIndex(
                array('moduleName'=>get_class($model), 'targetId'=>$model->id)
            ),
            'addFile'=>FCore::getInstance()->getControllerByName('FileController')->actionAdd(),
        ));
    }

    public function actionIndex($Filter)
    {
        return $this->render('index', array(
            'render_type'=>self::TEMPLATE_EXTENSION_SMARTY,
            'items'=>$this->model->getAll($Filter),
        ));
    }
} 