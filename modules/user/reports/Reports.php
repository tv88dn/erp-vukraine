<?php
/**
 * Created by PhpStorm.
 * User: tv88d_000
 * Date: 02.11.2014
 * Time: 11:33
 */

abstract class Reports extends CModel{

    protected $model;

    protected $data, $shortData;

    public function attributeNames()
    {
        return array(
            'data',
            'shortData',
        );
    }

    protected function setShortData($val)
    {
        $this->shortData = $val;
        return $this->shortData;
    }

    protected abstract function makeShortReport();

    protected abstract function makeReport();

    public function getShortReport()
    {
        return ($this->isReportActual('shortData')) ? $this->attributes['shortData'] : $this->makeShortReport();
    }

    public function __construct($model)
    {
        $this->model = $model;
        $behaviorName = 'Report'.get_class($model).'Behavior';
        include_once(FConfig::$pathToReports.'/behaviors/'.$behaviorName.'.php');
        $this->model->attachBehavior(get_class($model), new $behaviorName);
    }

    public function isReportActual($name)
    {
        if($this->attributes[$name])
        {
            if(get_class($this->attributes[$name]->model) == get_class($this->model))
            {
                return true;
            }
        }
        return false;
    }


} 