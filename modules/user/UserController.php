<?php

/**
 * Class UserController
 *
 * @params User $model
 */
class UserController extends FController implements FComponentInterface
{

    public static $flashKeys = array('error', 'info', 'warning', 'success');

    public function checkFriendlyUrl($url)
    {
        return false;
    }

    public function actionControl()
    {
        return $this->render('control', array(
            'render_type'=>self::TEMPLATE_EXTENSION_SMARTY
        ));
    }

    public function getMetaData()
    {
        $metaData = array(
            'title' => 'Не задано',
            'description' => 'Не задано',
            'keywords' => 'Не задано',
            'h1' => 'Не задано',
            'robots' => 'Не задано',
        );
        if(isset($this->action))
        {
            if($this->action->getId() == 'add')
            {
                $metaData['needCache'] = true;
            }
        }

        return $metaData;
    }

    public function getTemplate($activePage)
    {
        return 'default';
    }

    public function actionLogout()
    {
        FCore::app()->user->logout();
        $this->redirect('/');
    }

    public function actionOAuth($code)
    {
        $client = User::getOAuthParams();
        $client->authenticate($code);
        $plus = new Google_Service_Plus($client);
        $this->model->authenticateWithOAuth($plus->people->get('me'));
        $this->redirect('/');
    }

    public function actionGetWebSocketId()
    {
        FCore::app()->user->setState('webSocketSecret', uniqid());
        return json_encode(array(
            'id'=>FCore::app()->user->id,
            'secret'=>base64_encode(FCore::app()->user->getState('webSocketSecret') ^ FCore::WEB_SOCKET_SECRET_CLIENT),
        ));
    }

    public function actionExternalAuthenticate($type)
    {
        if($type == User::GOOGLE_AUTH)
        {
            $this->redirect(User::getOAuthParams()->createAuthUrl());
            die();
        }
    }

    public function actionFlashes()
    {
        $flashes = array();
        for($i = 0; $i < count(self::$flashKeys); $i++)
        {
            if($flash = FCore::app()->user->hasFlash(self::$flashKeys[$i]))
            {
                $flashes[self::$flashKeys[$i]] = FCore::app()->user->getFlash(self::$flashKeys[$i]);
            }
        }
        return $this->render('flash', array(
            'render_type'=>self::TEMPLATE_EXTENSION_SMARTY,
            'flashes'=>$flashes,
        ));
    }

    public function actionCabinet()
    {
        $this->attachBehavior('reports', new ReportsBehavior());
        return $this->render('cabinet', array(
            'render_type'=>self::TEMPLATE_EXTENSION_SMARTY,
            'listTime'=>FCore::getInstance()->getControllerByName('TimeController')->actionlist(
                mktime(0, 0, 0),
                mktime(23, 59, 59),
                FCore::app()->user->getState('id')
            ),
            'activity'=>FCore::getInstance()->getControllerByName('ActivityController')->actionList(),
            'tasks'=>FCore::getInstance()->getControllerByName('TaskController')->actionActive(),
        ));
    }

    public function actionProfileMenu()
    {

        return $this->render('profileMenu', array(
            'render_type'=>self::TEMPLATE_EXTENSION_SMARTY,
            'user'=>FCore::app()->user->getState('userModel'),
        ));

    }

    public function actionMainPage()
    {
        if(FCore::app()->user->getState('id'))
        {
            return $this->actionCabinet();
        }
        else
        {
            return $this->actionLogin();
        }
    }

    public function actionProfile()
    {
        return $this->render('add', array(
            'render_type'=>self::TEMPLATE_EXTENSION_SMARTY,
            'model'=>FCore::app()->user->getState('userModel'),
        ));
    }

    public function actionLogin()
    {
        return $this->render('login', array('render_type' => self::TEMPLATE_EXTENSION_SMARTY));
    }

    public function actionAuthorize($login, $password)
    {
        $user = $this->model->authenticate($login, $password);
        if ($this->model->identity->errorCode == CUserIdentity :: ERROR_NONE && $this->model->identity->getState("id")) {
            FCore :: app()->user->login($this->model->identity);
            FCore :: app()->user->setState("id", $this->model->identity->getState("id"));
            FCore::app()->user->setState('userModel', $user);
            $this->redirect('/');
        } else {
            $this->redirect(FConfig :: $rootPath . "/");
        }

    }

    public function actionEdit($id, $User = null)
    {
        if($User)
        {
            $model = User::model()->loadModel($User);
        }
        else
        {
            $model = User::model()->findByPk($id);
        }
        return $this->render('add', array(
            'render_type' => self::TEMPLATE_EXTENSION_SMARTY,
            'model'=> $model,
        ));
    }

    public function actionIndex()
    {
        return $this->render('index', array(
            'render_type'=>self::TEMPLATE_EXTENSION_SMARTY,
            'items'=>User::model()->getAll(array('criteria'=>array('with'=>array('userContact')))),
        ));
    }

    public function actionCheckLogin($login)
    {
        return ($this->model->findByAttributes(array('login'=>$login))) ? '1' : '-1';
    }

    public function actionGenerateRandomPassword($length)
    {
        $length = ($length) ? $length : User::$defaultPasswordLength;
        return FCore::app()->getSecurityManager()->generateRandomString($length);
    }

    public function actionAdd($User)
    {
        $model = User::model()->loadModel($User);
        $model->clearPassword();
        $model->company = 'lookmy.info';
        return $this->render('add', array(
            'render_type' => self::TEMPLATE_EXTENSION_SMARTY,
            'model'=> $model,
        ));
    }



} 