<div class="panel panel-primary warp">
    <div class="panel-heading">
        <a href="/user/index" class="glyphicon glyphicon-th-list right addButtonInTitle"></a>
        {if $model->id}
            <h1>Редактирование профиля</h1>
        {else}
            <h1>Добавление пользователя</h1>
        {/if}
    </div>
    <div class="panel-body">
        <div class="well">
            <form id="addUser" method="post" class="form-horizontal" enctype="multipart/form-data" action="{if $model->id}/user/edit?id={$model->id}{else}/user/add{/if}">
                {if $model->hasErrors()}
                    {foreach from=$model->getErrors() item=error}
                        {for $var=0 to count($error)-1}
                            <div class="alert alert-error">
                                <a class="close" data-dismiss="alert" href="#">×</a>{$error[$var]}
                            </div>
                        {/for}
                    {/foreach}
                {/if}
                {if $model->id}
                    {FBootStrap::activeHiddenField($model, 'id')}
                {/if}
                {FBootStrap::formGroup($model, 'login', 'activeTextField', [], [
                    'control'=>['width'=>4],
                    'label'=>['width'=>2],
                    'button'=>[
                        'type'=>'success',
                        'position'=>FBootStrap::POSITION_RIGHT,
                        'label'=>'Проверить',
                        'htmlOptions'=>[
                        'id'=>'loginCheck'
                        ]
                    ]
                ])}

                <div class="form-group warp">
                    <label class="col-lg-2 control-label" width="2" for="User_ФИО"></label>
                    <div class="col-lg-4">
                        <span id="password-change" class="btn btn-info">Сменить пароль</span>
                    </div>
                </div>
                <div id="change-password-block" class="well alert hidden">
                    <button type="button" id="hide-block" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    {FBootStrap::formGroup($model, 'password', 'activePasswordField', [], [
                    'control'=>['width'=>4],
                    'label'=>['width'=>2],
                    'button'=>[
                        'type'=>'info',
                        'position'=>FBootStrap::POSITION_RIGHT,
                        'label'=>'Генерировать',
                        'htmlOptions'=>[
                            'id'=>'passwordGenerate'
                            ]
                        ]
                    ])}

                    {FBootStrap::formGroup($model, 'passwordRepeat', 'activePasswordField', [], [
                        'control'=>['width'=>4],
                        'label'=>['width'=>2]
                    ])}
                </div>


                <legend>Данные о пользователе</legend>
                {foreach from=$model->getExternalAttributes() key=name item=detail}

                    {if $detail->type->fieldName == 'company'}
                        {FBootStrap::formGroup($model, $name, $detail->type->type, $detail->getDictionary(), [
                        'control'=>['width'=>4, 'disabled'=>'disabled'],
                        'label'=>['width'=>2]
                        ])}
                    {else}
                        {FBootStrap::formGroup($model, $name, $detail->type->type, $detail->getDictionary(), [
                        'control'=>['width'=>4],
                        'label'=>['width'=>2]
                        ])}
                    {/if}
                {/foreach}

                <div class="col-lg-9 col-lg-offset-2">
                    {if $model->id}
                        {FBootStrap::btnDangerLink('Удалить', "/users/delete?id=`$model->id`")}
                    {else}
                        {FBootStrap::btnDefaultLink('Отмена', "/users/index")}
                    {/if}
                    {FBootStrap::btnPrimarySubmit('Сохранить')}
                </div>

            </form>
            <div class="clearfix"></div>
        </div>
    </div>
</div>