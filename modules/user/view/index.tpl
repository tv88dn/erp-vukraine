<div class="panel panel-primary warp">
    <div class="panel-heading">
        <a href="/user/add" class="glyphicon glyphicon-plus right addButtonInTitle"></a>
        <h1>Просмотр пользователей</h1>
    </div>
    <div class="panel-body">
        <table class="table table-striped table-bordered table-hover">
            <tr>
                {foreach from=$items->model->getAllAttributeLabels(['passwordRepeat', 'password']) item=label}
                    <th>{$label}</th>
                {/foreach}
                <th></th>
            </tr>
            {foreach from=$items->getData() item=row}
                <tr class="active">
                    {foreach from=$items->model->getAllAttributeLabels(['passwordRepeat', 'password']) item=label key=fieldName}
                        <td>
                            {if is_array($row->getAttribute($fieldName))}
                                {implode(', ', $row->getAttribute($fieldName))}
                            {else}
                                {$row->getAttribute($fieldName)}
                            {/if}
                        </td>
                    {/foreach}
                    <td class="control-group">
                        {FBootStrap::linkGroup(
                        [
                        ['url'=>"/user/edit?id=`$row->id`", 'text'=>FBootStrap::icon('edit')],
                        ['url'=>"/user/delete?id=`$row->id`", 'text'=>FBootStrap::icon('remove')]
                        ]
                        )}
                    </td>
                </tr>
            {/foreach}

        </table>
        {$this->smartyWidget('FLinkPager', [
        'pages'=>$items->pagination,
        'header'=>"<div class='pager'>",
        'footer'=>"</div>"])}
    </div>
</div>
