{foreach from=$flashes item=text key=type}
    <div class="warp alert alert-{$type}">
        <a class="close" data-dismiss="alert" href="#">×</a>{$text}
    </div>
{/foreach}