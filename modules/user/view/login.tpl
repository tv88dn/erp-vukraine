<div class="whiteOverlay"></div>
<div class="loginForm jumbotron screenCenter">
    <div class="row">
        {if FCore::app()->user->hasFlash(User::OAUTH_ERROR)}
            <div class="alert alert-warning text-left">
                <button type="button" class="close" data-dismiss="alert">×</button>
                <h3>Ая-й</h3>
                <span>{FCore::app()->user->getFlash(User::OAUTH_ERROR)}</span>
            </div>
        {/if}

        <a href="/user/externalAuthenticate?type={User::GOOGLE_AUTH}">
            <img class="loginLogo " src="/resources/images/head_logo.png" alt="lookmy.info"/>
        </a>

        <div class="col-lg-7 well">
            <legend>Пожалуйста, авторизируйтесь</legend>
            {if false}
                <div class="alert alert-error">
                    <a class="close" data-dismiss="alert" href="#">×</a>Логин или Пароль неверные
                </div>
            {/if}
            <form method="POST" action="/user/authorize" accept-charset="UTF-8">
                <div class="col-lg-10 block">
                    <input type="text" id="login" class="form-control" name="login" placeholder="Логин">
                </div>
                <div class="col-lg-10 block">
                    <input type="password" id="password" class="form-control" name="password" placeholder="Пароль">
                </div>

                <button type="submit" name="submit" class="btn btn-info btn-block col-lg-6">Вход</button>
                <div class="clearfix"></div>
            </form>
        </div>
        <div class="col-lg-5">
            <a href="/user/externalAuthenticate?type={User::GOOGLE_AUTH}">
                <img src="http://vukraine.in.ua/templates/_default_/images/profile/ua_logo.png" alt=""/>
            </a>
        </div>
        <div class="clearfix"></div>
        <div class="copyright row"><b>ERP inUkraine &copy;LookMY.info {date('Y')}</b></div>
    </div>
</div>
