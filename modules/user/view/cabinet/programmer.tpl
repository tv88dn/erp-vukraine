<script>
    $(document).ready(function(){
        user.setContainerForTasks($('#myTasksContent'));
        $.get('/time/add', function(data){
            $('#addTaskTimeForm').html(data);
        });
    })
</script>
<div class="col-lg-12">
    <div class="col-lg-2">
        <a href="/task/add" class="btn btn-info warp col-lg-12"><i class="glyphicon glyphicon-plus"></i> Добавить задачу</a>
        <legend>Входящие</legend>
        <ul class="nav nav-pills nav-stacked">
            <li class="active"><a onclick="user.loadTasks('active')" href="#active" data-toggle="tab">Активные</a></li>
            <li><a onclick="user.loadTasks('checked')" href="#check" data-toggle="tab">В проверке</a></li>
            <li><a onclick="user.loadTasks('deadline')" href="#deadline" data-toggle="tab">Deadline</a></li>
            <li><a onclick="user.loadTasks('timeIsUp')" href="#timeIsUp" data-toggle="tab">Просроченные</a></li>
            <li><a onclick="user.loadTasks('completed')" href="#done" data-toggle="tab">Завершенные</a></li>
        </ul>
        <legend><a onclick="user.loadTasks('myAgreement')" href="#timeIsUp" data-toggle="tab">На согласовании</a></legend>
    </div>
    <div class="col-lg-8">
        <div class="tasks">
            <div id="myTasksContent" class="tab-content">
                {$tasks}
            </div>
        </div>
    </div>
    <div class="col-lg-3" style="display: none">
        <legend>Мои отчеты</legend>
        {*$report*}
        <legend>Отчет за день</legend>
        {$listTime}
        <div class="dropdown">
            <button id="showButton" aria-expanded="true" class="btn btn-info warp" data-toggle="dropdown"><i class="glyphicon glyphicon-plus"></i> Добавить затраты времени</button>
            <div id="addTaskTimeForm" class="dropdown-menu" aria-labelledby="showButton">
                {*$addTime*}
            </div>

        </div>

    </div>
</div>