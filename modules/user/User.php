<?php
/**
 * Created by PhpStorm.
 * User: tv88d_000
 * Date: 16.08.14
 * Time: 18:47
 */

class User extends UserBase {

    public static $usersCompanyName = '<b>LOOK</b>';
    public static $pathToAvatar = 'resources/images/uploads/user/avatar/';
    public static $noAvatar = 'noavatar_user.gif';
    public static $defaultPasswordLength = 8;
    const USERS_DOMEN = 'lookmy.info';
    const ADDITIONAL_USERS_DOMEN = 'mirvesov.com.ua';
    const OAUTH_ERROR = 'oauthError';

    public $passwordRepeat;

    public $passwordHash;

    public $newPassword;

    public $identity;

    public $avatarFile;

    const GOOGLE_AUTH = 'google';

    public static function getOAuthParams()
    {
        FCore::initGoogle();
        $client = new Google_Client();
        $client->setClientId('398500954873-ao0fg95m5g8fatc8pcmb29rh4tkiu0lf.apps.googleusercontent.com');
        $client->setClientSecret('5rv9MnSE8cH4s4Ex_RbqjBnE');
        $client->setRedirectUri('http://'.FCore::app()->request->serverName.'/user/oAuth');

        $client->addScope(Google_Service_Plus::PLUS_ME);
        $client->addScope(Google_Service_Plus::PLUS_LOGIN);
        $client->addScope(Google_Service_Plus::USERINFO_EMAIL);
        return $client;
    }

    public function generateLoginFromName()
    {
        $this->login = explode(' ', trim($this->name));
        $this->login = mb_strtolower(FTranslit::transliterate($this->login[0]));
        if(self::checkUser($this->login))
        {
            $this->login .= $this->count();
        }
        return $this->login;
    }

    public function getUserByEmail($mail)
    {
        return $this->with(
            array(
                'userContact',
                'details'=>array('condition'=>'details.data="'.$mail.'"'),
                'parents'=>array('condition'=>'module="User" OR module="Contact"'),
                'types'=>array('condition'=>'fieldName="mail"'),
            )
        )->findByAttributes(array());
    }

    public function oAuthlogin($user)
    {
        if(!$this->identity)
        {
            $this->identity = new CUserIdentity($user->name, $user->password);
        }
        $this->identity->username = $user->name;
        $this->identity->errorCode=CUserIdentity::ERROR_NONE;
        $this->identity->setState("id",$user->id);

        FCore :: app()->user->login($this->identity);
        FCore :: app()->user->setState("id", $user->id);
        FCore::app()->user->setState('userModel', $user);
    }

    public static function validateDomenEmail($mail)
    {
        return (preg_match('/.*@'.self::USERS_DOMEN.'/i', $mail) || preg_match('/.*@'.self::ADDITIONAL_USERS_DOMEN.'/i', $mail)) ? true : false;
    }


    public function addNewUserFromOAuth($name, $email)
    {
        $model = new User();
        $model->passwordRepeat = $model->password = FCore::app()->getSecurityManager()->generateRandomString(self::$defaultPasswordLength);
        $model->name = $name;
        $model->mail = $email;
        $model->generateLoginFromName();
        $model->status = 1;
        if($model->save())
        {
            return $model;
        }
        else
        {
            return null;
        }


    }

    public static function parseNameFromGoogle($personName)
    {
        $name = ($personName->givenName)? $personName->givenName.' ' : '';
        $name .= ($personName->familyName)? $personName->familyName.' ' : '';
        $name .= ($personName->middleName)? $personName->middleName.' ' : '';
        return trim($name);
    }

    public function authenticateWithOAuth($me)
    {
        $emails = $me->getEmails();

        for($i = 0; $i < count($emails); $i++)
        {
            $user = $this->getUserByEmail($emails[$i]->value);
            if(!$user)
            {
                $user = Contact::model()->getUserByEmail($emails[$i]->value);
            }

            if($user)
            {
                $this->oAuthlogin($user);
            }

        }
        if(!FCore::app()->user->getState('id'))
        {
            $name = self::parseNameFromGoogle($me->getName());
            $mail = $emails[0]->value;
            if(self::validateDomenEmail($mail))
            {
                $user = $this->addNewUserFromOAuth($name, $mail);
                if($user)
                {
                    $this->oAuthlogin($user);
                }
                else
                {
                    throw new Exception('Не удалось добавить пользователя');
                }
            }
            else
            {
                FCore::app()->user->setFlash(self::OAUTH_ERROR, 'Авторизация на данном ресурсе возможна только для пользователей домена lookmy.info. Доступ других пользователей будет разрешен в ближайшее время');
            }

        }
    }

    public function getAvatar()
    {

        if(isset($this->details['avatar']))
        {
            return ($avatar = $this->details['avatar']) ? $avatar : self::$noAvatar;
        }
        else
        {
            return self::$noAvatar;
        }

    }

    public function setAttributes($values,$safeOnly=true)
    {
        $res = parent::setAttributes($values, $safeOnly);
        if (isset($values['avatar'])) {
            $this->avatarFile = CUploadedFile::getInstance($this, 'avatar');
            if ($this->avatarFile){
                $this->avatar = time() . '_' . $this->avatarFile->name;
            }
        }
        return $res;
    }

    public function __toString()
    {
        return (string)$this->id;
    }

    public function getCompanyName()
    {
        return self::$usersCompanyName;
    }

    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    public function relations()
    {
        return array_merge($this->baseDetailsRepations(), array(
            'userContact'=>array(self::HAS_ONE, 'Details', array('moduleId'=>'id'), 'condition'=>'userContact.parentId=17', 'together'=>true),
        ));
    }

    public function cryptPassword($key=null)
    {
        $this->password = CPasswordHelper::hashPassword($this->password);
    }

    public function validatePassword()
    {
        if($this->password != $this->passwordRepeat)
        {
            $this->addError('passwordRepeat', 'Вы неверно повторили пароль');
        }
    }

    public function beforeValidate()
    {
        if(!$this->password)
        {
            $this->password = $this->passwordHash;
            $this->passwordRepeat = $this->password;
            $this->newPassword = false;
        }
        else
        {
            $this->newPassword = true;
        }
        return parent::beforeValidate();
    }

    public function beforeSave()
    {

        if($this->scenario == 'insert')
        {
            $this->validatePassword();
        }
        if($this->newPassword)
        {
            $this->cryptPassword();
        }
        return parent::beforeSave();
    }

    public function clearPassword()
    {
        $this->passwordHash = $this->password;
        $this->password = null;
        $this->passwordRepeat = $this->password;
    }

    public function afterFind()
    {
        $this->clearPassword();
        return parent::afterFind();
    }

    public function afterSave()
    {
        $this->clearPassword();

        if($this->avatarFile && !$this->hasErrors())
        {

            $this->avatarFile->saveAs(self::$pathToAvatar.$this->avatar);
        }
        return parent::afterSave();
    }


    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'login' => 'Логин',
            'password' => 'Пароль',
            'passwordRepeat' => 'Повтор пароля',
        );
    }


    /**
     * Валидатор существования пользователя в базе
     */
    public static function checkUser($name){
        return User::model()->findByAttributes(array('login'=>$name));
    }
    /**
     * Метод проверки пользовательских данных при входе
     */
    public function authenticate($username, $password)
    {
        $this->identity = new CUserIdentity($username, $password);
        $user = self::checkUser($username);
        if ($user){
            $this->identity->username = $user->name;
            if(!CPasswordHelper::verifyPassword($password, $user->passwordHash)){
                $this->identity->errorCode=CUserIdentity::ERROR_PASSWORD_INVALID;
                Yii::app()->user->setFlash('ERROR',"Неверный пароль!");
            }else{
                $this->identity->errorCode=CUserIdentity::ERROR_NONE;
                $this->identity->setState("id",$user->id);
            }
        }else{
            $this->identity->errorCode=CUserIdentity::ERROR_USERNAME_INVALID;
            FCore::app()->user->setFlash('ERROR',"Такого пользователя нет!");
        }
        return $user;


    }
} 