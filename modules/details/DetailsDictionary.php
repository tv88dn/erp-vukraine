<?php
/**
 * Created by PhpStorm.
 * User: tv88d_000
 * Date: 02.09.14
 * Time: 15:05
 */

class DetailsDictionary extends DetailsDictionaryBase implements FFlashInterface{

    public $name;
    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return Details the static model class
     */
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    public function getFlashLabels()
    {
        return array(
            'insert'=>array(
                'type'=>'success',
                'text'=>'Элемент успешно добавлен в словарь',
            ),
            'update'=>array(
                'type'=>'info',
                'text'=>'Элемент словаря изменен',
            ),
            'delete'=>array(
                'type'=>'warning',
                'text'=>'Элемент удален из словаря',
            ),
        );
    }

} 