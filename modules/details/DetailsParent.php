<?php
/**
 * Created by PhpStorm.
 * User: tv88d_000
 * Date: 24.08.14
 * Time: 0:47
 */

/**
 * Class DetailsParent
 *
 * @property DetailsTypes $type
 */
class DetailsParent extends DetailsParentBase implements FFlashInterface {

    public $parent;
    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'module' => 'Модуль',
            'detailId' => 'Описатель',
            'parentId' => 'Родитель',
            'default' => 'По умолчанию',
            'display' => 'Отображать',
            'multiple' => 'Множесвенные значения',
        );
    }

    /**
     * @param $detailId
     */
    public function setDetailId($detailId)
    {
        if($detailId)
        {
            $this->detailId = $detailId;
            $this->parent =  DetailsTypes::model()->findByPk($detailId);
        }
        if($this->detailId)
        {
            $this->parent =  DetailsTypes::model()->findByPk($this->detailId);
        }
        return $this->detailId;
    }

    public function getFlashLabels()
    {
        return array(
            'insert'=>array(
                'type'=>'success',
                'text'=>'Связь успешно добавлена',
            ),
            'update'=>array(
                'type'=>'info',
                'text'=>'Связь успешно изменена',
            ),
            'delete'=>array(
                'type'=>'warning',
                'text'=>'Связь удалена',
            ),
        );
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return DetailsParentBase the static model class
     */
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    public function normalizeAllDetails()
    {
        $parents  = $this->findAll();
        $count = 0;
        for($i = 0; $i < count($parents); $i++)
        {
            $count += $parents[$i]->normalizeDetailsByParent();
        }
        return $count;
    }

    public function normalizeDetailsByParent()
    {
        $module = $this->module;
        $module = new $module;
        $items = $module->findAll();
        $normalizeCount = 0;
        for($i = 0; $i < count($items); $i++)
        {
            if(!Details::model()->findByAttributes(array('moduleId'=>$items[$i]->id, 'parentId'=>$this->id)))
            {
                $detail = new Details();
                $detail->moduleId = $items[$i]->id;
                $detail->parentId = $this->id;
                if(!$detail->save())
                {
                    $errors = var_export($detail->getErrors(), true);
                    throw new Exception("Не вышдло добавить описатель: модуль: {$errors}");
                }
                else
                {
                    $normalizeCount++;
                }
            }
        }
        return $normalizeCount;
    }

    public function getDictionary()
    {
        $dictionary = DetailsDictionary::model()->findAllByAttributes(array('parentId'=>$this->id));
        $rows = array();
        for($i = 0; $i < count($dictionary); $i++)
        {
            $rows[$dictionary[$i]->value] = $dictionary[$i]->data;
        }
        return $rows;
    }

    public function getDetailsCount($moduleName)
    {
        return $this->count('t.module="'.$moduleName.'"');
    }

    public function relations()
    {
        return array(
            'type'=>array( self::HAS_ONE, 'DetailsTypes', array('id'=>'detailId')),
        );
    }
} 