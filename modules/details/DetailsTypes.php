<?php
/**
 * Created by PhpStorm.
 * User: tv88d_000
 * Date: 23.08.14
 * Time: 20:07
 */

class DetailsTypes extends DetailsTypesBase implements FFlashInterface
{

    public function getFlashLabels()
    {
        return array(
            'insert'=>array(
                'type'=>'success',
                'text'=>'Описатель успешно добавлен',
            ),
            'update'=>array(
                'type'=>'info',
                'text'=>'Описатель успешно изменен',
            ),
            'delete'=>array(
                'type'=>'warning',
                'text'=>'Описатель удален',
            ),
        );
    }

    public function beforeValidate()
    {
        $this->validator = self::parseValidators($this->validator);
        return parent::beforeSave();
    }

    public function afterFind()
    {
        $this->validator = explode(', ', $this->validator);
        $this->isEdit = true;
        return parent::afterFind();
    }

    public function afterDelete()
    {
        $links = DetailsParent::model()->findAllByAttributes(array('detailId'=>$this->id));
        for($i =0; $i < count($links); $i++)
        {
            $links[$i]->delete();
        }
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'name' => 'Название описателя',
            'fieldName'=>'Название переменной',
            'type' => 'HTML-тип',
            'validator' => 'Валидатор',
        );
    }

    public function add()
    {
        return $this;
    }


    public static function parseValidators($validators)
    {
        return (is_array($validators) ) ? @implode(', ', $validators) : '';
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return DetailsTypes the static model class
     */
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    public function relations()
    {
        return array(
            'parents'=>array( self::HAS_MANY, 'DetailsParent', array('detailId'=>'id')),
        );
    }

} 