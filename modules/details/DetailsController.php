<?php

/**
 * Class DetailsController
 *
 * @property $model Details
 */
class DetailsController extends FController implements FComponentInterface
{

    public function checkFriendlyUrl($url)
    {
        return false;
    }

    public function getMetaData()
    {
        return null;
    }

    public function getTemplate($activePage)
    {
        return 'default';
    }

    public function actionAddType($DetailsTypes = null)
    {
        $baseModel = DetailsTypes::model()->loadModel($DetailsTypes);
        if ($baseModel->isEdit) {
            $this->redirect('/details/editType?id=' . $baseModel->id);
        }
        return $this->render('addType', array(
            'render_type' => self::TEMPLATE_EXTENSION_SMARTY,
            'model' => $baseModel,
            'validators' => CValidator::$builtInValidators,
            'types' => FBootStrap::getTypes(),
        ));
    }

    public function actionEditType($id)
    {
        return $this->render('addType', array(
            'render_type' => self::TEMPLATE_EXTENSION_SMARTY,
            'model' => DetailsTypes::model()->findByPk($id),
            'validators' => CValidator::$builtInValidators,
            'types' => FBootStrap::getTypes(),
            'addParent' => $this->actionAddParent($id),
            'viewParent' => $this->actionIndexParent($id),
        ));
    }

    public function actionDeleteType($id)
    {
        if (DetailsTypes::model()->findByPk($id)->delete()) {
            $this->redirect('/details/indexType');
            FCore::app()->end();
        }
    }

    public function actionNormalize()
    {
        return 'Добавлено описателей для нормализации: '.DetailsParent::model()->normalizeAllDetails();
    }

    public function actionIndexType()
    {

        return $this->render('indexType', array(
            'render_type' => self::TEMPLATE_EXTENSION_SMARTY,
            'types' => DetailsTypes::model()->getAll(array('criteria' => array(
                'order' => 't.id DESC',
                'with' => array('parents'),
            ))),
            'addType'=>$this->actionAddType(),
        ));
    }

    //словари
    public function actionIndexDictionary($parentId)
    {
        return $this->render('indexDictionary', array(
            'render_type' => self::TEMPLATE_EXTENSION_SMARTY,
            'items' => DetailsDictionary::model()->getAll(array(
                'criteria' => array(
                    'condition' => 't.parentId=' . $parentId,
                ),
            )),
        ));
    }

    public function actionEditDictionary($id)
    {
        $model = DetailsDictionary::model()->findByPk($id);
        return $this->render('addDictionary', array(
            'render_type' => self::TEMPLATE_EXTENSION_SMARTY,
            'model' => $model,
        ));
    }

    public function actionDeleteDictionary($id)
    {
        $model = DetailsDictionary::model()->findByPk($id);
        $parent = $model->parentId;
        if ($model->delete()) {
            $this->redirect('/details/editParent?id=' . $parent);
            FCore::app()->end();
        }
    }

    public function actionAddDictionary($id = null, $DetailsDictionary = null)
    {
        $model = DetailsDictionary::model()->loadModel($DetailsDictionary);
        if ($model->isEdit) {
            $this->redirect('/details/editParent?id=' . $model->parentId);
            FCore::app()->end();
        } else {
            $model->parentId = $id;
        }
        return $this->render('addDictionary', array(
            'render_type' => self::TEMPLATE_EXTENSION_SMARTY,
            'model' => $model,
        ));
    }

    //

    public function actionIndexParent($parentId)
    {
        return $this->render('indexParent', array(
            'render_type' => self::TEMPLATE_EXTENSION_SMARTY,
            'items' => DetailsParent::model()->getAll(array(
                'criteria' => array(
                    'condition' => 't.detailId=' . $parentId,
                    'with' => 'type',
                ),
            )),
        ));
    }

    public function actionEditParent($id)
    {
        $model = DetailsParent::model()->findByPk($id);
        $model->parent = DetailsTypes::model()->findByPk($model->detailId);
        return $this->render('addParent', array(
            'render_type' => self::TEMPLATE_EXTENSION_SMARTY,
            'model' => $model,
            'modules' => FCore::getInstance()->getModels(),
            'dictionary' => $this->actionAddDictionary($id),
            'viewDictionary' => $this->actionIndexDictionary($id),
        ));
    }


    public function actionDeleteParent($id)
    {
        $model = DetailsParent::model()->findByPk($id);
        $id = $model->detailId;
        if ($model->delete()) {
            $this->redirect('/details/editType?id=' . $id);
            FCore::app()->end();
        }
    }


    public function actionAddParent($id = null, $DetailsParent = null)
    {
        $model = DetailsParent::model()->loadModel($DetailsParent);
        if ($id) {
            $model->display = 1;
            $model->parentId = 1;
        } else if ($model->isEdit) {
            $this->redirect('/details/editType?id=' . $model->detailId);
            FCore::app()->end();
        }
        $model->setDetailId($id);
        return $this->render('addParent', array(
            'render_type' => self::TEMPLATE_EXTENSION_SMARTY,
            'model' => $model,
            'modules' => FCore::getInstance()->getModels(),

        ));

    }


    public function actionControl()
    {
        return $this->render('control', array(
            'render_type' => self::TEMPLATE_EXTENSION_SMARTY,
        ));
    }


} 