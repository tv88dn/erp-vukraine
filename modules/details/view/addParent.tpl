<div class="panel panel-info warp">
    <div class="panel-heading">
        <h1>Добавить связь</h1>
    </div>
    <div class="panel-body">
        {if $model->hasErrors()}
            {foreach from=$model->getErrors() item=error}
                {for $var=0 to count($error)-1}
                    <div class="alert alert-error">
                        <a class="close" data-dismiss="alert" href="#">×</a>{$error[$var]}
                    </div>
                {/for}
            {/foreach}
        {/if}
        <div class="{if $model->id} col-lg-6{/if}">
            <form class="form-horizontal" action="/details/addParent" method="post">
                {FBootStrap::activeHiddenField($model, 'id')}
                {FBootStrap::formGroup($model, 'module', 'activeDropDownList', $modules)}
                {FBootStrap::formGroup($model->parent, 'name', 'activeTextField', [], ['control'=>['disabled'=>'disabled']])}
                {FBootStrap::activeHiddenField($model, 'detailId')}
                {FBootStrap::formGroup($model, 'parentId', 'activeTextField')}
                {FBootStrap::formGroup($model, 'default', 'activeTextField')}
                {FBootStrap::formGroup($model, 'display', 'activeCheckBox')}
                {FBootStrap::formGroup($model, 'multiple', 'activeCheckBox')}
                <div class="form-group warp text-center">
                    {if $model->id}
                        {FBootStrap::btnDangerLink('Удалить', "/details/deleteParent?id=`$model->id`")}
                    {else}
                        {FBootStrap::btnDefaultLink('Отмена', "/details/editType?id=`$model->detailId`")}
                    {/if}
                    {FBootStrap::btnPrimarySubmit('Сохранить')}
                </div>

            </form>
            <div class="clearfix"></div>
        </div>
        {if $model->id}
            <div class="col-lg-6">
                {$viewDictionary}
                {$dictionary}
            </div>

        {/if}
    </div>
</div>