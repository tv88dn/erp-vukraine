<div class="panel panel-primary warp">
    <div class="panel-heading">
        {if $this->action->id == 'indexType'}
            <a class="glyphicon glyphicon-plus right addButtonInTitle"></a>
        {else}
            <a href="/details/indexType" class="glyphicon glyphicon-th-list right addButtonInTitle"></a>
        {/if}

        {if $model->id}
            <h1>Редактирование описателя #{$model->id} ({$model->name})</h1>
        {else}
            <h1>Добавление описателя</h1>
        {/if}

    </div>
    <div class="panel-body"{if $this->action->id == 'indexType'} style="display: none;" {/if}>
        {if $model->hasErrors()}
            {foreach from=$model->getErrors() item=error}
                {for $var=0 to count($error)-1}
                    <div class="alert alert-error">
                        <a class="close" data-dismiss="alert" href="#">×</a>{$error[$var]}
                    </div>
                {/for}
            {/foreach}
        {/if}
        <div class="well">
            <div class="col-lg-{if $model->isEdit}6{else}12{/if}">
                <form class="form-horizontal" action="/details/addType" method="post">
                    {FBootStrap::activeHiddenField($model, 'id')}
                    {FBootStrap::formGroup($model, 'name', 'activeTextField')}
                    {FBootStrap::formGroup($model, 'fieldName', 'activeTextField')}
                    {FBootStrap::formGroup($model, 'type', 'activeDropDownList', $types)}
                    {FBootStrap::formGroup($model, 'validator', 'activeCheckBoxList', $validators)}
                    <div class="col-lg-9 col-lg-offset-2">
                        {if $model->id}
                            {FBootStrap::btnDangerLink('Удалить', "/details/deleteType?id=`$model->id`")}
                        {else}
                            {FBootStrap::btnDefaultLink('Отмена', "/details/editType?id=`$model->id`")}
                        {/if}
                        {FBootStrap::btnPrimarySubmit('Сохранить')}
                    </div>
                </form>
            </div>
            {if $model->isEdit}
                <div class="col-lg-6">
                    <div class="warp">{$addParent}</div>
                </div>
            {/if}

            <div class="clearfix"></div>
        </div>
        {if $model->isEdit}
            <div class="col-lg-10">
                <div class="warp">
                    <legend>Существующие связи</legend>
                    {$viewParent}
                </div>
            </div>

        {/if}
    </div>
</div>
