<div class="panel panel-primary warp">
    <div class="panel-heading">
        <h1>Просмотр описателей</h1>
    </div>
    <div class="panel-body">
        <div class="col-lg-7 list-group">
        {foreach from=$types->getData() item=type}

                <div class="col-lg-12 list-group-item">

                    <div class="col-lg-4">
                        <h3>{$type->name}</h3>
                        <label>({$type->fieldName})</label>
                    </div>
                        <ul class="list-group col-lg-5">
                            <li class="list-group-item"><label>тип:</label> <i>{$type->type}</i></li>
                            <li class="list-group-item"><label>валидаторы:</label> <i>{DetailsTypes::parseValidators($type->validator)}</i></li>
                        </ul>


                    <div class="col-lg-3 well">

                            {for $i = 0 to count($type->parents)-1}
                                <div class="left warp">
                                    <a href="#" class="label label-info">{$type->parents[$i]->module}</a>
                                </div>

                            {/for}

                        <div class="right detail-button-block">
                            {FBootStrap::linkGroup(
                            [
                            ['url'=>"/details/editType?id=`$type->id`", 'text'=>FBootStrap::icon('edit')],
                            ['url'=>"/details/deleteType?id=`$type->id`", 'text'=>FBootStrap::icon('remove')]
                            ]
                            )}
                        </div>

                    </div>
                </div>


        {/foreach}
        	
            	{$this->smartyWidget('FLinkPager', [
            		'pages'=>$types->pagination,
            		'header'=>"<div class='pager'>",
            		'footer'=>"</div>"
            		])}
            </div>
        </div>
        <div class="col-lg-5">
            {$addType}
        </div>


    </div>
</div>