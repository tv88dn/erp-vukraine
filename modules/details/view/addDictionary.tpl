
    <div class="panel panel-info warp">
        <div class="panel-heading">
            <h1>Добавить словарь</h1>
        </div>
        <div class="panel-body">
            {if $model->hasErrors()}
                {foreach from=$model->getErrors() item=error}
                    {for $var=0 to count($error)-1}
                        <div class="alert alert-error">
                            <a class="close" data-dismiss="alert" href="#">×</a>{$error[$var]}
                        </div>
                    {/for}
                {/foreach}
            {/if}
            <div class="well">
                <form class="form-horizontal" action="/details/addDictionary" method="post">
                    {FBootStrap::activeHiddenField($model, 'id')}
                    {FBootStrap::activeHiddenField($model, 'parentId')}
                    {FBootStrap::formGroup($model, 'data', 'activeTextField')}
                    {FBootStrap::formGroup($model, 'value', 'activeTextField')}
                    <div class="form-group warp text-center">
                        {if $model->id}
                            {FBootStrap::btnDangerLink('Удалить', "/details/deleteDictionary?id=`$model->id`")}
                        {else}
                            {FBootStrap::btnDefaultLink('Отмена', "/details/editDictionary?id=`$model->id`")}
                        {/if}
                        {FBootStrap::btnPrimarySubmit('Сохранить')}
                    </div>

                </form>
                <div class="clearfix"></div>
            </div>
        </div>
    </div>
