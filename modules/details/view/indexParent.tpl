<table class="table table-info table-bordered table-hover">
    <tr>
        {foreach from=$items->model->attributeLabels() item=label}
            <th>{$label}</th>
        {/foreach}
    </tr>
    {foreach from=$items->getData() item=item }
        <tr onclick="location.href = '/details/editParent?id={$item->id}'" class="active">
            {foreach from=$item->getAttributes() item=attribute key="name"}
                {if $name == 'detailId'}
                    <td>{$item->type->name}</td>
                {else}
                    <td>{$attribute}</td>
                {/if}

            {/foreach}
        </tr>
    {/foreach}
    {$this->smartyWidget('FLinkPager', [
    	'pages'=>$items->pagination,
    	'header'=>"<div class='pager'>",
		'footer'=>"</div>"])}
</table>
