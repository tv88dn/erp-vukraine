<?php

/**
 * Created by PhpStorm.
 * User: tv88d_000
 * Date: 23.08.14
 * Time: 20:18
 */
class Details extends DetailsBase
{

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return Details the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function relations()
    {
        return array(
            'patents'=>array( self::HAS_ONE, 'DetailsParent', array('id'=>'parentId')),
        );
    }

    public static function normalizeAray($array)
    {
        for($i = 0, $j = 0; $i < count($array) && $j < count(array()); $i++)
        {
            if($i > $j && isset($array[$i]))
            {
                $array[$j] = $array[$i];
            }
            if(isset($array[$i]))
            {
                $j++;
            }
        }
        return $array;
    }

    public static function getDetailData($parent, $id = null)
    {
        $detail = self::getDetail($parent, $id);
        if(is_array($detail))
        {
            $data = array();
            for($i = 0; $i < count($detail); $i++)
            {
                $data[] = $detail[$i]->data;
            }
        }
        else
        {
            $data = $detail->data;
        }
        return $data;
    }

    protected static function createDetailsArray($count)
    {
        $details = array();
        for($i = 0; $i < $count; $i++)
        {
            $details[] = new Details();
        }
        return $details;
    }

    public static function getDetail($parent, $id = null, $newValues = array())
    {
        if(is_array($newValues))
        {
            for($i = 0; $i < count($newValues); $i++)
            {
                if(!isset($newValues[$i]))
                {
                    continue;
                }
                if(!$newValues[$i])
                {
                    unset($newValues[$i]);
                }
            }
        }

        if ($id === null) {
            if($parent->multiple)
            {
                return self::createDetailsArray(count($newValues));
            }
            else
            {
                return new Details;
            }
        } else {
            if($parent)
            {
                if($parent->multiple)
                {
                    $detail = Details::model()->findAllByAttributes(array(
                        'parentId' => $parent->id,
                        'moduleId' => $id,
                    ));

                    if(count($detail) < count($newValues))
                    {
                        $detail = array_merge($detail, self::createDetailsArray(count($newValues) - count($detail)));

                    }
                    else if(count($detail) > count($newValues) && $newValues)
                    {
                        for($i = 0; $i < count($detail); $i++)
                        {
                            $exists = false;
                            for($j = 0; $j < count($newValues); $j++)
                            {
                                if($detail[$i]->data == $newValues[$j])
                                {
                                    $exists = true;
                                    break;
                                }
                            }

                            if(!$exists)
                            {
                                $detail[$i]->delete();
                            }
                        }
                    }

                }
                else
                {
                    $detail = Details::model()->findByAttributes(array(
                        'parentId' => $parent->id,
                        'moduleId' => $id,
                    ));
                }
            }
            else{
                var_dump($parent->type->name);
                die();
            }

            if (!$detail) {
                if($parent->multiple)
                {
                    $details = array();
                    for($i = 0; $i < count($newValues); $i++)
                    {
                        $details[] = new Details();
                    }
                    return $details;
                }
                else
                {
                    return new Details;
                }
            } else {
                return $detail;
            }
        }
    }
}