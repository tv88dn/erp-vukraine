<?php

/**
 * Class AppealController
 *
 * @property Appeal $model
 */
class AppealController extends FController implements FComponentInterface {

    /**
     * @param string $url
     * @return bool
     */
    public function checkFriendlyUrl($url)
    {
        return false;
    }

    /**
     * @return string
     */
    public function actionControl()
    {
        return '<li class="dropdown"><a href="/appeals/listAll">Обращения</a></li>';
    }

    /**
     * @return array
     */
    public function getMetaData()
    {
        $metaData = array(
            'title' => 'Не задано',
            'description' => 'Не задано',
            'keywords' => 'Не задано',
            'h1' => 'Не задано',
            'robots' => 'Не задано',
        );
        if(isset($this->action))
        {
            if($this->action->getId() == 'add')
            {
                $metaData['needCache'] = true;
            }
        }
        return $metaData;
    }

    /**
     * @param object $activePage
     * @return string
     */
    public function getTemplate($activePage)
    {
        return 'default';
    }

    /**
     * @param null $companyId
     * @param null $Appeal
     * @return mixed|string
     */
    public function actionAdd($Appeal=null, $companyId=null)
    {
        $model = $this->model->loadModel($Appeal);
        $model->setCompany($companyId);
        if($model->isEdit)
        {
            $this->redirect('/company/view?id='.$model->companyId);
        }
        return $this->render('add', array(
            'render_type'=>self::TEMPLATE_EXTENSION_SMARTY,
            'model'=>$model,
        ));
    }

    /**
     * @param $id
     * @return mixed|string
     */
    public function actionEdit($id)
    {
        $model = $this->model->findByPk($id);
        return $this->render('add', array(
            'render_type'=>self::TEMPLATE_EXTENSION_SMARTY,
            'model'=>$model,
        ));
    }

    public function actionDelete($id)
    {
        $model = $this->model->findByPk($id);
        $companyId = $model->companyId;
        if($model->delete())
        {
            $this->redirect('/company/edit?id='.$companyId);
        }
    }

    /**
     * @param $companyId
     * @return mixed|string
     */
    public function actionList($companyId)
    {
        return $this->render('list', array(
            'render_type'=>self::TEMPLATE_EXTENSION_SMARTY,
            'items'=>$this->model->getAll(array(
                'criteria' => array(
                    'condition'=>'t.companyId='.$companyId,
                    'order'=>'t.id desc',
                ),
            ))
        ));
    }

} 