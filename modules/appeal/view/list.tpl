<ul class="list-group appeals-list">
    {foreach from=$items->getData() item=appeal}
        <li class="list-group-item appeal-item appeal-type-{$appeal->type}" onclick="$(this).find('.appeal-text').toggle()">
            <div class="col-lg-12">
                <a href="/appeal/edit?id={$appeal->id}" class="glyphicon glyphicon-edit appeal-edit"></a>
                <div class="appeal-label">
                    {if $appeal->type == 'incoming'}
                        <span class="label label-warning">
                            {$appeal->getFromDictionary('type')}
                        </span>
                    {elseif $appeal->type == 'outgoing'}
                        <span class="label label-success">
                            {$appeal->getFromDictionary('type')}
                        </span>
                    {/if}
                </div>
                <div class="col-lg-4 manager-name">
                    <dl>
                        <dt>Сотрудник</dt>
                        <dd>
                            {if $appeal->getManager()->avatar}
                                <img class="contact-avatar left" src="/{User::$pathToAvatar}{$appeal->getManager()->avatar}">
                            {else}
                                <img class="contact-avatar left" src="/resources/images/noavatar_user.gif">
                            {/if}

                            {$appeal->managerName}
                            <div class="clearfix"></div>
                        </dd>
                    </dl>

                </div>
                <div class="col-lg-4">
                    <dl>
                        <dt>Клиент</dt>
                        <dd>
                            {if $appeal->getContact()->avatar}
                                <img class="contact-avatar left" src="/{User::$pathToAvatar}{$appeal->getContact()->avatar}">
                            {else}
                                <img class="contact-avatar left" src="/resources/images/noavatar_user.gif">
                            {/if}
                            {$appeal->contactPersonName}
                            <div class="clearfix"></div>
                        </dd>
                    </dl>

                </div>
                <div class="col-lg-2 appeal-add-date">
                    <dl>
                        <dt>Добавлено</dt>
                        <dd>
                            <i class="glyphicon glyphicon-calendar"></i>
                            {$appeal->date}
                        </dd>
                    </dl>

                </div>
                <div class="col-lg-2 appeal-remind">
                    <dl>
                        <dt>Напомнить</dt>
                        <dd>
                            <i class="glyphicon glyphicon-bell"></i>
                            {$appeal->remind}
                        </dd>
                    </dl>

                </div>



            </div>
            <div class="col-lg-12 appeal-text">
                <i class="glyphicon glyphicon-pencil"></i>
                {$appeal->text}
            </div>
            <div class="clearfix"></div>
        </li>
    {/foreach}
</ul>