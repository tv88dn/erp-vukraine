

<div class="col-lg-6">
    <div class="panel panel-info warp">
        <div class="panel-heading">
            <h3>Добавление Обращения</h3>
        </div>
        <div class="panel-body">
            <form class="form-horizontal" action="/appeal/add?companyId={$model->companyId}" method="post">
                {if $model->hasErrors()}
                    {foreach from=$model->getErrors() item=error}
                        {for $var=0 to count($error)-1}
                            <div class="alert alert-error">
                                <a class="close" data-dismiss="alert" href="#">×</a>{$error[$var]}
                            </div>
                        {/for}
                    {/foreach}
                {/if}
                {FBootStrap::activeHiddenField($model, 'id')}
                {FBootStrap::activeHiddenField($model, 'companyId')}
                {FBootStrap::formGroup($model, 'companyName', 'activeTextField', [], [
                    'control'=>['width'=>10, 'disabled'=>'disabled'],
                    'label'=>['width'=>2]
                ])}
                {foreach from=$model->getExternalAttributes() key=name item=detail}
                    {FBootStrap::formGroup($model, $name, $detail->type->type, $model->getDictionary($detail, $name), [
                    'control'=>['width'=>10],
                    'label'=>['width'=>2]
                    ])}
                {/foreach}
                <div class="form-group warp text-center">
                    {if $model->id}
                        {FBootStrap::btnDangerLink('Удалить', "/appeal/delete?id=`$model->id`")}
                    {else}
                        {FBootStrap::btnDefaultLink('Отмена', "/company/view?id=`$model->companyId`")}
                    {/if}
                    {FBootStrap::btnPrimarySubmit('Сохранить')}
                </div>
            </form>
        </div>
    </div>
</div>