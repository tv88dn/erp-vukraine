<?php
/**
 * Created by PhpStorm.
 * User: tv88d_000
 * Date: 24.09.2014
 * Time: 1:02
 */

class Appeal extends AppealBase
{
    public $companyName;
    public $manager;
    public $contact;
    /**
     * @param string $className
     * @return AppealBase
     */
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    public function beforeValidate()
    {
        $this->userId = FCore::app()->user->getState('id');
        return parent::beforeValidate();
    }

    public function getCompanyName()
    {
        return Company::model()->findByPk($this->companyId)->name;
    }

    public function getManager()
    {
        if(!$this->manager)
        {
            $this->manager = User::model()->findByPk($this->userId);
        }
        return $this->manager;
    }

    public function getManagerName()
    {
        return $this->getManager()->name;
    }

    public function getContact()
    {
        if(!$this->contact)
        {
            $this->contact = Contact::model()->findByPk($this->contactPerson);
        }
        return $this->contact;
    }

    public function getContactPersonName()
    {
        return $this->getContact()->name;
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'companyId' => 'Company',
            'userId' => 'User',
            'companyName' => 'Название компании',
        );
    }

    /**
     * @param $companyId
     */
    public function setCompany($companyId)
    {
        if($companyId)
        {
            $this->companyId = $companyId;
            $this->companyName = Company::model()->findByPk($companyId)->name;
        }
    }

    /**
     * @param DetailsParent $detail
     * @return mixed
     */
    public function getDictionary($detail)
    {
        if($detail->type->fieldName == 'contactPerson')
        {
            $contacts = Contact::model()->getAll(array(
                    'criteria' => array(
                        'condition'=>'contactCompany.data='.$this->companyId,
                        'with'=>array('contactCompany')
                    ),
                )
            )->getData();
            $contactsNames = array();
            for($i = 0; $i < count($contacts); $i++)
            {
                $contactsNames[$contacts[$i]->id] = $contacts[$i]->name.' ('.$contacts[$i]->group.')';
            }
            return $contactsNames;
        }
        else
        {
            return $detail->getDictionary();
        }

    }
} 