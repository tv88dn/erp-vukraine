<div class="panel panel-primary warp">
    <div class="panel-heading">
        <h1 class="col-lg-10 text-default">{$model->title} <a class="glyphicon glyphicon-pencil text-default" href="/task/edit?id={$model->id}"></a></h1>
        {if $model->isDaysRemainStillNeded()}
            {if $model->timeRemain->invert}
                <div class="badge right task-time-danger text-center">Осталось дней:  {$model->timeRemain->days}</div>
            {else}
                <div class="badge right task-time-info text-center">Просрочено дней:  {$model->timeRemain->days}</div>
            {/if}
        {/if}
        {if $model->canMake()}
            <a href="/task/done?id={$model->id}" class="btn btn-success warp left" onclick="event.stopPropagation()">
                <i class="glyphicon glyphicon-ok"></i> Пометить выполненной
            </a>
        {/if}
        {if $model->canApply()}
            <a href="/task/apply?id={$model->id}" class="btn btn-success warp left" onclick="event.stopPropagation()">
                <i class="glyphicon glyphicon-ok"></i> Принять задачу
            </a>
        {/if}
        <div class="clearfix"></div>
    </div>
    <div class="panel-body">
        <i class="left warp label label-{$model->getPriorityStyle()}">{$model->getFromDictionary('priority')}</i>
        <div class="text-danger right warp"><i class="glyphicon glyphicon-tag"></i>{$model->getFromDictionary('type')}</div>
        <div class="clearfix"></div>
        <div class="well warp">
            <div class="task-date">
                <i class="glyphicon glyphicon-calendar text-info"></i>{$model->dateStart}
                <span class="glyphicon glyphicon-arrow-right warp"></span>
                <i class="glyphicon glyphicon-calendar text-info"></i>{$model->dateEnd}
            </div>
            <legend></legend>
            <div class="clearfix"></div>
            <div class="task-persons col-lg-6">
                <div class="left">
                    <img class="contact-avatar left" src="/{User::$pathToAvatar}{$model->author->avatar}" alt="Автор задачи"/>
                    <div class="right">
                        <div>{$model->author->name}</div>
                        <i>(автор задачи)</i>
                    </div>
                </div>
                <div class="left">
                    <img class="contact-avatar left" src="/{User::$pathToAvatar}{$model->responsible->avatar}" alt="Автор задачи"/>
                    <div class="right">
                        <div> {$model->responsible->name}</div>
                        <i>(исполнитель)</i>
                    </div>
                </div>
                <div class="clearfix"></div>
                <div class="warp well task-description">{$model->text}</div>

            </div>
            <div class="col-lg-6">
                {if !$model->parentId}
                    <legend>Подзадачи:</legend>
                    <a class="btn btn-info" href="/task/add?parentId={$model->id}">
                        <span class="glyphicon glyphicon-tasks warp"></span>Добавить подзадачу
                    </a>
                    {if isset($listSubTasks)}
                        <div class="warp">{$listSubTasks}</div>
                    {/if}
                {else}
                    {if $taskParent}
                        {$taskParent}
                    {/if}
                {/if}
                {if $listFiles}
                    <div class="warp">
                        <legend>Файлы:</legend>
                        {$listFiles}
                    </div>
                {/if}
            </div>

            <div class="clearfix"></div>
        </div>


        <div class="clearfix"></div>
        <legend>Комментарии:</legend>
        {$addComment}
    </div>
</div>