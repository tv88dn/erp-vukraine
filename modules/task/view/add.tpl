<div class="col-lg-12">
    <div class="panel panel-info warp">
        <div class="panel-heading">
            <h3>Добавление Задания</h3>
        </div>
        <div class="panel-body">
            <form class="form-horizontal" action="/task/add" method="post">
                {if $model->hasErrors()}
                    {foreach from=$model->getErrors() item=error}
                        {for $var=0 to count($error)-1}
                            <div class="alert alert-error">
                                <a class="close" data-dismiss="alert" href="#">×</a>{$error[$var]}
                            </div>
                        {/for}
                    {/foreach}
                {/if}
                {FBootStrap::activeHiddenField($model, 'id')}
                {*
                {FBootStrap::activeHiddenField($model, 'companyId')}
                {FBootStrap::activeHiddenField($model, 'projectId')}
*}
                {FBootStrap::activeHiddenField($model, 'parentId')}

                {FBootStrap::formGroup($model, 'title', 'activeTextField', [], [
                'control'=>['width'=>9],
                'label'=>['width'=>3]
                ])}

                {FBootStrap::formGroup($model, 'description', 'activeTextArea', [], [
                'control'=>['width'=>9],
                'label'=>['width'=>3]
                ])}

                {FBootStrap::activeHiddenField($model, 'authorId')}

                {FBootStrap::formGroup($model, 'dateStart', 'activeDateField', [], [
                'control'=>['width'=>2],
                'label'=>['width'=>3]
                ])}

                {FBootStrap::formGroup($model, 'dateEnd', 'activeDateField', [], [
                'control'=>['width'=>2],
                'label'=>['width'=>3]
                ])}

                <div class="col-lg-5 well warp">
                    {FBootStrap::formGroup($model, 'responsibleId', 'activeDropDownList', $model->userCanExecuteTasks(), [
                    'control'=>['width'=>5],
                    'label'=>['width'=>3]
                    ])}

                    <div class="">
                        {$addAgreement}
                    </div>
                </div>

                {*foreach from=$model->getExternalAttributes(['projectId']) key=name item=detail}
                    {FBootStrap::formGroup($model, $name, $detail->type->type, $detail->getDictionary($detail, $name), [
                    'control'=>['width'=>9],
                    'label'=>['width'=>3]
                    ])}
                {/foreach*}

                <div class="col-lg-6 well warp">
                    <legend><i class="glyphicon glyphicon-file"></i> Файлы</legend>
                    {if isset($listFiles)}
                        {$listFiles}
                    {/if}
                    {$addFile}
                    <div class="form-horizontal col-lg-12">
                        <div id="files"></div>
                    </div>
                </div>
                <div class="clearfix"></div>
                <div class="form-group warp text-center">
                    {if $model->id}
                        {FBootStrap::btnDangerLink('Удалить', "/appeal/delete?id=`$model->id`")}
                    {else}
                        {FBootStrap::btnDefaultLink('Отмена', "/company/view?id=`$model->companyId`")}
                    {/if}
                    {FBootStrap::btnPrimarySubmit('Сохранить')}
                </div>
            </form>
        </div>
    </div>
</div>
{if $model->parent}
    <div class="col-lg-6">
        <div class="panel panel-success warp">
            <div class="panel-heading">
                <h3>Базовая задача</h3>
            </div>
            <div class="panel-body">
                {$taskParent}
            </div>
        </div>
    </div>
{/if}
