<div class="right warp">
    {$this->getSort('index', 'Task')}
</div>
<div class="clearfix"></div>
<ul class="list-group task-list">
    {foreach from=$items->getData() item=task}
        <li class="list-group-item task-item">
            <div class="task-body" onclick="$(this).find('.task-text').slideToggle('fast'); event.stopPropagation();">
                <span class="task-check left">
                    <input type="checkbox"/>
                </span>
                <div class="col-lg-2 person-data">
                    <img class="contact-avatar left" src="/{User::$pathToAvatar}{$task->author->avatar}"
                         alt="Автор задачи"/>

                    <div class="person-name">
                        {$task->author->name}
                    </div>
                </div>

                <div class="task-title left{$task->statusClass}">
                    <a onclick="event.stopPropagation();" class="text-default"
                       href="/task/view?id={$task->id}">{$task->title}</a>
                </div>
                <div class="task-additional-info right">
                    <div class="task-statuses">
                        <span class="label label-{$task->getPriorityStyle()}">{$task->getFromDictionary('priority')}</span>
                        <span class="label label-default">
                            <i class="glyphicon glyphicon-paperclip"></i> {$task->getFromDictionary('status')}
                        </span>
                        <i class="glyphicon glyphicon-calendar text-info warp"></i><b>{$task->dateStart}</b>
                    </div>
                </div>
                <div class="clearfix"></div>
                <div class="task-text warp">
                    <div class="col-lg-12 warp">

                        <div class="left">
                            <div class="left warp"><i>Начало: <i
                                            class="glyphicon glyphicon-calendar text-info"></i><b>{$task->dateStart}</b></i>
                            </div>
                            <div class="left warp"><i>Окончание:</i> <i
                                        class="glyphicon glyphicon-calendar text-info"></i><b>{$task->dateEnd}</b></div>
                        </div>

                    </div>
                    <div class="col-lg-12">
                        <legend></legend>
                        {$task->text}
                    </div>

                </div>
                <div class="clearfix"></div>
            </div>
        </li>
    {/foreach}
</ul>