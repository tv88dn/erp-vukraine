<div class="dropdown warp">
    <a class="dropdown-toggle text-default sort-menu" data-toggle="dropdown" href="#">Сортировать по<span class="caret"></span>
        {if $this->model->sort}
            <span class="badge">{$sort->sortFields[$this->model->sort]}</span>
        {/if}
    </a>
    <ul class="dropdown-menu">
        <li><a href="{$sort->getLink('dateEnd.asc')}">{$sort->sortFields['dateEnd.asc']}</a></li>
        <li><a href="{$sort->getLink('priority.asc')}">{$sort->sortFields['priority.asc']}</a></li>
        <li><a href="{$sort->getLink('budget.desc')}">{$sort->sortFields['budget.desc']}</a></li>
    </ul>
</div>