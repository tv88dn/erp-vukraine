<div class="list-group-item light-green-type" onclick="$(this).find('.task-text').slideToggle('fast'); event.stopPropagation();">
    <div class="badge right task-time-info text-center">Осталось дней:  {$task->timeRemain->days}</div>
    <div class="clearfix"></div>
    <div class="task-title left">
        <a onclick="event.stopPropagation();" class="text-default" href="/task/view?id={$task->id}">{$task->title}</a>
        <a onclick="event.stopPropagation();" class="glyphicon glyphicon-pencil" href="/task/edit?id={$task->id}"></a>
    </div>
    <div class="clearfix"></div>
    <div class="col-lg-5 task-type">
        <div class="text-danger left warp"><i class="glyphicon glyphicon-tag"></i>{$task->getFromDictionary('type')}</div>
    </div>
    <div class="task-date col-lg-5">
        <i class="glyphicon glyphicon-calendar text-info"></i>{$task->dateStart}
        <span class="glyphicon glyphicon-arrow-right warp"></span>
        <i class="glyphicon glyphicon-calendar text-info"></i>{$task->dateEnd}
    </div>
    <div class="col-lg-2 text-right">
        <i class="left warp label label-{$task->getPriorityStyle()}">{$task->getFromDictionary('priority')}</i>
    </div>
    <div class="task-persons col-lg-12">
        <div class="col-lg-5">
            <img class="contact-avatar left" src="/{User::$pathToAvatar}{$task->author->avatar}" alt="Автор задачи"/>
            {$task->author->name}<br/>
            <i>(автор задачи)</i>
        </div>
        <div class="col-lg-5">
            <img class="contact-avatar left" src="/{User::$pathToAvatar}{$task->responsible->avatar}" alt="Автор задачи"/>
            {$task->responsible->name}<br/>
            <i>(исполнитель)</i>
        </div>
    </div>
    <div class="clearfix"></div>
    <div class="task-text warp">{$task->text}</div>
</div>