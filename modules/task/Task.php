<?php

/**
 * Class Task
 */
class Task extends TaskBase implements FActivityInterface {

    public $parent;

    private $timeRemain;
    const STATUS_DETAIL_NUMBER = 63;
    const DEADLINE_DETAIL_NUMBER = 59;
    const DATE_START_DETAIL_NUMBER = 58;
    const ADD_TIME_DETAIL_NUMBER = 80;


    public function activityRule()
    {
        return array(
            'insert'=>array(
                'view'=>'task/show',
                'type'=>'success',
                'text'=>'добавил',
            ),
            'update'=>array(
                'view'=>'task/show',
                'type'=>'success',
                'text'=>'обновил',
            ),
            'delete'=>array(
                'view'=>'task/show',
                'type'=>'danger',
                'text'=>'удалил',
            )
        );
    }


    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    public function getLink($sortField)
    {
        return '?'.get_class($this).'[sort]='.$sortField;
    }

    public function getSortFields()
    {
        return array(
            'dateEnd.asc'=>'дате окончания',
            'priority.asc'=>'приоритету',
            'budget.desc'=>'бюджету',
        );
    }

    public function setViewStatus()
    {
        if($this->status == 'add')
        {
            if($this->responsibleId == FCore::app()->user->getState('id'))
            {
                $this->setStatus('view');
            }
        }
        $this->setUserView();
    }

    public function canApply()
    {
        if($this->status == 'make')
        {
            if($this->authorId == FCore::app()->user->getState('id'))
            {
                return true;
            }
        }
    }

    public function isDaysRemainStillNeded()
    {
        if($this->status == 'add' || $this->status == 'view' || $this->status == 'execute')
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    public function canMake()
    {
        if($this->status == 'add' || $this->status == 'view' || $this->status == 'execute')
        {
            if($this->responsibleId == FCore::app()->user->getState('id'))
            {
                return true;
            }
        }
    }

    public function setStatus($status)
    {
        $this->status = $status;
        $this->save();
    }

    public function relations()
    {
        return array(
            'company'=>array(self::HAS_ONE, 'Company', array('id'=>'companyId')),
            'project'=>array(self::HAS_ONE, 'Project', array('id'=>'projectId')),
            'author'=>array(self::HAS_ONE, 'User', array('id'=>'authorId')),
            'responsible'=>array(self::HAS_ONE, 'User', array('id'=>'responsibleId')),
            'subTasks'=>array(self::HAS_MANY, 'Task', array('parentId'=>'id')),
            'detailStatus'=>array(self::HAS_ONE, 'Details', array('moduleId'=>'id'), 'condition'=>'detailStatus.parentId='.self::STATUS_DETAIL_NUMBER),
            'deadline'=>array(self::HAS_ONE, 'Details', array('moduleId'=>'id'), 'condition'=>'deadline.parentId='.self::DEADLINE_DETAIL_NUMBER),
            'comments'=>array(self::HAS_MANY, 'Comments', array('targetId'=>'id'), 'condition'=>'comments.model="'.get_class($this).'"'),
            'agreement'=>array(self::HAS_ONE, 'Agreement', array('taskId'=>'id') ),
            'files'=>array(self::HAS_MANY, 'File', array('targetId'=>'id') ),
        );
    }

    public function afterSave()
    {
        if(!empty($_POST['File']) && !empty($_POST['File']['id'])){
            foreach($_POST['File']['id'] as $id){
                $files = File::model()->findByPk($id);
                $files->targetId = $this->id;
                $files->save();
            }
        }
        parent::afterSave();
    }

    public function checkView()
    {
        if(is_array($this->usersThatView))
        {
            return (in_array(FCore::app()->user->id, $this->usersThatView));
        }
        return false;
    }

    public function setUserView()
    {
        if(!$this->checkView())
        {
            $this->usersThatView = array_merge($this->usersThatView, array(FCore::app()->user->id));
            $this->save();
        }

    }

    public function getStatusClass()
    {
        $status = '';
        if($this->dateEnd < date('Y.m.d'))
        {
            $status .= ' expire';
        }
        if(!$this->checkView())
        {
            $status .= ' not-view';
        }
        return $status;
    }

    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'companyId' => 'Company',
            'projectId' => 'Project',
            'authorId' => 'Автор задачи',
            'responsibleId' => 'Ответсвенный',
            'title'=>'Название задачи',
            'description'=>'Описание задачи'
        );
    }

    public function getPriorityStyle()
    {
        switch ($this->priority)
        {
            case '1': return 'info';
            case '2': return 'success';
            case '3': return 'warning';
            case '4': return 'danger';
            case '-1': return 'default';
        }
    }

    public function addSubTask($parentId)
    {
        $parent = $this->findByPk($parentId);
        if($parent)
        {
            $model = new Task();
            $model->companyId = $parent->companyId;
            $model->projectId = $parent->projectId;
            $model->parentId = $parent->id;
            $model->authorId = FCore::app()->user->getState('id');
            $model->responsibleId = $parent->responsibleId;
            $model->dateStart = date('Y-m-d');
            $model->dateEnd = $parent->dateEnd;
            $model->parent = $parent;
            return $model;
        }
    }

    public function userCanAddTask()
    {
        $users = User::model()->with('userContact')->findAll();
        $contacts[] = '';
        for($i = 0; $i < count($users); $i++)
        {
            $contacts[$users[$i]->id] = $users[$i]->name;
        }
        return $contacts;
    }

    public function getTaskTime()
    {
        return Time::getTaskTime($this->id);
    }

    public function userCanExecuteTasks()
    {
        $users = User::model()->with('userContact')->findAll();
        $contacts = array();
        for($i = 0; $i < count($users); $i++)
        {
            $contacts[$users[$i]->id] = $users[$i]->name;
        }
        return $contacts;
    }

    public function setFileAttributes($File)
    {
        if($File)
        {
            $File['targetId'] = $this->id;
            $File['moduleName'] = get_class($this);
            $File['projectId'] = $this->projectId;
        }
        /*if(!$File['name'])
        {
            return null;
        }*/
        return $File;
    }

    public function setAgreementAttributes($Agreement)
    {
        if($Agreement)
        {
            foreach($Agreement as $key => &$val)
            {
                if($val['userId'])
                {
                    $val['taskId'] = $this->id;
                }
            }
        }
        else
        {
            return null;
        }
        return $Agreement;
    }

    public function getTimeRemain()
    {
        if(!$this->timeRemain)
        {
            $start = new DateTime($this->dateEnd.' 23:59:59');
            $end = new DateTime();
            if($this->dateStart && $this->dateEnd)
            {
                $end->setTimestamp(time());
                $this->timeRemain = $start->diff($end);
            }
        }

        return $this->timeRemain;
    }

} 