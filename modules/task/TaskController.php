<?php

/**
 * Class TaskController
 * @property Task $model
 */
class TaskController extends FController implements FComponentInterface
{
    /**
     * @param string $url
     * @return bool
     */
    public function checkFriendlyUrl($url)
    {
        return false;
    }

    /**
     * @param object $activePage
     * @return string
     */
    public function getTemplate($activePage)
    {
        return 'default';
    }

    /**
     * @return array
     */
    public function getMetaData()
    {
        $metaData = array(
            'title' => 'Не задано',
            'description' => 'Не задано',
            'keywords' => 'Не задано',
            'h1' => 'Не задано',
            'robots' => 'Не задано',
        );
        if(isset($this->action))
        {
            if($this->action->getId() == 'add')
            {
                $metaData['needCache'] = true;
            }
        }
        return $metaData;
    }

    public function actionEdit($id)
    {
        $model = $this->model->findByPk($id);
        return $this->render('add', array(
            'render_type'=>self::TEMPLATE_EXTENSION_SMARTY,
            'model'=>$model,
            'addFile'=>FCore::getInstance()->getControllerByName('FileController')->actionAdd(),
            'listFiles'=>FCore::getInstance()->getControllerByName('FileController')->actionIndex(
                array('moduleName'=>get_class($model), 'targetId'=>$model->id)
            ),
            'addAgreement'=>FCore::getInstance()->getControllerByName('AgreementController')->actionAdd($id, null),
        ));
    }

    public function viewParent($parentId)
    {
        if($parentId)
        {
            return $this->render('parentTask', array(
                'render_type'=>self::TEMPLATE_EXTENSION_SMARTY,
                'task'=>$this->model->findByPk($parentId)
            ));
        }
        else
        {
            return null;
        }
    }

    public function actionDone($id, $time)
    {
        $model = $this->model->findByPk($id);
        if($model->canMake())
        {
            if($time)
            {
                Time::addTimeSection($id, $time);
            }
            $model->time = $model->taskTime;
            $model->setStatus('make');
        }
        $this->redirect(FCore::app()->request->urlReferrer);
    }

    public function actionApply($id)
    {
        $model = $this->model->findByPk($id);
        if($model->canApply())
        {
            $model->applyTime = time();
            $model->setStatus('apply');

        }
        $this->redirect(FCore::app()->request->urlReferrer);
    }

    public function actionView($id)
    {
        $model = $this->model->findByPk($id);
        $model->setViewStatus();

        return $this->render('view', array(
            'render_type'=>self::TEMPLATE_EXTENSION_SMARTY,
            'model'=> $model,
            'listFiles'=>FCore::getInstance()->getControllerByName('FileController')->actionIndex(
                array('moduleName'=>get_class($model), 'targetId'=>$model->id)
            ),
            'addComment'=>FCore::getInstance()->getControllerByName('CommentsController')->addComments($model),
            'taskParent'=>$this->viewParent($model->parentId),
            'listSubTasks'=>$this->actionList(null, null, $model->id)
        ));
    }

    public function actionDelete($id)
    {
        if($this->model->findByPk($id)->delete())
        {
            $this->redirect(FCore::app()->request->urlReferrer);
        }
    }

    public function actionAdd($companyId = null, $projectId = null, $parentId=null,  $Task = null, $File = null, $Agreement = null)
    {
        FCore::initWebSocket();
        $webSocket = new websocketClient('212.66.44.52:9000', 1);
        $webSocket->sendData('tests', FCore::app()->user->id, FCore::app()->user->getState('webSocketSecret'));

        if($companyId === null)
        {
            $companyId = 0;
        }
        if($parentId)
        {
            $model = $this->model->addSubTask($parentId);
        }
        else
        {
            $model = $this->model->loadModel($Task);
            $model->companyId = (isset($Task['companyId'])) ? $Task['companyId'] : $companyId;
            $model->projectId = (isset($Task['projectId'])) ? $Task['projectId'] : $projectId;
            $model->parentId = (isset($Task['parentId'])) ? $Task['parentId'] : $parentId;
        }

        if(!$Task)
        {
            $model->dateStart = date('d.m.Y');
        }

        $model->status = 'add';
        $model->authorId = FCore::app()->user->id;
        $File = $model->setFileAttributes($File);
        $Agreement = $model->setAgreementAttributes($Agreement);

        return $this->render('add', array(
            'render_type'=>self::TEMPLATE_EXTENSION_SMARTY,
            'model'=>$model,
            'addFile'=>FCore::getInstance()->getControllerByName('FileController')->actionAdd($File),
            'addAgreement'=>FCore::getInstance()->getControllerByName('AgreementController')->actionAdd($model->id, $Agreement),
            'taskParent'=>$this->viewParent($model->parentId),
        ));
    }

    public function actionInbox()
    {
        return $this->actionTasks(array(
            'condition'=>'t.responsibleId='.FCore::app()->user->id,
            'with'=>array('detailStatus'=>array(
                'condition'=>'detailStatus.data="view" OR detailStatus.data="add" OR detailStatus.data="execute"'
            ),
                'author',
                'responsible',
            )
        ));
    }


    public function actionActive()
    {
        //var_dump($this->model->relations());

        return $this->actionTasks(array(
            'condition'=>'t.responsibleId='.FCore::app()->user->id,
            'with'=>array('detailStatus'=>array(
                'condition'=>'detailStatus.data="view" OR detailStatus.data="add" OR detailStatus.data="execute"'
            ),
                'author',
                'responsible',
            )
        ));
    }

    public function actionChecked()
    {
        return $this->actionTasks(array(
            'condition'=>'t.responsibleId='.FCore::app()->user->id,
            'with'=>array('detailStatus'=>array(
                'condition'=>'detailStatus.data="make"'
            ))
        ));
    }

    public function actionCompleted()
    {
        return $this->actionTasks(array(
            'condition'=>'t.responsibleId='.FCore::app()->user->id,
            'with'=>array('detailStatus'=>array(
                'condition'=>'detailStatus.data="apply"'
            ))
        ));
    }

    public function actionDeadline()
    {
        $deadlineTime = date('Y-m-d', time() + 3*24*60*60);
        $today = date('Y-m-d');
        return $this->actionTasks(array(
            'condition'=>'t.responsibleId='.FCore::app()->user->id,
            'with'=>array(
                'deadline'=>array(
                    'condition'=>'deadline.data<"'.$deadlineTime.'" AND deadline.data>"'.$today.'"',
                ),
                'detailStatus'=>array(
                    'condition'=>'detailStatus.data="view" OR detailStatus.data="add" OR detailStatus.data="execute"'
                )
            )
        ));
    }

    public function actionMyAgreement()
    {
        return $this->actionTasks(array(
            'with'=>array(
                'agreement'=>array(
                    'condition'=>'agreement.userId='.FCore::app()->user->id,
                )
            ),
        ));
    }

    public function actionTimeIsUp()
    {
        return $this->actionTasks(array(
            'condition'=>'t.responsibleId='.FCore::app()->user->id,
            'with'=>array(
                'deadline'=>array(
                    'condition'=>'deadline.data<"'. date('Y-m-d').'"',
                ),
                'detailStatus'=>array(
                    'condition'=>'detailStatus.data="view" OR detailStatus.data="add" OR detailStatus.data="execute"'
                )
            )
        ));
    }

    public function actionTasks($criteria)
    {
        return $this->render('list', array(
            'render_type'=>self::TEMPLATE_EXTENSION_SMARTY,
            'items'=>$this->model->getAll(array('criteria'=>$criteria))
        ));
    }

    public function actionList($companyId, $projectId = null, $parentId=null)
    {
        return $this->render('list', array(
            'render_type'=>self::TEMPLATE_EXTENSION_SMARTY,
            'items'=>$this->model->getAll(array(
                'criteria' => array(
                    /*'condition'=>'t.companyId='.$companyId.(($projectId !== null) ? ' AND t.projectId='.$projectId : '').' AND t.parentId='.(($parentId !== null) ? $parentId : '0'),*/
                    'condition'=>(($projectId !== null) ? 't.projectId='.$projectId : '').' t.parentId='.(($parentId !== null) ? $parentId : '0'),
                ),
            ))
        ));
    }
} 