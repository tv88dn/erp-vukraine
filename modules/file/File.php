<?php
/**
 * Created by PhpStorm.
 * User: tv88d_000
 * Date: 28.09.2014
 * Time: 17:27
 */

class File extends FileBase {

    public $file;
    public static $pathToFile = 'resources/files/';

    /**
     * @param string $className
     * @return FileBase
     */
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'targetId' => 'Target',
            'moduleName' => 'Module Name',
            'name' => 'Файл',
        );
    }

    public function addFile($File)
    {
        $model = $this->loadModel($File);
    }

    public function getExtension()
    {
        if($this->name)
        {
            $ext = explode('.', $this->name);
            $ext = end($ext);
            return $ext;
        }
        else
        {
            return '';
        }
    }

    public function getCreateDate()
    {
        return date('H:i:s d.m.Y', $this->time);
    }

    public function beforeValidate()
    {
        $this->time = time();
        return parent::beforeValidate();
    }

    public function afterSave()
    {
        if($this->file && !$this->hasErrors())
        {
            $res = $this->file->saveAs(self::$pathToFile.$this->name);
        }
        parent::afterSave();
    }

    public function afterDelete()
    {
        unlink('/'.self::$pathToFile.$this->name);
        return parent::afterDelete();
    }

    public function setAttributes($values,$safeOnly=true)
    {
        //var_dump($values);die;
        /*if(!$values['name'])
        {
            $values['name'] = $this->name;
        }*/
        $res = parent::setAttributes($values, $safeOnly);
        if (isset($values['name'])) {
            $this->file = CUploadedFile::getInstance($this, 'name');

            if ($this->file){
                $this->name = time() . '_' . $this->file->name;
            }
        }
        return $res;
    }
} 