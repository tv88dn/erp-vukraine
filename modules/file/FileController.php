<?php

/**
 * Class FileController
 *
 * @property File $model
 */
class FileController extends FController implements FComponentInterface
{

    /**
     * @param string $url
     * @return bool
     */
    public function checkFriendlyUrl($url)
    {
        return false;
    }

    /**
     * @param object $activePage
     * @return string
     */
    public function getTemplate($activePage)
    {
        return 'default';
    }

    /**
     * @return array
     */
    public function getMetaData()
    {
        $metaData = array(
            'title' => 'Не задано',
            'description' => 'Не задано',
            'keywords' => 'Не задано',
            'h1' => 'Не задано',
            'robots' => 'Не задано',
        );
        if (isset($this->action)) {
            if ($this->action->getId() == 'add') {
                $metaData['needCache'] = true;
            }
        }
        return $metaData;
    }

    public function actionIndex($Filter)
    {
        $criteria = new CDbCriteria();
        foreach ($Filter as $field => $param) {
            $criteria->compare($field, $param);
        }
        return $this->render('index', array(
            'render_type' => self::TEMPLATE_EXTENSION_SMARTY,
            'items' => $this->model->getAll(
                array(
                    'criteria' => $criteria,
                )
            ),
        ));
    }

    public function actionDelete($id)
    {
        $model = $this->model->findByPk($id);
        $module = $model->moduleName;
        $target = $model->targetId;
        if ($model->delete()) {
            $this->redirect('/' . $module . '/edit?id=' . $target);
        }
    }

    public function actionEdit($id, $File = null)
    {
        if ($id) {
            $model = $this->model->findByPk($id);
        } else {
            $model = $this->model->loadModel($File);
        }
        return $this->render('form', array(
            'render_type' => self::TEMPLATE_EXTENSION_SMARTY,
            'model' => $model
        ));
    }

    public function actionAdd($File = null)
    {

        $model = $this->model->loadModel($File);
        if ($model->isEdit) {
            $this->redirect('/' . $model->moduleName . '/edit?id=' . $model->targetId);
        }
        return $this->render('add', array(
            'render_type' => self::TEMPLATE_EXTENSION_SMARTY,
            'model' => $model,
        ));
    }

    public function actionAjax($File = null)
    {
        //var_dump($_FILES);
        $model = new File();
        $model->setAttributes(array('targetId'=>null, 'moduleName'=>'Task', 'name'=>$_FILES['File']['name']['name']));

        $model->save();

        return json_encode(array('id'=>$model->id,'fileID'=>$_POST['fileID'],'name'=>$model->name));
    }
} 