<div class="form-horizontal col-lg-12">
    {if $model->hasErrors()}
        {foreach from=$model->getErrors() item=error}
            {for $var=0 to count($error)-1}
                <div class="alert alert-error">
                    <a class="close" data-dismiss="alert" href="#">×</a>{$error[$var]}
                </div>
            {/for}
        {/foreach}
    {/if}
    {FBootStrap::activeHiddenField($model, 'id')}
    {FBootStrap::activeHiddenField($model, 'targetId')}
    {FBootStrap::activeHiddenField($model, 'moduleName')}

    <!--<div class="col-lg-4" id="dropzone">
        {*FBootStrap::formGroup($model, 'name', 'activeFileField', [], [
        'control'=>['width'=>8, 'multiple'=>true],
        'label'=>['width'=>4]
        ])*}
    </div>-->
    <div id="dropzone">
        <p class="add_file">
            <span style="padding: 0 5px" class="glyphicon glyphicon-plus"></span>
            Загрузить файл<br/>
            <span class="drop">(можно просто перетащить нужный файл в эту область)</span>
        </p>
        <input type="file" id="File_name" name="File[name]" multiple />
    </div>
</div>
