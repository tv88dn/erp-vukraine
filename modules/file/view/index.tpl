{if $items->itemCount}
    <div class="col-lg-12 list-group">
        {foreach from=$items->getData() item=file}
            <div class="col-lg-12 file-item list-group-item light-green-type">
                <div class="left file-img">
                    <img src="/resources/images/docicon.png" alt="Файл" width="100%"/>
                    <div class="label label-danger file-extension">.{$file->getExtension()}</div>
                </div>
                <div class="left file-info col-lg-10">
                    <blockquote>
                        <p>{$file->title}</p>
                        <small>{$file->description}</small>
                        <div class="file-date right">
                            <i class="glyphicon glyphicon-time"></i><span class="warp">{$file->createDate}</span>
                        </div>
                    </blockquote>
                </div>
                <div class="file-control-buttons">
                    <a class="btn btn-info right warp" href="/{File::$pathToFile}{$file->name}">
                        <div class="glyphicon glyphicon-download-alt"></div>
                    </a>
                    <a class="btn btn-success right warp" href="/file/edit?id={$file->id}">
                        <div class="glyphicon glyphicon-edit"></div>
                    </a>
                    <a class="btn btn-danger right warp" href="/file/delete?id={$file->id}">
                        <div class="glyphicon glyphicon-remove"></div>
                    </a>
                </div>
            </div>
        {/foreach}
    </div>
{/if}