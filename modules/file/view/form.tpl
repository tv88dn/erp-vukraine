<br/>
<form class="form-horizontal col-lg-8 col-lg-offset-2 well" action="/file/edit">
    <h3>Редактировать файл</h3>
    <div class="col-lg-12 file-item list-group-item warp">
        {if $model->hasErrors()}
            {foreach from=$model->getErrors() item=error}
                {for $var=0 to count($error)-1}
                    <div class="alert alert-error">
                        <a class="close" data-dismiss="alert" href="#">×</a>{$error[$var]}
                    </div>
                {/for}
            {/foreach}
        {/if}
        {FBootStrap::activeHiddenField($model, 'id')}
        {FBootStrap::activeHiddenField($model, 'targetId')}
        {FBootStrap::activeHiddenField($model, 'moduleName')}
        {FBootStrap::activeHiddenField($model, 'projectId')}
        <div class="col-lg-6">
            <table class="standard">
                <tr>
                    <td>
                        <div class="left file-img">
                            <img src="/resources/images/docicon.png" alt="Файл" width="100%"/>
                            <div class="label label-danger file-extension">.{$model->getExtension()}</div>
                        </div>
                    </td>
                    <td>
                        {$model->name}
                    </td>
                </tr>
            </table>
            <div class="clearfix"></div>

        </div>
        <div class="col-lg-6">
            <div class="col-lg-12">
                {FBootStrap::formGroup($model, 'name', 'activeFileField', [], [
                'control'=>['width'=>9],
                'label'=>['width'=>3]
                ])}
            </div>
            <div class="col-lg-12">
                {FBootStrap::formGroup($model, 'title', 'activeTextField', [], [
                'control'=>['width'=>9],
                'label'=>['width'=>3]
                ])}
            </div>

        </div>
        <div class="col-lg-12">
            {FBootStrap::formGroup($model, 'description', 'activeTextArea', [], [
            'control'=>['width'=>10],
            'label'=>['width'=>2]
            ])}
        </div>
        <div class="form-group warp text-center">
            {if $model->id}
                {FBootStrap::btnDangerLink('Удалить', "/file/delete?id=`$model->id`")}
            {else}
                {FBootStrap::btnDefaultLink('Отмена', "/{$model->moduleName}/view?id=`$model->tergetId`")}
            {/if}
            {FBootStrap::btnPrimarySubmit('Сохранить')}
        </div>

    </div>
</form>