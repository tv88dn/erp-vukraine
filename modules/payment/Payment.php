<?php

/**
 * Class Payment
 *
 * @property string $returnUrl
 */
class Payment extends PaymentBase
{
    const SEND_BILL_FIELD = 'sendBill';
    const DO_NOT_SEND_BILL = 'doNotSend';

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'userId' => 'User',
            'companyId' => 'Company',
            'projectId' => 'Project',
            'amount' => 'Сумма',
        );
    }

    public function relations()
    {
        return array(
            'company'=>array(self::HAS_ONE, 'Company', array('id'=>'companyId')),
            'user'=>array(self::HAS_ONE, 'User', array('id'=>'userId')),
        );
    }

    public function getDictionary($detail)
    {
        if($detail->type->fieldName == self::SEND_BILL_FIELD)
        {
            $contacts = Company::model()->findByPk($this->companyId)->getContacts()->getData();
            $dictionaryContacts = array();
            $dictionaryContacts[self::DO_NOT_SEND_BILL] = 'Не отправлять счет';
            for($i = 0; $i < count($contacts); $i++)
            {
                if($contacts[$i]->mail)
                {
                    $dictionaryContacts[$contacts[$i]->mail] = $contacts[$i]->name . ' ('.$contacts[$i]->mail.')';
                }
            }
            return $dictionaryContacts;
        }
        else
        {
            return $detail->getDictionary();
        }
    }

    public function beforeValidate()
    {
        $this->userId = FCore::app()->user->getState('id');
        $this->time = time();
        return parent::beforeValidate();
    }

    public function getReturnUrl()
    {
        return '/'.(($this->projectId) ? 'project' : 'company').'/view?id='.(($this->projectId) ? $this->projectId : $this->companyId);
    }
} 