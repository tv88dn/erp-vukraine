<div class="col-lg-6 col-lg-offset-3">
    <div class="panel panel-primary">
        <div class="panel-heading">
            <h1>Добавление оплаты</h1>
        </div>
        <div class="panel-body">
            <form class="form-horizontal" action="/payment/add" method="post">
                {if $model->hasErrors()}
                    {foreach from=$model->getErrors() item=error}
                        {for $var=0 to count($error)-1}
                            <div class="alert alert-error">
                                <a class="close" data-dismiss="alert" href="#">×</a>{$error[$var]}
                            </div>
                        {/for}
                    {/foreach}
                {/if}
                {FBootStrap::activeHiddenField($model, 'id')}
                {FBootStrap::activeHiddenField($model, 'companyId')}
                {FBootStrap::activeHiddenField($model, 'projectId')}
                {FBootStrap::formGroup($model, 'amount', 'activeTextField', [], [
                'control'=>['width'=>4],
                'label'=>['width'=>3]
                ])}
                <div class="clearfix"></div>
                {foreach from=$model->getExternalAttributes() key=name item=detail}

                    {FBootStrap::formGroup($model, $name, $detail->type->type, $model->getDictionary($detail), [
                    'control'=>['width'=>9, 'id'=>$detail->type->fieldName],
                    'label'=>['width'=>3]
                    ])}

                {/foreach}
                <div class="col-lg-6 col-lg-offset-3 add-company-btn-group">
                    {if $model->id}
                        {FBootStrap::btnDangerLink('Удалить', "/users/delete?id=`$model->id`")}
                    {/if}
                    {FBootStrap::btnPrimarySubmit('Сохранить')}
                </div>
            </form>
        </div>
    </div>
</div>
