<ul class="list-group payment-list">
{foreach from=$items->getData() item=payment}
    <li class="payment list-group-item light-green-type" onclick="$(this).find('.payment-additional').slideToggle('fast')">
        <div class="img-thumbnail col-lg-2 text-center warp">
            <img src="/{Company::$pathToLogo}{$payment->company->photo}" height="40">
            {if $payment->debt}
                <a class="btn btn-success" href="/payment/repay?id={$payment->id}" onclick="event.stopPropagation();">Погасить долг</a>
            {/if}
        </div>
        <div class="col-lg-5 warp payment-contact">
            <img class="contact-avatar left warp" src="/{User::$pathToAvatar}{$payment->user->avatar}">
            <div class="left warp">
                {$payment->user->name}
                <div><i class="glyphicon glyphicon-time warp"></i>{date('H:i:s d-m-Y', $payment->time)}</div>
            </div>
        </div>
        <div class="col-lg-2 payment-remind">
            <i class="glyphicon glyphicon-bell"></i>{$payment->remind}
        </div>
        <div class="col-lg-2 payment-amount">
            <i class="glyphicon glyphicon-usd"></i>{$payment->amount}
            {if $payment->debt}
                <span class="label label-danger">
                    <i class="glyphicon glyphicon-warning-sign"></i> Долг
                </span>
            {/if}
        </div>
        <a href="/payment/edit?id={$payment->id}" onclick="event.stopPropagation();" class="right glyphicon glyphicon-pencil"></a>
        <div class="clearfix"></div>
        <div class="payment-additional">
            <div class="row">
                <div class="col-lg-3 text-right"><b>Тип оплаты:</b></div>
                <div class="col-lg-9"><i>{$payment->getFromDictionary('type')}</i></div>
            </div>
            <div class="row">
                <div class="col-lg-3 text-right"><b>Назначение платежа:</b></div>
                <div class="col-lg-9"><i>{$payment->getFromDictionary('apointment')}</i></div>
            </div>
            {if $payment->sendBill && $payment->sendBill != Payment::DO_NOT_SEND_BILL}
                <div class="row">
                    <div class="col-lg-3 text-right"><b>Счет на оплату отправлен:</b></div>
                    <div class="col-lg-9"><i>{$payment->sendBill}</i></div>
                </div>
            {/if}
            <div class="row">
                <div class="col-lg-3 text-right"><b>Комментарии:</b></div>
                <div class="col-lg-9"><i>{$payment->comment}</i></div>
            </div>
        </div>
        <div class="clearfix"></div>
    </li>
{/foreach}
</ul>
<div class="clearfix"></div>