<?php

/**
 * Class PaymentController
 *
 * @property Payment $model
 */
class PaymentController extends FController implements FComponentInterface {

    /**
     * @param string $url
     * @return bool
     */
    public function checkFriendlyUrl($url)
    {
        return false;
    }

    /**
     * @param object $activePage
     * @return string
     */
    public function getTemplate($activePage)
    {
        return 'default';
    }

    /**
     * @return array
     */
    public function getMetaData()
    {
        $metaData = array(
            'title' => 'Не задано',
            'description' => 'Не задано',
            'keywords' => 'Не задано',
            'h1' => 'Не задано',
            'robots' => 'Не задано',
        );
        if(isset($this->action))
        {
            if($this->action->getId() == 'add')
            {
                $metaData['needCache'] = true;
            }
        }
        return $metaData;
    }

    public function actionList($companyId = null, $projectId = null)
    {
        $criteria = new CDbCriteria();
        if($companyId !== null)
        {
            $criteria->addCondition('companyId='.$companyId);
        }
        if($projectId !== null)
        {
            $criteria->addCondition('projectId='.$projectId);
        }

        return $this->render('list', array(
            'render_type'=>self::TEMPLATE_EXTENSION_SMARTY,
            'items'=>$this->model->getAll(array('criteria'=>$criteria)),
        ));
    }

    public function actionEdit($id)
    {
        $model = $this->model->findByPk($id);
        return $this->render('add', array(
            'render_type'=>self::TEMPLATE_EXTENSION_SMARTY,
            'model'=>$model,
        ));
    }

    public function actionAdd($companyId, $projectId = 0, $Payment = null)
    {
        $model = $this->model->loadModel($Payment);
        if(!$Payment)
        {
            $model->companyId = $companyId;
            $model->projectId = $projectId;
        }
        if($Payment && !$model->hasErrors())
        {
            $this->redirect($model->returnUrl);
        }
        return $this->render('add', array(
            'render_type'=>self::TEMPLATE_EXTENSION_SMARTY,
            'model'=>$model,
        ));
    }

    public function actionRepay($id)
    {
        $model = $this->model->findByPk($id);
        $model->debt = 0;
        if($model->save())
        {
            $this->redirect(FCore::app()->request->urlReferrer);
        }
    }

} 