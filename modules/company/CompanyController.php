<?php
/**
 * Created by PhpStorm.
 * User: tv88d_000
 * Date: 08.09.14
 * Time: 2:44
 */

class CompanyController extends FController implements FComponentInterface
{
    public function checkFriendlyUrl($url)
    {
        return false;
    }

    public function getMetaData()
    {
        return null;
    }

    public function getTemplate($activePage)
    {
        return 'default';
    }

    public function actionControl()
    {
        return $this->render('control', array(
            'render_type'=>self::TEMPLATE_EXTENSION_SMARTY
        ));
    }

    public function actionDelete($id)
    {
        $model = Company::model()->findByPk($id);
        if($model->delete())
        {
            $this->redirect('/company/index');
        }
        else
        {
            throw new Exception('Ошибка удаления компании');
        }
    }

    public function actionAdd($Company)
    {
        if(($id = $this->actionCheckCompanyName($Company['name'])) >= 0 && !isset($Company['id']))
        {
            $model = new Company;
            $model->setAttributes($Company);
            if(!$model->saveAnyway)
            {
                $model->alreadyExists = true;
                return $this->render('add', array(
                    'render_type'=>self::TEMPLATE_EXTENSION_SMARTY,
                    'existsCompany'=>$id,
                    'model'=>$model,
                ));
            }
        }
        $model = Company::model()->loadModel($Company);
        if($model->isEdit)
        {
            $this->redirect('/company/index');
        }
        $model->managerName = FCore::app()->user->getState('userModel')->name;
        $model->manager = FCore::app()->user->getState('userModel')->id;
        $model->bills = 'notSet';
        return $this->render('add', array(
            'render_type'=>self::TEMPLATE_EXTENSION_SMARTY,
            'model'=>$model,
        ));
    }

    public function actionEdit($id)
    {
        $model = Company::model()->findByPk($id);
        return $this->render('add', array(
            'render_type'=>self::TEMPLATE_EXTENSION_SMARTY,
            'model'=>$model,
            'contacts'=>FCore::getInstance()->initController('ContactController')->actionAdd($id),
            'listContacts'=>FCore::getInstance()->initController('ContactController')->actionList($id),
        ));
    }

    public function actionView($id)
    {
        $model = Company::model()->findByPk($id);
        return $this->render('view', array(
            'render_type'=>self::TEMPLATE_EXTENSION_SMARTY,
            'model'=>$model,
            'contacts'=>FCore::getInstance()->initController('ContactController')->actionAdd($id),
            'listContacts'=>FCore::getInstance()->initController('ContactController')->actionList($id),
            'listAppeals'=>FCore::getInstance()->initController('AppealController')->actionList($id),
            'listTasks'=>FCore::getInstance()->initController('TaskController')->actionList($id),
            'listPayments'=>FCore::getInstance()->initController('PaymentController')->actionList($id),
            'projects'=>FCore::getInstance()->initController('ProjectController')->actionIndex(array(
                'criteria'=>array(
                    'condition'=>'companyId = '.$model->id,
                )
            )),
        ));
    }

    public function actionIndex()
    {
        return $this->render('index', array(
            'render_type'=>self::TEMPLATE_EXTENSION_SMARTY,
            'items'=>$this->model->getAll(),
        ));
    }

    public function actionCheckCompanyName($name)
    {
        $company =  Company::model()->findByAttributes(array('name'=>$name));
        return $company ? $company->id : '-1';
    }
} 