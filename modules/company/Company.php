<?php
/**
 * Created by PhpStorm.
 * User: tv88d_000
 * Date: 08.09.14
 * Time: 2:45
 */

class Company extends CompanyBase {

    public $logo;

    public $managerName;
    public $saveAnyway;
    public $alreadyExists;

    public static $pathToLogo = 'resources/images/uploads/company/logo/';

    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'companyManager'=>array(self::HAS_ONE, 'User', array('id' => 'manager')),
        );
    }

    public function afterSave()
    {
        if($this->logo && !$this->hasErrors())
        {
            $this->logo->saveAs(self::$pathToLogo.$this->photo);
        }
        parent::afterSave();
    }

    public function setAttributes($values,$safeOnly=true)
    {
        $res = parent::setAttributes($values, $safeOnly);
        if (isset($values['logo'])) {
            $this->logo = CUploadedFile::getInstance($this, 'logo');
            if ($this->logo){
                $this->photo = time() . '_' . $this->logo->name;
            }
        }
        return $res;
    }


    public function afterConstruct()
    {
        $res = parent::afterConstruct();
        if(FCore::app()->user->getState('userId'))
        {
            $this->managerName = FCore::app()->user->getState('userModel')->name;
            $this->manager = FCore::app()->user->getState('userModel')->id;
            $this->bills = 'notSet';
        }
        return $res;
    }

    public function afterFind()
    {
        $this->managerName = User::model()->findByPk($this->manager)->name;
        return parent::afterFind();
    }


    public function parseAttribute($name)
    {
        switch($name)
        {
            case 'phone': $val = (is_array($this->$name)) ? implode(', ', $this->$name) : $this->$name; break;
            case 'site': $val = FBootStrap::link(
                str_replace('http://', '', $this->$name),
                'http://'.str_replace('http://', '', $this->$name),
                array('class'=>'companySiteLink'));
            break;
            case 'status': $val = DetailsDictionary::model()->findByAttributes(array('value'=>$this->$name))->data; break;
            default: $val =  $this->$name; break;
        }
        return $val;
    }

    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    public function getContacts()
    {
        return Contact::model()->getAll(array(
                'criteria' => array(
                    'condition'=>'contactCompany.data='.$this->id,
                    'with'=>array('contactCompany')
                ),
            )
        );
    }

    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'name' => 'Название компании',
            'manager' => 'Менеджер',
            'managerName' => 'Менеджер',
        );
    }

} 