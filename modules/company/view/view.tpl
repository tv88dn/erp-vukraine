<script type="text/javascript" src="/resources/js/User.js"></script>
<div class="panel panel-primary warp">
    <div class="panel-heading">
        <h1 class="left">Компания <b class="text-info">{$model->name}</b></h1>
        <div class="clearfix"></div>
    </div>
    <div class="panel-body">
        <div class="well col-lg-5">
            <div class="col-lg-4">
                <div id="company-logo" class="left">
                    <img class="img-thumbnail" data-src="holder.js/200x200" src="/{Company::$pathToLogo}{$model->photo}" style="width: 150px;"  alt="фото" >
                </div>
                <div class="form-group col-lg-12">
                    <label class="col-lg-4 text-right">Менеджер:</label>
                    <span class="col-lg-12">
                        <img class="contact-avatar left" src="/{User::$pathToAvatar}{$model->companyManager->avatar}" alt="Программист"/>
                        <i>{$model->companyManager->name}</i>
                    </span>
                </div>
            </div>
            <div class="col-lg-8">
                <a class="btn btn-success right warp company-edit-button" href="/company/edit?id={$model->id}">
                    <div class="glyphicon glyphicon-edit"></div>
                </a>
                <div class="form-group col-lg-12">

                    <label class="col-lg-4 text-right">Город:</label>
                    <span class="col-lg-8">
                        {if $model->city}
                            <i class="glyphicon glyphicon-home text-success"></i>
                            <i>{$model->city}</i>
                        {else}
                            <i class="glyphicon glyphicon-home text-success"></i>
                            <span class="text-danger">не указано</span>
                        {/if}
                    </span>
                </div>
                <div class="form-group col-lg-12">
                    <label class="col-lg-4 text-right">Адрес:</label>
                    <span class="col-lg-8">
                        {if $model->address}
                            <i class="glyphicon glyphicon-map-marker text-success"></i>
                            <i>{$model->address}</i>
                        {else}
                            <i class="glyphicon glyphicon-map-marker text-danger"></i>
                            <span class="text-danger">не указано</span>
                        {/if}
                    </span>
                </div>
                <div class="form-group col-lg-12">
                    <label class="col-lg-4 text-right">Статус:</label>
                    <span class="col-lg-8">
                        {if $model->status == 'client'}
                            <i class="glyphicon glyphicon-ok-circle text-success"></i>
                        {elseif $model->status == 'potential'}
                            <i class="glyphicon glyphicon-question-sign text-warning"></i>
                        {elseif $model->status == 'competitor'}
                            <i class="glyphicon glyphicon-exclamation-sign text-danger"></i>
                        {/if}
                        <i>{$model->getFromDictionary('status')}</i>
                    </span>
                </div>
                <div class="form-group col-lg-12">
                    <label class="col-lg-4 text-right">Телефоны:</label>
                    <span class="col-lg-8">
                        {foreach from=$model->phone item=phone}
                            {if $phone}
                                <i class="glyphicon glyphicon-phone text-success"></i>
                                <i>{$phone}</i>
                            {else}
                                <i class="glyphicon glyphicon-phone text-danger"></i>
                                <span class="text-danger">не указано</span>
                            {/if}
                        {foreachelse}
                            <i class="glyphicon glyphicon-phone text-danger"></i>
                            <span class="text-danger">не указано</span>
                        {/foreach}
                    </span>
                </div>
                <div class="form-group col-lg-12">
                    <label class="col-lg-4 text-right">Email:</label>
                    <span class="col-lg-8">
                        {if $model->mail}
                            <i class="glyphicon glyphicon-envelope text-success"></i>
                            <i>{$model->mail}</i>
                        {else}
                            <i class="glyphicon glyphicon-envelope text-danger"></i>
                            <span class="text-danger">не указано</span>
                        {/if}
                    </span>
                </div>
                {if $model->status == 'client'}
                    <div class="form-group col-lg-12">
                        <label class="col-lg-4 text-right">Юрлицо для счетов:</label>
                    <span class="col-lg-8">
                        {if $model->bills == 'notSet'}
                            <i class="glyphicon glyphicon-credit-card text-danger"></i>
                            <span class="text-danger">не указано</span>
                        {else}
                            <i class="glyphicon glyphicon-credit-card text-success"></i>
                            <i>{$model->getFromDictionary('bills')}</i>
                        {/if}
                    </span>
                    </div>
                {/if}
            </div>
            <div class="clearfix"></div>
        </div>
        <div class="col-lg-7">
            <ul class="nav nav-tabs">
                <li class="active">
                    <a href="#projects" data-toggle="tab">Проекты</a>
                </li>
                <li>
                    <a href="#contacts" data-toggle="tab">Контакты</a>
                </li>
                <li>
                    <a href="#appeals" data-toggle="tab">Обращения</a>
                </li>
                <li>
                    <a href="#tasks" data-toggle="tab">Задачи</a>
                </li>
                <li>
                    <a href="#payments" data-toggle="tab">Оплаты</a>
                </li>
            </ul>
            <div class="tab-content well">
                <div class="tab-pane active" id="projects">
                    <a href="/project/add?companyId={$model->id}" class="btn btn-success">
                        <i class="glyphicon glyphicon-plus"></i>
                        Добавить проект
                    </a>
                    {if isset($projects)}
                        <div class="clearfix"></div>
                        {$projects}
                    {/if}
                    <div class="clearfix"></div>
                </div>
                <div class="tab-pane fade" id="contacts">
                    <a href="/contact/add?companyId={$model->id}" class="btn btn-info">
                        <i class="glyphicon glyphicon-user"></i>
                        Новый контакт
                    </a>
                    {if isset($listContacts)}
                        <div class="clearfix"></div>
                        {$listContacts}
                    {/if}
                    <div class="clearfix"></div>
                </div>
                <div class="tab-pane fade" id="appeals">
                    <a class="btn btn-success left warp" href="/appeal/add?companyId={$model->id}">
                        <div class="glyphicon glyphicon-transfer"></div>
                        Добавить обращение
                    </a>
                    {if isset($listAppeals)}
                        <div class="clearfix"></div>
                        {$listAppeals}
                    {/if}
                    <div class="clearfix"></div>
                </div>
                <div class="tab-pane fade" id="tasks">
                    <a class="btn btn-success left warp" href="/task/add?companyId={$model->id}">
                        <div class="glyphicon glyphicon-tasks"></div>
                        Добавить задачу
                    </a>
                    {if isset($listTasks)}
                        <div class="clearfix"></div>
                        {$listTasks}
                    {/if}
                </div>
                <div class="tab-pane fade" id="payments">
                    <a class="btn btn-success left warp" href="/payment/add?companyId={$model->id}">
                        <div class="glyphicon glyphicon-usd"></div>
                        Добавить оплату
                    </a>
                    {if isset($listPayments)}
                        <div class="clearfix"></div>
                        {$listPayments}
                    {/if}
                </div>
            </div>
        </div>




    </div>
</div>