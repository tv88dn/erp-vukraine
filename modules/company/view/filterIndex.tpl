<div class="filter-block well ">
    <form class="form-horizontal col-lg-12" method="get">
        {for $i = 0 to count($filter->fields)-1}
            <div class="left">
                {FBootStrap::formGroup(
                $filter->fields[$i]->getFieldModel(),
                $filter->fields[$i]->attribute,
                $filter->fields[$i]->type,
                $filter->fields[$i]->getUniqueFields(),
                [
                'control'=>[
                'name'=>$filter->fields[$i]->getRequestName(),
                'width'=>8
                ],
                'label'=>[
                'width'=>4
                ]
                ]
                )}
            </div>


        {/for}
        <div class="right">
            {FBootStrap::btnSuccessSubmit('Искать')}
            {FBootStrap::btnDangerLink('Сбросить', "/company/index")}
        </div>
        <div class="clearfix"></div>
    </form>
    <div class="clearfix"></div>
</div>
