<script type="text/javascript" src="/resources/js/User.js"></script>
<div class="panel panel-primary warp">
    <div class="panel-heading">
        <a href="/company/index" class="glyphicon glyphicon-th-list right addButtonInTitle"></a>
        <h1>Добавление пользователя</h1>
    </div>
    <div class="panel-body">
        <div class="well">
            <form id="addCompany" name="addCompany" method="post" enctype="multipart/form-data" class="form-horizontal" action="/company/add">
                    {if $model->hasErrors()}
                        {foreach from=$model->getErrors() item=error}
                            {for $var=0 to count($error)-1}
                                <div class="alert alert-error">
                                    <a class="close" data-dismiss="alert" href="#">×</a>{$error[$var]}
                                </div>
                            {/for}
                        {/foreach}
                    {/if}
                    {if $model->alreadyExists && !$model->saveAnyway}
                        <div class="alert alert-warning">
                            <p>Компания с таким именем уже существует. {FBootStrap::link('Просмотреть существующую компанию', "/company/edit?id=`$existsCompany`")}</p>
                            <p>Вы уверены, что все равно хотите сохранить компанию? </p>
                            <p>{FBootStrap::activeCheckBox($model, 'saveAnyway')}<label>Да, сохраняем!</label></p>
                        </div>
                    {/if}
                    {if $model->id}
                        {FBootStrap::activeHiddenField($model, 'id')}
                    {/if}
                    {FBootStrap::formGroup($model, 'name', 'activeTextField', [], [
                    'control'=>['width'=>6],
                    'label'=>['width'=>2],
                    'button'=>[
                    'type'=>'success',
                    'position'=>FBootStrap::POSITION_RIGHT,
                    'label'=>'Проверить',
                    'htmlOptions'=>[
                    'id'=>'companyCheck'
                    ]
                    ]
                    ])}
                <div class="clearfix"></div>
                <div class="col-lg-2">
                    <div class="warp">
                        <legend class="add-company-info">Лого компании</legend>
                        <div id="company-logo">
                            <img class="img-thumbnail left" data-src="holder.js/200x200" src="/{Company::$pathToLogo}{$model->photo}" style="width: 150px;"  alt="фото" >
                            <div class="load-img-block left">
                                <a href="#" id="load" class="glyphicon glyphicon-upload col-lg-6 text-center text-success"></a>
                                <a href="#" id="delete" class="glyphicon glyphicon-remove-circle col-lg-6 text-center text-danger"></a>
                            </div>
                            {FBootStrap::activeFileField($model, 'logo')}
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <div class="warp">
                        <legend class="add-company-info">Проекты:</legend>
                        <div class="clearfix"></div>
                    </div>
                </div>
                <div class="col-lg-4">

                    <legend class="add-company-info">Информация о компании</legend>

                    {FBootStrap::activeHiddenField($model, 'manager')}

                    {FBootStrap::formGroup($model, 'managerName', 'activeTextField', [], [
                    'control'=>['width'=>9, 'disbled'=>'disabled'],
                    'label'=>['width'=>3]
                    ])}

                    {foreach from=$model->getExternalAttributes() key=name item=detail}

                           {FBootStrap::formGroup($model, $name, $detail->type->type, $detail->getDictionary(), [
                           'control'=>['width'=>9, 'id'=>$detail->type->fieldName],
                           'label'=>['width'=>3]
                           ])}

                    {/foreach}


                    <div class="col-lg-6 col-lg-offset-1 add-company-btn-group">
                        {if $model->id}
                            {FBootStrap::btnDangerLink('Удалить', "/users/delete?id=`$model->id`")}
                        {else}
                            {FBootStrap::btnDefaultLink('Отмена', "/users/index")}
                        {/if}
                        {FBootStrap::btnPrimarySubmit('Сохранить')}
                    </div>
                </div>
            </form>
            {if isset($contacts)}
                <div id="addContactForm" class="col-lg-5">
                    {$contacts}
                </div>
            {/if}
            <div class="clearfix"></div>
            {if isset($listContacts)}
                <legend>Контакты</legend>
                {$listContacts}
                <div class="clearfix"></div>
            {/if}

        </div>
    </div>
</div>