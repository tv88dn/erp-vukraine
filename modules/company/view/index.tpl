<div class="panel panel-primary warp">
    <div class="panel-heading">
        <a href="/company/add" class="glyphicon glyphicon glyphicon-plus right addButtonInTitle"></a>
        <h1>Список компаний</h1>
    </div>
    <div class="panel-body">
        {$this->filter}
        {foreach from=$items->data item=row}
            <div class="company-block col-lg-3" onclick="location.href='/company/view?id={$row->id}'">
                <div class="well">
                    <div class="img-thumbnail col-lg-12 text-center">
                        <img src="/{Company::$pathToLogo}{$row->photo}">
                    </div>
                    <div class="col-lg-12 company-name">{$row->name}</div>
                    <div class="clearfix"></div>
                    <div class="company-description">
                        <div class="well company-card">
                            <div class="warp text-right">
                                {if $row->status == 'client'}
                                    <b class="text-success"><i class="glyphicon glyphicon-ok"></i> {$row->parseAttribute('status')}</b>
                                {/if}
                            </div>
                            {if $row->city || $row->address}
                            <div class="warp text-right">
                                <i class="glyphicon glyphicon-map-marker"></i>
                                {$row->city} {$row->address}
                            </div>
                            {/if}
                            {if $row->mail}
                                <div class="warp text-right">
                                    <i class="glyphicon glyphicon-envelope"></i>
                                    {$row->mail}
                                </div>
                            {/if}
                            {if $row->phone}
                                <div class="warp text-right">
                                    <i class="glyphicon glyphicon-phone-alt"></i>
                                    <b>{$row->parseAttribute('phone')}</b>
                                </div>
                            {/if}
                            <i class="label label-info right manager-label">
                                <b>Менеджер: </b>
                                <i>{$row->managerName}</i>
                            </i>
                            <ul class="user-contact list-group company-contacts clearfix">
                                {foreach from=$row->getContacts()->getData() item=contact key=i}
                                    {if $i < 4}
                                        <li class="list-group-item company-contact-row" location="/contact/view?id={$contact->id}">
                                            {if $contact->avatar}
                                                <img class="contact-avatar left" src="/{User::$pathToAvatar}{$contact->avatar}">
                                            {else}
                                                <img class="contact-avatar left" src="/resources/images/noavatar_user.gif">
                                            {/if}
                                            <div class="contact-name">{$contact->name}</div>
                                            <div class="clearfix"></div>
                                        </li>
                                    {/if}
                                {/foreach}
                            </ul>
                        </div>

                    </div>
                </div>

            </div>
        {/foreach}
        {$this->smartyWidget('FLinkPager', [
        	'pages'=>$items->pagination,
        	'header'=>"<div class='pager'>",
            'footer'=>"</div>"])}
    </div>
</div>