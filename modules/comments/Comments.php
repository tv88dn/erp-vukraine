<?php

/**
 * Class Comments
 *
 */
class Comments extends CommentsBase implements FActivityInterface {

    const TIME_DETAIL_NUMBER = 70;

    protected $target;

    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'userId' => 'User',
            'model' => 'Model',
            'targetId' => 'Target',
            'text' => 'Текст комментария',
        );
    }

    public function activityRule()
    {
        return array(
            'insert'=>array(
                'view'=>'comments/show',
                'type'=>'info',
                'text'=>'добавил',
            ),
            'update'=>array(
                'view'=>'comments/show',
                'type'=>'info',
                'text'=>'обновил',
            )
        );
    }

    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    public function getReturnUrl()
    {
        return '/'.strtolower($this->model).'/view?id='.$this->targetId;
    }

    public function relations()
    {
        return array(
            'user'=>array(self::HAS_ONE, 'User', array('id'=>'userId')),
            'task'=>array(self::HAS_ONE, 'Task', array('id'=>'targetId'), 'condition'=>'model="Task"'),
        );
    }

    public function beforeValidate()
    {
        $this->time = time();
        return parent::beforeValidate();
    }

    public static function creatComment($_model)
    {
        $model = new Comments();
        $model->model = get_class($_model);
        $model->userId = FCore::app()->user->getState('id');
        $model->targetId = $_model->id;
        return $model;
    }

    public function getTarget()
    {
        $model =  call_user_func_array(array($this->model, 'model'), array());
        return $model->findByPk($this->targetId);
    }
} 