<div class="col-lg-10">
    {foreach from=$items->getData() item=comment}
        <div class="well">
            <a href="/comments/edit?id={$comment->id}" class="glyphicon glyphicon-pencil"></a>
            <img class="contact-avatar left" src="/{User::$pathToAvatar}{$comment->user->avatar}">
            <div class="left warp">
                <div>
                    {$comment->user->name}
                    <div><i class="glyphicon glyphicon-time warp"></i>{date('H:i:s d-m-Y', $comment->time)}</div>
                </div>
            </div>
            <div class="clearfix"></div>
            <legend></legend>
            <div>{$comment->text}</div>
        </div>
    {/foreach}
</div>