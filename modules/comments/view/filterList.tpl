<div class="filter-block">
    <form class="form-horizontal" name="list" method="get">
        {for $i = 0 to count($filter->fields)-1}
            {call_user_func_array("FBootStrap::`$filter->fields[$i]->type`", [
                $filter->fields[$i]->getFieldModel(),
                $filter->fields[$i]->attribute,
                [
                    'control'=>[
                        'name'=>$filter->fields[$i]->getRequestName()
                    ]
                ]
            ])}
        {/for}
    </form>
</div>
