<form class="form-horizontal" action="/comments/add" method="post">
    {if $model->hasErrors()}
        {foreach from=$model->getErrors() item=error}
            {for $var=0 to count($error)-1}
                <div class="alert alert-error">
                    <a class="close" data-dismiss="alert" href="#">×</a>{$error[$var]}
                </div>
            {/for}
        {/foreach}
    {/if}
    {FBootStrap::activeHiddenField($model, 'id')}
    {FBootStrap::activeHiddenField($model, 'targetId')}
    {FBootStrap::activeHiddenField($model, 'model')}
    {FBootStrap::activeHiddenField($model, 'userId')}
    {FBootStrap::formGroup($model, 'text', 'activeEditField', [], [
    'control'=>['width'=>10],
    'label'=>['width'=>2]
    ])}
    {if $model->id}
        <div class="right">
            <a href="/comments/delete?id={$model->id}" class="btn btn-danger">Удалить</a>
            {FBootStrap::btnSuccessSubmit('Изменить комментарий')}
        </div>

    {else}
        <div class="right">{FBootStrap::btnSuccessSubmit('Добавить комментарий')}</div>
    {/if}
</form>

