{foreach from=$items->getData() item=comment}

        <div class="well">
            <div class="warp">
                <div class="warp">
                    <a href="/{get_class($comment->task)}/view?id={$comment->task->id}"><i class="glyphicon glyphicon-tasks"></i>{$comment->task->title}</a>
                </div>
                <div class="text-right warp">
                    <img class="contact-avatar right" src="/{User::$pathToAvatar}{$comment->user->avatar}">
                    <div>
                        {$comment->user->name}
                        <div><i class="glyphicon glyphicon-time warp"></i>{date('H:i:s d-m-Y', $comment->time)}</div>
                    </div>
                </div>
                <div class="clearfix"></div>
            </div>
            <div class="clearfix"></div>
            <legend></legend>
            <div>{$comment->text}</div>
        </div>
{/foreach}