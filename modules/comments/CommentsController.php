<?php

/**
 * Class CommentsController
 * @property Comments $model
 */
class CommentsController extends FController implements FComponentInterface {

    /**
     * @param string $url
     * @return bool
     */
    public function checkFriendlyUrl($url)
    {
        return false;
    }

    /**
     * @param object $activePage
     * @return string
     */
    public function getTemplate($activePage)
    {
        return 'default';
    }

    /**
     * @return array
     */
    public function getMetaData()
    {
        $metaData = array(
            'title' => 'Не задано',
            'description' => 'Не задано',
            'keywords' => 'Не задано',
            'h1' => 'Не задано',
            'robots' => 'Не задано',
        );
        if(isset($this->action))
        {
            if($this->action->getId() == 'add')
            {
                $metaData['needCache'] = true;
            }
        }
        return $metaData;
    }

    public function actionAdd($Comments = null)
    {
        $model = $this->model->loadModel($Comments);
        if($Comments && !$model->hasErrors())
        {
            $this->redirect($model->returnUrl);
        }
        return $this->render('add', array(
            'render_type'=>self::TEMPLATE_EXTENSION_SMARTY,
            'model'=>$model,
        ));
    }

    public function actionDelete($id)
    {
        $model = $this->model->findByPk($id);
        $returnUrl = $model->returnUrl;
        if($model->delete())
        {
            $this->redirect($returnUrl);
        }
    }

    public function actionGetTasksComments()
    {
        return $this->render('modelList', array(
            'render_type'=>self::TEMPLATE_EXTENSION_SMARTY,
            'items'=>$this->model->getAll(array(
                'criteria'=>array(
                    'with'=>array('task'=>array('condition'=>'task.responsibleId='.FCore::app()->user->getState('id'))),
                ),
            )),
            /*
            'items'=>Task::model()->getAll(array(
                'criteria'=>array(
                    'condition'=>'t.responsibleId="'.FCore::app()->user->getState('id').'"',
                    'with'=>array('comments', 'comments.addTime'=>array('condition'=>'addTime.data>'.$from.' AND addTime.data<'.$to)),
                ),
            )),
            */
        ));
    }

    public function actionList($model, $target=null)
    {
        $criteria = new CDbCriteria();
        $criteria->addCondition('model="'.$model.'"');
        if($target !== null)
        {
            $criteria->addCondition('targetId='.$target);
        }
        return $this->render('list', array(
            'render_type'=>self::TEMPLATE_EXTENSION_SMARTY,
            'items'=>$this->model->getAll(array('criteria'=>$criteria)),
        ));
    }

    public function actionEdit($id)
    {
        $model = $this->model->findByPk($id);
        return $this->render('add', array(
            'render_type'=>self::TEMPLATE_EXTENSION_SMARTY,
            'model'=>$model,
        ));
    }

    public function actionForm($model)
    {
        $model = Comments::creatComment($model);
        return $this->render('add', array(
            'render_type'=>self::TEMPLATE_EXTENSION_SMARTY,
            'model'=>$model,
        ));
    }

    public function addComments($model)
    {
        return $this->render('commentsBlock', array(
            'render_type'=>self::TEMPLATE_EXTENSION_SMARTY,
            'listComments'=>$this->actionList(get_class($model), $model->id),
            'addForm'=>$this->actionForm($model),
        ));
    }
} 