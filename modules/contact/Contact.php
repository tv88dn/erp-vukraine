<?php
/**
 * Created by PhpStorm.
 * User: tv88d_000
 * Date: 12.09.14
 * Time: 1:43
 */
FCore::includeDirectory('modules/user/');
class Contact extends User {

    const ONLY_CONTACT = 'contact';

    public function relations()
    {
        return array_merge(array(
            'companyPlace'=>array(self::HAS_ONE, 'Company', array('id'=>'company')),
            'contactCompany'=>array(self::HAS_ONE, 'Details', array('moduleId'=>'id'), 'condition'=>'contactCompany.parentId = 35', 'together'=>true),
        ), $this->baseDetailsRepations());
    }

    public function getUserByEmail($mail)
    {
        return $this->with(
            array(
                'details'=>array('condition'=>'data="'.$mail.'"'),
                'parents'=>array('condition'=>'module="User" OR module="Contact"'),
                'types'=>array('condition'=>'fieldName="mail"'),
            )
        )->findByAttributes(array());
    }

    public static function disabledDetails()
    {
        return array('group', 'status');
    }


    public function loadModel($modelFields, $key = 'id')
    {
        $model = parent::loadModel($modelFields, $key);
        if($modelFields)
        {
            $model->companyPlace = Company::model()->findByPk($model->company);
            $model->clearErrors();
            $model->save();
        }
        return $model;
    }

    public function getAllUsers($criteria)
    {
        $contacts = Contact::model()->getAll($criteria);
        $users = User::model()->getAll($criteria);
        $usersRecords = array();
        for($i = 0; $i < count($users->data); $i++)
        {
            if($users->data[$i]->name)
            {
                $usersRecords[] = $users->data[$i];
            }
        }
        $records = (array_merge($usersRecords, $contacts->data));
        return new CArrayDataProvider(array_unique($records), array(
            'pagination' => array('pageSize' => 20),
        ));
    }

    public function getCompanyName()
    {
        if($this->company)
        {
            return Company::model()->findByPk($this->company)->name;
        }
    }

    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    public function attributeLabels()
    {
        return array_merge(array(
            'place'=>'Организация',
        ), parent::attributeLabels());
    }
} 