<div class="panel panel-info warp">
    <div class="panel-heading">
        <h3>Добавление контакта</h3>
    </div>
    <div class="panel-body">
        <form id="addContact" name="addContact" method="post" class="form-horizontal" enctype="multipart/form-data" action="/contact/add">
            {if $model->hasErrors()}
                {foreach from=$model->getErrors() item=error}
                    {for $var=0 to count($error)-1}
                        <div class="alert alert-error">
                            <a class="close" data-dismiss="alert" href="#">×</a>{$error[$var]}
                        </div>
                    {/for}
                {/foreach}
            {/if}
            {if $model->id}
                {FBootStrap::activeHiddenField($model, 'id')}
            {/if}
            {foreach from=$model->getExternalAttributes() key=name item=detail}
                {FBootStrap::formGroup($model, $name, $detail->type->type, $detail->getDictionary(), [
                    'control'=>['width'=>7],
                    'label'=>['width'=>4]
                ])}
            {/foreach}

            {if $model->isEdit && !$model->company}
                {FBootStrap::formGroup($model->companyPlace, 'name', 'activeTextField', [], [
                'control'=>['width'=>7, 'disabled'=>'disabled', 'name'=>'Contact["name"]'],
                'label'=>['width'=>4]
                ])}
            {else}
            {*    <div class="form-group warp text-danger text-center"><b>Компания, к кторой относился пользователь удалена</b></div>*}
            {/if}

            <div class="col-lg-12 col-lg-offset-4">
                {if $model->id}
                    {FBootStrap::btnDangerLink('Удалить', "/contact/delete?id=`$model->id`")}
                {else}
                    {FBootStrap::btnDefaultLink('Отмена', "/contact/listAll")}
                {/if}
                {FBootStrap::btnPrimarySubmit('Сохранить')}
            </div>
        </form>
    </div>
</div>

