<div class="contact-block">
    {foreach from=$items->getData() item=row}
        <div class="user-contact-item">
            <div class="well">
                <a class="contact-edit text-info" href="/{mb_strtolower(get_class($row))}/edit?id={$row->id}">
                    <div class="glyphicon glyphicon-edit right"></div>
                </a>
                {if $row->avatar}
                    <img class="img-thumbnail left col-lg-4" src="/{User::$pathToAvatar}{$row->avatar}"  alt="аватар" >
                {else}
                    <img class="img-thumbnail left col-lg-4" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAMgAAADICAYAAACtWK6eAAAH60lEQVR4Xu2ZWU8UaxRFDyEyKJNhVAgCwUCARCTh//8BgsoDQxgihOlBhgcZo2K+StoY7m2z+4A+sFcnN9Hb59zqvXatW1XdTaenp3fBCwIQ+F8CTQjCmQGB+gQQhLMDAn8ggCCcHhBAEM4BCOQIcAXJcWPLhACCmBRNzBwBBMlxY8uEAIKYFE3MHAEEyXFjy4QAgpgUTcwcAQTJcWPLhACCmBRNzBwBBMlxY8uEAIKYFE3MHAEEyXFjy4QAgpgUTcwcAQTJcWPLhACCmBRNzBwBBMlxY8uEAIKYFE3MHAEEyXFjy4QAgpgUTcwcAQTJcWPLhACCmBRNzBwBBMlxY8uEAIKYFE3MHAEEyXFjy4QAgpgUTcwcAQTJcWPLhACCmBRNzBwBBMlxY8uEAIKYFE3MHAEEyXFjy4QAgpgUTcwcAQTJcWPLhACCmBRNzBwBBMlxY8uEAIKYFE3MHAEEyXFjy4QAgpgUTcwcAQTJcWPLhACCmBRNzBwBBMlxY8uEAIKYFE3MHAEEyXFjy4QAgpgUTcwcAQTJcWPLhACCmBRNzBwBBMlxY8uEAIKYFE3MHAEEyXFjy4QAgpgUTcwcAQTJcWPLhACCmBRNzBwBBMlxY8uEAIKYFE3MHAEEyXFjy4QAgpgUTcwcAQTJcWPLhACCmBRNzBwBBMlxY8uEAIKYFE3MHAEEyXFjy4QAgpgUTcwcAQTJcWPLhACCmBRNzBwBBMlxY8uEAIKYFE3MHAEEyXFjy4QAgpgUTcwcAQTJcWPLhACCmBRNzBwBBMlxY8uEAIKYFE3MHAEEyXFjy4QAgpgUTcwcAQTJcWPLhACCmBRNzBwBBMlxY8uEAIKYFE3MHAEEyXFjy4QAgpgUTcwcAQTJcWPLhACCmBRNzBwBBMlxY8uEAIKYFE3MHAEEyXFjy4QAgpgUTcwcAQTJcWPLhACCmBRNzBwBBMlxY8uEAIKYFE3MHAEEyXFjy4QAgjRY9Ldv32J1dTVOTk7i+/fv0dzcHKOjozExMRFNTU3x5cuX2NjYiKurq2htbY3x8fEYGRmpjvKn9+p9jH99vAZxPPlxBGmw4p2dnSj/DA4OxszMTGxvb8f+/n5MTk7Gq1evYmlpKe7u7mJ+fj7W19fj69ev8e7du3j+/Hnd97q7u+t+in99vAZxPPlxBHlgxcfHx7G2thY9PT2VIOXPAwMDMTs7G58/f46tra148+ZNdHZ21n3v7du38qf418eTP9gTHUSQBxZbhDg4OIje3t54+fLlLyHKSV87mfv7+6Ojo6Pue11dXbG5uVldZRYWFqo/l90iXLlKlVu32usxjjc3N/fA1D7rCPKAro+OjqrbqPIsMjY2Fre3t3F4eFjdbpW/1wQpcrx48aLue0WKT58+xdnZWfT19cXp6Wm0tbVVt2nt7e2/PuFjHW9xcfEBqb1WESTZ9+8nazmZ379/H7u7uylBygl7fn4eKysrlWTlwX96erq6gtRej328ZGy7NQRJVF7vZP79maORW6xyy1Me7D9+/Fh9O1auNuWqUr4FK6+/cbxEbMsVBGmw9uvr6/jw4UNcXFxU/6efmpqK169fV/+VcktVvgIuD+nlpK8JU74CLs8X9d4r79euED9+/KhkKV8Nl6vI3zpeg7FtxxGkwer39vaqh+jyKr9xlJO79ion8/LycvXX+1/zlqtBvfdaWloq6W5ubqpnl/K1cXmuKd+Eld9THvt4f/pauUEcT34cQRqsuJzI5Tbo/qucdLVniXKluLy8/M8PheVW6f57w8PD1b8rV5ChoaFKivLbR7n6lFutZ8+eVQ/vj3W82o+WDca2HUcQ2+oJrhBAEIUSM7YEEMS2eoIrBBBEocSMLQEEsa2e4AoBBFEoMWNLAEFsqye4QgBBFErM2BJAENvqCa4QQBCFEjO2BBDEtnqCKwQQRKHEjC0BBLGtnuAKAQRRKDFjSwBBbKsnuEIAQRRKzNgSQBDb6gmuEEAQhRIztgQQxLZ6gisEEEShxIwtAQSxrZ7gCgEEUSgxY0sAQWyrJ7hCAEEUSszYEkAQ2+oJrhBAEIUSM7YEEMS2eoIrBBBEocSMLQEEsa2e4AoBBFEoMWNLAEFsqye4QgBBFErM2BJAENvqCa4QQBCFEjO2BBDEtnqCKwQQRKHEjC0BBLGtnuAKAQRRKDFjSwBBbKsnuEIAQRRKzNgSQBDb6gmuEEAQhRIztgQQxLZ6gisEEEShxIwtAQSxrZ7gCgEEUSgxY0sAQWyrJ7hCAEEUSszYEkAQ2+oJrhBAEIUSM7YEEMS2eoIrBBBEocSMLQEEsa2e4AoBBFEoMWNLAEFsqye4QgBBFErM2BJAENvqCa4QQBCFEjO2BBDEtnqCKwQQRKHEjC0BBLGtnuAKAQRRKDFjSwBBbKsnuEIAQRRKzNgSQBDb6gmuEEAQhRIztgQQxLZ6gisEEEShxIwtAQSxrZ7gCgEEUSgxY0sAQWyrJ7hCAEEUSszYEkAQ2+oJrhBAEIUSM7YEEMS2eoIrBBBEocSMLQEEsa2e4AoBBFEoMWNLAEFsqye4QgBBFErM2BJAENvqCa4QQBCFEjO2BBDEtnqCKwQQRKHEjC0BBLGtnuAKAQRRKDFjSwBBbKsnuEIAQRRKzNgSQBDb6gmuEEAQhRIztgQQxLZ6gisEEEShxIwtAQSxrZ7gCgEEUSgxY0sAQWyrJ7hCAEEUSszYEvgJ1IR1HKkp5OIAAAAASUVORK5CYII="  alt="фото" >
                {/if}

                <div class="col-lg-7 contact-short-info{if get_class($row) == 'User'} text-success{/if}">
                    <h4>{$row->name}</h4>
                    <i class="warp {if get_class($row) == 'User'} text-success{/if}">
                        <i class="glyphicon glyphicon-briefcase {if get_class($row) == 'User'} text-success{/if}"></i>
                        {$row->group}
                    </i>
                </div>
                <div class="clearfix"></div>
                <ul class="user-contact list-group">
                    <li class="list-group-item">
                        <dt>
                            <i class="glyphicon glyphicon-phone {if count($row->phone)}text-success{else}text-danger{/if}"></i>
                            Телефон
                        </dt>
                        {for $i=0 to count($row->phone)-1}
                            <span class="col-lg-12"> {$row->phone[$i]}</span>
                            <div class="clearfix"></div>
                        {/for}
                    </li>
                    <li class="list-group-item">
                        <dt>
                            <i class="glyphicon glyphicon-envelope {if $row->mail}text-success{else}text-danger{/if}"></i>
                            Email
                        </dt>
                        <span class="col-lg-12"> {$row->mail}</span>
                        <div class="clearfix"></div>
                    </li>
                    <li class="list-group-item">
                        <dt>
                            <i class="glyphicon glyphicon-camera {if $row->skype}text-success{else}text-danger{/if}"></i>
                            Skype
                        </dt>
                        <span class="col-lg-12">{$row->skype}</span>
                        <div class="clearfix"></div>
                    </li>
                </ul>
                {if $row->company}
                    <div class="label label-info right company-name-label">&copy;  <a href="/company/edit?id={$row->company}">{$row->getCompanyName()}</a></div>
                {else}
                    <div class="label label-info right company-name-label">&copy; Компания удалена</div>
                {/if}

                <div class="clearfix"></div>
            </div>
        </div>

    {/foreach}
    <div class="clearfix"></div>
</div>
