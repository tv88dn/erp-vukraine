<?php
/**
 *@property Contact $model
 */

class ContactController extends UserController {

    public function actionAdd($companyId, $Contact = null)
    {
        $model = $this->model->loadModel($Contact);
        if(!$model->company)
        {
            $model->company = $companyId;
        }
        if($Contact && !$model->hasErrors())
        {
            $this->redirect('/company/edit?id='.$model->company);
        }
        return $this->render('add', array(
            'render_type' => self::TEMPLATE_EXTENSION_SMARTY,
            'model'=> $model,
        ));
    }

    public function actionEdit($id, $Contact = null)
    {
        $model = $this->model->findByPk($id);
        return $this->render('add', array(
            'render_type' => self::TEMPLATE_EXTENSION_SMARTY,
            'model'=> $model,
        ));
    }

    public function actionList($companyId)
    {
        return $this->render('list', array(
            'render_type'=>self::TEMPLATE_EXTENSION_SMARTY,
            'items'=>Contact::model()->getAll(array(
                        'criteria' => array(
                            'condition'=>'contactCompany.data='.$companyId,
                            'with'=>array('contactCompany')
                        ),
                    )
                ),
        ));
    }

    public function actionDelete($id)
    {
        if($this->model->findByPk($id)->delete())
        {
            $this->redirect('/contact/listAll');
        }
    }

    public function actionView($id)
    {
        $contact = $this->model->findByPk($id);
        return $this->render('view', array(
            'render_type'=>self::TEMPLATE_EXTENSION_SMARTY,
            'row'=>$contact,
        ));
    }

    public function actionListAll()
    {
        return $this->render('list', array(
            'render_type'=>self::TEMPLATE_EXTENSION_SMARTY,
            'items'=>Contact::model()->getAllUsers(array()),
        ));
    }

    public function actionControl()
    {
        return '<li class="dropdown"><a href="/contact/listAll">Контакты</a></li>';
    }
} 