<?php

/**
 * Class SeoController
 *
 * Контроллер модуля сеоданных
 * Формирует метоописание важное для СЕО
 *
 * @todo На данный момент не реализованы значения по умолчанию для метаданных.
 *
 * @package modules.controllers
 * @author Trunov Vadim trunov@lookmy.info
 * @copyright 2014 LookMy.info
 */
class SeoController extends FController
{

    /**
     * Стандартный конструктор. Внутри происходит инициализация массива specCode и вызов родительского конструктора
     *
     * @param string $id уникальное имя модуля
     * @param null $module модуль, который так же управляется этим контроллером
     */
    function __construct($id, $module = null)
    {
        parent::__construct($id);
        $this->initModel(new Seo());

    }

    /**
     * Получает заголовок страницы
     *
     * @param sting $title заголовок по умолчанию
     * @return string заголовок страницы
     */
    public function generateTitle($title = null)
    {
        $generateTitle =
            function () {
                return 'generate title';
            };

        return ($title) ? $title : $generateTitle();
    }

    /**
     * Получает описание страницы
     *
     * @param sting $description описание по умолчанию
     * @return string описание страницы
     */
    public function generateDescription($description = null)
    {
        $generateDescription =
            function () {
                return 'generate title';
            };

        return ($description) ? $description : $generateDescription();
    }

    /**
     * Получает ключевые слова для страницы
     *
     * @param sting $keywords ключевые слова по умолчанию
     * @return string  ключевые слова для страницы
     */
    public function generateKeyword($keywords = null)
    {
        $generateKeyword =
            function () {
                return 'generate title';
            };

        return ($keywords) ? $keywords : $generateKeyword();
    }

    /**
     * Получает заглавие H1 страницы
     *
     * @param sting $h1 заглавие H1 умолчанию
     * @return string  заглавие H1
     */
    public function generateH1($h1 = null)
    {
        $generateH1 =
            function () {
                return 'generate title';
            };

        return ($h1) ? $h1 : $generateH1();
    }

    /**
     * Получает значение метатега robots страницы
     *
     * @param sting $robots значение метатега robots умолчанию
     * @return string  значение метатега robots
     */
    public function generateRobots($robots = null)
    {
        $generateRobots =
            function () {
                return 'index, follow';
            };

        return ($robots) ? $robots : $generateRobots();
    }

    /**
     * Действие получения метоописания страницы
     *
     * @return mixed|string отрендериное метоописание
     */
    public function actionMetaData()
    {
        $data = $this->getMetaData();
        $this->data = $data;
        return $this->render('index', array('render_type' => self::TEMPLATE_EXTENSION_PHP));
    }

    /**
     * Получает метаданные для страницы. Сперва ищет соответсвие в база модуля метаданных.
     * Если данные не найдены, вызывает метод активного компонента getMetadata() для получения метаданных
     *
     * @return array массив метаданных
     */
    public function getMetaData()
    {
        $request = new FRequest();
        $res = null;
        $metaData = FCore::getInstance()->getActiveController()->getMetadata();
        if (!$metaData) {
            $metaData = array(
                'title' => 'Не задано',
                'description' => 'Не задано',
                'keywords' => 'Не задано',
                'h1' => 'Не задано',
                'robots' => 'Не задано',
            );
        }
        if (!$res) {
            $res = $metaData;
        } else {
            $res['title'] = ($res['title']) ? $res['title'] : $metaData['title'];
            $res['description'] = ($res['description']) ? $res['description'] : $metaData['description'];
            $res['keywords'] = ($res['keywords']) ? $res['keywords'] : $metaData['keywords'];
            $res['h1'] = ($res['h1']) ? $res['h1'] : $metaData['h1'];
        }
        if (isset($res['robots'])) {
            $res['robots'] = ($res['robots']) ? $res['robots'] : $metaData['robots'];
        } else {
            $res['robots'] = $metaData['robots'];
        }

        return $res;

    }
}