<?php
/**
 * View Seo.index
 * Выводит метаданные для SEO
 * Метаданные находятся в $this->data.
 * Название переменной в масиве соответсвует имени метатега
 *
 * @package modules.views
 * @author Trunov Vadim trunov@lookmy.info
 * @copyright 2014 LookMy.info
 */
?>
<title><?php echo $this->data['title'];?></title>
<meta name="description" content="<?php echo $this->data['description'];?>">
<meta name="keywords" content="<?php echo $this->data['keywords']; ?>">
<meta name="robots" content="<?php echo $this->data['robots']; ?>">
<?php
    if(isset($this->data['needCache']))
    {
?>
        <meta http-equiv="Cache-Control" content="private">
<?php
    }
?>