<?php

/**
 * Class SiteController
 *
 * Главный контроллер сайта. Cодержит сновную логику сайта и позволяет
 *
 * @package system
 * @author Trunov Vadim trunov@lookmy.info
 * @copyright 2014 LookMy.info
 */
class SiteController extends FController
{
    const SEARCH_REQUEST_LEN = 3;

    /**
     * Шаблон по умолчанию
     */
    const DEFAULT_TEMPLATE = 'default';

    /**
     * Имя контроллера, занимающегося роутингом главной страницы
     */
    const ROUTER_MAIN_PAGE = 'SiteController';

    /**
     * @var string путь к шаблонам
     */
    protected $templatePath;
    /**
     * @var string имя активного шаблона
     */
    protected $template;
    /**
     * @var object экземпляр класса AR, относяшгося к текущей активной странице
     */
    protected $activePage;
    /**
     * @var int уникальный идентификатор сайта
     */
    private $contentModule;

    /**
     * @var bool если 404-я
     */
    private $notFound = false;


    public $routerPageContent;
    /**
     *
     *
     * @param object $controller активный контроллер для рендеринга
     */
    public function renderAjax($controller)
    {
        //$this->init();
        //var_dump($controller->);
        echo $this->routerPageContent;
        FCore::app()->end();
    }

    /**
     * @param string $view
     * @param null $data
     * @param bool $return
     * @throws CException
     * @return string|void
     * Обработка главного шаблона и вывод его на экран
     */
    public function render($view, $data = null, $return = false)
    {
        if (!isset($data['render_type'])) {
            $this->template = $this->getHtmlTemplate($this->templatePath . $view . '.' . self::TEMPLATE_EXTENSION_HTML);
        } else {
            throw new CException('не задан тип рендеринга для главного контроллера');
        }
        $res = $this->displayTemplate(FCore::getInstance()->processModules($this->template));

        echo $res;
    }

    /**
     * @param $finalReplace array массив подстановок
     * @return mixed отрендереная страница сайта
     * @throws CException
     */
    public function displayTemplate($finalReplace)
    {
        $this->contentModule = array();
        $result = $this->createReplacements($finalReplace);
        if (count($this->contentModule) > 1) {
            throw new CException('Боллее одной замены контента');
        }
        return str_replace($result['searches'], $result['replacements'], $this->template);
    }

    /**
     * @param $finalReplace array масив конечных подстановок
     * @return array массив для подстановки
     * создает массив ключей и подстановок, анализирует записи с помощью анонимной функции, которая опреелет обрабатывающий компонент
     */
    private function createReplacements($finalReplace)
    {
        $searches = array();
        $replacements = array();
        foreach ($finalReplace as $tag => $val) {
            $searches[] = $tag;
            $replacements[] = $val['data'];
        }
        return array(
            'searches' => $searches,
            'replacements' => $replacements
        );
    }

    public function ajaxInit()
    {
        $request = new FRequest();
        $requestUri = $request->getRequestUriWithoutContext();
        $this->routerPageContent = FCore::getInstance()->routing($requestUri);
        //$routerPage может быть false, если неверные параметры используемых переменных в запросе
        if(!$this->routerPageContent && strlen($requestUri) > 0)
        {
            $this->notFound = true;
        }

        if(!FCore::getInstance()->getActiveController() && !$this->notFound)
        {
            $this->routerPageContent = FCore::getInstance()->getMainPage();
            $this->activePage = FCore::getInstance()->getActiveController()->model;
        }
        else if(!$this->notFound)
        {
            $this->activePage = FCore::getInstance()->getActiveController()->model;
        }


        if ($this->activePage) {
        } else {
            throw new CException('Не найдена главная (текущая) страница сайта');
        }
    }

    /**
     * @throws CException
     * Инициализация модели сайта. Получение главного темплейта сайта исходя из урл.
     * Получение текущей активной страници
     */
    public function init()
    {
        $request = new FRequest();
        $requestUri = $request->getRequestUriWithoutContext();
        $this->routerPageContent = FCore::getInstance()->routing($requestUri);
        //$routerPage может быть false, если неверные параметры используемых переменных в запросе
        if(!$this->routerPageContent && strlen($requestUri) > 0)
        {
            $this->notFound = true;
        }

        if(!FCore::getInstance()->getActiveController() && !$this->notFound)
        {
            $this->routerPageContent = FCore::getInstance()->getMainPage();
            $this->activePage = FCore::getInstance()->getActiveController()->model;
        }
        else if(!$this->notFound)
        {
            $this->activePage = FCore::getInstance()->getActiveController()->model;
        }

        $this->setTemplatePath(FCore::getInstance()->getActiveController()->getTemplate($this->activePage));


        if ($this->activePage) {
        } else {
            throw new CException('Не найдена главная (текущая) страница сайта');
        }


    }

    /**
     * @param $templateName string имя шаблона
     * @return string путь к установленному шаблону
     */
    public function setTemplatePath($templateName)
    {

        if ($templateName) {
            $this->templatePath = FConfig::LAYOUT_PATH . $templateName . '/';
        } else {
            $this->templatePath = FConfig::LAYOUT_PATH . 'default/';
        }
        return $this->templatePath;
    }

    /**
     * Возвращает путь к активному шаблону.
     *
     * @return string путь к шаблону
     */
    public function actionPathToLayout()
    {
        return '/' . FConfig::$rootPath . $this->templatePath;
    }

    /**
     * Возвращает путь к корню сайта
     *
     * @return string путь к корню сайта
     */
    public function actionPathResources()
    {
        return '/' . FConfig::$rootPath;
    }

    public function actionSearch()
    {
        if(strlen(FCore::app()->request->getParam('key')) < self::SEARCH_REQUEST_LEN)
        {
            return 'Длина поискового запроса не должна быть меньше '.self::SEARCH_REQUEST_LEN.'-х символов';
        }
        $searchModule = FCore::app()->request->getParam('module') ? ucfirst(FCore::app()->request->getParam('module')) : 'Tenders';
        $controller = FCore::getInstance()->initController($searchModule.'Controller');
        return call_user_func(array($controller, 'actionSearch'), FCore::app()->request->getParam('key'));
    }

    /**
     * Исполнени модуль-ссылки и спец. модулей.
     * Если в Pages существует pagesSpec[code], то происходит переход сюда.
     * В данном методе происходит анализ значения pagesSpec[code] и в зависимости от него вызов контроллера внешнего
     * модуля (компонента) в качестве активного на получение данных.
     *
     *
     * @param $model CActiveRecord модель внешнего модуля
     * @return object экземпляр AR активной страницы
     */
    public function externalContentModule($model)
    {
        $pageSpec = $model['pagesSpec']['code'];
        $activePage = null;
        if (isset(FConfig::$specCode[$pageSpec])) {
            $controller = FCore::initController(FConfig::$specCode[$pageSpec]);
            if($controller)
            {
                //отработка модуля-ссылки
                if($controller instanceof SiteController)
                {

                    $this->activePage = $model;
                    $controller->actionMain();
                }
                else
                {
                    $activePage = $controller->actionMain();
                }

            }
            else
            {
                $this->errorNotFound('404');
            }
        }

        return $activePage;
    }

    /**
     * Действие для модуля-ссылки
     */
    public function actionMain()
    {
        $this->redirect('/'.FConfig::$rootPath.$this->activePage['text']);
    }

    /**
     * Вывод контента главной страницы
     *
     * @return string Результат вымолнения метода контроллера. Данные для области контента.
     */
    public function actionContent()
    {
        $content = $this->routerPageContent;
        return ($content && !$this->notFound) ? $content : $this->errorNotFound('404');
    }

    /**
     * @param $name
     * @return string
     */
    public function errorNotFound($name)
    {
        return $this->getHtmlTemplate($this->templatePath . $name . '.' . self::TEMPLATE_EXTENSION_HTML);
    }


}
