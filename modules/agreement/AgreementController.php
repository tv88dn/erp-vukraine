<?php

/**
 * Class AgreementController
 *
 * @property Agreement $model
 */
class AgreementController extends FController {

    public function actionAdd($taskId, $Agreement = null)
    {
        $model = new Agreement();
        $model->taskId = $taskId;
        $this->model->saveAll($Agreement);
        return $this->render('add', array(
            'render_type'=>self::TEMPLATE_EXTENSION_SMARTY,
            'model'=>$model,
            'listItems'=>$this->actionList($model->taskId),
        ));
    }

    public function actionList($taskId)
    {
        $items = $this->model->findAllByAttributes(array('taskId'=>$taskId));

        return $this->render('list', array(
            'render_type'=>self::TEMPLATE_EXTENSION_SMARTY,
            'items'=>$items,
        ));
    }

    public function actionForm()
    {
        return $this->render('form', array(
            'render_type'=>self::TEMPLATE_EXTENSION_SMARTY,
            'model'=>Agreement::model(),
        ));
    }

} 