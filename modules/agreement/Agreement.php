<?php

/**
 * Class Agreement
 */
class Agreement extends AgreementBase
{
    private $_uniqid;

    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'taskId' => 'Задание',
            'userId' => 'Имя пользователя',
        );
    }

    public function relations()
    {
        return array(
            'user'=>array(self::HAS_ONE, 'User', array('id'=>'userId'), 'together'=>true),
            'task'=>array(self::HAS_ONE, 'Task', array('id'=>'taskId')),
        );
    }

    /**
     * @param string $className
     * @return Agreement
     */
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    public function uniqid()
    {
        if(!$this->_uniqid)
        {
            $this->_uniqid = uniqid();
        }
        return $this->_uniqid;
    }

    public function saveAll($items)
    {
        $data = array();
        if($items)
        {
            foreach($items as $key=>$val)
            {
                if(isset($val['id']))
                {
                    if($val['id'])
                    {
                        $model = $this->findByPk($val['id']);
                    }
                    else
                    {
                        $model = new Agreement();
                    }
                }
                else
                {
                    $model = new Agreement();
                }
                $model->setAttributes($val);
                if($model->save($val))
                {
                    $data[] = $model;
                }
            }
        }
        return $data;

    }
}