{FBootStrap::formGroup($model, 'userId', 'activeDropDownList', Task::model()->userCanAddTask(), [
    'control'=>[
        'name'=>"Agreement[`$model->uniqid()`][userId]",
        'onchange'=>'task.addAgreement($(this))'
    ]
])}
