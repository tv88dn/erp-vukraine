<div class="well">
    <legend>На согласовании</legend>
    {$listItems}
    {if $model->hasErrors()}
        {foreach from=$model->getErrors() item="error"}
            {for $var=0 to count($error)-1}
                <div class="alert alert-error">
                    <a class="close" data-dismiss="alert" href="#">×</a>{$error[$var]}
                </div>
            {/for}
        {/foreach}
    {/if}
    {FBootStrap::activeHiddenField($model, 'id', ['name'=>"Agreement[`$model->uniqid()`][id]"])}
    {FBootStrap::activeHiddenField($model, 'taskId', ['name'=>"Agreement[`$model->uniqid()`][taskId]"])}
    {FBootStrap::formGroup($model, 'userId', 'activeDropDownList', Task::model()->userCanAddTask(), [
        'control'=>[
            'name'=>"Agreement[`$model->uniqid()`][userId]",
            'onchange'=>'task.addAgreement($(this))'
        ]
    ])}
</div>