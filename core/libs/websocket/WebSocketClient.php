<?php
/**
 * Created by PhpStorm.
 * User: vadim
 * Date: 08.01.15
 * Time: 11:30
 */

class WebSocketClient extends CComponent {

    private $host;
    protected $socket;
    protected $webSocketId;
    protected $magicGUID = "258EAFA5-E914-47DA-95CA-C5AB0DC85B11";
    private $secretKey;
    protected $responseBuffer;

    const ACCEPT_HEADER = 'Sec-WebSocket-Accept';

    public function getSecretKey()
    {
        return $this->secretKey;
    }

    public function getSocket()
    {
        return $this->socket;
    }

    private function getMask()
    {
        return substr(uniqid(), 1, 4);
    }

    protected function frame($message, $messageType='text', $messageContinues=false) {
        switch ($messageType) {
            case 'continuous':
                $b1 = 0;
                break;
            case 'text':
                $b1 = 1;
                break;
            case 'binary':
                $b1 = 2;
                break;
            case 'close':
                $b1 = 8;
                break;
            case 'ping':
                $b1 = 9;
                break;
            case 'pong':
                $b1 = 10;
                break;
        }
        if (!$messageContinues) {
            $b1 += 128;
        }

        $length = strlen($message);
        $lengthField = "";
        if ($length < 126) {
            $b2 = $length;
        }
        elseif ($length <= 65536) {
            $b2 = 126;
            $hexLength = dechex($length);
            //$this->stdout("Hex Length: $hexLength");
            if (strlen($hexLength)%2 == 1) {
                $hexLength = '0' . $hexLength;
            }
            $n = strlen($hexLength) - 2;

            for ($i = $n; $i >= 0; $i=$i-2) {
                $lengthField = chr(hexdec(substr($hexLength, $i, 2))) . $lengthField;
            }
            while (strlen($lengthField) < 2) {
                $lengthField = chr(0) . $lengthField;
            }
        }
        else {
            $b2 = 127;
            $hexLength = dechex($length);
            if (strlen($hexLength)%2 == 1) {
                $hexLength = '0' . $hexLength;
            }
            $n = strlen($hexLength) - 2;

            for ($i = $n; $i >= 0; $i=$i-2) {
                $lengthField = chr(hexdec(substr($hexLength, $i, 2))) . $lengthField;
            }
            while (strlen($lengthField) < 8) {
                $lengthField = chr(0) . $lengthField;
            }
        }

        return chr($b1) . chr($b2) . $lengthField . $this->getMask() . $message;
    }

    protected function formattingData($data, $user = null, $secret = null)
    {
        return json_encode(array(
            'type'=>'SERVER_MSG_MEDIATOR',
            'action'=>'client_route',
            'data'=>$data,
            'user'=>$user,
            'secret'=>$secret
        ));
    }

    public function __construct($host, $socketId)
    {
        $this->host = $host;
        $this->webSocketId = $socketId;
        $this->secretKey = $this->getNewSecretKey();
    }

    protected function close()
    {
        $this->ask($this->frame('test', 'close'), function($socket, $row){
            fclose($socket->socket);
            return true;
        });
    }

    public function sendData($data, $user = null, $secret = null)
    {
        $this->setUpConnection();
        $this->handshake();

        $this->ask($this->frame($this->formattingData($data, $user, $secret)), function($socket, $row){
            return true;
        });

        $this->close();

    }

    protected function getNewSecretKey()
    {
        return base64_encode(substr(uniqid('', true), 2, 16));
    }

    public function generateResponseKey()
    {
        $webSocketKeyHash = sha1($this->secretKey . $this->magicGUID);
        $rawToken = "";
        for ($i = 0; $i < 20; $i++) {
            $rawToken .= chr(hexdec(substr($webSocketKeyHash,$i*2, 2)));
        }
        return base64_encode($rawToken);
    }

    protected function send($data)
    {
        return true;
        fwrite($this->socket, $data);
    }

    protected function getHandshakeRequest()
    {
        return "GET / HTTP/1.1\r\n
                Host: localhost\r\n
                Upgrade: websocket\r\n
                Connection: Upgrade\r\n
                Sec-WebSocket-Key: ".$this->secretKey."\r\n
                Sec-WebSocket-Version: 13\r\n\r\n";
    }

    protected function listenForResponse($callback)
    {
        return true;
        $this->responseBuffer = null;
        while(!feof($this->socket))
        {
            $res = fgets($this->socket);
            $this->responseBuffer = $res;
            if($callback($this, $res))
            {
                break;
            }
        }
    }

    protected function ask($data, $callback)
    {
        $this->send($data);
        $this->listenForResponse($callback);
    }

    public function requestFieldExplode($field)
    {
        $data = explode(':', $field);
        if(count($data) == 2)
        {
            return array('key'=>trim($data[0]), 'value'=>trim($data[1]) );
        }
        else if(count($data) < 2)
        {
            return array('key'=>'', 'value'=>trim($data[0]));
        }
        else
        {
            throw new Exception('Unexpected numbers of arguments');
        }
    }

    protected function handshake()
    {
        $this->ask($this->getHandshakeRequest(), function($socket, $row){
            $data = $socket->requestFieldExplode($row);
            if($data['key'] == websocketClient::ACCEPT_HEADER && $data['value'] == $socket->generateResponseKey())
            {
                return true;
            }
            return false;
        });

    }

    protected function setUpConnection()
    {
        return true;
        $this->socket = stream_socket_client($this->host, $errorNumber, $errorString, 1);
        if($this->socket)
        {
            return $this->socket;
        }
        else
        {
            throw new Exception('Error: '.$errorNumber.' in string'.$errorString);
        }
    }

}