<?php
/**
 * Created by PhpStorm.
 * User: vadim
 * Date: 23.12.14
 * Time: 22:34
 */
require_once('websockets.php');
class outputServer extends WebSocketServer {
    //protected $maxBufferSize = 1048576; //1MB... overkill for an echo server, but potentially plausible for other applications.

    const WEB_SOCKET_SECRET_CLIENT = 'GunjPYhs';
    const WEB_SOCKET_SECRET_SERVER = 'fpbnwhcc';

    public static $secret = 'niep7wzZLw';

    protected function getFromBuffer()
    {
        foreach($this->users as $id=>$user)
        {
            if(file_exists(dirname(__FILE__).'/buffers/'.$id.'.bfr'))
            {
                $data = file(dirname(__FILE__).'/buffers/'.$id.'.bfr');
                if($data)
                {
                    for($j = 0; $j < count($data); $j++)
                    {
                        $this->send($user, $data[$j]);
                    }
                    $fp = fopen(dirname(__FILE__).'/buffers/'.$id.'.bfr', "w");
                    fputs($fp, "");
                    fclose($fp);
                }
            }


        }

    }

    public static function putToBuffer($id, $data)
    {
        $file = fopen(dirname(__FILE__).'/buffers/'.$id.'.bfr', 'w+');
        fputs($file, $data.';'.PHP_EOL);
        fclose($file);
    }

    public static function strCode($str, $passw="")
    {
        $salt = "Dn8*#2n!9j";
        $len = strlen($str);
        $gamma = '';
        $n = $len>100 ? 8 : 2;
        while( strlen($gamma)<$len )
        {
            $gamma .= substr(pack('H*', sha1($passw.$gamma.$salt)), 0, $n);
        }
        return $str^$gamma;
    }


    protected function validateSetupMessages($user, $message)
    {
        $result = json_decode($message);
    }

    protected function setUpClient($user, $data)
    {
        $user->secret = base64_decode($data->secret) ^ self::WEB_SOCKET_SECRET_CLIENT;
        $user->userId = $data->id;
    }

    protected function decryptServerSecret($secret)
    {
        return base64_decode($secret) ^ self::WEB_SOCKET_SECRET_SERVER;
    }

    protected function validateUsersCoupleSecret($client, $server)
    {
        return ($client == $this->decryptServerSecret($server)) ? true : false;
    }

    protected function findUserByIdAndSecret($id, $secret)
    {
        foreach($this->users as $id => $user)
        {
            if($user->userId = $id)
            {
                //return ($this->validateUsersCoupleSecret($user->secret, $secret)) ? $user : null;
                return $user;
            }
        }
        return null;
    }

    protected function resendToClient($message)
    {
        $user = $this->findUserByIdAndSecret($message->user, $message->secret);
        if($user)
        {
            $this->send($user, $message->data);
        }
    }

    /**
     * @param $user
     * @param $message
     */
    protected function process ($user, $message) {
        $message = json_decode($message);
        switch($message->type)
        {
            case 'CLIENT_MSG_SETUP': $this->setUpClient($user, $message->data); break;
            case 'SERVER_MSG_MEDIATOR': $this->resendToClient($message); break;
            default: var_dump($message); break;
        }
    }

    protected function connected ($user) {
        //echo urlencode(base64_encode(self::strCode($user->id, self::$secret)));
        //$this->send($user, json_encode(array('afterConnect'=>base64_encode(self::strCode($user->id, self::$secret)))));
    }

    protected function closed ($user) {
        // Do nothing: This is where cleanup would go, in case the user had any sort of
        // open files or other objects associated with them.  This runs after the socket
        // has been closed, so there is no need to clean up the socket itself here.
    }
}