<?php
/**
 * Created by PhpStorm.
 * User: tv88d_000
 * Date: 13.11.2014
 * Time: 14:23
 */

class FAction extends CAction {

    public function run($params)
    {
        $method='action'.$this->getId();
        //FCore::getInstance()->setTag($this->tagName, call_user_func_array(array($this->controller, $method), $params));
        return call_user_func_array(array($this->controller, $method), $params);
    }
    
    public function runWithParams($params)
	{
	    $method=new ReflectionMethod($this, 'run');
	    if($method->getNumberOfParameters()>0)
	        return $this->runWithParamsInternal($this, $method, $params);
	    else
	        return $this->run();
	}
} 