<?php

	//Базовые классы CMS
	include_once(dirname(__FILE__).'/cms.php');

	//Конфигурация сайта
	include_once(dirname(__FILE__).'/FConfig.php');

    //класс ядра
    include_once(dirname(__FILE__).'/FCore.php');

    //ядро Yii
    include_once(dirname(__FILE__).'/yii/framework/yii.php');
