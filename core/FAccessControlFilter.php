<?php

class FAccessControlFilter extends CAccessControlFilter{
	
	public function filter($filterChain){
		if($this->preFilter($filterChain)){
			$bufer = $filterChain->action->run($filterChain->controller->getActionParameters($filterChain->action->getId()));
			$this->postFilter($filterChain);
			return $bufer;
		}else{
			return FCore::app()->user->getFlash('accessError');
		}
	}
	
}