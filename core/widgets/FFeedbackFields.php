<?php

/**
 * Class FFeedbackFields
 *
 * Виджет для отображения полей формы обратной связи
 * На данный момент сделаны поля:
 * -input
 * -telephone
 * -captcha
 * -label
 * -textArea
 *
 *
 * @package widgets
 * @author Trunov Vadim trunov@lookmy.info
 * @copyright 2014 LookMy.info
 */
class FFeedbackFields extends CWidget
{
    /**
     * @var array список типов полей формы
     */
    public $fieldsTypes;

    /**
     * @var string тип поля
     */
    public $type;

    /**
     * @var array список атрибутов тега
     */
    public $htmlOptions;

    /**
     * @var CActiveRecord модель, соответсвующая полю
     */
    public $model;

    /**
     * @var string имя поля
     */
    public $name;

    /**
     * Префикс метода для рендеринга поля
     */
    const METHOD_PREFIX = '';

    /**
     * Класс-хелпер занимающийся рендерингом полей
     */
    const HTML_HELPER = 'CHtml';

    /**
     * Стандартный тип для капчи
     */
    const CAPTCHA_TYPE = 'captcha';
    /**
     * Стандартный тип для телефона
     */
    const TEL_TYPE = 'telField';

    /**
     * @return array получение мссива типов полей по умолчанию
     */
    public static function getFieldsTypes()
    {
        return array(
            't' => 'textField',
            'tel' => 'telField',
            'ta' => 'textArea',
            'scode' => self::CAPTCHA_TYPE,
            'label' => 'label',
        );
    }

    /**
     * @return string рендеринг поля ввода капчи
     */
    protected function captchaField()
    {
        // Капча перерисовывается, если ввод ее был совершен более $testLimit (свойство задается в CCaptchaAction)
        // Если капча перерисовывается, то очищаем поле ввода для нее
        FCore::app()->session->open();
        $controller = FCore::getInstance()->getControllerByName('FeedbackController');
        $name = strtolower(CCaptchaAction::SESSION_VAR_PREFIX . FCore::app()->getId() . '.' . $controller->getUniqueId() . '.' . $controller->getId());
        $count = FCore::app()->session[$name . 'count'];
        if ($count == 1) {
            $this->model->data = '';
        }

        //Создание поля ввода капчи как стандартного input
        $res = FCore::getModuleController(get_class($this->model))->smartyWidget('CCaptcha');
        $res .= CHtml::textField($this->name, $this->model->data, $this->htmlOptions);
        return $res;
    }

    /**
     * Поле ввода телефона с маской
     *
     * @return string
     */
    protected function telephoneField()
    {
        return FCore::getModuleController(get_class($this->model))->smartyWidget('FMaskedInput',
            array(
                'mask' => '+38(999)999-99-99',
                'name' => $this->name,
                'value' => $this->model->data,
            )
        );
    }

    /**
     * Выполнение виджета
     */
    public function run()
    {
        if (($method = $this->fieldsTypes[$this->type]) == self::CAPTCHA_TYPE) {
            echo $this->captchaField();
        } elseif ($method == self::TEL_TYPE) {
            echo $this->telephoneField();
        } else {
            echo call_user_func_array(array(self::HTML_HELPER, self::METHOD_PREFIX . ucfirst($method)), array($this->name, $this->model->data, $this->htmlOptions));
        }
    }

} 