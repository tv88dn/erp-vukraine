<?php
/**
 * Created by PhpStorm.
 * User: tv88d_000
 * Date: 28.04.14
 * Time: 11:57
 */

class FMaskedInput extends CMaskedTextField
{
    /**
     * Подключение js-библиотеки и
     */
    public function registerClientScript()
    {
        $id=$this->htmlOptions['id'];
        $miOptions=$this->getClientOptions();
        $options=$miOptions!==array() ? ','.CJavaScript::encode($miOptions) : '';
        $js = "jQuery(document).ready(function(){jQuery(\"#{$id}\").mask(\"{$this->mask}\"{$options});});";
        FCore::includeJs('libs/maskedInput', array(), true);
        FCore::printJs($js);
    }



} 