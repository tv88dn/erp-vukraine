<?php
/**
 * Class FLinkPager
 *
 * Класс для вывода элементов управления пагинацией (номеров страниц и т.д.)
 *
 * @package widgets
 * @author Trunov Vadim trunov@lookmy.info
 * @copyright 2014 LookMy.info
 */
class FLinkPager extends CLinkPager
{

    /**
     * Инициализация атрибутов начальными значениями
     */
    public function init()
    {
        if ($this->nextPageLabel === null)
            $this->nextPageLabel = Yii::t('yii', 'Следующая &gt;');
        if ($this->prevPageLabel === null)
            $this->prevPageLabel = Yii::t('yii', '&lt; Предыдущая');
        if ($this->firstPageLabel === null)
            $this->firstPageLabel = Yii::t('yii', '&lt;&lt; Первая');
        if ($this->lastPageLabel === null)
            $this->lastPageLabel = Yii::t('yii', 'Последняя &gt;&gt;');
        if ($this->header === null)
            $this->header = Yii::t('yii', 'Перейти к странице: ');
        if (!isset($this->htmlOptions['id']))
            $this->htmlOptions['id'] = $this->getId();
        if (!isset($this->htmlOptions['class']))
            $this->htmlOptions['class'] = 'yiiPager';
    }

    protected function createPageUrl($page)
    {
        return "/" . $this->getPages()->createPageUrl($this->getController(),$page);
    }


}