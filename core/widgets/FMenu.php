<?php
/**
 * Class FMenu
 *
 * Виджет для рекурсивного вывода иерархического меню
 *
 * @package widgets
 * @author Trunov Vadim trunov@lookmy.info
 * @copyright 2014 LookMy.info
 */
class FMenu extends CWidget
{
    /**
     * @var модель дерева меню
     */
    public $menu;
    /**
     * @var имя
     * @deprecated
     */
    public $name;
    /**
     * @var имя модуля
     */
    public $controller;
    /**
     * @var имя действия
     */
    public $action;
    /**
     * @var $maxLevel
     */
    public $maxLevel;
    /**
     * @var $minLevel
     */
    public $minLevel;
    /**
     * @var тег, обарачивающий меню
     */
    private $warp;
    /**
     * @var html-представление дерева меню
     */
    private $htmlMenu;
	/**
	 * @var $pool Хранилище пунктов меню
	 */
	private $pool;

    public $count;
    /**
     * Конструктор меню
     *
     * @param null $owner родительский котнтроллер
     */
    function __construct($owner = null)
    {
        parent::__construct($owner);
        $this->count = 0;
    }

    /**
     * Создание "обертки" для меню
     *
     * @param FTag $tag тег
     * @param array $attr массив атрибутов
     * @return FTag
     */
    public function warp($tag, $attr = array())
    {
        $this->warp = new FTag($tag, $attr);
        return $this->warp;
    }

    /**
     * Получение обертки меню
     *
     * @return FTag обертка
     */
    public function getWarp()
    {
        return $this->warp;
    }

    /**
     * Добавляет шаблон отображения для меню определенного уровня вложенности.
     *
     * @param int $id уровень меню
     * @param FTag $tag имя тега, который будет основой представления
     * @param array $attr атрибуты тега
     * @return FTag соданный тег
     */
    public function addSubMenu($id, $tag, $attr = array())
    {
        $this->htmlMenu[$id]['menu'] = new FTag($tag, $attr);
        return $this->htmlMenu[$id]['menu'];
    }

    /**
     * Добавляет шаблон отображения для пунктов меню определенного уровня вложенности.
     *
     * @param int $id уровень меню
     * @param FTag $tag имя тега, который будет основой представления
     * @param array $attr атрибуты тега
     * @return FTag соданный тег
     */
    public function addSubMenuItem($id, $tag, $attr = array())
    {
        $this->htmlMenu[$id]['subMenu'] = new FTag($tag, $attr);
        return $this->htmlMenu[$id]['subMenu'];
    }

    /**
     * Инициализация виджета
     */
    public function init()
    {
        $this->pool = FPool::getInstance();
        $this->warp = new FTag();
        $this->count = 0;
        $this->htmlMenu = array();
        $this->action = ($this->action) ? $this->action : 'index';

    }

    /**
     * Исполнение виджета
     */
    public function run()
    {
        echo $this->warp->beginTag();
        echo $this->displayChild($this->menu, $this->maxLevel, $this->minLevel, 'products_menu');
        echo $this->warp->endTag();
    }

    /**
     * Выводит всех детей текущей категории
     *
     * @param mixed $tree дерево категорий
     * @param int $parent родителя
     * @param int $level текущий уровень дерева
     * @param string $class html-класс элементов
     */
    private function displayChild($tree, $parent, $level, $class = "item_block")
    {
    	!isset($level)? $level = 0:false;


        $tmpMenu = $this->getMenuLevel($level);

        $menu = $tmpMenu['menu'];
        $menu->addAttr(array('class' => 'level' . $level));

        echo $menu->beginTag();
        for($i = 0; $i < count($tree);$i++) {
			$subMenu = clone $tmpMenu['subMenu'];
			if($level >0 ){
			$subMenu->addAttr(array('class' => 'level' . $level));
			}

            if ($level == $this->pool->getItemLevel($tree[$i]))
				echo $this->replaces($this->pool->extractItem($tree[$i]), $subMenu->beginTag());

            if ($this->pool->getChildrenNum($tree[$i]) > 0) {
                $this->displayChild($this->pool->getChildrenList($tree[$i]), $tree[$i], $level + 1);
            }

            if ($i >= 0 && $level == $this->pool->getItemLevel($tree[$i]))
                echo $this->replaces($this->pool->extractItem($tree[$i]), $subMenu->endTag());
        }
        if ($level > 0)
            echo $menu->endTag();
    }

    /**
     * Возвращает уровень первого родительского меню, для которого задан шаблон
     *
     * @param $id текущий уровень меню
     * @return int номер уровня меню с шаблоном
     */
    private function getMenuLevel($id)
    {
        while (!isset($this->htmlMenu[$id]) && $id >= 0) {
            $id--;
        };
        return $this->htmlMenu[$id];
    }

    /**
     * Проводит обработку подставляемых значений
     *
     * @param ActiveRecord $item пункт меню
     * @param string $html сформированный шаблон вывода текущего пункта меню
     * @return string html-представление пункта меню
     */
    private function replaces($item, $html)
    {

        $tags = array(
            '<:NAME:>' => $this->getItemName($item),
            '<:LINK:>' => $this->makeLink($item),
            '<:ACTIVE:>' => $this->getActive($item),
        );
        $searches = array();
        $replaces = array();
        foreach ($tags AS $k => $v) {
            $searches[] = $k;
            $replaces[] = $v;

        }
        return str_replace($searches, $replaces, $html);
    }

    /**
     * Возвращает имя текущей категории
     *
     * @param mixed $item категория
     * @return mixed имя
     */
    private function getItemName($item)
    {
        return $item['item'][$this->name];
    }

    /**
     * Создает ссылку для помещения в пункт меню
     *
     * @param ActiveRecord $item пункт меню
     * @return string html-представление ссылки
     */
    private function makeLink($item)
    {
        if ($this->controller && $this->action) {
            return FCore::link($item['item'], $this->controller, $this->action);
        } else {
            return FCore::link($item['item']);
        }
    }

    /**
     * Проверяет, является ли пункт меню активным, или нет
     *
     * @param ActiveRecord $item пункт меню
     * @return string признак активности
     */
    private function getActive($item)
    {
        return $item['item']->isActive;
    }

    /**
     * Возвращает id родительской категории
     *
     * @param mixed $item категория
     * @return int id родителя
     */
    private function getItemParent($item)
    {
        return isset($item['item']['gallid']) ? $item['item']['gallid'] : -1;
    }


}
