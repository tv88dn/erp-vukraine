<?php
/**
 * Конфигурационный файл
 */
$config = array(
    //ID сайта
    'siteId' => 1,
    //Доступы к БД
    'dbName' => 'newkernel',
    'dbUser' => 'newkernel_user',
    'dbPass' => 'L2fBheHNuT5WmYrq',
    'yiiOptions' => array(
        'basePath' => dirname(__FILE__).'/yii/framework',
    ),
    'rootPath' => '',
    'charset' => 'utf8',
    'gallImgPath' => 'http://img.happyland.com.ua/portal/4nAlbum/album/3981',
    'mainJsPath' => 'resources/js/',
    'secretKey'=>'21',//должно быть число

    'specCodes' => array(
        '4nalbum' => 'CatalogController',
        'news' => 'NewsController',
        'link' => 'SiteController',
        'feedback' => 'FeedbackController',
        'authent' => 'UserController',
        'calc'=>'CalculatorController',
    ),

    //Настройки SMARTY
    'smarty' => array(
        'compileDir' => 'cache/smarty/compile',
        'debugging' => true,
        'path' => 'core/libs/smarty',
    ),
);
