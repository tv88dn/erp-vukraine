<?php

require_once('yii/framework/YiiBase.php');

/**
 * Class FCore
 *
 * Основной класс web-приложения, на ряду с SiteController выполняющий роль координатора приложения.
 * Синглтон. Производит инициализайю БД и фреймверка. Подключает контроллеры внешних модулей и упраляет их выполнением.
 * Так же управляет подключением файлов и папок к приложению
 *
 * @param $app FWebApplication
 * @package system
 * @author Trunov Vadim trunov@lookmy.info
 * @copyright 2014 LookMy.info
 */
class FCore extends YiiBase implements FSingleton
{
    const WEB_SOCKET_SECRET_CLIENT = 'GunjPYhs';
    const WEB_SOCKET_SECRET_SERVER = 'fpbnwhcc';

    /**
     * Признак включения ЧПУ адресов
     */
    const FRIENDLY_URL = false;
    /**
     * @var object FCore
     * Признак инициализации синглтона
     */
    private static $instance;
    /**
     * @var array массив контроллеров внешних модулей
     */
    private static $controllers;
    /**
     * @var array массив имен модулей, к которым подключенны именные JS-файлы
     */
    private static $includeJs = array();
    private static $app;
    /**
     * @var class экземпляр класса FRequest, который управляет процессом получения данных из $_POST и $_GET запросов
     */
    public $request;
    /**
     * @var string доменное имя сайта
     */
    public $homeUrl;
    /**
     * @var bool
     * признак инициализации фреймверка
     */
    private $isInitFramework = false;
    /**
     * @var object CDbConnetcion
     * ссылка на БД
     */
    private $dbLink;
    /**
     * @var array результирующий массив тегов, содержащий результат отработки контроллера и экземпляр контроллера.
     * Имеет формат [спец.тег]=>{[data]=>результат, [controller]=>контроллер модуля}
     */
    private $tags;
    /**
     * @var object главный контроллер сайта, экземпляр класса SiteController
     */
    private $mainController;
    /**
     * @var object экземпляр єкземпляр класса, производного от FController. Контроллек текущего активного модуля, т.е. такого, который выводит данные в область контента
     */
    private $activeController;
    /**
     * @var фабрика подключения виджетов
     */
    private $widgetFactory;

    private static $allModels;

    /**
     * Приватный контруктор.
     */
    private function __construct()
    {
        $this->homeUrl = $_SERVER['HTTP_HOST'];
        $this->widgetFactory = new CWidgetFactory();
    }

    public function getModels()
    {
        $models = CFileHelper::findFiles(FConfig::MODULE_PATH, array(
            'fileTypes'=>array('php'),
            'exclude'=>array('Controller', 'view'),
        ));
        $finalModels = array();
        for($i = 0; $i < count($models); $i++)
        {
            $models[$i] = (explode(DIRECTORY_SEPARATOR, $models[$i]));
            $models[$i] = str_replace('.php', '', end( $models[$i] ));
            $finalModels[$models[$i]] = $models[$i];
        }
        return $finalModels;
    }

    public function getModules()
    {
        $files = CFileHelper::findFiles(FConfig::MODULE_PATH, array(
            'fileTypes'=>array('php'),
            'exclude'=>array('Controller', 'view'),
            'level'=>1
        ));
        $modules = array();
        for($i = 0; $i < count($files); $i++)
        {
            $names = explode('/', $files[$i]);
            $modules[$names[2]] = $names[2];
        }
        return $modules;
    }

    public function getModuleModels($module)
    {
        $files = CFileHelper::findFiles(FConfig::MODULE_PATH.$module, array(
            'fileTypes'=>array('php'),
            'exclude'=>array('Controller', 'view'),
            'level'=>1
        ));
        $models = array();
        for($i = 0; $i < count($files); $i++)
        {
            $name = explode('/', $files[$i]);
            $name = end($name);
            $name = explode('.', $name);
            $models[$name[0]] = $name[0];
        }
        return $models;
    }

    public function getModuleActions($moduleName)
    {

        $methods = get_class_methods(ucfirst($moduleName.'Controller'));
        $actions = array();
        for($i =0; $i < count($methods); $i++)
        {
            if(preg_match('/^action[A-Z].*/', $methods[$i]))
            {
                $actions[$methods[$i]] = $methods[$i];
            }
        }
        return $actions;
    }


    /**
     * инициализация синглтона
     *
     * @return FCore экземпляр класса
     */
    static public function getInstance()
    {
        if (self::$instance == NULL) {
            self::$instance = new FCore();
        }
        return self::$instance;
    }

    static public function accessMessage($rule){
        if($rule->message != null){
            if(is_array($rule->message)){
                self::app()->user->setFlash("accessError",implode(" ",$rule->message));
            }else{
                self::app()->user->setFlash("accessError",$rule->message);
            }
        }
    }

    /**
     * Обертка для getInstance для более удобного вызова
     *
     * @return FCore экземпляр класса
     */
    static public function app()
    {
        return self::$app;
    }

    /**
     * Управляет подключением именных JS-файлов к приложению.
     *
     * Каждый модуль может иметь именной JS файл, в который помещена логика работы клиентской части модуля.
     * Если такой есть, то ядро подключает его автоматически
     *
     * @param string $moduleName имя модуля
     * @param bool $includeAnyway подключить JS даже повторно
     * @param bool $lib
     * @return string HTML-строка для подключения JS файла
     */
    public static function includeJs($moduleName, $includeAnyway = false, $lib = false)
    {
        if(!$lib)
        {
            $moduleName = ucfirst($moduleName);
        }
        $jsFile = '';
        $fileName = FConfig::$mainJsPath . $moduleName . '.js';

        $isNew = true;
        for ($i = 0; $i < count(self::$includeJs); $i++) {
            if (self::$includeJs[$i] == $moduleName) {
                $isNew = false;
                break;
            }
        }
        if ($includeAnyway) {
            $jsFile = self::makeJsInclusion($fileName, $moduleName);
        } else if ($isNew) {
            $jsFile = self::makeJsInclusion($fileName, $moduleName);
        }

        // echo $jsFile;
    }

    public static function printJs($js)
    {
        echo '<script language="javascript" type="text/javascript">'.$js.'</script>';
    }

    /**
     * Формирует JS-код для подключения скрипта, если файл со скриптом существует
     *
     * @param $filePath  string путь к скрипту
     * @param $moduleName string имя модуля
     * @return string код подключения
     */
    private static function makeJsInclusion($filePath, $moduleName)
    {
        if (file_exists(FConfig::$absoluteRootPath . $filePath)) {
            self::$includeJs[] = $moduleName;
            return '<script language="javascript" type="text/javascript" src="/' . FConfig::$rootPath . $filePath . '"></script>';
        }
    }

    /**
     * Функция формирует ссылку исходя из экземпляра модели.
     * В простейшем случае ьерется модель, находится ее первичный ключ и исходя из него формируется ссылка
     * Так же предусмотрен вариант передачи всех параметров урл в функцию
     *
     * @param object $item AR-модель класса
     * @param string $module имя модуля, в который будет вести ссылка
     * @param string $action имя экшена, в который будет вести ссылка
     * @param int $id идентификатор ключевого поля из $item
     * @param array $params массив дополнительных переменных для запроса
     * @return string сгенерированный адресс ссылки
     * @return string
     */
    public static function link($item, $module = null, $action = 'index', $id = null, $params = null)
    {
        if( is_string($item))
        {
            $res = $item;
        }
        else
        {
            if (self::FRIENDLY_URL) {
                if (isset($item['fullurl'])) {
                    $res = $item['fullurl'];
                } else {
                    $res = self::getHref($item, $module, $action, $id);
                }
            } else {
                $res = self::getHref($item, $module, $action, $id);
            }
        }

        $res = '/' . FConfig::$rootPath . $res;
        $res .= self::explodeParams($params);
        return strtolower($res);
    }

    /**
     * Формирует ссылку для станартного (не ЧПУ) роутинга
     *
     * @param object $item AR-модель класса
     * @param string $module имя модуля, в который будет вести ссылка
     * @param string $action имя экшена, в который будет вести ссылка
     * @param int $id идентификатор ключевого поля из $item
     * @return string сгенерированный адресс ссылки
     * @throws CException несоответсвие индентификатора модели
     */
    private static function getHref($item, $module, $action, $id)
    {
        if ($id === null) {
            $id = $item->tableSchema->primaryKey;
        }
        if ($module === null) {
            $module = strtolower(get_class($item));
        }
        if (isset($item[$id])) {
            return $module . '/' . $action . '/?' . $id . '=' . $item[$id];
        } else {
            throw new CException("Страница не найдена");
        }
    }

    /**
     * преобразовывает массив параметров в строку УРЛ запроса
     * @param $params
     * @return string
     */
    private static function explodeParams($params)
    {
        $res = '';
        if ($params) {
            foreach ($params as $k => $v) {
                $res = '&' . $k . '=' . $v;
            }
        }
        return $res;
    }

    /**
     * Получение action'a контроллера исходя из тэга
     *
     * @param $tag
     * @return string
     */
    private static function getActionName($tag)
    {

        $action = str_replace(array('<:', ':>'), '', $tag);
        $action = explode('::', $action);

        $actionName = 'action';
        if (count($action) > 1) {
            $action = strtolower($action[1]);
            $action = explode('_', $action);
            for ($i = 0; $i < count($action); $i++) {
                $actionName .= ucfirst($action[$i]);
            }
        } else {
            $actionName .= 'Index';
        }
        return $actionName;
    }

    /**
     * Возвращает "фабрику виджетов" - класс, служащий для инициализации виджетов в приложение
     *
     * @return CWidgetFactory
     */
    public function getWidgetFactory()
    {
        return $this->widgetFactory;
    }

    /**
     * Завершение приложения
     */
    public function end()
    {
        die();
    }

    /**
     * Возвращает экземпляр SiteController, для возможности обращения к главному контроллеру из внешних модулей и сервисных классов
     *
     * @return object главный контроллер сайта, экземпляр класса SiteController
     */
    public function getMainController()
    {
        return $this->mainController;
    }

    /**
     * @param $name string имя контроллера
     * @return CController єкземпляр класса, производного от FController
     */
    public function getControllerByName($name)
    {
        if(isset(self::$controllers[$name]))
        {
            return self::$controllers[$name];
        }
        else
        {
            return self::initController($name);
        }
    }

    /**
     * Возвращает контроллер текущего актвного модуля.
     *
     * @return object  экземпляр єкземпляр класса, производного от FController.
     */
    public function getActiveController()
    {
        return $this->activeController;
    }


    public static function initMailing()
    {
        include_once('libs/mailing/Mailer.php');
    }

    public static function initWebSocket()
    {
        require_once(FConfig::CORE_PATH . 'libs/websocket/WebSocketClient.php');
    }

    /**
     * Инициализация фреймверка.
     * Подключение контроллера сайта.
     * Подключение AR-моделей.
     *
     * @param string $rootDir путь корню сайта
     * @return bool true - если произведено подключение, false - иначе
     */
    public function initFramework($rootDir = '')
    {
        if (!$this->isInitFramework) {
            self::includeDirectory($rootDir . FConfig::INTERFACE_PATH);
            include_once($rootDir . FConfig::FRAMEWORK_ACTIVE_RECORD_MODELS.'FActiveRecord.php');
            self::includeDirectory($rootDir . FConfig::FRAMEWORK_ACTIVE_RECORD_MODELS);
            self::includeDirectory($rootDir . FConfig::BEHAVIOR_PATH);
            self::includeDirectory($rootDir . FConfig::WIDGET_PATH);
            self::includeDirectory($rootDir . FConfig::ENTITIES_PATH);
            self::includeDirectory($rootDir . FConfig::HELPERS_PATH);
            self::initSmarty();
            self::initMailing();
            include_once($rootDir . FConfig::CORE_PATH . 'FController.php');
            include_once($rootDir . FConfig::CORE_PATH . 'FAccessControlFilter.php');
            include_once($rootDir . FConfig::MODULE_PATH . 'SiteController.php');
            include_once($rootDir . FConfig::CORE_PATH . 'FRequest.php');
            include_once($rootDir . FConfig::CORE_PATH . 'FWebApplication.php');
            include_once($rootDir . FConfig::CORE_PATH . 'FAction.php');
            self::$app = $this->createWebApplication(FConfig::$yiiOptions);
            self::includeDirectory($rootDir . FConfig::CORE_PATH . '/yii/framework/validators');
            self::includeDirectory($rootDir . FConfig::MODULE_PATH, 1);
            self::$app->setLanguage('ru');
            $this->request = new FRequest();
        }

    }

    protected static function parseClassPath($path)
    {
        $pathArr = explode('/', $path);
        $pathArr = explode('.', end($pathArr));
        return reset($pathArr);
    }

    /**
     * Выполняет подключение всех файлов в дирректории
     * Функция производит рекурсивный поиск во вложенных папках до уровня $level.
     * По умолчанию поиск производится в корне переданной дирректории.
     *
     * @param $dir
     * @param int $level
     * @return array
     * @throws CException
     */
    public static function includeDirectory($dir, $level = 0)
    {
        $includesClasses = array();
        if (is_dir($dir)) {
            set_include_path($dir);
            $arr = CFileHelper::findFiles($dir, array('fileTypes' => array('php'), 'level' => $level));
            for ($i = 0; $i < count($arr); $i++) {
                $includes = str_replace('//', '/', $arr[$i]);
                $includesClasses[]  = self::parseClassPath($includes);
                include_once($includes);
            }
        } else {
            var_dump(debug_backtrace());
            var_dump($dir);
            throw new CException('Неврный путь к дирректории ' . $dir);
        }
        return $includesClasses;
    }

    /**
     * Инициализирует библиотеку шаблонизации SMARTY
     */
    public function initSmarty()
    {
        include_once('libs/smarty/Smarty.class.php');
    }

    public static function initSphinx()
    {
        include_once('libs/sphinx/sphinxapi.php');
    }

    public static function initGoogle()
    {
        require_once('libs/google/autoload.php');
    }

    /**
     * Инициализирует приложение Yii
     *
     * @param null $config массив конфигурации
     * @return CWebApplication|mixed
     */
    public static function createWebApplication($config = null)
    {
        return self::createApplication('FWebApplication', $config);
    }

    /**
     *
     * Подключение и инициализация БД
     *
     * @param string $name имя БД
     * @param string $user пользователь БД
     * @param string $password пароль к БД
     * @return bool рузультат подключения
     */
    public function initDatabase($name, $user, $password)
    {
        try {
            $this->dbLink = new CDbConnection('mysql:host=localhost;dbname=' . $name, $user, $password);
            $this->dbLink->charset = FConfig::$charset;
            $this->dbLink->setActive(true);

            if ($this->dbLink === false) {
                throw new FError('Нет подключения к базе данных.', '0101');
            }

        } catch (FError $e) {
            echo $e->display();
        }
        return true;
    }

    /**
     * Получение ссылки на экземпляр БД (СDbConnection)
     *
     * @return object
     */
    public function getDb()
    {
        return $this->dbLink;
    }

    /**
     * Запуск web-приложения
     * Производится инициализация главного контроллера.
     * Затем инициализация массива подключенных контроллеров внешних модулей ($controllers).
     * В массив контроллеров добавляется главный контроллер.
     * После инициализации главного контроллера выполняется проверка на принадлежность текущего запроса к ajax
     * Если запрос аяксовый, то производится упращенный рендеринг без подключения шаблона.
     * В противном случае выполняется стандартный рендеринг главного контроллера
     */
    public function run()
    {
        $this->mainController = new SiteController('siteController');
        self::$controllers = array();
        self::$controllers['SiteController'] = $this->mainController;

        if ($this->request->getIsAjaxRequest()) {
            $this->mainController->ajaxInit();
            $this->mainController->renderAjax($this->activeController);
        } else {
            $this->mainController->init();
            $this->mainController->render('index');
        }

    }

    /**
     * Обработка и выполнение всех модулей из шаблона
     * Производится поиск таких методов и их выполнение.
     *
     *
     * @param $template текущий шаблон
     * @return array результирующий массив тегов с результатом работы модулей
     */
    public function processModules($template)
    {

        $tagsConf = self::getSpecTags($template);
        $this->modules = array();
        $this->tags = array();
        for ($i = 0; $i < count($tagsConf); $i++) {
            $this->externalModuleExecute($tagsConf[$i]);
        }
        return $this->tags;
    }

    /**
     * Получение массива спец.тегов из главного layout'а.
     * Под спец.тегами следует понимать конструкции вида <:<i>moduleName</i>_<i>moduleAction</i>:>
     *
     * @param string $template шаблон для поиска тегов
     * @return array массив тегов
     */
    public static function getSpecTags($template)
    {
        if ($template) {
            $tagsList = array();
            preg_match_all('/\<\:.*\:\>/', $template, $tagsList);
            return $tagsList[0];
        }
    }

    /**
     * @param $specTag
     * @param $runResult
     * @return mixed
     */
    public function setTag($specTag, $runResult)
    {
        if(!isset($this->tags[$specTag]['data']))
        {
            $this->tags[$specTag]['data'] = $runResult;
        }
        return $runResult;
    }

    /**
     * Выполнение внешнего модуля. По спец.тегу производится подключение контроллера модуля и выполнение action'а указанного в теге
     * Результат записывается в соответсвующий тег результирующего массива.
     *
     * @param string $specTag спец.тег
     */
    public function externalModuleExecute($specTag)
    {
        $cfg = $this->getConfigRecord($specTag);
        $module = $cfg['controller'] . 'Controller';
        $actionName = 'action' . $cfg['action'];
        if (!isset(self::$controllers[$module])) {
            self::$controllers[$module] = FCore::initController($module);
        }
        $controller = self::$controllers[$module];
        $this->tags[$specTag]['data'] = $controller->run($cfg['action']);
    }

    /**
     * Получение связки module-action исходя из спец.тега
     * Возвращаются исключительно имена без подстановки суффикса Controller и преффикса action
     *
     * @param $configRecord спец.тег
     * @return array именная связка module-action
     */
    private function getConfigRecord($configRecord)
    {
        $configRecord = str_replace(array('<:', ':>'), '', $configRecord);
        $res = explode('::', $configRecord);
        $res[0] = ucfirst(strtolower($res[0]));
        $res[1] = explode('_', $res[1]);
        $actionName = '';
        for ($i = 0; $i < count($res[1]); $i++) {
            $actionName .= ucfirst(strtolower($res[1][$i]));
        }
        $res[1] = $actionName;
        return array('controller' => $res[0], 'action' => $res[1], 'tag'=>$configRecord);
    }

    /**
     * Вызывает встроенный метод обработки главной страницы
     *
     * @return mixed результат выполнения action'а для компонента главной страницы
     */
    public function getMainPage()
    {
        $this->activeController = self::initController('UserController');
        $this->activeController->currentActive = 'mainPage';
        return $this->activeController->run('mainPage');
    }

    public function getNotFoundPage()
    {
        $this->activeController = self::initController('SiteController');
        $this->activeController->currentActive = 'content';
        return $this->activeController->run('content');
    }

    /**
     * Проверяет является имя класса удовлетворительным для контроллера
     *
     * @param $name string имя контроллера
     * @return bool
     */
    public static function isController($name)
    {
        return (strpos($name, 'Controller')) ? true : false;
    }

    /**
     * Проверяет есть ли такой файл контроллера
     *
     * @param $name string имя контроллера
     * @return bool
     */
    public static function isControllerExist($name)
    {
        return file_exists(FConfig::MODULE_PATH . self::getModuleName($name) . '/' . $name . '.php');
    }

    /**
     * Получает имя модуля по имени его контроллера
     *
     * @param $controllerName string имя контроллера
     * @return string
     */
    public static function getModuleName($controllerName)
    {
        return strtolower(str_replace('Controller', '', $controllerName));
    }

    /**
     * Формирует имя модели для контроллера с заданым именем
     *
     * @param $name имя контроллера
     * @return mixed
     */
    public static function makeControllerModelName($name)
    {
        $modelName = str_replace('Controller', '', $name);
        return $modelName;
    }


    public static function getModuleController($name)
    {
        return self::initController(ucfirst($name) . 'Controller');
    }

    /**
     * Подключение контроллера модуля к приложению.
     * Выполняется проверка, был ли такой контроллер уже подключен. Если да, то возвращается его экземпляр.
     * В противном случае выполняется подключение после чеко так же выозвращается экземпляр этого контроллера.
     *
     * @param string $controllerName имя контроллера
     * @return object  экземпляр єкземпляр класса, производного от FController.
     */
    public static function initController($controllerName)
    {
        if (!self::$controllers) {
            self::$controllers = array();
        }
        if (isset(self::$controllers[$controllerName])) {
            return self::$controllers[$controllerName];
        }
        if (self::isController($controllerName) && self::isControllerExist($controllerName)) {
            $modelName = self::getModuleName($controllerName);
            $controller = new $controllerName($modelName);
            $controller->initModel(new $modelName);
            if ($controller instanceof FController) {
                self::$controllers[$controllerName] = $controller;
            }
            return $controller;
        }

    }

    /**
     * Главный роутер приложения.
     * Обрабатывает ЧПУ ссылку поиском по ключевому полю в моделях модулей.
     * В поиске участвуют только модели, контроллером которых является имплементация интерфейса FComponent.
     * Если обработка не дала результата, вызывает стандартный роутер
     *
     * @param string $urlRequest строка запроса
     * @return bool|mixed|null результат работы экшена активного контроллера
     * @throws FError страница не найдена
     */
    public function routing($urlRequest)
    {
        if($urlRequest == 'index.php')
        {
            FCore::getMainController()->redirect('/'.FConfig::$rootPath);
            FCore::app()->end();
        }
        $routingComponents = self::getRoutingModels(self::getBaseARModels());
        $processingComponents = array();
        for ($i = 0; $i < count($routingComponents); $i++) {
            if ($routingComponents[$i]->checkFriendlyUrl($urlRequest)) {
                $processingComponents[] = $routingComponents[$i];
            }
        }
        if (($len = count($processingComponents)) <= 1) {
            if($len)
            {
                $this->activeController = $processingComponents[0];
                return $processingComponents[0]->model;
            }
            else
            {
                return (!strlen($urlRequest)) ? null : $this->standardRouting($urlRequest);
            }
            // return ($len) ? $processingComponents[0] : ((!strlen($urlRequest)) ? null : $this->standardRouting($urlRequest));
        } else {
            throw new FError('Более одного компонента');
        }
    }

    /**
     *  Получает массив моделей, которые могут выступать как главные страницы
     * Определяющим фактором является имплементация интерфейса FComponent
     *
     * @param $controllers
     * @return array
     */
    public static function getRoutingModels($controllers)
    {
        $routingModels = array();
        for ($i = 0; $i < count($controllers); $i++) {
            $controller = self::initController($controllers[$i]);
            if ($controller instanceof FComponent) {
                $routingModels[] = $controller;
            }
        }
        return $routingModels;
    }

    /**
     * Список AR-моделей методов
     * @return array
     */
    public static function getBaseARModels()
    {
        $models = array();
        $dir = FConfig::MODULE_PATH;
        $arr = CFileHelper::findFiles($dir, array('fileTypes' => array('php'), 'level' => 1));
        for ($i = 0; $i < count($arr); $i++) {
            $file = str_replace('//', '/', $arr[$i]);
            include_once($file);
            $file = explode('/', $file);
            $model = ($file[count($file) - 1]);
            if (strtolower($model) != 'config.php' && count($file) > 2) {
                $model = explode('.', $model);
                $models[] = $model[0];
            }
        }
        return $models;
    }

    /**
     * Стандартный роутер не ЧПУ страниц.
     * Исходя из урл строки определяет текущий главный компонент и его action.
     * Возврощает результат их выполнения
     *
     * @param string $urlRequest строка запроса
     * @return bool|mixed результат работы экшена активного контроллера
     */
    public function standardRouting($urlRequest)
    {
        $param = explode('/', $urlRequest);
        if(count($param) > 2)
        {
            header("HTTP/1.0 404 Not Found");
            die();
        }
        $controllerName = ucfirst($param[0]) . 'Controller';

        $this->activeController = self::initController($controllerName);
        if ($this->activeController && strlen($param[1]) > 0  ) {
            $this->activeController->currentActive = $param[1];
            return $this->activeController->run($param[1]);
        } else {
            return null;
        }

    }

    /**
     * Выдает имя тега. Под тегом понимаем конструкцию типа <:TAG_NAME:>
     *
     * @param $tag string тег
     * @return string
     */
    public static function clearTag($tag)
    {
        return strtolower(str_replace(array('<:', ':>'), '', $tag));
    }

    /**
     * Формирует тег. Под тегом понимаем конструкцию типа <:TAG_NAME:>
     *
     * @param $name string имя тега
     * @return string
     */
    public static function createTag($name)
    {
        $tag = self::clearTag($name);
        return '<:' . strtoupper($tag) . ':>';
    }

}
