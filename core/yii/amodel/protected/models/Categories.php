<?php

/**
 * This is the model class for table "inform_4nalbum_categories".
 *
 * The followings are the available columns in table 'inform_4nalbum_categories':
 * @property integer $gallid
 * @property string $gallname
 * @property string $gallimg
 * @property string $galloc
 * @property string $small_description
 * @property string $description
 * @property integer $parent
 * @property integer $visible
 * @property string $template
 * @property string $name_templ
 * @property string $thumbwidth
 * @property integer $numcol
 * @property integer $numSubCol
 * @property string $total
 * @property string $lastadd
 * @property string $user
 * @property string $page_template
 * @property integer $priority
 * @property integer $setPopupImage
 * @property integer $displayChildFoto
 * @property string $sortCatMedia
 * @property integer $numberCatMedia
 * @property string $linkto
 * @property string $gurl
 * @property string $fullurl
 * @property integer $isImageOnImages
 */
class Categories extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'inform_4nalbum_categories';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('gallname, gallimg, small_description, description, name_templ', 'required'),
			array('parent, visible, numcol, numSubCol, priority, setPopupImage, displayChildFoto, numberCatMedia, isImageOnImages', 'numerical', 'integerOnly'=>true),
			array('gallname, name_templ', 'length', 'max'=>250),
			array('gallimg', 'length', 'max'=>50),
			array('template, total, user', 'length', 'max'=>10),
			array('thumbwidth', 'length', 'max'=>2),
			array('page_template, sortCatMedia', 'length', 'max'=>40),
			array('linkto, gurl, fullurl', 'length', 'max'=>255),
			array('galloc, lastadd', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('gallid, gallname, gallimg, galloc, small_description, description, parent, visible, template, name_templ, thumbwidth, numcol, numSubCol, total, lastadd, user, page_template, priority, setPopupImage, displayChildFoto, sortCatMedia, numberCatMedia, linkto, gurl, fullurl, isImageOnImages', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'gallid' => 'Gallid',
			'gallname' => 'Gallname',
			'gallimg' => 'Gallimg',
			'galloc' => 'Galloc',
			'small_description' => 'Small Description',
			'description' => 'Description',
			'parent' => 'Parent',
			'visible' => 'Visible',
			'template' => 'Template',
			'name_templ' => 'Name Templ',
			'thumbwidth' => 'Thumbwidth',
			'numcol' => 'Numcol',
			'numSubCol' => 'Num Sub Col',
			'total' => 'Total',
			'lastadd' => 'Lastadd',
			'user' => 'User',
			'page_template' => 'Page Template',
			'priority' => 'Priority',
			'setPopupImage' => 'Set Popup Image',
			'displayChildFoto' => 'Display Child Foto',
			'sortCatMedia' => 'Sort Cat Media',
			'numberCatMedia' => 'Number Cat Media',
			'linkto' => 'Linkto',
			'gurl' => 'Gurl',
			'fullurl' => 'Fullurl',
			'isImageOnImages' => 'Is Image On Images',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('gallid',$this->gallid);
		$criteria->compare('gallname',$this->gallname,true);
		$criteria->compare('gallimg',$this->gallimg,true);
		$criteria->compare('galloc',$this->galloc,true);
		$criteria->compare('small_description',$this->small_description,true);
		$criteria->compare('description',$this->description,true);
		$criteria->compare('parent',$this->parent);
		$criteria->compare('visible',$this->visible);
		$criteria->compare('template',$this->template,true);
		$criteria->compare('name_templ',$this->name_templ,true);
		$criteria->compare('thumbwidth',$this->thumbwidth,true);
		$criteria->compare('numcol',$this->numcol);
		$criteria->compare('numSubCol',$this->numSubCol);
		$criteria->compare('total',$this->total,true);
		$criteria->compare('lastadd',$this->lastadd,true);
		$criteria->compare('user',$this->user,true);
		$criteria->compare('page_template',$this->page_template,true);
		$criteria->compare('priority',$this->priority);
		$criteria->compare('setPopupImage',$this->setPopupImage);
		$criteria->compare('displayChildFoto',$this->displayChildFoto);
		$criteria->compare('sortCatMedia',$this->sortCatMedia,true);
		$criteria->compare('numberCatMedia',$this->numberCatMedia);
		$criteria->compare('linkto',$this->linkto,true);
		$criteria->compare('gurl',$this->gurl,true);
		$criteria->compare('fullurl',$this->fullurl,true);
		$criteria->compare('isImageOnImages',$this->isImageOnImages);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Categories the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
