<?php

/**
 * This is the model class for table "inform_opimiz".
 *
 * The followings are the available columns in table 'inform_opimiz':
 * @property string $id
 * @property integer $user_id
 * @property string $url
 * @property string $title
 * @property string $keywords
 * @property string $description
 * @property string $h1
 * @property string $key1
 * @property string $key2
 * @property integer $isfull
 * @property string $seotext
 */
class UserSeoData extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'inform_opimiz';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id, user_id, url, title, keywords, description, h1, key1, key2, isfull, seotext', 'required'),
			array('user_id, isfull', 'numerical', 'integerOnly'=>true),
			array('id', 'length', 'max'=>20),
			array('url', 'length', 'max'=>100),
			array('key1, key2', 'length', 'max'=>50),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, user_id, url, title, keywords, description, h1, key1, key2, isfull, seotext', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'user_id' => 'User',
			'url' => 'Url',
			'title' => 'Title',
			'keywords' => 'Keywords',
			'description' => 'Description',
			'h1' => 'H1',
			'key1' => 'Key1',
			'key2' => 'Key2',
			'isfull' => 'Isfull',
			'seotext' => 'Seotext',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('user_id',$this->user_id);
		$criteria->compare('url',$this->url,true);
		$criteria->compare('title',$this->title,true);
		$criteria->compare('keywords',$this->keywords,true);
		$criteria->compare('description',$this->description,true);
		$criteria->compare('h1',$this->h1,true);
		$criteria->compare('key1',$this->key1,true);
		$criteria->compare('key2',$this->key2,true);
		$criteria->compare('isfull',$this->isfull);
		$criteria->compare('seotext',$this->seotext,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Seo the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
