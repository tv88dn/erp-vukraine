<?php

/**
 * This is the model class for table "inform_4nalbum_description_categories".
 *
 * The followings are the available columns in table 'inform_4nalbum_description_categories':
 * @property integer $id
 * @property integer $gallid
 * @property string $description
 * @property string $necessary
 * @property integer $quote
 * @property string $search
 * @property integer $sType
 * @property string $diapazon
 * @property integer $compare
 */
class AlbumDescriptionCategories extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'inform_4nalbum_description_categories';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('gallid, quote, sType, compare', 'numerical', 'integerOnly'=>true),
			array('necessary, search', 'length', 'max'=>1),
			array('description, diapazon', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, gallid, description, necessary, quote, search, sType, diapazon, compare', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'gallid' => 'Gallid',
			'description' => 'Description',
			'necessary' => 'Necessary',
			'quote' => 'Quote',
			'search' => 'Search',
			'sType' => 'S Type',
			'diapazon' => 'Diapazon',
			'compare' => 'Compare',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('gallid',$this->gallid);
		$criteria->compare('description',$this->description,true);
		$criteria->compare('necessary',$this->necessary,true);
		$criteria->compare('quote',$this->quote);
		$criteria->compare('search',$this->search,true);
		$criteria->compare('sType',$this->sType);
		$criteria->compare('diapazon',$this->diapazon,true);
		$criteria->compare('compare',$this->compare);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return AlbumDescriptionCategories the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
