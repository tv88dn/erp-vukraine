<?php

/**
 * This is the model class for table "inform_pages".
 *
 * The followings are the available columns in table 'inform_pages':
 * @property integer $pid
 * @property integer $cid
 * @property string $title
 * @property string $subtitle
 * @property integer $active
 * @property string $parent
 * @property string $quote
 * @property string $parent_quote
 * @property string $page_header
 * @property string $text
 * @property string $page_footer
 * @property string $signature
 * @property string $date
 * @property integer $counter
 * @property string $clanguage
 * @property string $user
 * @property string $template
 * @property integer $main
 * @property string $spec
 * @property integer $use_form
 * @property string $daycounter
 * @property string $gurl
 * @property string $fullurl
 */
class Pages extends CActiveRecord
{
    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'inform_pages';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('title, subtitle, page_header, text, page_footer, signature', 'required'),
            array('cid, active, counter, main, use_form', 'numerical', 'integerOnly'=>true),
            array('title, subtitle, gurl, fullurl', 'length', 'max'=>255),
            array('parent, quote, spec', 'length', 'max'=>10),
            array('parent_quote', 'length', 'max'=>6),
            array('clanguage', 'length', 'max'=>30),
            array('user', 'length', 'max'=>11),
            array('template, daycounter', 'length', 'max'=>20),
            array('date', 'safe'),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('pid, cid, title, subtitle, active, parent, quote, parent_quote, page_header, text, page_footer, signature, date, counter, clanguage, user, template, main, spec, use_form, daycounter, gurl, fullurl', 'safe', 'on'=>'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'pagesSpec' => array(self::HAS_ONE, 'PagesSpec', 'id'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'pid' => 'Pid',
            'cid' => 'Cid',
            'title' => 'Title',
            'subtitle' => 'Subtitle',
            'active' => 'Active',
            'parent' => 'Parent',
            'quote' => 'Quote',
            'parent_quote' => 'Parent Quote',
            'page_header' => 'Page Header',
            'text' => 'Text',
            'page_footer' => 'Page Footer',
            'signature' => 'Signature',
            'date' => 'Date',
            'counter' => 'Counter',
            'clanguage' => 'Clanguage',
            'user' => 'User',
            'template' => 'Template',
            'main' => 'Main',
            'spec' => 'Spec',
            'use_form' => 'Use Form',
            'daycounter' => 'Daycounter',
            'gurl' => 'Gurl',
            'fullurl' => 'Fullurl',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search()
    {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria=new CDbCriteria;

        $criteria->compare('pid',$this->pid);
        $criteria->compare('cid',$this->cid);
        $criteria->compare('title',$this->title,true);
        $criteria->compare('subtitle',$this->subtitle,true);
        $criteria->compare('active',$this->active);
        $criteria->compare('parent',$this->parent,true);
        $criteria->compare('quote',$this->quote,true);
        $criteria->compare('parent_quote',$this->parent_quote,true);
        $criteria->compare('page_header',$this->page_header,true);
        $criteria->compare('text',$this->text,true);
        $criteria->compare('page_footer',$this->page_footer,true);
        $criteria->compare('signature',$this->signature,true);
        $criteria->compare('date',$this->date,true);
        $criteria->compare('counter',$this->counter);
        $criteria->compare('clanguage',$this->clanguage,true);
        $criteria->compare('user',$this->user,true);
        $criteria->compare('template',$this->template,true);
        $criteria->compare('main',$this->main);
        $criteria->compare('spec',$this->spec,true);
        $criteria->compare('use_form',$this->use_form);
        $criteria->compare('daycounter',$this->daycounter,true);
        $criteria->compare('gurl',$this->gurl,true);
        $criteria->compare('fullurl',$this->fullurl,true);

        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
        ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return Pages the static model class
     */
    public static function model($className=__CLASS__)
    {

        return parent::model($className);
    }
}
