<?php

/**
 * This is the model class for table "inform_4nalbum_pictures".
 *
 * The followings are the available columns in table 'inform_4nalbum_pictures':
 * @property string $pid
 * @property integer $gid
 * @property string $img
 * @property string $previewpic
 * @property string $counter
 * @property string $submitter
 * @property string $date
 * @property string $lastmod
 * @property string $name
 * @property string $description
 * @property string $votes
 * @property double $rate
 * @property string $extension
 * @property integer $width
 * @property integer $height
 * @property string $quote
 * @property string $user
 * @property integer $show
 * @property integer $hot_prop
 * @property string $cena_shop
 * @property string $gurl
 * @property string $fullurl
 * @property string $last_view
 */
class AlbumPictures extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'inform_4nalbum_pictures';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('img, previewpic, name, description', 'required'),
			array('gid, width, height, show, hot_prop', 'numerical', 'integerOnly'=>true),
			array('rate', 'numerical'),
			array('img, previewpic, name, cena_shop, gurl, fullurl', 'length', 'max'=>255),
			array('counter, votes, extension, quote, user', 'length', 'max'=>10),
			array('submitter', 'length', 'max'=>24),
			array('date, lastmod, last_view', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('pid, gid, img, previewpic, counter, submitter, date, lastmod, name, description, votes, rate, extension, width, height, quote, user, show, hot_prop, cena_shop, gurl, fullurl, last_view', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'pid' => 'Pid',
			'gid' => 'Gid',
			'img' => 'Img',
			'previewpic' => 'Previewpic',
			'counter' => 'Counter',
			'submitter' => 'Submitter',
			'date' => 'Date',
			'lastmod' => 'Lastmod',
			'name' => 'Name',
			'description' => 'Description',
			'votes' => 'Votes',
			'rate' => 'Rate',
			'extension' => 'Extension',
			'width' => 'Width',
			'height' => 'Height',
			'quote' => 'Quote',
			'user' => 'User',
			'show' => 'Show',
			'hot_prop' => 'Hot Prop',
			'cena_shop' => 'Cena Shop',
			'gurl' => 'Gurl',
			'fullurl' => 'Fullurl',
			'last_view' => 'Last View',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('pid',$this->pid,true);
		$criteria->compare('gid',$this->gid);
		$criteria->compare('img',$this->img,true);
		$criteria->compare('previewpic',$this->previewpic,true);
		$criteria->compare('counter',$this->counter,true);
		$criteria->compare('submitter',$this->submitter,true);
		$criteria->compare('date',$this->date,true);
		$criteria->compare('lastmod',$this->lastmod,true);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('description',$this->description,true);
		$criteria->compare('votes',$this->votes,true);
		$criteria->compare('rate',$this->rate);
		$criteria->compare('extension',$this->extension,true);
		$criteria->compare('width',$this->width);
		$criteria->compare('height',$this->height);
		$criteria->compare('quote',$this->quote,true);
		$criteria->compare('user',$this->user,true);
		$criteria->compare('show',$this->show);
		$criteria->compare('hot_prop',$this->hot_prop);
		$criteria->compare('cena_shop',$this->cena_shop,true);
		$criteria->compare('gurl',$this->gurl,true);
		$criteria->compare('fullurl',$this->fullurl,true);
		$criteria->compare('last_view',$this->last_view,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return AlbumPictures the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
