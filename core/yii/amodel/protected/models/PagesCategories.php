<?php

/**
 * This is the model class for table "inform_pages_categories".
 *
 * The followings are the available columns in table 'inform_pages_categories':
 * @property integer $cid
 * @property string $title
 * @property string $description
 * @property string $parent
 * @property string $user
 * @property string $template
 * @property integer $weight
 * @property string $gurl
 * @property string $fullurl
 */
class PagesCategories extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'inform_pages_categories';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('title, description, template', 'required'),
			array('weight', 'numerical', 'integerOnly'=>true),
			array('title, gurl, fullurl', 'length', 'max'=>255),
			array('parent', 'length', 'max'=>10),
			array('user', 'length', 'max'=>11),
			array('template', 'length', 'max'=>20),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('cid, title, description, parent, user, template, weight, gurl, fullurl', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'cid' => 'Cid',
			'title' => 'Title',
			'description' => 'Description',
			'parent' => 'Parent',
			'user' => 'User',
			'template' => 'Template',
			'weight' => 'Weight',
			'gurl' => 'Gurl',
			'fullurl' => 'Fullurl',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('cid',$this->cid);
		$criteria->compare('title',$this->title,true);
		$criteria->compare('description',$this->description,true);
		$criteria->compare('parent',$this->parent,true);
		$criteria->compare('user',$this->user,true);
		$criteria->compare('template',$this->template,true);
		$criteria->compare('weight',$this->weight);
		$criteria->compare('gurl',$this->gurl,true);
		$criteria->compare('fullurl',$this->fullurl,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return PagesCategories the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
