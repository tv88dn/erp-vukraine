<?php

/**
 * This is the model class for table "inform_4nalbum_description_select".
 *
 * The followings are the available columns in table 'inform_4nalbum_description_select':
 * @property integer $id
 * @property string $text
 * @property integer $quote
 * @property integer $desc_cat_id
 * @property integer $gid
 * @property integer $user
 */
class AlbumDescriptionSelect extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'inform_4nalbum_description_select';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('text, desc_cat_id, gid, user', 'required'),
			array('quote, desc_cat_id, gid, user', 'numerical', 'integerOnly'=>true),
			array('text', 'length', 'max'=>100),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, text, quote, desc_cat_id, gid, user', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'text' => 'Text',
			'quote' => 'Quote',
			'desc_cat_id' => 'Desc Cat',
			'gid' => 'Gid',
			'user' => 'User',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('text',$this->text,true);
		$criteria->compare('quote',$this->quote);
		$criteria->compare('desc_cat_id',$this->desc_cat_id);
		$criteria->compare('gid',$this->gid);
		$criteria->compare('user',$this->user);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return AlbumDescriptionSelect the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
