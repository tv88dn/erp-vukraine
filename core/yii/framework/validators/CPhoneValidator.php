<?php
/**
 * Created by PhpStorm.
 * User: tv88d_000
 * Date: 30.08.14
 * Time: 2:01
 */

class CPhoneValidator extends CValidator {

    public $allowEmpty = true;

    protected function validateAttribute($object,$attribute)
    {
        $value=$object->$attribute;
        if($this->allowEmpty && ($value===null || $value===''))
            return true;
        else {
            $message=$this->message!==null?$this->message:Yii::t('yii','{attribute} is empty');
            $this->addError($object,$attribute,$message);
        }


        if(!preg_match('/^\+\d{2}\(\d{3}\)\d{3}-\d{2}-\d{2}$/', $value))
        {
            $message=Yii::t('yii','{attribute} seems wrong');
            $this->addError($object,$attribute,$message);
        }
    }
} 