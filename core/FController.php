<?php

/**
 * Class FController
 *
 * Класс контроллеров. Наследуется от стандартного класса yii CController.
 * Перегружает методы рендеринга
 *
 * @package system
 * @author Trunov Vadim trunov@lookmy.info
 * @copyright 2014 LookMy.info
 */
class FController extends CController
{
    /**
     * стандартное разширение html отображений
     */
    const TEMPLATE_EXTENSION_HTML = 'html';

    /**
     * стандартное разширение smarty отображений
     */
    const TEMPLATE_EXTENSION_SMARTY = 'tpl';

    /**
     * стандартное разширение php отображений
     */
    const TEMPLATE_EXTENSION_PHP = 'php';

    /**
     * @var object основная АР-модель контроллера
     */
    public $model;

    /**
     * @var стандартная переменная для хранения результатов работы action'а
     */
    public $data;

    /**
     *
     */
    public $actionName;

    protected  $currentActive;

    public function setCurrentActive($value)
    {
        $this->currentActive = $value;
    }

    /**
     * Ставит в соответсвие контроллеру модель.
     * Инициализирует data
     *
     * @param object $model основная АР-модель контроллера
     */
    public function initModel($model)
    {
        $this->model = $model;
        $this->data = array();
    }

	public function filterAccessControl($filterChain){
		$filter = new FAccessControlFilter;
		$filter->setRules($this->accessRules());
		return $filter->filter($filterChain);
	}

    private function getModuleName()
    {
        return strtolower(get_class($this->model));
    }

    public function getViewFile($view)
    {
        return 'modules/' . $this->getModuleName() . '/view/' . $view . '.' . self::TEMPLATE_EXTENSION_PHP;
    }

    /**
     * Перегрузка рендеринга yii.
     * Параметр $data['render_type'] определяет тип рендерина в зависимости от представления
     *
     * @param string $view
     * @param null $data
     * @param bool $return
     * @return mixed|string
     * @see http://www.yiiframework.com/doc/api/1.1/CController
     */
    public function render($view, $data = null, $return = true)
    {
    	if($this->beforeRender($view)){
	        $modName = $this->getModuleName();
	        $renderType = $data['render_type'];
	        unset($data['render_type']);
	        if (!isset($renderType)) {
	            $renderTemplate = parent::renderPartial($view, $data, $return);
	        } else if ($renderType == self::TEMPLATE_EXTENSION_PHP) {
	            $renderTemplate = $this->getPhpTemplate('modules/' . $modName . '/view/' . $view . '.' . self::TEMPLATE_EXTENSION_PHP, $data);
	        } else if ($renderType == self::TEMPLATE_EXTENSION_SMARTY) {
	            $renderTemplate = $this->getSmartyTemplate('modules/' . $modName . '/view/', $view . '.' . self::TEMPLATE_EXTENSION_SMARTY, $data);
	        }
	        $req = new FRequest();
	        if (!$req->getIsAjaxRequest()) {
	            $renderTemplate = FCore::includeJs(ucfirst($modName)) . $renderTemplate;
	        }
	        
	        if ($return){
	        	$this->afterRender($view,$renderTemplate);
	            return $renderTemplate;
	        }else
	            echo $renderTemplate;
            	$this->afterRender($view,$renderTemplate);
    	}else{
    		return null;
    	}
    }

    /**
     * Получение HTML представления
     *
     * @param string $view имя представления
     * @return string представление
     * @throws FError
     */
    protected function getHtmlTemplate($view)
    {
        $templateFile = file_get_contents($view);
        if ($templateFile) {
            return $templateFile;
        } else {
            throw new FError('Getting template error. File ' . $view . ' not found', '0901');
        }

    }

    /**
     * Получение PHP представления
     *
     * @param string $view имя представления
     * @return string представление
     * @throws FError
     */
    protected function getPhpTemplate($view, $data)
    {
        extract($data);
        ob_start();
        include $view;
        $templateFile = ob_get_contents();
        ob_clean();
        if ($templateFile) {
            return $templateFile;
        } else {
            //throw new ErrorException('ExceptionMessage');
            throw new FError('Getting template error. File ' . $view . ' not found', '0901');
        }
    }

    /**
     * Получение и обработка SMARTY-шаблона
     *
     * @param $viewPath путь к представлению
     * @param string $view имя представления
     * @param $data
     * @return string представление
     */
    protected function getSmartyTemplate($viewPath, $view, $data)
    {
        spl_autoload_unregister('smartyAutoload');
        Yii::registerAutoloader('smartyAutoload');

        $smarty = new Smarty();
        $smarty->template_dir = $viewPath;
        $smarty->compile_dir = FConfig::$data['smarty']['compileDir'];
        $smarty->debugging = false;
        $smarty->assign('this', $this);

        foreach ($data as $key => $value) {
            $smarty->assign($key, $value);
        }

        ob_start();
        $smarty->display($view);
        $templateFile = ob_get_contents();
        ob_clean();

        return $templateFile;
    }

    public function smartyWidget($className, $properties = array(), $captureOutput = false)
    {
        $this->widget($className, $properties, $captureOutput);
    }

    /**
     * Creates a widget and initializes it.
     * This method first creates the specified widget instance.
     * It then configures the widget's properties with the given initial values.
     * At the end it calls {@link CWidget::init} to initialize the widget.
     * Starting from version 1.1, if a {@link CWidgetFactory widget factory} is enabled,
     * this method will use the factory to create the widget, instead.
     * @param string $className class name (can be in path alias format)
     * @param array $properties initial property values
     * @return CWidget the fully initialized widget instance.
     */
    public function createWidget($className, $properties = array())
    {
        $widget = FCore::app()->getWidgetFactory()->createWidget($this, $className, $properties);
        $widget->init();
        return $widget;
    }

    protected function findAction($controller, $action)
    {
        if(!$action)
        {
            if($this->getAction())
            {
                $action = ucfirst($controller->getAction()->id);
            }
            else
            {
                $action = ucfirst($controller->defaultAction);
            }
        }
        else
        {
            $action = ucfirst($action);
        }
        return $action;
    }

    public function getFilter($action = null, $module=null)
    {
        if(!$module)
        {
            $module = str_replace('Controller', '', get_class($this));
        }
        $controller = FCore::getInstance()->getControllerByName(ucfirst($module).'Controller');
        $action = $this->findAction($controller, $action);

        $viewName = 'filter' . $action;
        $action = 'action'.$action;

        if (file_exists(FCore::getPathOfAlias('webroot') . DIRECTORY_SEPARATOR . 'modules' . DIRECTORY_SEPARATOR .
            strtolower($module) . DIRECTORY_SEPARATOR . 'view' . DIRECTORY_SEPARATOR . $viewName . '.tpl'))
        {
            return $controller->render($viewName, array(
                'render_type' => self::TEMPLATE_EXTENSION_SMARTY,
                'filter' => FCore::getInstance()->getControllerByName('FilterController')->model->attachFilter($module, $action),
            ));
        }
        else
        {
            return FCore::getInstance()->getControllerByName('FilterController')->actionAttachFilter($module, $action);
        }
    }

    public function  getSort($action = null, $module=null)
    {
        if(!$module)
        {
            $module = str_replace('Controller', '', get_class($this));
        }
        $controller = FCore::getInstance()->getControllerByName(ucfirst($module).'Controller');
        $action = $this->findAction($controller, $action);

        $viewName = 'sort' . $action;
        $action = 'action'.$action;

        if (file_exists(FCore::getPathOfAlias('webroot') . DIRECTORY_SEPARATOR . 'modules' . DIRECTORY_SEPARATOR .
            strtolower($module) . DIRECTORY_SEPARATOR . 'view' . DIRECTORY_SEPARATOR . $viewName . '.tpl'))
        {
            return $controller->render($viewName, array(
                'render_type' => self::TEMPLATE_EXTENSION_SMARTY,
                'sort' => FCore::getInstance()->getControllerByName('FilterController')->model->attachSort($module, $action),
            ));
        }
        else
        {
            return FCore::getInstance()->getControllerByName('FilterController')->actionAttachSort($module, $action);
        }
    }



    public function run($actionId)
    {
        $action = $this->createAction($actionId);
        if($this->currentActive == $actionId)
        {
            $this->setAction($action);
            Yii::app()->setController($this);
        }
    	$chain = CFilterChain::create($this,$action,$this->filters());
    	return $this->filterAccessControl($chain);
    }

    /**
     * Класс определяет по имени контроллера и дествия, список имен параметров, которые действие принимает.
     * В процессе список этих параметров извлекается из запроса и автоматически передается действию
     *
     * @param $controllerAction
     * @throws Exception
     * @return array
     */
    public function getActionParameters($controllerAction)
    {
        $reflector = new ReflectionClass(get_class($this));
        $controllerAction = 'action'.$controllerAction;
        if ($reflector->hasMethod($controllerAction)) {
            $parameters = $reflector->getMethod($controllerAction)->getParameters();
            $actionParameters = array();
            for ($i = 0; $i < count($parameters); $i++) {
                $request = new FRequest();
                $actionParameters[] = $request->getParam($parameters[$i]->name);
            }
            return $actionParameters;
        } else {
            throw new Exception('NotFound');
        }
    }

    public function createAction($actionID)
    {
        if($actionID==='')
            $actionID=$this->defaultAction;
        if(method_exists($this,'action'.$actionID) && strcasecmp($actionID,'s')) // we have actions method
            return new FAction($this,$actionID);
        else
        {
            $action=$this->createActionFromMap($this->actions(),$actionID,$actionID);
            if($action!==null && !method_exists($action,'run'))
                throw new CException(Yii::t('yii', 'Action class {class} must implement the "run" method.', array('{class}'=>get_class($action))));
            return $action;
        }
    }

}
