<?php

/**
 * Class FFormValidatorsBehavior
 *
 * Модель поведения для валидации данных, поступающих от форм
 *
 * @package behaviors
 * @author Trunov Vadim trunov@lookmy.info
 * @copyright 2014 LookMy.info
 */
class FFormValidatorsBehavior extends CActiveRecordBehavior
{
    /**
     * Валидатор поля для ввода e-mail
     *
     * @param $data string строка данных
     * @return mixed
     */
    public function emailValidator($data)
    {
        return filter_var($data, FILTER_VALIDATE_EMAIL);
    }

    /**
     * Валидатор обязательного заполнения
     *
     * @param $data string строка данных
     * @return bool
     */
    public function requireValidator($data)
    {
        return ($data) ? $data : false;
    }

    public function intValidator($data)
    {
        return (int) $data;
    }

    public function floatValidator($data)
    {
        $data = str_replace(',', '.', $data);
        return (float) $data;
    }

    /**
     * Валидатор корректного ввода мобильного телефона
     *
     * @param $data string строка данных
     * @return bool
     */
    public function phoneValidator($data)
    {
        return preg_filter('/\+38\([\d]{3}\)[\d]{3}\-[\d]{2}-[\d]{2}/', $data, $data);
    }

    /**
     * Валидатор капчи
     *
     * @param $data string строка данных
     * @return bool
     */
    public function captchaValidator($data)
    {
        $validator = new CCaptchaAction(FCore::getInstance()->getControllerByName('FeedbackController'), 'feedback');
        return ( $validator->validate($data, false)) ? $data : false;
    }

    /**
     * Предварительный парсинг строки валидаторов.
     *
     * @param $validatorsString string строка с именами валидаторов, резделенная запятыми
     * @return array массив валидаторов
     */
    public function explodeValidators($validatorsString)
    {
        $validators = explode(',', $validatorsString);
        for($i = 0; $i < count($validators); $i++)
        {
            $validators[$i] = trim($validators[$i]).'Validator';
        }
        return $validators;
    }

    /**
     * Выполнение валидации набора полей формы.
     * При ошибке валидации вызывается riseValidateError в контроллере объекта
     *
     * @param $dataArray array массив полей формы
     * @param $rules array массив правил валидации
     * @param $dataFieldName string название поля, из которого берутся данные, для валидации
     * из элементов массива $dataArray
     * @return bool
     */
    public function makeValidation($dataArray, $rules, $dataFieldName)
    {
        $validateSuccess = true;
        for($i = 0; $i < count($dataArray); $i++)
        {
            if(isset($rules[$dataArray[$i]->id]))
            {
                $validators = $this->explodeValidators($rules[$dataArray[$i]->id]);
                for($j = 0; $j < count($validators); $j++)
                {

                    $dataArray[$i][$dataFieldName] = $this->{$validators[$j]}($dataArray[$i][$dataFieldName]);
                    if(!$dataArray[$i][$dataFieldName])
                    {
                        $validateSuccess = false;
                        $dataArray[$i]->riseValidateError();
                        break;
                    }
                }
            }
        }
        return $validateSuccess;
    }

    public static function getValidators()
    {
        $methods = get_class_methods('FFormValidatorsBehavior');
        $res = array(
            '' => '',
        );
        for($i =0; $i < count($methods); $i++)
        {
            if(preg_match('/.*Validator$/', $methods[$i]))
            {
                $res[$methods[$i]] = $methods[$i];
            }
        }
        return $res;
    }
} 