<?php
/**
 * Created by PhpStorm.
 * User: tv88d_000
 * Date: 01.11.2014
 * Time: 22:09
 */

class ReportsBehavior extends CBehavior
{
    public function __construct($sender=null,$params=null)
    {
        FCore::includeDirectory(FConfig::$pathToReports);
    }


    public function getShortProgrammersReport($month)
    {
        $prevMonth = strtotime('first day of '.($month-1).' month midnight');
        $currentMonth = strtotime('first day of '.$month.' month midnight');

        $report = new ProgrammersReports(Task::model());
        $report->getShortReport();
        return $this->owner->render('reports/shortProgrammerReport', array(
            'render_type'=>FController::TEMPLATE_EXTENSION_SMARTY,
            'report'=>$report->getShortReport(),
        ));
    }
} 