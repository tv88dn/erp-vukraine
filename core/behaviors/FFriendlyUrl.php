<?php
/**
 * Class FFriendlyUrl
 *
 * Модель поведения для получения страницы компонента по ЧПУ адресу
 *
 * @package behaviors
 * @author Trunov Vadim trunov@lookmy.info
 * @copyright 2014 LookMy.info
 */
class FFriendlyUrl extends CActiveRecordBehavior
{
    /**
     * Поле из БД, отвечающе за адрес страницы.
     */
    const NAME_OF_ROUTER_FIELD = 'fullurl';

    /**
     * Проверяет, является ли текущий ЧПУ урл адресом одной из страниц модуля. Если находит возвращает ее
     *
     * @param string $url текущий урл
     * @param СModel $model модель компонентп
     * @return bool|CActiveRecord найденная страница
     */
    public function getFriendlyUrl($url, $model)
    {
        $criteria = new CDbCriteria();
        $criteria->select = '*';
        $criteria->condition = self::NAME_OF_ROUTER_FIELD . '=:' . self::NAME_OF_ROUTER_FIELD;
        $criteria->params = array(':' . self::NAME_OF_ROUTER_FIELD => $url);
        $res = $model->find($criteria);
        return $res;
    }
} 