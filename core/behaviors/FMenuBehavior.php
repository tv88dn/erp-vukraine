<?php
/**
 * Class FMenuBehavior
 *
 * Модель поведения для компонентов, использующих меню, предпологающие рекурсивный вывод дерева категорий
 *
 * @package behaviors
 * @author Trunov Vadim trunov@lookmy.info
 * @copyright 2014 LookMy.info
 */
class FMenuBehavior extends CActiveRecordBehavior
{
    /**
     * @var mixed дерево меню
     */
    private $menu;
    /**
     * @var int ключевое поле активного элемента
     */
    private $activeId;
    /**
     * @var int значение ключевого поля
     */
    private $activeKey;
    /**
     * @var имя поля, по которому определяется связь родитель-потомок
     */
    private $parentNames;
    /**
     * @var int id ключевого поля по умолчанию
     */
    private $defaultId;

    /**
     * @var array текущие "листы" в дереве меню, в которые могут записываться новые узлы.
     * Располагается по уровням дерева
     */
    private $leafs;

	private $pool;

    /**
     * Строит и инициализирует дерево категорий для меню. Дерево состоит из узлов.
     * Каждый узел - это массив из 3-х эдементов:
     * item - объект, значение узла;
     * child - массив, потомки узла;
     * active - признак того, что узел активен, т.е. выводится в области контента, а пункт меню должен подсветится.
     *
     *
     * @param CActiveRecord[] $categories массив категорий
     * @param array $active пара, для связки потомок-родитель (внутренние ключи)
     * @param int $id id активного элемента
     * @param int $root id корня дерева
     * @return mixed меню
     */
    public function setCategoriesTree($categories, $active, $id, $root)
    {
    	$this->pool = FPool::getInstance();
        $this->menu = array();
        $this->defaultId = $root;
        $this->addLeaf($root, $this->defaultId);
        $this->addCategoriesTree($categories, $active, $id);
        return $this->menu;
    }

    /**
     * Создает узел дерева.
     *
     * @param $item значение узла
     * @param string $active
     * @return array
     */
    private static function createTreeItem($item, $active = '')
    {
        return $item = array(
            'child' => array(),
            'item' => array($item),
            'active' => $active,
        );
    }

    /**
     * Добавляет массив пунктов меню к дереву меню.
     *
     * @param $categories массив пунктов меню
     * @param array $active пара, для связки потомок-родитель (внутренние ключи)
     * @param int $id id активного элемента
     * @param int $level начальный уровень рекурсии
     * @return mixed меню
     */
    public function addCategoriesTree($categories, $active, $id, $level = 0)
    {
        $active = explode(':=', $active);
        $this->activeId = $id;
        $this->activeKey = $active[0];
        $this->parentNames = $active[1];
        for ($i = 0; $i < count($categories); $i++) {
        	($categories[$i][$this->activeKey] == $this->activeId)?$categories[$i]['isActive']='active':false;
            $this->addItemToCategoryTree($this->menu, $categories[$i], $level);
        }

        return $this->menu;
    }


    private function addLeaf($level, $id)
    {
        if(!isset($this->leafs[$level]))
        {
            $this->leafs[$level] = array();
        }
        $this->leafs[$level][] = $id;
    }

    /**
     * Добавляет узел в дерево
     *
     * @param $tree дерево
     * @param $treeItem узел
     * @param $currentLevel текущий уровень рекурсии
     */
    private function addItemToCategoryTree(&$tree, $treeItem, $currentLevel)
    {
    	$this->pool->insertItem(array(
    		$this->activeKey,
    		$this->parentNames,
    		$treeItem));
    		if($this->pool->getItemLevel($treeItem[$this->activeKey])==0){ 
				$tree[]=$treeItem[$this->activeKey];
    		}
    }

    /**
     * Проверяет, является ли $key родилем $item
     *
     * @param int $key id родительского пункта
     * @param CActiveRecord $item пункт меню
     * @return bool
     */
    private function isParent($key, $item)
    {
        return ($key == $item[$this->parentNames]);
    }


}
