<?php

/**
 * Class FConfig
 *
 * Основной конфигурационній класс. Синглтон.
 * Служит оболочкой для класса конфигурации (config.php),
 * а так же содержит основные пути для подключаемых компонентов ядра
 *
 * @package system
 * @author Trunov Vadim trunov@lookmy.info
 * @copyright 2014 LookMy.info
 */

class FConfig implements FSingleton
{
    /**
     * Путь к ядру
     */
    const CORE_PATH = 'core/';
    /**
     * путь к AR моделям
     */
    const FRAMEWORK_ACTIVE_RECORD_MODELS = 'core/models/';
    /**
     * Путь к поведениям
     */
    const BEHAVIOR_PATH = 'core/behaviors/';

    /**
     * путь к виджетам
     */
    const WIDGET_PATH = 'core/widgets/';

    /**
     * путь к пользовательским типам данных
     */
    const ENTITIES_PATH = 'core/entities/';

    /**
     * путь к шаблонам
     */
    const LAYOUT_PATH = 'layout/';
    /**
     * путь к модулям
     */
    const MODULE_PATH = 'modules/';
    /**
     * путь к интерфейсам
     */
    const INTERFACE_PATH = 'core/interfaces/';
    /**
     * @var array
     * массив конфига
     */

    /**
     * Путь к папке с хелперами
     */
    const HELPERS_PATH = 'core/helpers/';

    public static $data;
    /**
     * @var int ID сайта
     */
    public static $siteId;
    /**
     * @var string имя БД
     */
    public static $dbName;
    /**
     * @var string имя пользователя БД
     */
    public static $dbUser;
    /**
     * @var string пароль от БД
     */
    public static $dbPass;
    /**
     * @var string корневой каталог сайта
     *
     * Этот параметр следует изменять, если корень сайта находится в отличном от корня урл месте.
     * Т.е если index.php лежит по адресу mysite.com/some_folder/ то в rootPath следует написать some_folder/
     */
    public static $rootPath;

    public static $secretKey;

    public static $yiiOptions;

    /**
     * @var string кодировка сайта
     */
    public static $charset;
    /**
     * @var string путь к папке с каотинками для фотоальбома
     */
    public static $gallImgPath;
    /**
     * Экземпляр класса
     */
    static private $instance = NULL;

    /**
     * Путь к основной папке JS
     */
    public static $mainJsPath;


    /**
     * @var Путь от корня сервера к корню сайта
     */
    public static $absoluteRootPath;

    /**
     * @var array список всех мадулей, которые подключаются через ContentSpecCode
     */
    public static $specCode;

    public static $pathToReports = 'modules/user/reports';

    /**
     * Cтавит в соответсвие переменным POST и GET запросов тип данных.
     * Типы данных, используемых методом, описаны в классе FRequest
     *
     * @return array массив используемых параметров урл и соответсвующих им типов данных
     * @todo функция не особо удобна. Следует ее переписать и инициализацию переменных вынести в config.php
     */
    public static function requestFieldData()
    {
        return array(
            'show' => FRequest::TYPE_MIXED,
            'pid' => FRequest::TYPE_INT,
            'page' => FRequest::TYPE_INT,
            'gid' => FRequest::TYPE_INT,

        );
    }

    /**
     * инициализация синглтона
     *
     * @return FConfig
     */
    static public function getInstance()
    {
        if (self::$instance == NULL) {
            self::$instance = new FConfig();
            self::initConfig();

        }
        return self::$instance;
    }

    /**
     * Приватный конструктор
     */
    private function __construct()
    {
    }

    /**
     * Загрузка и перенос данных из файла с настройками сайта в экземпляр класса
     */
    public static function initConfig()
    {
        include_once(dirname(__FILE__).'/config.php');
        self::$siteId = $config['siteId'];
        self::$dbName = $config['dbName'];
        self::$dbUser = $config['dbUser'];
        self::$dbPass = $config['dbPass'];
        self::$rootPath = $config['rootPath'];
        self::$charset = $config['charset'];
        self::$secretKey = $config['secretKey'];
        self::$gallImgPath = $config['gallImgPath'];
        self::$yiiOptions = $config['yiiOptions'];
        self::$mainJsPath = $config['mainJsPath'];
        self::$data = $config;
        self::$absoluteRootPath = dirname(__FILE__).'/../';
        self::$specCode = $config['specCodes'];

    }

    /**
     * Приватный клон
     */
    private function __clone()
    {
    }


}