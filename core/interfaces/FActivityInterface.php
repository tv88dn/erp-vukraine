<?php

/**
 * Interface FActivityInterface
 */

interface FActivityInterface {

    /**
     * @return mixed
     */
    public function activityRule();

} 