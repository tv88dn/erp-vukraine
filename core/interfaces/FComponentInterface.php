<?php

/**
 * Interface FComponent
 *
 * Интерфейс для контроллера, который может выступать, как главный модуль страницы, т.е. выводить данные в область контента
 *
 * @package system
 * @author Trunov Vadim trunov@lookmy.info
 * @copyright 2014 LookMy.info
 */
interface FComponentInterface
{
    /**
     * Возвращает имя активного шаблона, учитывая что данный компонент активный
     *
     * @param object $activePage экземпляр AR текущей активной страницы
     * @return string имя активного layout
     */
    public function getTemplate($activePage);

    /**
     * Проверяет существует ли для текущего урл страница с такми же ЧПУ урл. Если да, то возвращается эта страница.
     *
     * @param string $url текущий урл
     * @return obkect|bool объект AR или false
     */
    public function checkFriendlyUrl($url);

    /**
     * Формирует метаданные компонента, такие как title, description, keywords, h1, robots
     *
     * @return array массив метаданных
     */
    public function getMetaData();
}
