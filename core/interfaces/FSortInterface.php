<?php

/**
 * Interface FSortInterface
 *
 * Реализация сортровки для класса FSort
 */
interface FSortInterface
{
    /**
     * Применение критерия сортировки, исходя из внутренних реляций и модели
     *
     * @param $criteria СDbCriteria критерий сортировки
     * @param $field string имя описателя, по которому сортируем
     * @param $sortType
     * @return mixed
     */
    public function applySortCriteria($criteria, $field, $sortType);
}