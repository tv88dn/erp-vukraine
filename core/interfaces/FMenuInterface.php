<?php

/**
 * Interface FMenuInterface
 */
interface FMenuInterface
{
    /**
     * Строит меню для категории
     *
     * @param int $parentId id родителя
     * @param int $activeId id активного пункта меню
     * @return array список пунктов меню
     */
    public function getMenu($parentId, $activeId);

    /**
     * Устанавливает текущий активный пункт меню
     *
     * @param array $activeRecordsList список пунктов меню
     * @param int $activeId id активного пункта
     * @return array список пунктов меню
     */
    public function setActiveMenuItem($activeRecordsList, $activeId);
}