<?php
/**
 * Class FRequest
 * Предоставляет механизмы работы с HTTP-запросами
 * Наследует стандартный класс yii CHttpRequest
 *
 * @package system
 * @author Trunov Vadim trunov@lookmy.info
 * @copyright 2014 LookMy.info
 */
class FRequest extends CHttpRequest
{
    /**
     * код типа данных int
     */
    const TYPE_INT = 0x01;
    /**
     * код типа данных string
     */
    const TYPE_STRING = 0x02;
    /**
     * код типа данных mixed
     */
    const TYPE_MIXED = 0x02;
    /**
     * код типа данных float
     */
    const TYPE_FLOAT = 0x04;

    /**
     * Получение параметра из HTTP запроса по имени
     *
     * @param string $name имя параметра
     * @param null $defaultValue занчение по умолчанию
     * @return mixed|null
     */
    public function getParam($name, $defaultValue = null)
    {
        return parent::getParam($name, $defaultValue);
    }

    /**
     * Приведение параметров запросов к типам
     *
     * @param $data значение параметра
     * @param $name имя параметра
     * @return int|string
     */
    protected function parseData($data, $name)
    {
        $type = self::getFieldType($name);

        switch ($type) {
            case self::TYPE_INT:
                return (int)$data;
                break;
            case self::TYPE_STRING:
                return (urldecode($data));
        }
    }

    /**
     * Возвращает код значения в зависимости от имени переменной
     *
     * @param $name имя переменной в запросе
     * @return int код типа
     */
    public static function getFieldType($name)
    {
        $reqFieldData = FConfig::requestFieldData();
        return (isset($reqFieldData[$name])) ? $reqFieldData[$name] : FRequest::TYPE_MIXED;
    }

    /**
     * Получение "чистой" строки HTTP-запроса, без домена и субдоменов.
     *
     * @return string строка HTTP запроса
     */
    public function getRequestUriWithoutContext()
    {
        $urlRequest = substr(parse_url($this->getRequestUri(), PHP_URL_PATH), 1);
        return str_replace(FConfig::$rootPath, '', $urlRequest);
    }

}