<?php
/**
 * Class FWebApplication
 *
 * Основной класс web-приложения
 * Используется для полной интеграции с Yii,
 * а так же для перегрузки методов ядра Yii,
 * логика поведения которых
 * должна отличаться от классической реализации
 */
class FWebApplication extends CWebApplication
{
    /**
     * Создает ссылку
     *
     * @param string $route маршрут для ссылки
     * @param array $params параметры ссылки
     * @param string $ampersand соединитель для параметров запроса
     * @return string сформированная ссылка
     */
    public function createUrl($route, $params = array(), $ampersand = '&')
    {
        $paramStr = '?';
        $i = 0;
        foreach ($params as $k => $v) {
            if(!is_array($v))
            {
                $paramStr .= $k . '=' . $v;
            }
            else
            {
                $paramStr .= http_build_query(array($k=>$v));
            }
            $paramStr .= ($i == count($params) - 1) ? '' : $ampersand;
            $i++;
        }
        return FConfig::$rootPath . $route . $paramStr;
    }
} 