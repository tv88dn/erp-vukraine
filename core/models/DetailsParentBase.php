<?php

/**
 * This is the model class for table "detailsParent".
 *
 * The followings are the available columns in table 'detailsParent':
 * @property string $id
 * @property string $module
 * @property string $detailId
 * @property string $parentId
 * @property string $default
 * @property integer $display
 * @property integer $multiple
 */
class DetailsParentBase extends FActiveRecord
{
    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'detailsParent';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('module, detailId, parentId, display', 'required'),
            array('display, multiple', 'numerical', 'integerOnly'=>true),
            array('module', 'length', 'max'=>155),
            array('detailId, parentId', 'length', 'max'=>10),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('id, module, detailId, parentId, default, display, multiple', 'safe', 'on'=>'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'module' => 'Module',
            'detailId' => 'Detail',
            'parentId' => 'Parent',
            'default' => 'Default',
            'display' => 'Display',
            'multiple' => 'Multiple',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search()
    {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria=new CDbCriteria;

        $criteria->compare('id',$this->id,true);
        $criteria->compare('module',$this->module,true);
        $criteria->compare('detailId',$this->detailId,true);
        $criteria->compare('parentId',$this->parentId,true);
        $criteria->compare('default',$this->default,true);
        $criteria->compare('display',$this->display);
        $criteria->compare('multiple',$this->multiple);

        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
        ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return DetailsParent the static model class
     */
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }
}