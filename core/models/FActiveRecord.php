<?php

class FActiveRecord extends CActiveRecord {

    const ERROR_FIELD_NOT_FOUND = '**___ERROR_FIELD_NOT_FOUND___**';

    public $isEdit = false;

    public $details;

    public $sort;

    public $detailsAttributes;
    public $detailsCount;

    public $dbAction;


    public function afterConstruct()
    {
        $this->getExternalAttributes();
        return parent::afterConstruct();
    }

    public function getDictionaryRows($name)
    {
        if (isset($this->detailsAttributes[$name])) {
            return $this->detailsAttributes[$name]->getDictionary();
        }
        else
        {
            array();
        }
    }

    public function getHtmlType($name)
    {
        return $this->detailsAttributes[$name]->type->type;
    }

    public function getFromDictionary($name)
    {
        if(isset($this->detailsAttributes[$name]))
        {
            $parent = $this->detailsAttributes[$name];
        }
        else
        {
            $type = DetailsTypes::model()->with('parents')->find(array('condition'=>'t.fieldName="'.$name.'" AND parents.module="'.get_class($this).'"'));
            if(isset($this->detailsAttributes[$type->name]))
            {
                $parent = $this->detailsAttributes[$type->name];
            }
            else
            {
                throw new CException(Yii::t('yii','Property "{class}.{property}" is not defined.',
                    array('{class}'=>get_class($this), '{property}'=>$name)));
            }
        }
        $item = DetailsDictionary::model()->findByAttributes(array(
            'parentId'=>$parent->id,
            'value'=>$this->$name,
        ));
        if($item)
        {
            return $item->data;
        }
        else
        {
            return '';
        }
    }

    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }


    public function validateDetail($name)
    {
        if(isset($this->detailsAttributes[$name]))
        {
            for($i = 0; $i < count($this->detailsAttributes[$name]->type->validator); $i++)
            {
                if($this->detailsAttributes[$name]->type->validator[$i])
                {
                    $validator = new CValidator::$builtInValidators[$this->detailsAttributes[$name]->type->validator[$i]];
                    $validator->attributes = array($name);
                    //$validator->validate($this, array($name));
                }
            }
        }
    }

    public function afterValidate()
    {
        if($this->details)
        {
            foreach($this->details as $name => $row)
            {
                $this->validateDetail($name);
            }
        }
    }

    /**
     * @param $modelFields
     * @param string $key
     * @return $this
     */
    public function loadModel($modelFields, $key = 'id')
    {
        $baseClassName = get_class($this);
        $model = call_user_func(array($baseClassName, 'model'));
        if($modelFields)
        {
            $modelFields[$key] = (isset($modelFields[$key])) ? $modelFields[$key] : null;
            if($modelFields[$key])
            {
                $model = $model->findByPk($modelFields['id']);
                $model->setAttributes($modelFields);
            }
            else
            {
                $model = new $baseClassName;
                $model->setAttributes($modelFields);
            }

            if($model->save())
            {
                $model->isEdit = true;
            }

        }
        $model->getExternalAttributes();
        return $model;
    }

    public function setAttribute($name,$value)
    {
        if(isset($this->detailsAttributes[$name]))
        {
            $this->details[$name] = $value;
        }
        else
        {
            return parent::setAttribute($name, $value);
        }
        return true;
    }

    public function __set($name, $val)
    {
        if(isset($this->detailsAttributes[$name]))
        {
            $this->details[$name] = $val;
        }
        else
        {
            $type = DetailsTypes::model()->findByAttributes(array('fieldName'=>$name));
            if($type && get_class($this) != 'DetailsTypes')
            {

                if(isset($this->detailsAttributes[$type->name]))
                {
                    $this->details[$type->name] = $val;
                    return true;
                }
            }
            return parent::__set($name, $val);
        }
        return true;
    }

    public function __get($name)
    {
        $result = parent::__get($name);

        if($result === self::ERROR_FIELD_NOT_FOUND)
        {
            /*
            if($name == 'phone' && get_class($this) == 'Company')
            {
                var_dump($result);
                die();
            }
            */
            if(isset($this->detailsAttributes[$name]))
            {
                if($this->details[$name] !== null)
                {
                    return $this->details[$name];
                }
                else
                {
                    /*
                    if(get_class(FCore::getInstance()->getActiveController()) != 'FilterController')
                    {

                    }
                    */
                    if(FCore::getInstance()->getActiveController()->action)
                    {
                        $action = FCore::getInstance()->getActiveController()->action->id;
                    }
                    else
                    {
                        $action = null;
                    }
                    return Filter::getFilterFieldValueFromRequest(
                        get_class($this),
                        'action'.ucfirst($action),
                        $name
                    );



                }

            }
            else
            {
                $type = DetailsTypes::model()->with('parents')->find(array('condition'=>'t.fieldName="'.$name.'" AND parents.module="'.get_class($this).'"'));

                if($type && get_class($this) != 'DetailsTypes')
                {
                    if(isset($this->detailsAttributes[$type->name]))
                    {
                        return $this->details[$type->name];
                    }
                }
                /*
                throw new CException(Yii::t('yii','Property "{class}.{property}" is not defined.',
                    array('{class}'=>get_class($this), '{property}'=>$name)));
                */
                return null;
            }
        }
        else
        {
            if($result)
            {
                return $result;
            }
            else
            {
                $action = ($controller = FCore::getInstance()->getActiveController()) ? $controller->action : null;

                if($action)
                {

                    return Filter::getFilterFieldValueFromRequest(
                        get_class($this),
                        'action'.ucfirst($action->id),
                        $name
                    );

                }
                else
                {
                    return $result;
                }
            }
        }
    }

    public function setAttributes($values,$safeOnly=true)
    {
        if(!is_array($values))
            return;
        foreach($values as $name=>$value)
        {
            if(isset($this->detailsAttributes[$name]))
            {
                $this->details[$name] = $value;
            }
        }
        return parent::setAttributes($values,$safeOnly);
    }

    public function addDetailsRealations()
    {
        if($this instanceof DetailsParent || $this instanceof Details || $this instanceof DetailsTypes)
        {
            return array();
        }
        $this->getMetaData()->addRelation('detailsItems'.get_class($this), array(
            self::HAS_MANY,
            'Details',
            array('moduleId'=>'id'),
            'together'=>true,
            'joinType'=>'LEFT JOIN',
        ));
        $this->getMetaData()->addRelation('detailsParents'.get_class($this), array(
            self::HAS_MANY,
            'DetailsParent',
            array('parentId'=>'id'),
            'condition'=>'detailsParents'.get_class($this).'.module="'.get_class($this).'"',
            'through'=>'detailsItems'.get_class($this),
            'together'=>true,
            'joinType'=>'LEFT JOIN',
        ));
        $this->getMetaData()->addRelation('detailsTypes'.get_class($this), array(
            self::HAS_MANY,
            'DetailsTypes',
            array('detailId'=>'id'),
            'through'=>'detailsParents'.get_class($this),
            'together'=>true,
            'joinType'=>'LEFT JOIN'
        ));
        $with = array();
        $with[] = 'detailsItems'.get_class($this);
        $with[] = 'detailsParents'.get_class($this);
        $with[] = 'detailsTypes'.get_class($this);
        return $with;
    }

    public function getExternalAttributes($filter = array())
    {
        if($this->detailsAttributes)
        {
            return $this->detailsAttributes;
        }
        if($this instanceof DetailsParent || $this instanceof Details || $this instanceof DetailsTypes)
        {
            return array();
        }
        else if(isset($this->getMetaData()->relations['detailsItems'.get_class($this)]))
        {
            $details = $this->getRelated('detailsItems'.get_class($this));
            $parents = $this->getRelated('detailsParents'.get_class($this));
            $types = $this->getRelated('detailsTypes'.get_class($this));
            for($i = 0; $i < count($parents); $i++)
            {
                if(isset($types[$i]) && isset($parents[$i]))
                {
                    $this->detailsAttributes[$types[$i]->fieldName] = $parents[$i];
                    $this->details[$types[$i]->fieldName] = null;
                    for($j = 0; $j < count($details); $j++)
                    {
                        if($details[$j]->parentId == $parents[$i]->id)
                        {
                            if($parents[$i]->multiple)
                            {
                                $this->details[$types[$i]->fieldName][] = $details[$j]->data;
                            }
                            else
                            {
                                $this->details[$types[$i]->fieldName] = $details[$j]->data;
                            }
                        }
                    }
                }

            }
        }
        if(!$this->details)
        {
            $this->detailsAttributes = array();
            $module = mb_strtolower(get_class($this));
            $attributes = DetailsParent::model()->with('type')->findAllByAttributes(array('module'=>$module));
            for($i = 0; $i < count($attributes); $i++)
            {

                $this->detailsAttributes[$attributes[$i]->type['fieldName']] = $attributes[$i];
                $this->details[$attributes[$i]->type['fieldName']] = Details::getDetailData($attributes[$i], $this->getAttribute('id'));
            }
        }

        return self::hideFields($this->detailsAttributes, $filter);
    }

    public function getAttribute($attribute)
    {
        return $this->__get($attribute);
    }

    public function baseDetailsRepations()
    {
        return array(
            'details'=>array(self::HAS_MANY, 'Details', array('moduleId'=>'id')),
            'parents'=>array(self::HAS_MANY, 'DetailsParent', array('parentId'=>'id'), 'through'=>'details'),
            'types'=>array(self::HAS_MANY, 'DetailsTypes', array('detailId'=>'id'), 'through'=>'parents'),
        );
    }

    /**
     * @param array $requestParams
     * @param bool $withoutDetails
     * @return CActiveDataProvider
     */
    public function getAll($requestParams = array(), $withoutDetails = false)
    {
        $detailsCount = 1;
        if(isset($requestParams['criteria']))
        {
            if(is_array($requestParams['criteria']))
            {
                $requestParams['criteria'] = new CDbCriteria($requestParams['criteria']);
            }
            $requestParams['criteria']->mergeWith(Filter::processFilter($this));
            $requestParams['criteria']->mergeWith(Filter::processSort($this));
        }
        else
        {
            $requestParams['criteria'] = Filter::processFilter($this);
        }
        if(!$requestParams['criteria']->with)
        {
            $requestParams['criteria']->with = array();
        }
        if(!$withoutDetails)
        {
            if(($detailsCount = DetailsParent::model()->getDetailsCount(get_class($this))) > 0)
            {
                if(is_array($requestParams['criteria']->with))
                {
                    $requestParams['criteria']->with = array_merge($requestParams['criteria']->with, $this->addDetailsRealations());
                }
                else
                {
                    $requestParams['criteria']->with = $this->addDetailsRealations();
                }
            }
            else
            {
                $detailsCount = 1;
            }


        }

        $detailsCount = ($detailsCount == 0) ? 1 : $detailsCount;
        $requestParams = array_merge($requestParams, array(
            'pagination' => array(
                'pageSize' => 20 * $detailsCount,
            ),
        ));
        return new CActiveDataProvider(get_class($this), $requestParams);
    }

    protected function addModelFlash($scenario)
    {
        if($this instanceof FFlashInterface)
        {
            $labels = $this->getFlashLabels();
            if(isset($labels[$scenario]))
            {
                FCore::app()->user->setFlash($labels[$scenario]['type'], $labels[$scenario]['text']);
            }
        }
    }

    public function getAttributeLabel($attribute)
    {
        if(isset($this->detailsAttributes[$attribute]))
        {
            return $this->detailsAttributes[$attribute]->type->name;
        }
        else
        {
            return parent::getAttributeLabel($attribute);
        }
    }

    public static function hideFields($fields, $hidden)
    {
        $newFields = array();
        foreach($fields as $name => $value)
        {
            $hide = false;
            for($i = 0; $i < count($hidden); $i++)
            {
                if($hidden[$i] == $name )
                {
                    $hide = true;
                    break;
                }
            }
            if(!$hide)
            {
                $newFields[$name] = $value;
            }
        }

        return $newFields;
    }

    public function getAllAttributes($hidden = array(), $reload = false)
    {
        if(!$this->detailsAttributes || $reload)
        {
            $this->getExternalAttributes();
        }
        $externalAttributes = array();

        foreach($this->detailsAttributes as $label => $item)
        {
            $externalAttributes[$item->type->fieldName] = $this->details[$label];
        }
        return self::hideFields(array_merge($this->getAttributes(), $externalAttributes), $hidden);
    }

    public function getAllAttributeLabels($hidden = array())
    {
        if(!$this->detailsAttributes)
        {
            $this->getExternalAttributes();
        }
        $externalLabels = array();
        foreach($this->detailsAttributes as $label => $item)
        {
            $externalLabels[$item->type->fieldName] = $label;
        }
        return self::hideFields(array_merge($this->attributeLabels(), $externalLabels), $hidden);
    }

    public function afterFind()
    {
        $this->getExternalAttributes();
        return parent::afterFind();
    }

    public function beforeDelete()
    {
        $this->dbAction = 'delete';
        FCore::getInstance()->getControllerByName('ActivityController')->addActivity($this);
        return parent::beforeDelete();
    }



    public function beforeSave()
    {
        $this->dbAction = $this->getScenario();
        return parent::beforeSave();
    }

    public function afterSave()
    {
        $this->addModelFlash($this->getScenario());
        $this->addDetails();
        FCore::getInstance()->getControllerByName('ActivityController')->addActivity($this);
        return parent::afterSave();
    }

    protected function fillDetailFields($detail, $name, $value, $id = null)
    {
        $detail->parentId = $this->detailsAttributes[$name]->id;
        $detail->moduleId = $this->id;
        if(is_array($value) && $id !== null)
        {
            if(!empty($value))
                $detail->data = $value[$id];
        }
        else
        {
            $detail->data = $value;
        }

        if(!$detail->save())
        {
            throw new Exception('Внутрення ошибка при добавлении описателя');
        }

    }

    public function addDetails()
    {
        if($this->details)
        {
            if(is_array($this->details))
            {
                foreach($this->details as $name=>$value)
                {
                    $detail = Details::getDetail($this->detailsAttributes[$name], $this->id, $value);
                    if(is_array($detail))
                    {

                        for($i = 0; $i < count($detail); $i++)
                        {
                            $this->fillDetailFields($detail[$i], $name, $value, $i);
                        }

                    }
                    else
                    {
                        $this->fillDetailFields($detail, $name, $value);
                    }
                }
            }
        }

    }

    public function afterDelete()
    {
        $this->addModelFlash('delete');
        return parent::afterDelete();
    }

} 