<?php

/**
 * This is the model class for table "timeSections".
 *
 * The followings are the available columns in table 'timeSections':
 * @property string $id
 * @property string $userId
 * @property string $taskId
 * @property integer $timeStart
 * @property integer $timeEnd
 * @property string $time
 * @property string $comment
 */
class BaseTimeSections extends FActiveRecord
{
    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'timeSections';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('userId, taskId', 'required'),
            array('timeStart, timeEnd', 'numerical', 'integerOnly'=>true),
            array('userId, taskId', 'length', 'max'=>8),
            array('time', 'length', 'max'=>5),
            array('comment', 'safe'),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('id, userId, taskId, timeStart, timeEnd, time, comment', 'safe', 'on'=>'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'userId' => 'User',
            'taskId' => 'Task',
            'timeStart' => 'Time Start',
            'timeEnd' => 'Time End',
            'time' => 'Time',
            'comment' => 'Comment',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search()
    {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria=new CDbCriteria;

        $criteria->compare('id',$this->id,true);
        $criteria->compare('userId',$this->userId,true);
        $criteria->compare('taskId',$this->taskId,true);
        $criteria->compare('timeStart',$this->timeStart);
        $criteria->compare('timeEnd',$this->timeEnd);
        $criteria->compare('time',$this->time,true);
        $criteria->compare('comment',$this->comment,true);

        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
        ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return BaseTimeSections the static model class
     */
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }
}