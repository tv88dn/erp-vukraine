<?php

/**
 * This is the model class for table "filterFields".
 *
 * The followings are the available columns in table 'filterFields':
 * @property string $id
 * @property string $filterId
 * @property string $model
 * @property string $attribute
 * @property string $type
 */
class BaseFilterFields extends FActiveRecord
{
    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'filterFields';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('filterId, model, attribute, type', 'required'),
            array('filterId', 'length', 'max'=>10),
            array('model', 'length', 'max'=>155),
            array('attribute, type', 'length', 'max'=>255),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('id, filterId, model, attribute, type', 'safe', 'on'=>'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'filterId' => 'Filter',
            'model' => 'Model',
            'attribute' => 'Attribute',
            'type' => 'Type',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search()
    {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria=new CDbCriteria;

        $criteria->compare('id',$this->id,true);
        $criteria->compare('filterId',$this->filterId,true);
        $criteria->compare('model',$this->model,true);
        $criteria->compare('attribute',$this->attribute,true);
        $criteria->compare('type',$this->type,true);

        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
        ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return FilterFieldsBase the static model class
     */
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }
}