<?php

/**
 * This is the model class for table "task".
 *
 * The followings are the available columns in table 'task':
 * @property string $id
 * @property string $title
 * @property string $companyId
 * @property string $projectId
 * @property string $authorId
 * @property string $responsibleId
 * @property string $parentId
 */
class TaskBase extends FActiveRecord
{
    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'task';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('title, authorId, responsibleId', 'required'),
            array('title', 'length', 'max'=>255),
            array('companyId, projectId, authorId, responsibleId, parentId', 'length', 'max'=>10),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('id, title, companyId, projectId, authorId, responsibleId, parentId', 'safe', 'on'=>'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'title' => 'Title',
            'companyId' => 'Company',
            'projectId' => 'Project',
            'authorId' => 'Author',
            'responsibleId' => 'Responsible',
            'parentId' => 'Parent',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search()
    {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria=new CDbCriteria;

        $criteria->compare('id',$this->id,true);
        $criteria->compare('title',$this->title,true);
        $criteria->compare('companyId',$this->companyId,true);
        $criteria->compare('projectId',$this->projectId,true);
        $criteria->compare('authorId',$this->authorId,true);
        $criteria->compare('responsibleId',$this->responsibleId,true);
        $criteria->compare('parentId',$this->parentId,true);

        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
        ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return TaskBase the static model class
     */
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }
}