<?php

/**
 * class FTree
 *
 * Абстрактный класс структуры дерева
 */
abstract class FTree
{
    /**
     * @var array массив элементов
     */
    protected $tree;
    /**
     * @var integer глубина наследования элементов в массиве
     */
    protected $level;

    /**
     * Добавление элемента в общий массив
     *
     * @param array $item массив содержащий элемент
     */
    public function insertItem($item)
    {
        $id = $item['gallid'];
        $parent = $item['parent'];
        if ($id != NULL) {
            $this->tree[$parent]['child'][] = $id;
            $this->tree[$id]['item'] = $item;
        }
    }

    /**
     * Проверка на активность пункта меню по id
     *
     * @param integer $id Идентификатор пункта меню
     * @return bool
     */
    public function isActive($id)
    {
        return isset($this->tree[$id]['isActive']);
    }

    /**
     * Получение дерева элементов
     * return $this->tree;
     */
    public function getTree()
    {
        return $this->tree;
    }

    /**
     * Получение элемента по идентификатору из дерева
     *
     * @param integer $id Идентификатор элемента
     * @return array
     */
    public function extractItem($id)
    {
        if (isset($id)) {
            return $this->tree[$id];
        } else {
            return array();
        }
    }

    /**
     * Возвращает весь омут пунктов меню
     */
    public function extractTree()
    {
        return $this->tree;
    }

    /**
     * Тест на отцовство
     *
     * @param CActiveRecord $item объект пункта меню
     * @param integer $parent Идентификатор родителя
     * @return bool
     */
    private function isParent($item, $parent)
    {
        return isset($item[$parent]);
    }

    /**
     * Получение числа детей по идентификатору родителя
     *
     * @param integer $id Идентификатор родителя
     * @return int|null|void
     */
    public function getChildrenNum($id)
    {
        if (isset($id) && isset($this->tree[$id]['child'])) {
            return count($this->tree[$id]['child']);
        } else {
            return 0;
        }
    }


    /**
     * Получение массива идентификаторов детей
     *
     * @param integer $id Идентификатор родителя
     * @return array|void
     */
    public function getChildrenList($id)
    {
        if (isset($id) && isset($this->tree[$id]['child'])) {
            return $this->tree[$id]['child'];
        } else {
            return array();
        }
    }

    /**
     * Получение глубины элемента в омуте
     *
     * @param integer $id Идентификатор пункта меню в омуте
     * @return null
     */
    public function getItemLevel($id)
    {
        if (isset($id) && isset($this->tree[$id]['level'])) {
            return $this->tree[$id]['level'];
        } else {
            return NULL;
        }
    }

    /**
     * Получение глубины омута
     */
    public function getTreeLevel()
    {
        return $this->level;
    }

}
