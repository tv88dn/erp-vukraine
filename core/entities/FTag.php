<?php
/**
 * Class FTag
 *
 * Класс реализирующий структуру HTML / XML тега
 *
 * @package entities
 * @author Trunov Vadim trunov@lookmy.info
 * @copyright 2014 LookMy.info
 */
class FTag
{
    /**
     * @var string имя тега
     */
    private $tag;
    /**
     * @var array атрибуты тега
     */
    private $attr;
    /**
     * @var данные, которые добвляются в голову тега (перед потомками)
     */
    private $dataHead;
    /**
     * @var данные, которые добвляются в хвост тега (после мотомков)
     */
    private $dataTail;

    /**
     * Контсруктор класса. Принемает в качестве параметров имя тега и массив атрибутов
     *
     * @param string $tag имя тега
     * @param array $attr массив атрибутов
     */
    function __construct($tag = '', $attr = array())
    {
        $this->tag = $tag;
        $this->attr = $attr;
        $this->data = array();
    }

    /**
     * Полная разовая обработка тега. Вызывате begin & endTag
     *
     * @return string
     */
    public function printTag()
    {
        $html = $this->beginTag();
        $html .= $this->endTag();
        return $html;
    }

    /**
     * Выводит открывающийся тег с атрибутами. Выводит данные, предназначенные для отображения в "голове" тега
     *
     * @return string
     */
    public function beginTag()
    {
        $html = ($this->tag) ? '<' . $this->tag . ' ' . $this->createAttributesString() . '>' : '';
        $html .= $this->processContent($this->dataHead);
        return $html;
    }

    /**
     * @return string перабразует массив атрибутов в строку, представляюшую собой пары ключ=значение
     */
    private function createAttributesString()
    {
        $res = '';
        foreach ($this->attr as $key => $val) {
            $res .= $key . '="' . $val . '"';
        }
        return $res;
    }

    /**
     * Обрабатывает внутренне содержимое тега. Если внутри находит еще теги, выполняет их обработку
     *
     * @param array $data массив тегов
     * @return string HTML / XML структура, построенная по массиву тегов
     */
    private function processContent($data)
    {
        $html = '';
        for ($i = 0; $i < count($data); $i++) {
            if ($data[$i] instanceof Ftag) {
                $html .= $data[$i]->beginTag();
                $html .= $data[$i]->endTag();
            } else {
                $html .= $data[$i];
            }
        }
        return $html;
    }

    /**
     * Выводит закрывающийся тег. Выводит данные, предназначенные для отображения в "хвосте" тега
     *
     * @return string
     */
    public function endTag()
    {
        $html = ($this->tag) ? '</' . $this->tag . '>' : '';
        $html .= $this->processContent($this->dataTail);
        return $html;
    }

    /**
     * Инициализация головы тега
     *
     * @param string|FTag $data строка или тег для инициализации
     * @return string|FTag mixed
     */
    public function head($data)
    {
        $this->dataHead[] = $data;
        return $data;
    }

    /**
     * Инициализация хвоста тега
     *
     * @param string|FTag $data строка или тег для инициализации
     * @return string|FTag mixed
     */
    public function tail($data)
    {
        $this->dataTail[] = $data;
        return $data;
    }

    /**
     * Добавляет данные в голову тега
     *
     * @param string|FTag $data строка или тег для добавления
     * @return string|FTag mixed
     */
    public function add($data)
    {
        $this->dataHead[] = $data;
        return $data;
    }

    /**
     * Добавляет новые атрибуты в массив атрибутов
     *
     * @param array $attr массив атрибутов
     */
    public function addAttr($attr)
    {
        foreach ($attr as $nk => $nval) {
            $isAdd = false;
            foreach ($this->attr as $k => $val) {
                if ($k == $nk) {
                    $this->attr[$k] .= ' ' . $nval;
                    $isAdd = true;
                    break;
                }
            }
            if (!$isAdd) {
                $this->attr[$nk] = $nval;
            }
        }
    }

} 