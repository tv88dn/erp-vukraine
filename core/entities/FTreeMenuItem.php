<?php

class FTreeMenuItem
{
    private $item;
    private $children;
    private $state;
    private $parent;

    function __construct($item, $children=array(), $parent=null, $state='')
    {
        $this->item = $item;
        $this->children = $children;
        $this->$parent = $parent;
        $this->$state = $state;
    }

    public function makeActive()
    {
        $this->state = 'active';
    }

    public function makeInactive()
    {
        $this->state = '';
    }

} 