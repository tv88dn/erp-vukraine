<?php
include_once("FTree.php");

/**
 * class FPool
 *
 * Класс реализующий синглтон хранилище элементов меню
 */
class FPool extends FTree implements FSingleton
{
    /**
     * @var FPool единственный экземпляр хранилища
     */
    private static $instance;


    private function __construct()
    {
        $this->tree = array();
        $this->level = 0;
    }

    /**
     * Выставляет активные элементы начиная с нижних элементов дерева
     *
     * @param $item string ключ єлемента
     * @param $parentKey string ключ родителя
     */
    private function setActive($item, $parentKey)
    {
        if (isset($item) && isset($parentKey)) {
            $parent = $item[$parentKey];
            $this->tree[$parent]['isActive'] = $item['isActive'];
            if (isset($this->tree[$parent]['item'])) {
                $this->setActive($this->tree[$parent]['item'], $parentKey);
            }
        }
    }

    /**
     * Внутренний метод получения экземпляра класса синглтон
     */
    static public function getInstance()
    {
        if (self::$instance == NULL) {
            self::$instance = new FPool();
        }
        return self::$instance;
    }

    /**
     * Добавление элемента в общий пул
     *
     * @param array $item Массив содержащий в себе
     * имена идентификаторов пункта меню и его родителя
     * для ассоциативного массива, и объект CActiveRecord
     * в котором эти имена будут использоваться
     */
    public function insertItem($item)
    {
        $idKey = $item[0];
        $parentKey = $item[1];
        $id = $item[2][$idKey];
        $parent = $item[2][$parentKey];
        $item = $item[2];
        $level = 0;
        if ($id != NULL) {
            $this->tree[$parent]['child'][] = $id;
            $this->tree[$id]['item'] = $item;
            if (isset($this->tree[$parent]['level'])) {
                $level = $this->tree[$id]['level'] = $this->tree[$parent]['level'] + 1;
            } else {
                $level = $this->tree[$id]['level'] = 0;
            }
            ($level > $this->level) ? $this->level = $level : false;
            if ($item['isActive'] != '') {
                $this->tree[$id]['isActive'] = $item['isActive'];
                $this->setActive($item, $parentKey);
            }
        }
    }
}
