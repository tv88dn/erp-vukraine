<?php

/**
 * Interface FSingleton
 *
 * Интерфейс для синглтонов
 * Для реализации так же необходимо сделать __construct и __clone приватными
 * Обращение к экземпляру класса доступно через стантический метод getInstance
 *
 * @package system
 * @author Trunov Vadim trunov@lookmy.info
 * @copyright 2014 LookMy.info
 */
interface FSingleton
{

    /**
     * Реализация Синглтона
     *
     */
    static function getInstance();


}