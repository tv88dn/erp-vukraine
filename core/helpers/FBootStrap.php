<?php
/**
 * Created by PhpStorm.
 * User: tv88d_000
 * Date: 22.08.14
 * Time: 10:52
 */

class FBootStrap extends CHtml
{
    const DEFAULT_LABEL_WIDTH = 4;
    const DEFAULT_CONTROL_WIDTH = 8;

    const POSITION_LEFT = 'left';
    const POSITION_RIGHT = 'right';

    public static function listData($models,$valueField,$textField,$groupField='')
    {
        if(count($models) > 0)
        {
            return parent::listData($models,$valueField,$textField,$groupField);
        }
        else
        {
            return array(''=>'');
        }
    }

    protected static function mergeHtmlOptions($base, $external)
    {
        if(isset($external['class']))
        {
            foreach($base as $k=>$v)
            {
                if(isset($external[$k]))
                {
                    $base[$k] .= ' '.$external[$k];
                    unset($external[$k]);
                }
                if($k == 'width')
                {
                    unset($external[$k]);
                }
            }
            return array_merge($base, $external);
        }
        else if(is_array($external))
        {
            return array_merge($base, $external);
        }
        else
        {
            return $base;
        }
    }

    protected static function getAttributeAdditionalInfo($attribute)
    {
        $attributeAdditionalInfo = explode('[', $attribute);
        for($i =0; $i < count($attributeAdditionalInfo); $i++)
        {
            $attributeAdditionalInfo[$i] = str_replace(']', '', $attributeAdditionalInfo[$i]);
        }
        if(count($attributeAdditionalInfo) == 2)
        {
            return array(
                'name'=>$attributeAdditionalInfo[0],
                'index'=>$attributeAdditionalInfo[1],
            );
        }
        else
        {
            return null;
        }

    }

    protected static function needButton($model, $attribute)
    {
        $attributeAdditionalInfo = self::getAttributeAdditionalInfo($attribute);
        if($attributeAdditionalInfo)
        {
            return (count($model->details[$attributeAdditionalInfo['name']]) == $attributeAdditionalInfo['index']) ? true : false;
        }
        else
        {
            return true;
        }
    }

    public static function activeDateField($model, $attribute, $htmlOptions = array())
    {
        return self::activeTextField($model, $attribute, self::mergeHtmlOptions(array(
            'class'=>'date',
        ), $htmlOptions));
    }

    /**
     * @param $model
     * @param $attribute
     * @param array $htmlOptions
     * @return string
     */
    public static function activeDateRangeField($model, $attribute, $htmlOptions = array())
    {
        $container = (isset($htmlOptions['container'])) ? $htmlOptions['conteiner'] : array();
        $label = (isset($htmlOptions['inLabel'])) ? $htmlOptions['inLabel'] : array();
        $submitBtn = (isset($htmlOptions['submitBtn'])) ? $htmlOptions['submitBtn'] : array();
        if(isset($htmlOptions['control']['name']))
        {
            $controlName = $htmlOptions['control']['name'];
            unset($htmlOptions['control']['name']);
        }
        else
        {
            $controlName = get_class($model).'[attribute]';
        }
        $control = (isset($htmlOptions['control'])) ? $htmlOptions['control'] : array();
        $idName = get_class($model).'_'.$attribute;
        $html = self::openTag('div', self::mergeHtmlOptions(array('class'=>'input-group'), $container));
        $html .= self::tag('span', self::mergeHtmlOptions(array('class'=>'input-group-addon'), $label), 'с');
        $html .= self::activeTextField($model, $attribute.'[from]',
            self::mergeHtmlOptions(array('class'=>'form-control date', 'id'=>$idName.'_from', 'name'=>$controlName.'[from]'), $control)
        );
        $html .= self::tag('span', self::mergeHtmlOptions(array('class'=>'input-group-addon'), $label), 'по');
        $html .= self::activeTextField($model, $attribute.'[to]',
            self::mergeHtmlOptions(array('class'=>'form-control date', 'id'=>$idName.'_to', 'name'=>$controlName.'[to]'), $control)
        );
        $html .= self::tag('span', self::mergeHtmlOptions(array('class'=>'input-group-btn'), $submitBtn),
            self::submitButton('OK', array('class'=>'btn btn-success', 'id'=>'loginCheck'))
        );
        $html .= self::closeTag('div');
        return $html;
    }

    public static  function activeTimeField($model, $attribute, $htmlOptions = array())
    {
        $html = self::openTag('div', array('class'=>'input-group'));
        $html .= self::openTag('span', array('class'=>'input-group-addon glyphicon glyphicon-time'));
        $html .= self::closeTag('span');
        self::resolveNameID($model, $attribute, $htmlOptions);
        $html .= self::activeInputField('time', $model, $attribute, self::mergeHtmlOptions(
            array(
                'class'=>'form-control',
            ), $htmlOptions
        ));
        $html .= self::closeTag('div');
        return $html;
    }

    public static function activeTextFieldWithButton($model, $attribute, $htmlOptions = array())
    {
        if(self::needButton($model, $attribute))
        {
            $html = '<div class="input-group">';
            $html .= FBootStrap::activeTextField($model, $attribute, $htmlOptions);
            $html .= self::inputGroupBtn(array(array(
                'type'=>'success',
                'position'=>FBootStrap::POSITION_RIGHT,
                'label'=>"Ещё",
                'htmlOptions'=>array(
                    'class'=>'disabled',
                ),
            )));
            $html .= '</div>';
        }
        else
        {
            $html = FBootStrap::activeTextField($model, $attribute, $htmlOptions);
            //$html .= FBootStrap::openTag('span', array('class'=>'glyphicon glyphicon-remove right deletePhone btn btn-danger'));
            $html .= FBootStrap::openTag('span', array('class'=>'glyphicon glyphicon-remove right deleteField btn btn-danger'));
            $html .= FBootStrap::closeTag('span');
        }
        return $html;
    }

    public static function activePhoneInputWithButton($model, $attribute, $htmlOptions = array())
    {
        if(self::needButton($model, $attribute))
        {
            $html = '<div class="input-group">';
            $html .= FBootStrap::activePhoneInput($model, $attribute, $htmlOptions);
            $html .= self::inputGroupBtn(array(array(
                'type'=>'success',
                'position'=>FBootStrap::POSITION_RIGHT,
                'label'=>"Ещё",
                'htmlOptions'=>array(
                    'class'=>'addPhoneNumber disabled',
                ),
            )));
            $html .= '</div>';
        }
        else
        {
            $html = FBootStrap::activePhoneInput($model, $attribute, $htmlOptions);
            //$html .= FBootStrap::openTag('span', array('class'=>'glyphicon glyphicon-remove right deletePhone btn btn-danger'));
            $html .= FBootStrap::openTag('span', array('class'=>'glyphicon glyphicon-remove right deletePhone btn btn-danger'));
            $html .= FBootStrap::closeTag('span');
        }
        return $html;
    }

    public static function activePhoneInput($model, $attribute, $htmlOptions = array())
    {
        return self::activeTextField($model, $attribute, self::mergeHtmlOptions(array('class'=>'phone'), $htmlOptions));
    }

    public static function activeFileField($model,$attribute,$htmlOptions=array()){
        self::resolveNameID($model,$attribute,$htmlOptions);
        $hiddenOptions=isset($htmlOptions['id']) ? array('id'=>self::ID_PREFIX.$htmlOptions['id']) : array('id'=>false);
        return self::hiddenField($htmlOptions['name'],$model->$attribute,$hiddenOptions)
        . self::activeInputField('file',$model,$attribute,$htmlOptions);
    }

    public static function activeEditField($model, $attribute, $htmlOptions = array())
    {
        $html = '<script src="//cdn.ckeditor.com/4.4.5/basic/ckeditor.js"></script>';
        $html .= parent::activeTextArea($model, $attribute, $htmlOptions);
        $html .= '<script>CKEDITOR.replace( "'.self::activeName($model, $attribute).'"  );</script>';
        return $html;
    }

    public static function activeStandardEditField($model, $attribute, $htmlOptions = array())
    {
        $html = '<script src="//cdn.ckeditor.com/4.4.5/standard/ckeditor.js"></script>';
        $html .= parent::activeTextArea($model, $attribute, $htmlOptions);
        $html .= '<script>CKEDITOR.replace( "'.self::activeName($model, $attribute).'"  );</script>';
        return $html;
    }

    public static function inputGroupBtn($buttons, $htmlOptions=array())
    {
        $html = self::openTag('span', self::mergeHtmlOptions(array('class'=>'input-group-btn'), $htmlOptions));
        for($i = 0; $i < count($buttons); $i++)
        {
            $buttons[$i]['htmlOptions'] = (!isset($buttons[$i]['htmlOptions'])) ? array() : $buttons[$i]['htmlOptions'];
            $htmlOptions = self::mergeHtmlOptions(array(
                'class'=>'btn btn-'.$buttons[$i]['type']
            ), $buttons[$i]['htmlOptions']);
            $html .= self::button($buttons[$i]['label'], $htmlOptions);
        }
        $html .= self::closeTag('span');
        return $html;
    }

    protected static function groupControl($control, $model, $attribute, $controlData, $htmlOptions)
    {
        $controlHtml =  array('class'=>'form-control', 'placeholder'=>$model->getAttributeLabel($attribute));
        $controlHtml =  self::mergeHtmlOptions($controlHtml, $htmlOptions['control']);
        $html = '';
        //if(isset($model->detailsAttributes[$attribute]->multiple))

        if(isset($htmlOptions['button']))
        {
            $html .= '<div class="input-group">';
            if($htmlOptions['button']['position'] == self::POSITION_LEFT)
            {
                $html .= self::inputGroupBtn(array($htmlOptions['button']));
            }
        }
        if(count($controlData) > 0)
        {
            $html .= call_user_func('FBootStrap::'.$control, $model, $attribute, $controlData, $controlHtml);
        }
        else
        {
            $html .= call_user_func('FBootStrap::'.$control, $model, $attribute, $controlHtml);
        }

        if(isset($htmlOptions['button']))
        {
            if($htmlOptions['button']['position'] == self::POSITION_RIGHT)
            {
                $html .= self::inputGroupBtn(array($htmlOptions['button']));
            }
            $html .= '</div>';
        }
        return self::controlAddOn($htmlOptions, 'left').$html.self::controlAddOn($htmlOptions, 'right');
    }

    public static function controlAddOn($htmlOptions, $position)
    {
        $html = '';
        if(isset($htmlOptions['addon']))
        {
            if($position == 'left')
            {
                $html .= '<div class="input-group">';
            }
            if(isset($htmlOptions['addon'][$position]))
            {
                $html .= $htmlOptions['addon'][$position];
            }
            if($position == 'right')
            {
                $html .= '</div>';
            }
        }
        return $html;

    }

    public static function formGroup($model, $attribute, $control, $controlData = array(), $htmlOptions = array())
    {
        if($control == 'activeHiddenField')
        {
            if(is_array($model->$attribute))
            {
                $res = '';
                for($i = 0; $i < count($model->$attribute); $i++)
                {
                    $res .= FBootStrap::activeHiddenField($model, $attribute.'['.$i.']');
                }
                return $res;
            }
            else
            {
                return FBootStrap::activeHiddenField($model, $attribute);
            }

        }
        if(isset($htmlOptions['label']))
        {
            $labelWidth = (isset($htmlOptions['label']['width'])) ? $htmlOptions['label']['width'] : self::DEFAULT_LABEL_WIDTH;
        }
        else
        {
            $htmlOptions['label'] = array();
            $labelWidth =  self::DEFAULT_LABEL_WIDTH;
        }
        if(isset($htmlOptions['control']))
        {
            $controlWidth = (isset($htmlOptions['control']['width'])) ? $htmlOptions['control']['width'] : self::DEFAULT_CONTROL_WIDTH;
        }
        else
        {
            $htmlOptions['control'] = array();
            $controlWidth =  self::DEFAULT_CONTROL_WIDTH;
        }
        $labelHtml =  self::mergeHtmlOptions(array(
            'class'=>'col-lg-'.$labelWidth.' control-label',
        ), $htmlOptions['label']);
        $labelHtml = self::mergeHtmlOptions($labelHtml, $htmlOptions['label']);


        $html = '<div class="form-group warp">';
        $html .= self::activeLabel($model, $attribute, $labelHtml);
        $html .= '<div class="col-lg-'.$controlWidth.'">';
        if($attribute == 'parentId')
        {
            //var_dump($model->getAttribute($attribute));

        }
        if(is_array($model->$attribute) && !($model instanceof DetailsTypes))
        {
            $attributesArray = $model->$attribute;
            if(count($attributesArray) > 0)
            {
                for($i = 0; $i < count($attributesArray)+1; $i++)
                {

                    $html .= self::groupControl($control, $model, $attribute.'['.$i.']', $controlData, $htmlOptions);
                }
            }
            else
            {
                $html .= self::groupControl($control, $model, $attribute.'[0]', $controlData, $htmlOptions);
            }
        }
        else
        {
            $html .= self::groupControl($control, $model, $attribute, $controlData, $htmlOptions);
        }
        $html .= '</div>';
        $html .= '</div>';
        return $html;
    }

    public static function btnDefault($caption, $htmlOptions = array())
    {
        return parent::button($caption, self::mergeHtmlOptions(array('class'=>'btn btn-default'), $htmlOptions));
    }

    public static function btnDefaultLink($caption, $link, $htmlOptions = array())
    {
        return parent::link($caption, $link, self::mergeHtmlOptions(array('class'=>'btn btn-default'), $htmlOptions));
    }

    public static function btnDangerLink($caption, $link, $htmlOptions = array())
    {
        return parent::link($caption, $link, self::mergeHtmlOptions(array('class'=>'btn btn-danger'), $htmlOptions));
    }

    public static function btnPrimarySubmit($caption, $htmlOptions = array())
    {
        return parent::submitButton($caption, self::mergeHtmlOptions(array('class'=>'btn btn-primary'), $htmlOptions));
    }

    public static function btnSuccessSubmit($caption, $htmlOptions = array())
    {
        return parent::submitButton($caption, self::mergeHtmlOptions(array('class'=>'btn btn-success'), $htmlOptions));
    }

    public static function getTypes()
    {
        $methods = get_class_methods('FBootStrap');
        $res = array(
            '' => '',
        );
        for($i =0; $i < count($methods); $i++)
        {
            if(preg_match('/^active.*/', $methods[$i]))
            {
                $res[$methods[$i]] = $methods[$i];
            }
        }
        return $res;
    }

    public static function linkDefault($button, $htmlOptions = array())
    {
        return parent::link(
            $button['text'],
            $button['url'],
            self::mergeHtmlOptions(array('class'=>'btn btn-default'), $htmlOptions)
        );
    }

    public static function linkGroup($buttons, $htmlOptions = array())
    {
        $html = parent::openTag('div', self::mergeHtmlOptions(array('class'=>'btn-group'), $htmlOptions));
        for($i = 0; $i < count($buttons); $i++)
        {
            $html .= self::linkDefault($buttons[$i]);
        }
        $html .= parent::closeTag('div');
        return $html;
    }

    public static function icon($icon, $htmlOptions = array(), $content = '')
    {
        $class = array('class'=>'glyphicon glyphicon-'.$icon);
        return parent::tag('span', self::mergeHtmlOptions($class, $htmlOptions), $content);
    }
} 