<?php

class FSort extends CSort
{

    private $sortType;

    private $sortField;

    private $needSort;



    public function resolveAttribute($attribute)
    {

        if($this->attributes!==array())
            $attributes=$this->attributes;
        elseif($this->modelClass!==null)
            $attributes=$this->getModel($this->modelClass)->attributeNames();
        else
            return false;
        foreach($attributes as $name=>$definition)
        {
            if(is_string($name))
            {
                if($name===$attribute)
                    return $definition;
            }
            elseif($definition==='*')
            {
                return $attribute;
            }
            elseif($definition===$attribute)
                return $attribute;
        }
        return false;
    }

    protected function getDetailName($order)
    {
        if($order)
        {
            $this->needSort = true;
            if(count(explode(',', $order)) > 1)
            {
                throw new Exception('multisort not supported yet');
                FCore::app()->end();
            }

            $orderArr = explode('.', $order);
            $orderName = explode(' ', $orderArr[1]);
            $this->sortField = str_replace('`', '', $orderName[0]);
            if(isset($orderName[1]))
            {
                $this->sortType = str_replace('`', '', $orderName[1]);
            }
            else
            {
                $this->sortType = '';
            }
        }
        else
        {
            $this->needSort = false;
        }
    }


    /**
     * @param $criteria
     * @param $detailTable
     */
    public function applyDetailsOrder($criteria, $detailTable)
    {
        $this->getDetailName( $this->getOrderBy($criteria) );
        if(parent::resolveAttribute($this->sortField) === false)
        {
            if($this->needSort)
            {
                $sortModel = new $detailTable;
                $sortModel->applySortCriteria($criteria, $this->sortField, $this->sortType);

            }
        }
        else
        {
            $this->applyOrder($criteria);
        }


    }

} 