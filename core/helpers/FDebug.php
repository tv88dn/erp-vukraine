<?php

/**
 * Class FDebug
 *
 * Вводит отладочную информацию в поток вывода.
 * Если включен глобальный флаг YII_DEBUG, то данные выодятся
 */
class FDebug
{
    /**
     * @var array массив запущенных таймеров
     */
    private static $timers;

    /**
     * включает xDebug
     */
    public static function xDebugActivate()
    {
        xdebug_enable();
    }

    /**
     * отключает xDebug
     */
    public static function xDebugDeactivate()
    {
        xdebug_disable();
    }

    /**
     * Обертка для стандартного var_dump
     *
     * @param $val переменная для вывода
     */
    public static function dump($val)
    {
        if(YII_DEBUG)
        {
            if(xdebug_is_enabled())
            {
                xdebug_var_dump($val);
            }
            else
            {
                var_dump($val);
            }
        }
    }

    /**
     * Выводит строку на экран.
     *
     * @param $string строка для вывода
     */
    public static function printString($string)
    {
        if(YII_DEBUG)
        {
            echo $string;
        }
    }

    /**
     * Запускает таймер
     *
     * @param string $name имя таймера
     */
    public static function startTimer($name = 'Main_timer')
    {
        if(!self::$timers)
        {
            self::$timers = array();
        }
        if(isset(self::$timers[$name]))
        {
            self::printTimer($name);
        }
        self::$timers[$name] = microtime(true);
    }

    /**
     * Выводит время, прошедшее с момента запуска таймера
     *
     * @param string $name имя таймера
     */
    public static function printTimer($name = 'Main_timer')
    {
        if(isset(self::$timers[$name]))
        {
            self::printString('<div class="debug_info_block">'.$name.': => '.number_format(microtime(true) - self::$timers[$name], 2).'</div>');
        }
    }

    /**
     * Останавливает таймер. Выводит время, прошедшее с момента старта таймера
     * @param string $name имя таймера
     */
    public static function stopTimer($name = 'Main_timer')
    {
        if(isset(self::$timers[$name]))
        {
            echo '<br>';
            self::printTimer($name);
            echo '<br>';
            unset(self::$timers[$name]);
        }
    }



}