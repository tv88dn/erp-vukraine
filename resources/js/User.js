var User = function()
{
    var self = this;
    /**
     * @type {number} Минимальная длинна логина
     */
    var minLoginLength = 3;
    var phoneReg = /\+38\(0\d\d\)\s\d\d\d-\d\d\d\d/;
    var deleteButton = $('<span class="glyphicon glyphicon-remove right deletePhone btn btn-danger"></span>');

    var containerForTasks;

    this.setContainerForTasks = function(val)
    {
        containerForTasks = val;
    };

    this.ready = function()
    {
        this.resetFormForChrome('addUser');
        this.isInputsFilled();
        this.normalizePhoneNames($('.phone:first').attr('name'));
        /**
         * Генерация пароля
         */
        $('#passwordGenerate').click(function(){
            var context = this;
            self.passwordGenerate(function(password){
                $('#User_password').val(password);
                $('#User_passwordRepeat').val(password);
                $('#passwordLabel').remove();
                $(context).parent().parent().parent().parent().append(getLabel(password, 'info'));
            });
        });
        /**
         * Проверка логина
         */
        $('#loginCheck').click(function(){
            var context = this;
            if($('#User_login').val().length < minLoginLength)
            {
                $(context).parent().parent().parent().parent().append(getLabel('Логин слишком короткий', 'danger'));
                return false;
            }
            self.checkUser($('#User_login').val(), function(check){
                $('#passwordLabel').remove();
                if(parseInt(check) > 0)
                {
                    $(context).parent().parent().parent().parent().append(getLabel('Такой пользователь уже есть', 'danger'));
                }
                else
                {
                    $(context).parent().parent().parent().parent().append(getLabel('Логин свободен', 'success'));
                }
            })
        });

        $('#password-change').click(function(){
            $('#change-password-block').show().removeClass('hidden');
        });

        $('#hide-block').click(function(e){
            $(this).parent().hide();
            e.stopPropagation();
        })

        $(document).on('click', '.deletePhone', function(){
            self.removePhoneInput($(this));
        });

        $(document).on('click', '.addPhoneNumber', function(){
            var control = $(this).parent().parent().find('input.phone').clone();
            $(this).parent().parent().parent().prepend(deleteButton);
            $(this).parent().parent().parent().prepend(control);
            $(this).parent().parent().find('input.phone').val('');
            $("input.phone").mask("+38(099) 999-9999");
            self.normalizePhoneNames(control.attr('name'));
        });

        $('input.phone').keyup(function(){
            self.isInputsFilled();
        })
    }

    this.normalizePhoneNames = function(name)
    {
        var i = 0;
        $('#addUser').find('input.phone').each(function(){
            var fieldName = $(this).attr('name');
            //console.log(fieldName);
            var number = fieldName.split('[')[2].replace(']', '');
            var placeholder = fieldName.split('[')[1].replace(']', '')
            fieldName = fieldName.replace('['+fieldName.split('[')[2].replace(']', '')+']', '['+i+']');
            $(this).attr('name', fieldName);
            $(this).attr('id', $(this).attr('id').replace('_'+number, '_'+i));
            $(this).attr('placeholder', placeholder+' №'+i);
            i++;
        });
        //alert();
    }

    /**
     * Отправляет запрос к серверу на генерацию пароля
     * @param callback
     */
    this.passwordGenerate = function(callback)
    {
        $.get('/user/generateRandomPassword', function(data){
            callback(data);
        })
    }



    this.resetFormForChrome = function(formId)
    {
        setTimeout(
            function(){
                var form = document.getElementById( formId );
                if(form != undefined)
                {
                    form.reset();
                }
            },
            10
        );
    }



    this.checkUser = function(login, callback)
    {
        $.get('/user/checkLogin', {'login': login}, function(data){
                callback(data);
        });
    }

    this.removePhoneInput = function(control)
    {
        var name = control.prev().attr('name');
        control.prev().remove();
        control.remove();
        self.normalizePhoneNames(name);
    }

    this.isInputsFilled = function()
    {
        $('.addPhoneNumber').each(function(){
            if(phoneReg.test($(this).parent().parent().find('input').val()))
            {
                $(this).removeClass('disabled');
            }
            else
            {
                $(this).addClass('disabled');
            }
        })
    };

    this.loadTasks = function(path)
    {
        console.log(containerForTasks);
        animateLoader(containerForTasks).load('/task/'+path, function(data){
            containerForTasks.append(data);
        });
    }
};
