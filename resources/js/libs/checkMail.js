(function($){
	$.fn.extend({
		checkMail: function(){
			var elid = this;
			if(elid.val().search(/^([a-z0-9_-]+\.)*[a-z0-9_-]+@[a-z0-9_-]+(\.[a-z0-9_-]+)*\.[a-z]{2,6}$/)<0){
				return false;
			}else{
				return true;
			}
		}
	})
})(jQuery);