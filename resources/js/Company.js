var Company = function()
{
    var self = this;
    var minCompanyNameLength = 3;

    this.ready = function()
    {
        self.setBillsVisibility();

        $('#status').change(function(){
            self.setBillsVisibility();
        });

        $('.company-name').each(function(){
            $(this).css('margin-top', -1*parseInt($(this).height()+20));
        });

        $('.company-contact-row').click(function(){
            fadeInWindow($(this).attr('location'));
        });

        $('.company-block').click(function(){
            if($(this).hasClass('active'))
            {
                $(this).removeClass('active');
            }
            else
            {
                $(this).addClass('active');
            }
        })

        $('#company-logo').hover(function(){
            $('.load-img-block').stop().css({
                'background-color': 'rgba(170, 170, 170, 0.3)',
                'height': '12px',
                'margin-top': '-10px'
            }).animate({
                'margin-top': "-=40",
                'height': "+=40"
            });

        }, function(){
            $('.load-img-block').stop().css({
                'background-color': 'rgba(170, 170, 170, 0.6)',
                'height': '52px',
                'margin-top': '-50px'
            }).animate({
                'margin-top': "+=40",
                'height': "-=40"
            });
        }).find('#load').click(function(){
            $('#Company_logo').trigger('click');
        });


        $('#companyCheck').click(function(){
            var context = this;
            if($('#Company_name').val().length < minCompanyNameLength)
            {
                $(context).parent().parent().parent().parent().append(getLabel('Название компаниии слишком короткое', 'danger'));
                return false;
            }
            self.checkCompany($('#Company_name').val(), function(check){
                if(parseInt(check) > 0)
                {
                    $(context).parent().parent().parent().parent().append(getLabel('Такая компания уже зарегистрирована. Проверте, может быть Вы добавляете дубль?', 'danger'));
                }
                else
                {
                    $(context).parent().parent().parent().parent().append(getLabel('Такой компании нет в базе', 'success'));
                }
            })
        });

    }

    this.checkCompany = function(name, callback)
    {
        $.get('/company/checkCompanyName', {'name': name}, function(data){
            callback(data);
        });
    }

    this.setBillsVisibility = function()
    {
        if($('#status').val() == 'client')
        {
            $('#bills').parent().parent().show();
        }
        else
        {
            $('#bills').parent().parent().hide();
        }
    }
}