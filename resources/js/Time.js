var Time = function()
{
    var self = this;
    var timer;
    var time;
    var screen;
    var interval;

    this.ready = function()
    {
        /*
        $.get('/time/timer', function(data){
            $('#timerContainer').html(data);
            timer = $('#timerControl');
            time = timer.find('#timeDiff').val();
            screen = timer.find('#timerScreen');
            if(time)
            {
                self.start();
            }
        });
        */


    };

    function parse(val)
    {
        val = val.toString();
        if(val.length < 2)
        {
            val = '0'+val;
        }
        return val;
    }

    function displayTime()
    {
        time++;
        var hours = parseInt(time/(60*60));
        var minutes = parseInt((time-(60*60*hours))/60);
        var seconds = parseInt(time%60);
        screen.html(parse(hours)+':'+parse(minutes)+':'+parse(seconds));
    }

    this.start = function()
    {
        interval = setInterval(displayTime, 1000);
    }
};