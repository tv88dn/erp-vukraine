/**
 * Created by tv88d_000 on 29.08.14.
 */
String.prototype.replaceAll = function(search, replace){
    return this.split(search).join(replace);
}

function fadeInWindow(action) {
    $('#buffer').css({'background': 'url(/resources/images/loading29.gif) no-repeat center center'});
    $('#fade').fadeIn(50, function () {
        $('#buffer').fadeIn(50, function () {
            $('#buffer').load(action, function () {
                $('#buffer').css({'background': 'none'});
            });
        });
    });
}

function animateLoader(conteiner)
{
    var Loader = function(conteiner)
    {
        var div = $('<div class="load-container"></div>');
        var self = this;
        conteiner.prepend(div).css('opacity', '0.5');

        this.load = function(url, callback){
            $.ajax({
                url: url,
                dataType: 'html',
                type: 'get',
                beforeSend: function(){
                    self.step(1);
                },
                error: function(textStatus){
                    div.html('Ошибка загрузки: '+textStatus);
                },
                dataFilter:function(data){
                    self.step(2);
                    return data;
                },
                complete: function()
                {
                    self.step(4);
                }
            }).done(function(data){
                conteiner.children().not('.load-container').remove();
                conteiner.css('opacity', '1');
                self.step(3);
                callback(data);
            });
        };

        this.step = function(number)
        {
            switch(number){
                case 1: div.animate({
                    width: (Math.random() *  (80 - 90) + 80).toString() + '%'
                }, 500); break;
                case 2: div.finish().animate({
                    width: (Math.random() *  (90 - 100) + 90).toString() + '%'
                }, 100); break;
                case 3: div.finish().animate({
                    width: '100%'
                }, 100); break;
                case 4: div.finish(); div.animate({'opacity': 0}, {
                    duration: 400,
                    complete: function(){
                        div.remove();
                    }
                });break;
                default: div.remove();
            }
        };
    };
    return new Loader(conteiner);
}

function getLabel(label, type)
{
    return '<div id="passwordLabel" class="col-lg-4"><div><div class="alert alert-'+type+'"><button type="button" class="close" data-dismiss="alert">×</button><span>'+label+'</span></div></div></div>';
}
var company, user, filter, time, task;

$(document).ready(function(){
    if(User != undefined)
    {
        user = new User();
        user.ready();
    }
    if(Company != undefined)
    {
        company = new Company();
        company.ready();
    }
    if(Filter != undefined)
    {
        filter = new Filter();
        filter.ready();
    }
    if(Time != undefined)
    {
        time = new Time();
        time.ready();
    }
    if(Task != undefined)
    {
        task = new Task();
        task.ready();
    }

    $("input.phone").mask("+38(099) 999-9999");
    $('.addButtonInTitle').click(function(){
        $(this).parent().parent().find('.panel-body').show();
        $(this).remove();
    });

    $('#fade').click(function(){
        $(this).hide();
        $('#buffer').hide();
    });

    $('.left-sidebar-menu').hover(function(){
        $(this).stop();
        $(this).animate({
            left: '0'
        }, 300);
    }, function(){
        $(this).stop();
        $(this).animate({
            left: '-74'
        }, 300);
    });

    $(function() {
        $( "input.date, input[type='date']").each(function(){
            $(this).datepicker({
                dateFormat: 'dd.mm.yy'
            });
        });
    });

    $('#logoHref').click(function(event){
        var url = $(this).attr('href');
        animateLoader($('#body')).load(url, function(data){
            $('#body').append(data);
            history.pushState(null, null, url);
        });
        event.preventDefault();
    });

    $('#mainMenu a').click(function(event){
        var url = $(this).attr('href');
        if($(this).attr('href') && $(this).attr('href') != '#')
        {
            animateLoader($('#body')).load($(this).attr('href'), function(data){
                $('#body').append(data);
                history.pushState(null, null, url);
            });
            event.preventDefault();
        }

    });
    user.setContainerForTasks($('#myTasksContent'));
/*
    user.setContainerForTasks($('#myTasksContent'));
    $.get('/time/add', function(data){
        $('#addTaskTimeForm').html(data);
    });
*/
});