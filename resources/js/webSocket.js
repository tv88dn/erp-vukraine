/**
 *
 * @param _host
 * @param _port
 * @constructor
 */
var WebSocketClient = function(_host, _port)
{
    var socket;
    var userId;
    var secret;
    var host = "ws://"+_host+":"+_port;

    function getCookie(name) {
        var matches = document.cookie.match(new RegExp(
            "(?:^|; )" + name.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, '\\$1') + "=([^;]*)"
        ));
        return matches ? decodeURIComponent(matches[1]) : null;
    }

    function setCookie(name, value)
    {
        if(!getCookie(name))
        {
            document.cookie = name+'='+value;
        }
    }

    this.init = function() {
        return true; //@todo расскоментить когда нужен будет web socket
        $.getJSON('/user/getWebSocketId', function(data){
            userId = data.id;
            secret = data.secret;
            var setUpMessage = {
                'type': 'CLIENT_MSG_SETUP',
                'data': data
            };
            try {
                socket = new WebSocket(host);
                socket.onopen = function (msg) {
                    console.log("Welcome - status " + this.readyState);
                    socket.send(JSON.stringify(setUpMessage));
                };
                socket.onmessage = function (msg) {
                    //console.log("Received: " + msg.data);
                    //$('html').prepend(msg.data);
                    alert(msg.data);
                };
                socket.onclose = function (msg) {
                    console.log("Disconnected - status " + this.readyState);
                };
            }
            catch(ex)
            {
                console.log(ex);
            }

        });

    };

    this.send = function($_message)
    {
        var message = {
            'user': uniqueId,
            'data': $_message
        }
        socket.send(JSON.stringify(message));
    };

    this.quit = function(){
        if (socket != null) {
            console.log("Goodbye!");
            socket.close();
            socket=null;
        }
    };

    this.reconnect = function() {
        this.quit();
        this.init();
    };

};

var ws = new WebSocketClient('212.66.44.52', '9000');
ws.init();