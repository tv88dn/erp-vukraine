$(function () {
    'use strict';

    $('#File_name').fileupload({
        url: '/file/ajax',
        dropZone: '#dropzone',
        submit: function(e, data){
            var file_id = 'i'+Math.random().toString().slice(-5);
            var progr = $('<div class="progress"></div>');
            var progr_bar = $('<div class="progress-bar progress-bar-striped active" role="progressbar"></div>');
            var input = $('<input type="hidden" name="File[name][]"/>');
            progr.append(input).append(progr_bar);
            data.formData = {fileID: file_id};
            $.each(data.files, function (index, file) {
                input.attr('value', file.name);
                progr_bar.text(file.name).attr('id', file_id);
                progr.appendTo('#files');
            });
        },
        done: function(e, data){
            var res = JSON.parse(data.result);
            var close_btn = $('<span class="close remove_file" aria-hidden="true">&times;</p>');
            close_btn.attr('onclick', 'remove_file("'+res.fileID+'")');
            console.log(res.id);
            var id_field = $('<input type="hidden" name="File[id][]"/>');
            id_field.attr('value', res.id);
            id_field.appendTo('#files');
            $('#'+res.fileID).addClass('done').append(close_btn);
        },
        progress: function (e, data) {

            var progress = parseInt(data.loaded / data.total * 100, 10);
            $('#'+data.formData.fileID).css('width', progress+'%');
        }
    }).prop('disabled', !$.support.fileInput)
        .parent().addClass($.support.fileInput ? undefined : 'disabled');

    $('#File_name').hover(
        function(){
            $('.add_file').addClass('active_add');
        },
        function(){
            $('.add_file').removeClass('active_add');
    });

    document.ondragover = function(e){
        e.preventDefault();
        $('.add_file').addClass('active_add');
    };

    document.onmouseup = function() {
        $('.add_file').removeClass('active_add');
    };
});

function remove_file(id){
    var targ = document.getElementById(id).parentNode;
    targ.remove();
}
