var Filter = function()
{
    var self = this;

    this.ready = function()
    {
        $('#Filter_module').change(function(){
            $.getJSON('/filter/getModuleActions', {'moduleName': $(this).val()}, function(data){
                $('#Filter_action').val('').html('');
                $.each(data, function(){
                    $('#Filter_action').append('<option value="'+this+'">'+this+'</option>');
                })
            });
        })
    }
}
