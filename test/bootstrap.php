<?php

include(dirname(__FILE__).'/../core/include.php');
FConfig::getInstance();
FCore::getInstance()->initFramework(dirname(__FILE__).'/../');
FCore::includeDirectory(FConfig::$yiiOptions['basePath'].'/test/');
FCore::getInstance()->initDatabase(FConfig::$dbName, FConfig::$dbUser, FConfig::$dbPass);
require_once(dirname(__FILE__).'/WebTestCase.php');
require_once(FConfig::$yiiOptions['basePath'].'/yiit.php');
