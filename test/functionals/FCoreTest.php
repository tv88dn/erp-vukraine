<?php

require_once('../core/FCore.php');

/**
 * Class FCoreTest
 *
 * Класс тесирования Ядра
 */
class FCoreTest extends PHPUnit_Framework_TestCase
{

    public function testGetInstance()
    {
        $this->assertTrue(FCore::getInstance() instanceof FCore);
    }

    public function testRequestIsSet()
    {
        $this->assertTrue(FCore::getInstance()->request instanceof FRequest);
    }

    public function testHomeUrlIsSetAndRight()
    {
        $this->assertTrue(FCore::getInstance()->homeUrl === $_SERVER['HTTP_HOST']);
    }

    public function testWidgetFactory()
    {
        $this->assertTrue(FCore::getInstance()->getWidgetFactory() !== CWidgetFactory);
    }

    public function testApp()
    {
        $this->assertTrue(FCore::app() instanceof CWebApplication);
    }

    /**
     * Создает код подключения JS файла
     *
     * @param $moduleName имя модуля
     * @return string код подключения
     */
    private function makeJSInclusion($moduleName)
    {
        $filename = FConfig::$mainJsPath . ucfirst($moduleName) . '.js';
        return '<script language="javascript" type="text/javascript" src="/'.$filename.'"></script>';
    }

    public function testIncludeJsIsRight()
    {
        $core = FCore::getInstance()->getInstance();
        //Подключаем JS к модулю Content
        $moduleName = 'content';
        //Такой модуль еще не подключен
        $this->assertEquals($core->includeJs($moduleName), $this->makeJSInclusion($moduleName));
        //Повторное подключение не выполняется
        $this->assertEquals($core->includeJs($moduleName), '');
        //Выполняется принудительное подключение
        $this->assertEquals($core->includeJs($moduleName, true), $this->makeJSInclusion($moduleName));
        //Несуществующий файл не подключится
        $moduleName = 'JSFileForThisModuleNotExist';
        $this->assertEquals($core->includeJs($moduleName), '');
        //Принудительно несуществующий файл так же не выйдет подключить
        $this->assertEquals($core->includeJs($moduleName,true), '');
    }





}
 