-- phpMyAdmin SQL Dump
-- version 3.5.1
-- http://www.phpmyadmin.net
--
-- Хост: 127.0.0.1
-- Время создания: Мар 31 2015 г., 02:52
-- Версия сервера: 5.5.25
-- Версия PHP: 5.3.13

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- База данных: `newkernel`
--

-- --------------------------------------------------------

--
-- Структура таблицы `activity`
--

CREATE TABLE IF NOT EXISTS `activity` (
  `id` int(8) unsigned NOT NULL AUTO_INCREMENT,
  `userId` int(8) unsigned NOT NULL,
  `targetId` int(8) unsigned NOT NULL,
  `module` varchar(155) CHARACTER SET utf8 NOT NULL,
  `scenario` varchar(155) CHARACTER SET utf8 NOT NULL,
  `text` text CHARACTER SET utf8 NOT NULL,
  `time` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `userId` (`userId`,`targetId`)
) ENGINE=MyISAM  DEFAULT CHARSET=cp1251 AUTO_INCREMENT=58 ;

--
-- Дамп данных таблицы `activity`
--

INSERT INTO `activity` (`id`, `userId`, `targetId`, `module`, `scenario`, `text`, `time`) VALUES
(1, 23, 3, 'Task', 'update', '<div class="activity-item success">\r\n    <div class="activity-title"><span>пользователь</span>\r\n        <img class="contact-avatar" src="/resources/images/uploads/user/avatar/23" alt="Автор задачи"/>\r\n        <span>Вадим Трунов обновил задачу</span>\r\n    </div>\r\n            <div class="activity-body"><a href="/task/view?id=3"> Проверка вывода задач</a></div>\r\n        <div class="activity-status"><span class="label label-default"></span></div>\r\n    <div class="clearfix"></div>\r\n</div>', 1424438146),
(2, 23, 3, 'Task', 'update', '<div class="activity-item success">\r\n    <div class="activity-title"><span>пользователь</span>\r\n        <img class="contact-avatar" src="/resources/images/uploads/user/avatar/23" alt="Автор задачи"/>\r\n        <span>Вадим Трунов обновил задачу</span>\r\n    </div>\r\n            <div class="activity-body"><a href="/task/view?id=3"> Проверка вывода задач</a></div>\r\n        <div class="activity-status"><span class="label label-default"></span></div>\r\n    <div class="clearfix"></div>\r\n</div>', 1424438147),
(3, 24, 3, 'Task', 'update', '<div class="activity-item success">\r\n    <div class="activity-title"><span>пользователь</span>\r\n        <img class="contact-avatar" src="/resources/images/uploads/user/avatar/24" alt="Автор задачи"/>\r\n        <span>Ряхин Илья обновил задачу</span>\r\n    </div>\r\n            <div class="activity-body"><a href="/task/view?id=3"> Проверка вывода задач</a></div>\r\n        <div class="activity-status"><span class="label label-default"></span></div>\r\n    <div class="clearfix"></div>\r\n</div>', 1424439191),
(4, 1, 3, 'Task', 'update', '<div class="activity-item success">\r\n    <div class="activity-title"><span>пользователь</span>\r\n        <img class="contact-avatar" src="/resources/images/uploads/user/avatar/1" alt="Автор задачи"/>\r\n        <span>Админов Админ Админович обновил задачу</span>\r\n    </div>\r\n            <div class="activity-body"><a href="/task/view?id=3"> Проверка вывода задач</a></div>\r\n        <div class="activity-status"><span class="label label-default"></span></div>\r\n    <div class="clearfix"></div>\r\n</div>', 1424440842),
(5, 24, 3, 'Task', 'update', '<div class="activity-item success">\r\n    <div class="activity-title"><span>пользователь</span>\r\n        <img class="contact-avatar" src="/resources/images/uploads/user/avatar/24" alt="Автор задачи"/>\r\n        <span>Ряхин Илья обновил задачу</span>\r\n    </div>\r\n            <div class="activity-body"><a href="/task/view?id=3"> Проверка вывода задач</a></div>\r\n        <div class="activity-status"><span class="label label-default"></span></div>\r\n    <div class="clearfix"></div>\r\n</div>', 1424503502),
(6, 23, 3, 'Task', 'update', '<div class="activity-item success">\r\n    <div class="activity-title"><span>пользователь</span>\r\n        <img class="contact-avatar" src="/resources/images/uploads/user/avatar/23" alt="Автор задачи"/>\r\n        <span>Вадим Трунов обновил задачу</span>\r\n    </div>\r\n            <div class="activity-body"><a href="/task/view?id=3"> Проверка вывода задач</a></div>\r\n        <div class="activity-status"><span class="label label-default"></span></div>\r\n    <div class="clearfix"></div>\r\n</div>', 1424504439),
(7, 23, 3, 'Task', 'update', '<div class="activity-item success">\r\n    <div class="activity-title"><span>пользователь</span>\r\n        <img class="contact-avatar" src="/resources/images/uploads/user/avatar/23" alt="Автор задачи"/>\r\n        <span>Вадим Трунов обновил задачу</span>\r\n    </div>\r\n            <div class="activity-body"><a href="/task/view?id=3"> Проверка вывода задач</a></div>\r\n        <div class="activity-status"><span class="label label-default">Выполнена</span></div>\r\n    <div class="clearfix"></div>\r\n</div>', 1424509891),
(8, 1, 3, 'Task', 'update', '<div class="activity-item success">\r\n    <div class="activity-title"><span>пользователь</span>\r\n        <img class="contact-avatar" src="/resources/images/uploads/user/avatar/1" alt="Автор задачи"/>\r\n        <span>Админов Админ Админович обновил задачу</span>\r\n    </div>\r\n            <div class="activity-body"><a href="/task/view?id=3"> Проверка вывода задач</a></div>\r\n        <div class="activity-status"><span class="label label-default">Выполнена</span></div>\r\n    <div class="clearfix"></div>\r\n</div>', 1424509952),
(9, 23, 3, 'Task', 'update', '<div class="activity-item success">\r\n    <div class="activity-title"><span>пользователь</span>\r\n        <img class="contact-avatar" src="/resources/images/uploads/user/avatar/23" alt="Автор задачи"/>\r\n        <span>Вадим Трунов обновил задачу</span>\r\n    </div>\r\n            <div class="activity-body"><a href="/task/view?id=3"> Проверка вывода задач</a></div>\r\n        <div class="activity-status"><span class="label label-default">Выполнена</span></div>\r\n    <div class="clearfix"></div>\r\n</div>', 1424510264),
(10, 1, 3, 'Task', 'update', '<div class="activity-item success">\r\n    <div class="activity-title"><span>пользователь</span>\r\n        <img class="contact-avatar" src="/resources/images/uploads/user/avatar/1" alt="Автор задачи"/>\r\n        <span>Админов Админ Админович обновил задачу</span>\r\n    </div>\r\n            <div class="activity-body"><a href="/task/view?id=3"> Проверка вывода задач</a></div>\r\n        <div class="activity-status"><span class="label label-default">Выполнена</span></div>\r\n    <div class="clearfix"></div>\r\n</div>', 1424513895),
(11, 23, 3, 'Task', 'update', '<div class="activity-item success">\r\n    <div class="activity-title"><span>пользователь</span>\r\n        <img class="contact-avatar" src="/resources/images/uploads/user/avatar/23" alt="Автор задачи"/>\r\n        <span>Вадим Трунов обновил задачу</span>\r\n    </div>\r\n            <div class="activity-body"><a href="/task/view?id=3"> Проверка вывода задач</a></div>\r\n        <div class="activity-status"><span class="label label-default">Выполнена</span></div>\r\n    <div class="clearfix"></div>\r\n</div>', 1424514129),
(12, 1, 2, 'Task', 'update', '<div class="activity-item success">\r\n    <div class="activity-title"><span>пользователь</span>\r\n        <img class="contact-avatar" src="/resources/images/uploads/user/avatar/1" alt="Автор задачи"/>\r\n        <span>Админов Админ Админович обновил задачу</span>\r\n    </div>\r\n            <div class="activity-body"><a href="/task/view?id=2"> Тестирование ERP</a></div>\r\n        <div class="activity-status"><span class="label label-default"></span></div>\r\n    <div class="clearfix"></div>\r\n</div>', 1424759144),
(13, 23, 2, 'Task', 'update', '<div class="activity-item success">\r\n    <div class="activity-title"><span>пользователь</span>\r\n        <img class="contact-avatar" src="/resources/images/uploads/user/avatar/23" alt="Автор задачи"/>\r\n        <span>Вадим Трунов обновил задачу</span>\r\n    </div>\r\n            <div class="activity-body"><a href="/task/view?id=2"> Тестирование ERP</a></div>\r\n        <div class="activity-status"><span class="label label-default"></span></div>\r\n    <div class="clearfix"></div>\r\n</div>', 1424759188),
(14, 1, 4, 'Task', 'insert', '<div class="activity-item success">\r\n    <div class="activity-title"><span>пользователь</span>\r\n        <img class="contact-avatar" src="/resources/images/uploads/user/avatar/1" alt="Автор задачи"/>\r\n        <span>Админов Админ Админович добавил задачу</span>\r\n    </div>\r\n            <div class="activity-body"><a href="/task/view?id=4"> Тестируем</a></div>\r\n        <div class="activity-status"><span class="label label-default"></span></div>\r\n    <div class="clearfix"></div>\r\n</div>', 1425480373),
(15, 1, 2, 'Task', 'update', '<div class="activity-item success">\r\n    <div class="activity-title"><span>пользователь</span>\r\n        <img class="contact-avatar" src="/resources/images/uploads/user/avatar/1" alt="Автор задачи"/>\r\n        <span>Админов Админ Админович обновил задачу</span>\r\n    </div>\r\n            <div class="activity-body"><a href="/task/view?id=2"> Тестирование ERP</a></div>\r\n        <div class="activity-status"><span class="label label-default"></span></div>\r\n    <div class="clearfix"></div>\r\n</div>', 1425480384),
(16, 1, 5, 'Task', 'insert', '<div class="activity-item success">\r\n    <div class="activity-title"><span>пользователь</span>\r\n        <img class="contact-avatar" src="/resources/images/uploads/user/avatar/1" alt="Автор задачи"/>\r\n        <span>Админов Админ Админович добавил задачу</span>\r\n    </div>\r\n            <div class="activity-body"><a href="/task/view?id=5"> тестирую Петр</a></div>\r\n        <div class="activity-status"><span class="label label-default"></span></div>\r\n    <div class="clearfix"></div>\r\n</div>', 1425573650),
(17, 1, 5, 'Task', 'update', '<div class="activity-item success">\r\n    <div class="activity-title"><span>пользователь</span>\r\n        <img class="contact-avatar" src="/resources/images/uploads/user/avatar/1" alt="Автор задачи"/>\r\n        <span>Админов Админ Админович обновил задачу</span>\r\n    </div>\r\n            <div class="activity-body"><a href="/task/view?id=5"> тестирую Петр</a></div>\r\n        <div class="activity-status"><span class="label label-default"></span></div>\r\n    <div class="clearfix"></div>\r\n</div>', 1425573666),
(18, 1, 5, 'Task', 'update', '<div class="activity-item success">\r\n    <div class="activity-title"><span>пользователь</span>\r\n        <img class="contact-avatar" src="/resources/images/uploads/user/avatar/1" alt="Автор задачи"/>\r\n        <span>Админов Админ Админович обновил задачу</span>\r\n    </div>\r\n            <div class="activity-body"><a href="/task/view?id=5"> тестирую Петр</a></div>\r\n        <div class="activity-status"><span class="label label-default"></span></div>\r\n    <div class="clearfix"></div>\r\n</div>', 1425573679),
(19, 1, 5, 'Task', 'update', '<div class="activity-item success">\r\n    <div class="activity-title"><span>пользователь</span>\r\n        <img class="contact-avatar" src="/resources/images/uploads/user/avatar/1" alt="Автор задачи"/>\r\n        <span>Админов Админ Админович обновил задачу</span>\r\n    </div>\r\n            <div class="activity-body"><a href="/task/view?id=5"> тестирую Петр</a></div>\r\n        <div class="activity-status"><span class="label label-default"></span></div>\r\n    <div class="clearfix"></div>\r\n</div>', 1425573682),
(20, 1, 5, 'Task', 'update', '<div class="activity-item success">\r\n    <div class="activity-title"><span>пользователь</span>\r\n        <img class="contact-avatar" src="/resources/images/uploads/user/avatar/1" alt="Автор задачи"/>\r\n        <span>Админов Админ Админович обновил задачу</span>\r\n    </div>\r\n            <div class="activity-body"><a href="/task/view?id=5"> тестирую Петр</a></div>\r\n        <div class="activity-status"><span class="label label-default"></span></div>\r\n    <div class="clearfix"></div>\r\n</div>', 1425573699),
(21, 1, 5, 'Task', 'update', '<div class="activity-item success">\r\n    <div class="activity-title"><span>пользователь</span>\r\n        <img class="contact-avatar" src="/resources/images/uploads/user/avatar/1" alt="Автор задачи"/>\r\n        <span>Админов Админ Админович обновил задачу</span>\r\n    </div>\r\n            <div class="activity-body"><a href="/task/view?id=5"> тестирую Петр</a></div>\r\n        <div class="activity-status"><span class="label label-default"></span></div>\r\n    <div class="clearfix"></div>\r\n</div>', 1425573701),
(22, 1, 5, 'Task', 'update', '<div class="activity-item success">\r\n    <div class="activity-title"><span>пользователь</span>\r\n        <img class="contact-avatar" src="/resources/images/uploads/user/avatar/1" alt="Автор задачи"/>\r\n        <span>Админов Админ Админович обновил задачу</span>\r\n    </div>\r\n            <div class="activity-body"><a href="/task/view?id=5"> тестирую Петр</a></div>\r\n        <div class="activity-status"><span class="label label-default"></span></div>\r\n    <div class="clearfix"></div>\r\n</div>', 1425573702),
(23, 1, 5, 'Task', 'update', '<div class="activity-item success">\r\n    <div class="activity-title"><span>пользователь</span>\r\n        <img class="contact-avatar" src="/resources/images/uploads/user/avatar/1" alt="Автор задачи"/>\r\n        <span>Админов Админ Админович обновил задачу</span>\r\n    </div>\r\n            <div class="activity-body"><a href="/task/view?id=5"> тестирую Петр</a></div>\r\n        <div class="activity-status"><span class="label label-default"></span></div>\r\n    <div class="clearfix"></div>\r\n</div>', 1425573702),
(24, 1, 5, 'Task', 'update', '<div class="activity-item success">\r\n    <div class="activity-title"><span>пользователь</span>\r\n        <img class="contact-avatar" src="/resources/images/uploads/user/avatar/1" alt="Автор задачи"/>\r\n        <span>Админов Админ Админович обновил задачу</span>\r\n    </div>\r\n            <div class="activity-body"><a href="/task/view?id=5"> тестирую Петр</a></div>\r\n        <div class="activity-status"><span class="label label-default"></span></div>\r\n    <div class="clearfix"></div>\r\n</div>', 1425573703),
(25, 1, 5, 'Task', 'update', '<div class="activity-item success">\r\n    <div class="activity-title"><span>пользователь</span>\r\n        <img class="contact-avatar" src="/resources/images/uploads/user/avatar/1" alt="Автор задачи"/>\r\n        <span>Админов Админ Админович обновил задачу</span>\r\n    </div>\r\n            <div class="activity-body"><a href="/task/view?id=5"> тестирую Петр</a></div>\r\n        <div class="activity-status"><span class="label label-default"></span></div>\r\n    <div class="clearfix"></div>\r\n</div>', 1425573703),
(26, 1, 5, 'Task', 'update', '<div class="activity-item success">\r\n    <div class="activity-title"><span>пользователь</span>\r\n        <img class="contact-avatar" src="/resources/images/uploads/user/avatar/1" alt="Автор задачи"/>\r\n        <span>Админов Админ Админович обновил задачу</span>\r\n    </div>\r\n            <div class="activity-body"><a href="/task/view?id=5"> тестирую Петр</a></div>\r\n        <div class="activity-status"><span class="label label-default"></span></div>\r\n    <div class="clearfix"></div>\r\n</div>', 1425573704),
(27, 1, 5, 'Task', 'update', '<div class="activity-item success">\r\n    <div class="activity-title"><span>пользователь</span>\r\n        <img class="contact-avatar" src="/resources/images/uploads/user/avatar/1" alt="Автор задачи"/>\r\n        <span>Админов Админ Админович обновил задачу</span>\r\n    </div>\r\n            <div class="activity-body"><a href="/task/view?id=5"> тестирую Петр</a></div>\r\n        <div class="activity-status"><span class="label label-default"></span></div>\r\n    <div class="clearfix"></div>\r\n</div>', 1425573709),
(28, 1, 6, 'Task', 'insert', '<div class="activity-item success">\r\n    <div class="activity-title"><span>пользователь</span>\r\n        <img class="contact-avatar" src="/resources/images/uploads/user/avatar/1" alt="Автор задачи"/>\r\n        <span>Админов Админ Админович добавил задачу</span>\r\n    </div>\r\n            <div class="activity-body"><a href="/task/view?id=6"> Сделать вкладку</a></div>\r\n        <div class="activity-status"><span class="label label-default"></span></div>\r\n    <div class="clearfix"></div>\r\n</div>', 1426174872),
(29, 1, 6, 'Task', 'update', '<div class="activity-item success">\r\n    <div class="activity-title"><span>пользователь</span>\r\n        <img class="contact-avatar" src="/resources/images/uploads/user/avatar/1" alt="Автор задачи"/>\r\n        <span>Админов Админ Админович обновил задачу</span>\r\n    </div>\r\n            <div class="activity-body"><a href="/task/view?id=6"> Сделать вкладку</a></div>\r\n        <div class="activity-status"><span class="label label-default"></span></div>\r\n    <div class="clearfix"></div>\r\n</div>', 1426174896),
(30, 1, 20, 'Task', 'insert', '<div class="activity-item success">\r\n    <div class="activity-title"><span>пользователь</span>\r\n        <img class="contact-avatar" src="/resources/images/uploads/user/avatar/1" alt="Автор задачи"/>\r\n        <span>Админов Админ Админович добавил задачу</span>\r\n    </div>\r\n            <div class="activity-body"><a href="/task/view?id=20"> new task</a></div>\r\n        <div class="activity-status"><span class="label label-default"></span></div>\r\n    <div class="clearfix"></div>\r\n</div>', 0),
(31, 1, 21, 'Task', 'insert', '<div class="activity-item success">\r\n    <div class="activity-title"><span>пользователь</span>\r\n        <img class="contact-avatar" src="/resources/images/uploads/user/avatar/1" alt="Автор задачи"/>\r\n        <span>Админов Админ Админович добавил задачу</span>\r\n    </div>\r\n            <div class="activity-body"><a href="/task/view?id=21"> new task</a></div>\r\n        <div class="activity-status"><span class="label label-default"></span></div>\r\n    <div class="clearfix"></div>\r\n</div>', 0),
(32, 1, 22, 'Task', 'insert', '<div class="activity-item success">\r\n    <div class="activity-title"><span>пользователь</span>\r\n        <img class="contact-avatar" src="/resources/images/uploads/user/avatar/1" alt="Автор задачи"/>\r\n        <span>Админов Админ Админович добавил задачу</span>\r\n    </div>\r\n            <div class="activity-body"><a href="/task/view?id=22"> 1</a></div>\r\n        <div class="activity-status"><span class="label label-default"></span></div>\r\n    <div class="clearfix"></div>\r\n</div>', 0),
(33, 1, 23, 'Task', 'insert', '<div class="activity-item success">\r\n    <div class="activity-title"><span>пользователь</span>\r\n        <img class="contact-avatar" src="/resources/images/uploads/user/avatar/1" alt="Автор задачи"/>\r\n        <span>Админов Админ Админович добавил задачу</span>\r\n    </div>\r\n            <div class="activity-body"><a href="/task/view?id=23"> 1</a></div>\r\n        <div class="activity-status"><span class="label label-default"></span></div>\r\n    <div class="clearfix"></div>\r\n</div>', 0),
(34, 1, 24, 'Task', 'insert', '<div class="activity-item success">\r\n    <div class="activity-title"><span>пользователь</span>\r\n        <img class="contact-avatar" src="/resources/images/uploads/user/avatar/1" alt="Автор задачи"/>\r\n        <span>Админов Админ Админович добавил задачу</span>\r\n    </div>\r\n            <div class="activity-body"><a href="/task/view?id=24"> new task</a></div>\r\n        <div class="activity-status"><span class="label label-default"></span></div>\r\n    <div class="clearfix"></div>\r\n</div>', 0),
(35, 1, 24, 'Task', 'update', '<div class="activity-item success">\r\n    <div class="activity-title"><span>пользователь</span>\r\n        <img class="contact-avatar" src="/resources/images/uploads/user/avatar/1" alt="Автор задачи"/>\r\n        <span>Админов Админ Админович обновил задачу</span>\r\n    </div>\r\n            <div class="activity-body"><a href="/task/view?id=24"> new task</a></div>\r\n        <div class="activity-status"><span class="label label-default"></span></div>\r\n    <div class="clearfix"></div>\r\n</div>', 0),
(36, 1, 25, 'Task', 'insert', '<div class="activity-item success">\r\n    <div class="activity-title"><span>пользователь</span>\r\n        <img class="contact-avatar" src="/resources/images/uploads/user/avatar/1" alt="Автор задачи"/>\r\n        <span>Админов Админ Админович добавил задачу</span>\r\n    </div>\r\n            <div class="activity-body"><a href="/task/view?id=25"> mine task</a></div>\r\n        <div class="activity-status"><span class="label label-default"></span></div>\r\n    <div class="clearfix"></div>\r\n</div>', 0),
(37, 1, 26, 'Task', 'insert', '<div class="activity-item success">\r\n    <div class="activity-title"><span>пользователь</span>\r\n        <img class="contact-avatar" src="/resources/images/uploads/user/avatar/1" alt="Автор задачи"/>\r\n        <span>Админов Админ Админович добавил задачу</span>\r\n    </div>\r\n            <div class="activity-body"><a href="/task/view?id=26"> mine task</a></div>\r\n        <div class="activity-status"><span class="label label-default"></span></div>\r\n    <div class="clearfix"></div>\r\n</div>', 0),
(38, 1, 26, 'Task', 'update', '<div class="activity-item success">\r\n    <div class="activity-title"><span>пользователь</span>\r\n        <img class="contact-avatar" src="/resources/images/uploads/user/avatar/1" alt="Автор задачи"/>\r\n        <span>Админов Админ Админович обновил задачу</span>\r\n    </div>\r\n            <div class="activity-body"><a href="/task/view?id=26"> mine task</a></div>\r\n        <div class="activity-status"><span class="label label-default"></span></div>\r\n    <div class="clearfix"></div>\r\n</div>', 0),
(39, 1, 26, 'Task', 'update', '<div class="activity-item success">\r\n    <div class="activity-title"><span>пользователь</span>\r\n        <img class="contact-avatar" src="/resources/images/uploads/user/avatar/1" alt="Автор задачи"/>\r\n        <span>Админов Админ Админович обновил задачу</span>\r\n    </div>\r\n            <div class="activity-body"><a href="/task/view?id=26"> mine task</a></div>\r\n        <div class="activity-status"><span class="label label-default"></span></div>\r\n    <div class="clearfix"></div>\r\n</div>', 0),
(40, 1, 26, 'Task', 'update', '<div class="activity-item success">\r\n    <div class="activity-title"><span>пользователь</span>\r\n        <img class="contact-avatar" src="/resources/images/uploads/user/avatar/1" alt="Автор задачи"/>\r\n        <span>Админов Админ Админович обновил задачу</span>\r\n    </div>\r\n            <div class="activity-body"><a href="/task/view?id=26"> mine task</a></div>\r\n        <div class="activity-status"><span class="label label-default"></span></div>\r\n    <div class="clearfix"></div>\r\n</div>', 0),
(41, 1, 26, 'Task', 'update', '<div class="activity-item success">\r\n    <div class="activity-title"><span>пользователь</span>\r\n        <img class="contact-avatar" src="/resources/images/uploads/user/avatar/1" alt="Автор задачи"/>\r\n        <span>Админов Админ Админович обновил задачу</span>\r\n    </div>\r\n            <div class="activity-body"><a href="/task/view?id=26"> mine task</a></div>\r\n        <div class="activity-status"><span class="label label-default"></span></div>\r\n    <div class="clearfix"></div>\r\n</div>', 0),
(42, 1, 27, 'Task', 'insert', '<div class="activity-item success">\r\n    <div class="activity-title"><span>пользователь</span>\r\n        <img class="contact-avatar" src="/resources/images/uploads/user/avatar/1" alt="Автор задачи"/>\r\n        <span>Админов Админ Админович добавил задачу</span>\r\n    </div>\r\n            <div class="activity-body"><a href="/task/view?id=27"> уццу</a></div>\r\n        <div class="activity-status"><span class="label label-default"></span></div>\r\n    <div class="clearfix"></div>\r\n</div>', 0),
(43, 1, 28, 'Task', 'insert', '<div class="activity-item success">\r\n    <div class="activity-title"><span>пользователь</span>\r\n        <img class="contact-avatar" src="/resources/images/uploads/user/avatar/1" alt="Автор задачи"/>\r\n        <span>Админов Админ Админович добавил задачу</span>\r\n    </div>\r\n            <div class="activity-body"><a href="/task/view?id=28"> ntcn</a></div>\r\n        <div class="activity-status"><span class="label label-default"></span></div>\r\n    <div class="clearfix"></div>\r\n</div>', 0),
(44, 1, 29, 'Task', 'insert', '<div class="activity-item success">\r\n    <div class="activity-title"><span>пользователь</span>\r\n        <img class="contact-avatar" src="/resources/images/uploads/user/avatar/1" alt="Автор задачи"/>\r\n        <span>Админов Админ Админович добавил задачу</span>\r\n    </div>\r\n            <div class="activity-body"><a href="/task/view?id=29"> news</a></div>\r\n        <div class="activity-status"><span class="label label-default"></span></div>\r\n    <div class="clearfix"></div>\r\n</div>', 0),
(45, 1, 30, 'Task', 'insert', '<div class="activity-item success">\r\n    <div class="activity-title"><span>пользователь</span>\r\n        <img class="contact-avatar" src="/resources/images/uploads/user/avatar/1" alt="Автор задачи"/>\r\n        <span>Админов Админ Админович добавил задачу</span>\r\n    </div>\r\n            <div class="activity-body"><a href="/task/view?id=30"> dsfdsf</a></div>\r\n        <div class="activity-status"><span class="label label-default"></span></div>\r\n    <div class="clearfix"></div>\r\n</div>', 0),
(46, 1, 31, 'Task', 'insert', '<div class="activity-item success">\r\n    <div class="activity-title"><span>пользователь</span>\r\n        <img class="contact-avatar" src="/resources/images/uploads/user/avatar/1" alt="Автор задачи"/>\r\n        <span>Админов Админ Админович добавил задачу</span>\r\n    </div>\r\n            <div class="activity-body"><a href="/task/view?id=31"> yyy</a></div>\r\n        <div class="activity-status"><span class="label label-default"></span></div>\r\n    <div class="clearfix"></div>\r\n</div>', 0),
(47, 1, 32, 'Task', 'insert', '<div class="activity-item success">\r\n    <div class="activity-title"><span>пользователь</span>\r\n        <img class="contact-avatar" src="/resources/images/uploads/user/avatar/1" alt="Автор задачи"/>\r\n        <span>Админов Админ Админович добавил задачу</span>\r\n    </div>\r\n            <div class="activity-body"><a href="/task/view?id=32"> таск</a></div>\r\n        <div class="activity-status"><span class="label label-default"></span></div>\r\n    <div class="clearfix"></div>\r\n</div>', 0),
(48, 1, 33, 'Task', 'insert', '<div class="activity-item success">\r\n    <div class="activity-title"><span>пользователь</span>\r\n        <img class="contact-avatar" src="/resources/images/uploads/user/avatar/1" alt="Автор задачи"/>\r\n        <span>Админов Админ Админович добавил задачу</span>\r\n    </div>\r\n            <div class="activity-body"><a href="/task/view?id=33"> dsda</a></div>\r\n        <div class="activity-status"><span class="label label-default"></span></div>\r\n    <div class="clearfix"></div>\r\n</div>', 0),
(49, 1, 33, 'Task', 'update', '<div class="activity-item success">\r\n    <div class="activity-title"><span>пользователь</span>\r\n        <img class="contact-avatar" src="/resources/images/uploads/user/avatar/1" alt="Автор задачи"/>\r\n        <span>Админов Админ Админович обновил задачу</span>\r\n    </div>\r\n            <div class="activity-body"><a href="/task/view?id=33"> dsda</a></div>\r\n        <div class="activity-status"><span class="label label-default"></span></div>\r\n    <div class="clearfix"></div>\r\n</div>', 0),
(50, 1, 34, 'Task', 'insert', '<div class="activity-item success">\r\n    <div class="activity-title"><span>пользователь</span>\r\n        <img class="contact-avatar" src="/resources/images/uploads/user/avatar/1" alt="Автор задачи"/>\r\n        <span>Админов Админ Админович добавил задачу</span>\r\n    </div>\r\n            <div class="activity-body"><a href="/task/view?id=34"> bffggf</a></div>\r\n        <div class="activity-status"><span class="label label-default"></span></div>\r\n    <div class="clearfix"></div>\r\n</div>', 0),
(51, 1, 34, 'Task', 'update', '<div class="activity-item success">\r\n    <div class="activity-title"><span>пользователь</span>\r\n        <img class="contact-avatar" src="/resources/images/uploads/user/avatar/1" alt="Автор задачи"/>\r\n        <span>Админов Админ Админович обновил задачу</span>\r\n    </div>\r\n            <div class="activity-body"><a href="/task/view?id=34"> bffggf</a></div>\r\n        <div class="activity-status"><span class="label label-default"></span></div>\r\n    <div class="clearfix"></div>\r\n</div>', 0),
(52, 1, 34, 'Task', 'update', '<div class="activity-item success">\r\n    <div class="activity-title"><span>пользователь</span>\r\n        <img class="contact-avatar" src="/resources/images/uploads/user/avatar/1" alt="Автор задачи"/>\r\n        <span>Админов Админ Админович обновил задачу</span>\r\n    </div>\r\n            <div class="activity-body"><a href="/task/view?id=34"> bffggf</a></div>\r\n        <div class="activity-status"><span class="label label-default"></span></div>\r\n    <div class="clearfix"></div>\r\n</div>', 0),
(53, 1, 34, 'Task', 'update', '<div class="activity-item success">\r\n    <div class="activity-title"><span>пользователь</span>\r\n        <img class="contact-avatar" src="/resources/images/uploads/user/avatar/1" alt="Автор задачи"/>\r\n        <span>Админов Админ Админович обновил задачу</span>\r\n    </div>\r\n            <div class="activity-body"><a href="/task/view?id=34"> bffggf</a></div>\r\n        <div class="activity-status"><span class="label label-default"></span></div>\r\n    <div class="clearfix"></div>\r\n</div>', 0),
(54, 1, 35, 'Task', 'insert', '<div class="activity-item success">\r\n    <div class="activity-title"><span>пользователь</span>\r\n        <img class="contact-avatar" src="/resources/images/uploads/user/avatar/1" alt="Автор задачи"/>\r\n        <span>Админов Админ Админович добавил задачу</span>\r\n    </div>\r\n            <div class="activity-body"><a href="/task/view?id=35"> 21312</a></div>\r\n        <div class="activity-status"><span class="label label-default"></span></div>\r\n    <div class="clearfix"></div>\r\n</div>', 0),
(55, 1, 36, 'Task', 'insert', '<div class="activity-item success">\r\n    <div class="activity-title"><span>пользователь</span>\r\n        <img class="contact-avatar" src="/resources/images/uploads/user/avatar/1" alt="Автор задачи"/>\r\n        <span>Админов Админ Админович добавил задачу</span>\r\n    </div>\r\n            <div class="activity-body"><a href="/task/view?id=36"> 21312</a></div>\r\n        <div class="activity-status"><span class="label label-default"></span></div>\r\n    <div class="clearfix"></div>\r\n</div>', 0),
(56, 1, 40, 'Task', 'insert', '<div class="activity-item success">\r\n    <div class="activity-title"><span>пользователь</span>\r\n        <img class="contact-avatar" src="/resources/images/uploads/user/avatar/1" alt="Автор задачи"/>\r\n        <span>Админов Админ Админович добавил задачу</span>\r\n    </div>\r\n            <div class="activity-body"><a href="/task/view?id=40"> 121</a></div>\r\n        <div class="activity-status"><span class="label label-default"></span></div>\r\n    <div class="clearfix"></div>\r\n</div>', 0),
(57, 1, 41, 'Task', 'insert', '<div class="activity-item success">\r\n    <div class="activity-title"><span>пользователь</span>\r\n        <img class="contact-avatar" src="/resources/images/uploads/user/avatar/1" alt="Автор задачи"/>\r\n        <span>Админов Админ Админович добавил задачу</span>\r\n    </div>\r\n            <div class="activity-body"><a href="/task/view?id=41"> 121</a></div>\r\n        <div class="activity-status"><span class="label label-default"></span></div>\r\n    <div class="clearfix"></div>\r\n</div>', 0);

-- --------------------------------------------------------

--
-- Структура таблицы `activityrelations`
--

CREATE TABLE IF NOT EXISTS `activityrelations` (
  `id` int(8) NOT NULL AUTO_INCREMENT,
  `userId` int(8) NOT NULL,
  `activityId` int(8) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `userId` (`userId`,`activityId`)
) ENGINE=MyISAM DEFAULT CHARSET=cp1251 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура таблицы `agreement`
--

CREATE TABLE IF NOT EXISTS `agreement` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `taskId` int(11) NOT NULL,
  `userId` int(11) NOT NULL,
  PRIMARY KEY (`taskId`,`userId`),
  UNIQUE KEY `id` (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=cp1251 AUTO_INCREMENT=27 ;

--
-- Дамп данных таблицы `agreement`
--

INSERT INTO `agreement` (`id`, `taskId`, `userId`) VALUES
(12, 42, 19),
(11, 42, 24),
(10, 42, 22),
(9, 41, 19),
(8, 41, 24),
(7, 41, 22),
(13, 1, 1),
(14, 1, 22),
(15, 1, 19),
(16, 2, 19),
(17, 2, 1),
(18, 2, 22),
(19, 3, 26),
(20, 3, 19),
(21, 3, 24),
(22, 4, 23),
(23, 5, 23),
(24, 5, 22),
(25, 6, 1),
(26, 34, 23);

-- --------------------------------------------------------

--
-- Структура таблицы `appeal`
--

CREATE TABLE IF NOT EXISTS `appeal` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `companyId` int(10) unsigned NOT NULL,
  `projectId` int(11) NOT NULL DEFAULT '0',
  `userId` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `companyId` (`companyId`,`userId`)
) ENGINE=MyISAM DEFAULT CHARSET=cp1251 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура таблицы `brand`
--

CREATE TABLE IF NOT EXISTS `brand` (
  `id` int(8) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8 NOT NULL,
  `description` text CHARACTER SET utf8 NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=cp1251 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура таблицы `cases`
--

CREATE TABLE IF NOT EXISTS `cases` (
  `id` int(8) unsigned NOT NULL AUTO_INCREMENT,
  `add_address` text,
  `date` varchar(100) DEFAULT NULL,
  `description` text,
  `forma` varchar(200) DEFAULT NULL,
  `involved` text,
  `judge` varchar(250) DEFAULT NULL,
  `number` varchar(100) DEFAULT NULL,
  `courtId` int(5) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура таблицы `comments`
--

CREATE TABLE IF NOT EXISTS `comments` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `userId` int(10) unsigned NOT NULL,
  `model` varchar(255) NOT NULL,
  `targetId` int(10) unsigned NOT NULL,
  `parentId` int(8) NOT NULL DEFAULT '0',
  `text` text NOT NULL,
  PRIMARY KEY (`id`),
  KEY `userId` (`userId`,`targetId`),
  KEY `parentId` (`parentId`)
) ENGINE=MyISAM  DEFAULT CHARSET=cp1251 AUTO_INCREMENT=3 ;

--
-- Дамп данных таблицы `comments`
--

INSERT INTO `comments` (`id`, `userId`, `model`, `targetId`, `parentId`, `text`) VALUES
(2, 1, 'Task', 3, 0, '<p>Во входящие вроде бы выводятся</p><p>&nbsp;</p>');

-- --------------------------------------------------------

--
-- Структура таблицы `company`
--

CREATE TABLE IF NOT EXISTS `company` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `manager` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `manager` (`manager`)
) ENGINE=MyISAM  DEFAULT CHARSET=cp1251 AUTO_INCREMENT=10 ;

--
-- Дамп данных таблицы `company`
--

INSERT INTO `company` (`id`, `name`, `manager`) VALUES
(1, 'lookmy.info', 1),
(2, 'интернет-магазин детских игрушек Happyland', 1),
(3, 'ВУСО. Страховая компания', 1),
(4, 'Green Мебель', 1),
(5, 'Sambooka. Туристическая компания', 1),
(6, 'Магазин мебели GoldenPlaza', 23),
(7, 'социальная сеть ВУкраине', 23),
(8, 'РОССИГР', 23),
(9, 'Robotnic ent.', 26);

-- --------------------------------------------------------

--
-- Структура таблицы `content`
--

CREATE TABLE IF NOT EXISTS `content` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `parentId` int(10) NOT NULL,
  `title` varchar(250) NOT NULL,
  `description` text,
  PRIMARY KEY (`id`),
  KEY `title` (`title`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=12 ;

--
-- Дамп данных таблицы `content`
--

INSERT INTO `content` (`id`, `parentId`, `title`, `description`) VALUES
(1, -1, 'Настройки сайта', 'Общие настройки, которые используются в целом для сайта'),
(2, -1, 'Главное меню', NULL),
(3, 2, 'Что такое XEYIRLI', NULL),
(4, 2, 'Как зарабатывать бонусы', NULL),
(5, 2, 'Как тратить бонусы', NULL),
(6, 2, 'Партнерская сеть', NULL),
(7, 2, 'Компании', NULL),
(8, -1, 'Слайдер на главной', NULL),
(9, 8, 'Что такое XEYIRLI', NULL),
(10, 8, 'Слайд 2', NULL),
(11, 8, 'Слайд 3', NULL);

-- --------------------------------------------------------

--
-- Структура таблицы `details`
--

CREATE TABLE IF NOT EXISTS `details` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `parentId` int(10) unsigned NOT NULL,
  `moduleId` int(10) unsigned NOT NULL,
  `data` text NOT NULL,
  PRIMARY KEY (`id`),
  KEY `typeId` (`moduleId`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=982 ;

--
-- Дамп данных таблицы `details`
--

INSERT INTO `details` (`id`, `parentId`, `moduleId`, `data`) VALUES
(1, 3, 1, 'Админов Админ Админович'),
(3, 15, 1, 'skype'),
(4, 16, 1, 'admins'),
(5, 17, 1, '1'),
(6, 18, 1, 'admin@lookmy.info'),
(36, 5, 1, '+38(033) 333-3333'),
(37, 5, 1, '+38(022) 222-2222'),
(39, 5, 1, '+38(011) 111-1111'),
(41, 19, 1, '+38(011) 111-1111'),
(42, 20, 1, 'lookmy.info'),
(43, 21, 1, 'Донецк'),
(44, 19, 2, '+38(011) 111-1111'),
(45, 20, 2, 'happyland.com.ua'),
(46, 21, 2, 'Харьков'),
(47, 19, 4, '+38(011) 111-1111'),
(48, 20, 4, 'http://green-mebel.dn.ua/'),
(49, 21, 4, 'Киев'),
(50, 24, 4, 'client'),
(51, 25, 4, 'info@company.com'),
(52, 26, 4, 'notSet'),
(53, 19, 5, '+38(011) 111-3333'),
(54, 20, 5, 'http://sambooka.com.ua/'),
(55, 21, 5, 'Донецк'),
(56, 24, 5, 'client'),
(57, 25, 5, 'info@company.com'),
(58, 26, 5, 'notSet'),
(59, 24, 6, 'client'),
(60, 26, 6, 'notSet'),
(61, 24, 7, 'client'),
(62, 26, 7, 'notSet'),
(63, 22, 1, 'Краснофлотская 56'),
(64, 24, 1, 'client'),
(65, 26, 1, 'notSet'),
(66, 33, 1, 'info@lookmy.info'),
(67, 29, 2, 'Директор'),
(68, 30, 2, 'Анедченко Александр Сергеевич'),
(69, 31, 2, '+38(011) 111-1111'),
(70, 35, 2, '1'),
(71, 37, 2, 'contact'),
(72, 29, 3, 'директор'),
(73, 30, 3, 'Анедченко Александр Сергеевич'),
(74, 31, 3, '+38(011) 111-1111'),
(75, 35, 3, '1'),
(76, 37, 3, 'contact'),
(77, 29, 4, 'TL'),
(78, 30, 4, 'Трунов Вадим Юрьевич'),
(79, 31, 4, '+38(063) 256-0214'),
(80, 32, 4, 'tv88dn'),
(81, 34, 4, 'trunov@lookmy.info'),
(82, 35, 4, '1'),
(83, 37, 4, 'contact'),
(84, 29, 5, 'программист'),
(85, 30, 5, 'Могильный Сергей Сергеевич'),
(86, 34, 5, 'sergey@lookmy.info'),
(87, 35, 5, '1'),
(88, 37, 5, 'contact'),
(89, 31, 4, '+38(095) 290-3046'),
(90, 32, 2, '0'),
(91, 39, 1, '1410733686_logo-lookmy.JPG'),
(92, 24, 2, 'client'),
(93, 26, 2, 'notSet'),
(94, 33, 2, 'info@happyland.com.ua'),
(95, 39, 2, '1411295150_hl-logo.JPG'),
(96, 29, 6, 'Директор'),
(97, 30, 6, 'Ильин Артем'),
(98, 29, 7, 'Директор'),
(99, 30, 7, 'Ильин Артем'),
(100, 29, 8, 'Директор'),
(101, 30, 8, 'Ильин Артем'),
(102, 29, 9, 'Директор'),
(103, 30, 9, 'Ильин Артем'),
(104, 29, 10, 'Директор'),
(105, 30, 10, 'Ильин Артем'),
(106, 29, 11, 'Директор'),
(107, 30, 11, 'Ильин Артем'),
(108, 29, 12, 'Директор'),
(109, 30, 12, 'Ильин Артем'),
(110, 29, 13, 'Директор'),
(111, 30, 13, 'Ильин Артем'),
(112, 38, 2, '+38(097) 431-9685'),
(113, 38, 2, '+38(050) 193-6332'),
(114, 38, 2, '+38(063) 446-5886'),
(115, 29, 14, 'Директор'),
(116, 30, 14, 'Артем Ильин'),
(117, 31, 14, '+38(095) 897-5187'),
(118, 34, 14, 'ilienart@gmail.com'),
(119, 35, 14, '2'),
(120, 37, 14, 'contact'),
(121, 20, 3, 'http://vuso.ua/'),
(122, 21, 3, 'Киев'),
(123, 22, 3, 'ул. Казимира Малевича, 31'),
(124, 24, 3, 'client'),
(125, 26, 3, 'studio'),
(126, 39, 3, '1411339334_vuso-logo.JPG'),
(127, 29, 15, 'Cпециалист отдела интернет-продаж'),
(128, 30, 15, 'Панарин Андрей'),
(129, 31, 15, '+38(050) 811-1543'),
(130, 34, 15, 'panarin@vuso.ua'),
(131, 35, 15, '3'),
(132, 37, 15, 'contact'),
(133, 29, 16, 'специалист по связям с общественностью / pr-менеджер'),
(134, 30, 16, 'Соколов Дмитрий Константинович'),
(135, 31, 16, '+38(066) 205-6701'),
(136, 34, 16, 'sokolovd@vuso.ua '),
(137, 35, 16, '3'),
(138, 37, 16, 'contact'),
(139, 39, 4, '1411339822_green-logo.JPG'),
(140, 29, 17, 'Директор'),
(141, 30, 17, 'Даниил Цапко'),
(142, 31, 17, '+38(099) 038-8919'),
(143, 34, 17, 'daniel-ts@yandex.ru'),
(144, 35, 17, '4'),
(145, 37, 17, 'contact'),
(146, 38, 4, '+38(066) 780-7125'),
(147, 38, 4, '+38(063) 330-2611'),
(148, 22, 5, 'пр. Ватутина, 21'),
(149, 33, 5, 'sambooka@sambooka.com.ua'),
(150, 38, 5, '+38(050) 900-2008'),
(151, 39, 5, '1411340829_sambooka-logo.JPG'),
(152, 40, 1, 'kernel'),
(153, 41, 1, '2014-09-25'),
(154, 42, 1, '2014-09-26'),
(155, 43, 1, '2'),
(156, 44, 1, 'incoming'),
(157, 40, 2, 'Определяет параметры видимости текста в блоке, если текст целиком не помещается в заданную область. Возможны два варианта: текст обрезается; текст обрезается и к концу строки добавляется многоточие. text-overflow работает в том случае, если для блока значение свойства overflow установлено как auto, scroll или hidden.Определяет параметры видимости текста в блоке, если текст целиком не помещается в заданную область. Возможны два варианта: текст обрезается; текст обрезается и к концу строки добавляется многоточие. text-overflow работает в том случае, если для блока значение свойства overflow установлено как auto, scroll или hidden.\r\nОпределяет параметры видимости текста в блоке, если текст целиком не помещается в заданную область. Возможны два варианта: текст обрезается; текст обрезается и к концу строки добавляется многоточие. text-overflow работает в том случае, если для блока значение свойства overflow установлено как auto, scroll или hidden.\r\nОпределяет параметры видимости текста в блоке, если текст целиком не помещается в заданную область. Возможны два варианта: текст обрезается; текст обрезается и к концу строки добавляется многоточие. text-overflow работает в том случае, если для блока значение свойства overflow установлено как auto, scroll или hidden.'),
(158, 41, 2, '2014-09-25'),
(159, 42, 2, '2014-09-28'),
(160, 43, 2, '5'),
(161, 44, 2, 'outgoing'),
(162, 40, 3, 'Еще одно обращение'),
(163, 41, 3, '2014-09-25'),
(164, 42, 3, '2014-09-28'),
(165, 43, 3, '2'),
(166, 44, 3, 'incoming'),
(167, 40, 4, 'Следующее обращение'),
(168, 41, 4, '2014-09-25'),
(169, 42, 4, '2014-09-30'),
(170, 43, 4, '4'),
(171, 44, 4, 'outgoing'),
(172, 45, 4, '1411690441_tumblr_ksbgh7OhyO1qzn2jso1_500.jpg'),
(173, 46, 1, '1411692026_uKRDb_iTpOs.jpg'),
(174, 45, 2, '1414344048_1411692611_upload65tnauv2d0.jpg'),
(175, 45, 5, '1411692819_Shirase-avatar-3404652955-7.png'),
(176, 45, 14, '1411692936_DSC_8495.jpg'),
(177, 45, 17, '1411692987_DSCN1181.JPG'),
(178, 29, 18, 'директор'),
(179, 30, 18, 'Мария'),
(180, 35, 18, '5'),
(181, 37, 18, 'contact'),
(182, 3, 19, 'Митичкин Андрей'),
(183, 5, 19, '+38(095) 367-7823'),
(184, 16, 19, 'programmers'),
(185, 17, 19, '1'),
(186, 18, 19, 'mitich@lookmy.info'),
(187, 28, 19, 'lookmy.info'),
(188, 46, 19, '1411901666_bali-tiger-habitat.jpg'),
(189, 3, 20, 'Антон Букин'),
(190, 5, 20, '+38(050) 806-3107'),
(191, 16, 20, 'managers'),
(192, 17, 20, '1'),
(193, 18, 20, 'anton@lookmy.info'),
(194, 28, 20, 'lookmy.info'),
(195, 47, 1, 'development'),
(196, 48, 1, '2014-10-02'),
(197, 49, 1, '2014-10-30'),
(198, 50, 1, '1000'),
(199, 51, 1, '32'),
(200, 53, 1, '1'),
(201, 55, 1, '19'),
(202, 47, 2, 'development'),
(203, 52, 2, '<p>Разработка новой редакции CRM-системы KERNEL. Программный продук должен состоять из следующих модулей:</p>\r\n\r\n<ul>\r\n	<li>Компании</li>\r\n	<li>Пользователи (Контакты)</li>\r\n	<li>Проекты</li>\r\n	<li>Задачи</li>\r\n	<li>Обращения</li>\r\n	<li>Оплаты</li>\r\n	<li>Файлы</li>\r\n	<li>Таймер и учет времени</li>\r\n	<li>Отчеты, контрольработы компании. Подсчет расходов и з/п сотрудников. Рентабельности проектов.</li>\r\n</ul>\r\n'),
(204, 53, 2, '1'),
(205, 55, 2, '23'),
(206, 48, 2, '2014-10-05'),
(207, 49, 2, '2014-12-31'),
(208, 56, 3, '1412542266'),
(209, 50, 2, '1000'),
(210, 51, 2, '32'),
(211, 41, 5, '2014-10-10'),
(212, 42, 5, '2014-10-15'),
(213, 43, 5, '5'),
(214, 44, 5, 'incoming'),
(215, 54, 5, 'class'),
(216, 57, 5, '2'),
(217, 40, 5, '<p>Начинаем проект</p>'),
(218, 40, 6, '<p>Еще одно обращение</p>'),
(219, 41, 6, '2014-10-11'),
(220, 42, 6, '2014-10-20'),
(221, 43, 6, '4'),
(222, 44, 6, 'outgoing'),
(223, 40, 7, '<p>Новое по проекту</p>'),
(224, 41, 7, '2014-10-11'),
(225, 42, 7, '2014-10-15'),
(226, 43, 7, '2'),
(227, 44, 7, 'outgoing'),
(228, 58, 1, '20.02.2015'),
(229, 59, 1, '23.02.2015'),
(230, 60, 1, ''),
(231, 63, 1, ''),
(232, 67, 1, 'Простестировать модели:\r\n- задачи\r\n- файлы\r\n- комментарии'),
(233, 58, 2, '20.02.2015'),
(234, 59, 2, '27.02.2015'),
(235, 60, 2, ''),
(236, 63, 2, ''),
(237, 67, 2, 'Необходимо протестировать следующие модули:\r\n- задачи\r\n- файлы\r\n- комментарии'),
(238, 56, 4, '1413070479'),
(239, 58, 3, '20.02.2015'),
(240, 59, 3, '20.02.2015'),
(241, 60, 3, ''),
(242, 63, 3, 'make'),
(243, 67, 3, 'Протестировать вывод задач в каждой папке'),
(244, 58, 4, '04.03.2015'),
(245, 59, 4, ''),
(246, 60, 4, ''),
(247, 63, 4, ''),
(248, 67, 4, ''),
(249, 58, 5, '05.03.2015'),
(250, 59, 5, '12.03.2015'),
(251, 63, 5, ''),
(252, 67, 5, 'Не понятно где будет этот текст'),
(253, 68, 4, '4'),
(254, 69, 4, ''),
(255, 68, 3, '1'),
(256, 69, 3, ''),
(257, 56, 5, '1413269326'),
(258, 70, 1, '1424357220'),
(259, 70, 2, '1424426679'),
(260, 70, 3, '1416522850'),
(261, 71, 5, '1413384118'),
(262, 71, 1, '1413455406'),
(263, 72, 1, 'nonecache2'),
(264, 73, 1, 'bannerLookMy'),
(265, 74, 1, '2014-10-21'),
(266, 75, 1, '<p>Что-то заплатили</p>'),
(267, 3, 21, 'Трунов Вадим'),
(268, 17, 21, '1'),
(269, 18, 21, 'tv88dn@gmail.com'),
(270, 58, 6, '17.03.2015'),
(271, 59, 6, '19.03.2015'),
(272, 67, 6, 's-kvartal.kiev.ua/'),
(273, 68, 6, '4'),
(274, 69, 6, ''),
(275, 56, 6, '1413730092'),
(276, 68, 2, ''),
(277, 69, 2, ''),
(278, 68, 5, '1'),
(279, 69, 5, ''),
(280, 68, 1, ''),
(281, 69, 1, ''),
(282, 3, 22, 'Aleksandr Anedchenko'),
(283, 17, 22, '1'),
(284, 18, 22, 'alex@lookmy.info'),
(285, 71, 2, '1414353200'),
(286, 72, 2, 'cash'),
(287, 73, 2, 'sell'),
(288, 74, 2, '2014-10-31'),
(289, 75, 2, '<p>Текст, текст, текст</p>'),
(290, 77, 2, '1'),
(291, 79, 2, 'doNotSend'),
(292, 34, 2, '0'),
(293, 60, 6, ''),
(294, 63, 6, ''),
(295, 58, 7, '20.03.2015'),
(296, 59, 7, ''),
(297, 60, 7, ''),
(298, 63, 7, ''),
(299, 67, 7, ''),
(300, 68, 7, ''),
(301, 69, 7, ''),
(302, 80, 7, ''),
(303, 80, 4, ''),
(304, 80, 3, '1'),
(305, 58, 8, '20.03.2015'),
(306, 59, 8, ''),
(307, 60, 8, ''),
(308, 63, 8, ''),
(309, 67, 8, ''),
(310, 68, 8, ''),
(311, 69, 8, ''),
(312, 80, 8, ''),
(313, 70, 4, '1416522961'),
(314, 58, 9, '23.03.2015'),
(315, 59, 9, ''),
(316, 60, 9, ''),
(317, 63, 9, ''),
(318, 67, 9, ''),
(319, 68, 9, ''),
(320, 69, 9, ''),
(321, 80, 9, ''),
(322, 81, 9, ''),
(323, 3, 23, 'Вадим Трунов'),
(324, 15, 23, ''),
(325, 16, 23, 'programmers'),
(326, 17, 23, '1'),
(327, 18, 23, 'trunov@lookmy.info'),
(328, 28, 23, ''),
(329, 46, 23, '1416224848_100825.jpg'),
(330, 47, 3, 'development'),
(331, 48, 3, '2014-09-01'),
(332, 49, 3, '2014-12-01'),
(333, 50, 3, ''),
(334, 51, 3, '32'),
(335, 52, 3, '<p>Разработка нового движка на базе фреймверка Yii</p>\r\n'),
(336, 53, 3, '1'),
(337, 55, 3, '23'),
(338, 20, 6, 'http://goldenplaza.com.ua/'),
(339, 21, 6, 'Харьков'),
(340, 22, 6, 'ул. Чернышевского, 85'),
(341, 33, 6, 'goldenplaza.com.ua@mail.ru'),
(342, 38, 6, '+38(066) 423-5088'),
(343, 39, 6, '1416152080_Снимок.JPG'),
(344, 47, 4, 'development'),
(345, 48, 4, ''),
(346, 49, 4, ''),
(347, 50, 4, ''),
(348, 51, 4, '32'),
(349, 52, 4, '<p>Доработки и исправление по сайту клиента</p>\r\n'),
(350, 53, 4, '1'),
(351, 55, 4, '23'),
(352, 80, 1, ''),
(353, 81, 1, ''),
(354, 80, 2, ''),
(355, 81, 2, ''),
(356, 81, 3, ''),
(357, 20, 7, 'http://vukraine.in.ua/'),
(358, 21, 7, 'Киев'),
(359, 22, 7, ''),
(360, 33, 7, ''),
(361, 39, 7, '1416170574_Снимок.JPG'),
(362, 47, 5, 'development'),
(363, 48, 5, '2014-11-16'),
(364, 49, 5, ''),
(365, 50, 5, ''),
(366, 51, 5, '32'),
(367, 52, 5, '<p>Этот проект касается непосредственно работам по соц. сети и api</p>\r\n'),
(368, 53, 5, '1'),
(369, 55, 5, '19'),
(370, 81, 4, ''),
(371, 3, 24, 'Ряхин Илья'),
(372, 5, 24, ''),
(373, 15, 24, ''),
(374, 16, 24, 'programmers'),
(375, 17, 24, '1'),
(376, 18, 24, 'ryahin@lookmy.info'),
(377, 28, 24, 'lookmy.info'),
(378, 46, 24, ''),
(379, 60, 5, ''),
(380, 80, 5, ''),
(381, 81, 5, ''),
(382, 80, 6, ''),
(383, 81, 6, ''),
(384, 81, 7, ''),
(385, 58, 10, '23.03.2015'),
(386, 59, 10, ''),
(387, 60, 10, ''),
(388, 63, 10, ''),
(389, 67, 10, ''),
(390, 68, 10, ''),
(391, 69, 10, ''),
(392, 80, 10, ''),
(393, 81, 10, ''),
(394, 58, 11, '23.03.2015'),
(395, 59, 11, ''),
(396, 60, 11, ''),
(397, 63, 11, ''),
(398, 67, 11, ''),
(399, 68, 11, ''),
(400, 69, 11, ''),
(401, 80, 11, ''),
(402, 81, 11, ''),
(403, 58, 12, '23.03.2015'),
(404, 59, 12, ''),
(405, 60, 12, ''),
(406, 63, 12, ''),
(407, 67, 12, ''),
(408, 68, 12, ''),
(409, 69, 12, ''),
(410, 80, 12, ''),
(411, 81, 12, ''),
(412, 3, 25, 'Юрий Дзюндзик'),
(413, 15, 25, ''),
(414, 16, 25, 'programmers'),
(415, 17, 25, '1'),
(416, 18, 25, 'sunderos@lookmy.info'),
(417, 28, 25, ''),
(418, 46, 25, ''),
(419, 58, 13, '23.03.2015'),
(420, 59, 13, ''),
(421, 60, 13, ''),
(422, 63, 13, ''),
(423, 67, 13, ''),
(424, 68, 13, ''),
(425, 69, 13, ''),
(426, 80, 13, ''),
(427, 81, 13, ''),
(428, 58, 14, '23.03.2015'),
(429, 59, 14, ''),
(430, 60, 14, ''),
(431, 63, 14, ''),
(432, 67, 14, ''),
(433, 68, 14, ''),
(434, 69, 14, ''),
(435, 80, 14, ''),
(436, 81, 14, ''),
(437, 58, 15, '23.03.2015'),
(438, 59, 15, ''),
(439, 60, 15, ''),
(440, 63, 15, ''),
(441, 67, 15, ''),
(442, 68, 15, ''),
(443, 69, 15, ''),
(444, 80, 15, ''),
(445, 81, 15, ''),
(446, 58, 16, '23.03.2015'),
(447, 59, 16, '11.03.2015'),
(448, 60, 16, ''),
(449, 63, 16, ''),
(450, 67, 16, ''),
(451, 68, 16, ''),
(452, 69, 16, ''),
(453, 80, 16, ''),
(454, 81, 16, ''),
(455, 58, 17, '23.03.2015'),
(456, 59, 17, '11.03.2015'),
(457, 60, 17, ''),
(458, 63, 17, ''),
(459, 67, 17, ''),
(460, 68, 17, ''),
(461, 69, 17, ''),
(462, 80, 17, ''),
(463, 81, 17, ''),
(464, 58, 18, '23.03.2015'),
(465, 59, 18, '24.03.2015'),
(466, 60, 18, ''),
(467, 63, 18, ''),
(468, 67, 18, ''),
(469, 68, 18, ''),
(470, 69, 18, ''),
(471, 80, 18, ''),
(472, 81, 18, ''),
(473, 58, 19, '23.03.2015'),
(474, 59, 19, '24.03.2015'),
(475, 60, 19, ''),
(476, 63, 19, ''),
(477, 67, 19, ''),
(478, 68, 19, ''),
(479, 69, 19, ''),
(480, 80, 19, ''),
(481, 81, 19, ''),
(482, 58, 20, '23.03.2015'),
(483, 59, 20, '24.03.2015'),
(484, 60, 20, ''),
(485, 63, 20, ''),
(486, 67, 20, ''),
(487, 68, 20, ''),
(488, 69, 20, ''),
(489, 80, 20, ''),
(490, 81, 20, ''),
(491, 58, 21, '23.03.2015'),
(492, 59, 21, '26.03.2015'),
(493, 60, 21, ''),
(494, 63, 21, ''),
(495, 67, 21, ''),
(496, 68, 21, ''),
(497, 69, 21, ''),
(498, 80, 21, ''),
(499, 81, 21, ''),
(500, 81, 8, ''),
(501, 58, 22, '24.03.2015'),
(502, 59, 22, ''),
(503, 60, 22, ''),
(504, 63, 22, ''),
(505, 67, 22, ''),
(506, 68, 22, ''),
(507, 69, 22, ''),
(508, 80, 22, ''),
(509, 81, 22, ''),
(510, 58, 23, '24.03.2015'),
(511, 59, 23, ''),
(512, 60, 23, ''),
(513, 63, 23, ''),
(514, 67, 23, ''),
(515, 68, 23, ''),
(516, 69, 23, ''),
(517, 80, 23, ''),
(518, 81, 23, ''),
(519, 82, 1, ''),
(520, 82, 2, ''),
(521, 82, 3, ''),
(522, 82, 4, ''),
(523, 20, 8, ''),
(524, 21, 8, 'Москва'),
(525, 22, 8, ''),
(526, 24, 8, 'client'),
(527, 26, 8, 'notSet'),
(528, 33, 8, 'rossigr@mail.ru'),
(529, 39, 8, '1416298814_Снимок.JPG'),
(530, 47, 6, 'development'),
(531, 48, 6, '2014-11-18'),
(532, 49, 6, '2015-04-01'),
(533, 50, 6, ''),
(534, 51, 6, ''),
(535, 52, 6, '<p>Создание портала и слайд-фильмов</p>\r\n'),
(536, 53, 6, '1'),
(537, 55, 6, '25'),
(538, 3, 26, 'Сергей Могильный'),
(539, 15, 26, 'test'),
(540, 16, 26, 'programmers'),
(541, 17, 26, '1'),
(542, 18, 26, 'sergey@lookmy.info'),
(543, 28, 26, ''),
(544, 46, 26, '1416300697_1305790961_2672120.jpg'),
(545, 5, 26, '+38(066) 103-4756'),
(547, 58, 24, '24.03.2015'),
(548, 59, 24, ''),
(549, 60, 24, ''),
(550, 63, 24, ''),
(551, 67, 24, ''),
(552, 68, 24, ''),
(553, 69, 24, ''),
(554, 80, 24, ''),
(555, 81, 24, ''),
(556, 56, 7, '1416300252'),
(557, 56, 8, '1416300612'),
(558, 3, 27, 'Ivan LookMy'),
(559, 15, 27, ''),
(560, 16, 27, 'programmers'),
(561, 17, 27, '1'),
(562, 18, 27, 'ivan@lookmy.info'),
(563, 28, 27, ''),
(564, 46, 27, ''),
(565, 58, 25, '24.03.2015'),
(566, 59, 25, ''),
(567, 60, 25, ''),
(568, 63, 25, ''),
(569, 67, 25, ''),
(570, 68, 25, ''),
(571, 69, 25, ''),
(572, 80, 25, ''),
(573, 81, 25, ''),
(574, 5, 27, ''),
(575, 58, 26, '24.03.2015'),
(576, 59, 26, ''),
(577, 60, 26, ''),
(578, 63, 26, ''),
(579, 67, 26, ''),
(580, 68, 26, ''),
(581, 69, 26, ''),
(582, 80, 26, ''),
(583, 81, 26, ''),
(584, 58, 27, '24.03.2015'),
(585, 59, 27, ''),
(586, 60, 27, ''),
(587, 63, 27, ''),
(588, 67, 27, ''),
(589, 68, 27, ''),
(590, 69, 27, ''),
(591, 80, 27, ''),
(592, 81, 27, ''),
(593, 20, 9, 'pooties.pingas.com'),
(594, 21, 9, 'Eggland'),
(595, 22, 9, 'Ark lab #13'),
(596, 24, 9, 'client'),
(597, 26, 9, 'notSet'),
(598, 33, 9, 'sergey@lookmy.info'),
(599, 38, 9, '+38(066) 111-2233'),
(600, 39, 9, '1425304624_gallery.gif.png'),
(601, 58, 28, '25.03.2015'),
(602, 59, 28, ''),
(603, 60, 28, ''),
(604, 63, 28, ''),
(605, 67, 28, ''),
(606, 68, 28, ''),
(607, 69, 28, ''),
(608, 80, 28, ''),
(609, 81, 28, ''),
(610, 58, 29, '25.03.2015'),
(611, 59, 29, ''),
(612, 60, 29, ''),
(613, 63, 29, ''),
(614, 67, 29, ''),
(615, 68, 29, ''),
(616, 69, 29, ''),
(617, 80, 29, ''),
(618, 81, 29, ''),
(619, 58, 30, '25.03.2015'),
(620, 59, 30, ''),
(621, 60, 30, ''),
(622, 63, 30, ''),
(623, 67, 30, ''),
(624, 68, 30, ''),
(625, 69, 30, ''),
(626, 80, 30, ''),
(627, 81, 30, ''),
(628, 58, 31, ''),
(629, 59, 31, ''),
(630, 60, 31, ''),
(631, 63, 31, ''),
(632, 67, 31, ''),
(633, 68, 31, ''),
(634, 69, 31, ''),
(635, 80, 31, ''),
(636, 81, 31, ''),
(637, 58, 32, '25.03.2015'),
(638, 59, 32, ''),
(639, 60, 32, ''),
(640, 63, 32, ''),
(641, 67, 32, ''),
(642, 68, 32, ''),
(643, 69, 32, ''),
(644, 80, 32, ''),
(645, 81, 32, ''),
(646, 58, 33, '26.03.2015'),
(647, 59, 33, ''),
(648, 60, 33, ''),
(649, 63, 33, ''),
(650, 67, 33, ''),
(651, 68, 33, ''),
(652, 69, 33, ''),
(653, 80, 33, ''),
(654, 81, 33, ''),
(655, 58, 34, '26.03.2015'),
(656, 59, 34, '16.03.2015'),
(657, 60, 34, ''),
(658, 63, 34, ''),
(659, 67, 34, ''),
(660, 68, 34, ''),
(661, 69, 34, ''),
(662, 80, 34, ''),
(663, 81, 34, ''),
(664, 58, 35, '27.03.2015'),
(665, 59, 35, ''),
(666, 60, 35, ''),
(667, 63, 35, ''),
(668, 67, 35, ''),
(669, 68, 35, ''),
(670, 69, 35, ''),
(671, 80, 35, ''),
(672, 81, 35, ''),
(673, 58, 36, '27.03.2015'),
(674, 59, 36, ''),
(675, 60, 36, ''),
(676, 63, 36, ''),
(677, 67, 36, ''),
(678, 68, 36, ''),
(679, 69, 36, ''),
(680, 80, 36, ''),
(681, 81, 36, ''),
(682, 58, 37, '27.03.2015'),
(683, 59, 37, ''),
(684, 60, 37, ''),
(685, 63, 37, ''),
(686, 67, 37, ''),
(687, 68, 37, ''),
(688, 69, 37, ''),
(689, 80, 37, ''),
(690, 81, 37, ''),
(691, 70, 5, '1416523053'),
(692, 70, 6, '1416523915'),
(693, 70, 7, '1416523957'),
(694, 58, 38, '27.03.2015'),
(695, 59, 38, ''),
(696, 60, 38, ''),
(697, 63, 38, ''),
(698, 67, 38, ''),
(699, 68, 38, ''),
(700, 69, 38, ''),
(701, 80, 38, ''),
(702, 81, 38, ''),
(703, 82, 5, ''),
(704, 82, 6, ''),
(705, 82, 7, ''),
(706, 82, 8, ''),
(707, 58, 39, '27.03.2015'),
(708, 59, 39, ''),
(709, 60, 39, ''),
(710, 63, 39, ''),
(711, 67, 39, ''),
(712, 68, 39, ''),
(713, 69, 39, ''),
(714, 80, 39, ''),
(715, 81, 39, ''),
(716, 82, 9, ''),
(717, 82, 10, ''),
(718, 82, 11, ''),
(719, 82, 12, ''),
(720, 82, 13, ''),
(721, 82, 14, ''),
(722, 82, 15, ''),
(723, 5, 22, ''),
(724, 5, 23, ''),
(725, 5, 25, ''),
(726, 15, 19, ''),
(727, 15, 20, ''),
(728, 15, 22, ''),
(729, 16, 22, ''),
(730, 22, 2, ''),
(731, 22, 4, ''),
(732, 27, 1, ''),
(733, 27, 2, ''),
(734, 27, 3, ''),
(735, 27, 4, ''),
(736, 27, 5, ''),
(737, 27, 6, ''),
(738, 27, 7, ''),
(739, 27, 8, ''),
(740, 27, 9, ''),
(741, 28, 1, ''),
(742, 28, 22, ''),
(743, 29, 1, ''),
(744, 29, 19, ''),
(745, 29, 20, ''),
(746, 29, 22, ''),
(747, 29, 23, ''),
(748, 29, 24, ''),
(749, 29, 25, ''),
(750, 29, 26, ''),
(751, 29, 27, ''),
(752, 30, 1, ''),
(753, 30, 19, ''),
(754, 30, 20, ''),
(755, 30, 22, ''),
(756, 30, 23, ''),
(757, 30, 24, ''),
(758, 30, 25, ''),
(759, 30, 26, ''),
(760, 30, 27, ''),
(761, 31, 1, ''),
(762, 31, 19, ''),
(763, 31, 20, ''),
(764, 31, 22, ''),
(765, 31, 23, ''),
(766, 31, 24, ''),
(767, 31, 25, ''),
(768, 31, 26, ''),
(769, 31, 27, ''),
(770, 32, 1, ''),
(771, 32, 19, ''),
(772, 32, 20, ''),
(773, 32, 22, ''),
(774, 32, 23, ''),
(775, 32, 24, ''),
(776, 32, 25, ''),
(777, 32, 26, ''),
(778, 32, 27, ''),
(779, 33, 3, ''),
(780, 33, 4, ''),
(781, 34, 1, ''),
(782, 34, 19, ''),
(783, 34, 20, ''),
(784, 34, 22, ''),
(785, 34, 23, ''),
(786, 34, 24, ''),
(787, 34, 25, ''),
(788, 34, 26, ''),
(789, 34, 27, ''),
(790, 35, 1, ''),
(791, 35, 19, ''),
(792, 35, 20, ''),
(793, 35, 22, ''),
(794, 35, 23, ''),
(795, 35, 24, ''),
(796, 35, 25, ''),
(797, 35, 26, ''),
(798, 35, 27, ''),
(799, 37, 1, ''),
(800, 37, 19, ''),
(801, 37, 20, ''),
(802, 37, 22, ''),
(803, 37, 23, ''),
(804, 37, 24, ''),
(805, 37, 25, ''),
(806, 37, 26, ''),
(807, 37, 27, ''),
(808, 38, 1, ''),
(809, 38, 3, ''),
(810, 38, 7, ''),
(811, 38, 8, ''),
(812, 45, 1, ''),
(813, 45, 19, ''),
(814, 45, 20, ''),
(815, 45, 22, ''),
(816, 45, 23, ''),
(817, 45, 24, ''),
(818, 45, 25, ''),
(819, 45, 26, ''),
(820, 45, 27, ''),
(821, 46, 20, ''),
(822, 46, 22, ''),
(823, 86, 1, ''),
(824, 86, 2, ''),
(825, 85, 17, ''),
(826, 82, 16, ''),
(827, 85, 22, ''),
(828, 82, 17, ''),
(829, 85, 27, ''),
(830, 82, 18, ''),
(831, 58, 40, '27.03.2015'),
(832, 59, 40, ''),
(833, 60, 40, ''),
(834, 63, 40, ''),
(835, 67, 40, ''),
(836, 68, 40, ''),
(837, 69, 40, ''),
(838, 80, 40, ''),
(839, 81, 40, ''),
(840, 82, 19, ''),
(841, 82, 20, ''),
(842, 82, 21, ''),
(843, 82, 22, ''),
(844, 82, 23, ''),
(845, 82, 24, ''),
(846, 82, 25, ''),
(847, 82, 26, ''),
(848, 82, 27, ''),
(849, 82, 28, ''),
(850, 82, 29, ''),
(851, 82, 30, ''),
(852, 82, 31, ''),
(853, 82, 32, ''),
(854, 82, 33, ''),
(855, 82, 34, ''),
(856, 82, 35, ''),
(857, 82, 36, ''),
(858, 82, 37, ''),
(859, 82, 38, ''),
(860, 82, 39, ''),
(861, 82, 40, ''),
(862, 82, 41, ''),
(863, 82, 42, ''),
(864, 82, 43, ''),
(865, 82, 44, ''),
(866, 82, 45, ''),
(867, 82, 46, ''),
(868, 82, 47, ''),
(869, 82, 48, ''),
(870, 82, 49, ''),
(871, 82, 50, ''),
(872, 82, 51, ''),
(873, 82, 52, ''),
(874, 82, 53, ''),
(875, 82, 54, ''),
(876, 82, 55, ''),
(877, 82, 56, ''),
(878, 82, 57, ''),
(879, 82, 58, ''),
(880, 82, 59, ''),
(881, 82, 60, ''),
(882, 82, 61, ''),
(883, 82, 62, ''),
(884, 82, 63, ''),
(885, 82, 64, ''),
(886, 82, 65, ''),
(887, 82, 66, ''),
(888, 82, 67, ''),
(889, 82, 68, ''),
(890, 82, 69, ''),
(891, 82, 70, ''),
(892, 82, 71, ''),
(893, 82, 72, ''),
(894, 82, 73, ''),
(895, 82, 74, ''),
(896, 85, 10, '23'),
(897, 82, 75, ''),
(898, 82, 76, ''),
(899, 82, 77, ''),
(900, 82, 78, ''),
(901, 82, 79, ''),
(902, 82, 80, ''),
(903, 82, 81, ''),
(904, 85, 25, ''),
(905, 82, 82, ''),
(909, 85, 4, '1'),
(910, 85, 5, '1'),
(911, 85, 6, '1'),
(912, 85, 7, ''),
(913, 85, 8, ''),
(914, 85, 9, ''),
(917, 85, 13, ''),
(918, 85, 14, ''),
(919, 85, 15, ''),
(920, 85, 16, ''),
(921, 85, 18, ''),
(922, 85, 19, ''),
(923, 85, 20, ''),
(924, 85, 21, ''),
(925, 85, 23, ''),
(926, 85, 24, ''),
(927, 85, 26, ''),
(928, 85, 39, ''),
(929, 85, 37, ''),
(930, 85, 38, ''),
(931, 85, 40, ''),
(932, 86, 19, ''),
(933, 86, 20, ''),
(934, 86, 22, ''),
(935, 86, 23, ''),
(936, 86, 24, ''),
(937, 86, 25, ''),
(938, 86, 26, ''),
(939, 86, 27, ''),
(940, 85, 11, '23'),
(941, 85, 12, '23'),
(942, 58, 41, '27.03.2015'),
(943, 59, 41, ''),
(944, 60, 41, ''),
(945, 63, 41, ''),
(946, 67, 41, ''),
(947, 68, 41, ''),
(948, 69, 41, ''),
(949, 80, 41, ''),
(950, 81, 41, ''),
(951, 58, 42, '20.02.2015'),
(952, 59, 42, '27.02.2015'),
(953, 60, 42, ''),
(954, 63, 42, ''),
(955, 67, 42, 'Тестbрование ERP'),
(956, 68, 42, ''),
(957, 69, 42, ''),
(958, 80, 42, ''),
(959, 81, 42, ''),
(960, 85, 1, ''),
(961, 85, 2, '1'),
(962, 56, 9, '1427122099'),
(963, 56, 10, '1427182725'),
(964, 56, 11, '1427193051'),
(965, 56, 12, '1427280157'),
(966, 56, 13, '1427281661'),
(967, 56, 14, '1427752592'),
(968, 56, 15, '1427752798'),
(969, 56, 16, '1427752960'),
(970, 56, 17, '1427752979'),
(971, 56, 18, '1427753007'),
(972, 56, 19, '1427753317'),
(973, 56, 20, '1427753405'),
(974, 56, 21, '1427753472'),
(975, 56, 22, '1427753483'),
(976, 56, 23, '1427753483'),
(977, 56, 24, '1427753483'),
(978, 56, 25, '1427753483'),
(979, 56, 26, '1427754200'),
(980, 56, 27, '1427754570'),
(981, 56, 28, '1427754599');

-- --------------------------------------------------------

--
-- Структура таблицы `detailsdictionary`
--

CREATE TABLE IF NOT EXISTS `detailsdictionary` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `parentId` int(10) unsigned NOT NULL,
  `data` text NOT NULL,
  `value` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `parentId` (`parentId`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=81 ;

--
-- Дамп данных таблицы `detailsdictionary`
--

INSERT INTO `detailsdictionary` (`id`, `parentId`, `data`, `value`) VALUES
(1, 16, 'Администраторы', 'admins'),
(5, 16, 'Менеджеры', 'managers'),
(6, 16, 'Программисты', 'programmers'),
(7, 16, 'Дизайнеры', 'designers'),
(8, 17, 'Активен', '1'),
(9, 17, 'Забанен', '0'),
(10, 26, 'ООО Сутдио Консалтинг', 'studio'),
(11, 26, 'ФЛП Анедченко О.И', 'flp'),
(12, 26, 'ЧП Анедченко А.С.', 'selfEmployed'),
(13, 24, 'Потенциальный клиент', 'potential '),
(14, 24, 'Наш клиент', 'client'),
(15, 24, 'Конкурент', 'competitor'),
(16, 26, ' Не указано', 'notSet'),
(17, 37, 'Контакт', 'contact'),
(18, 44, 'Входящее', 'incoming'),
(19, 44, 'Исходящее', 'outgoing'),
(20, 47, 'разработка', 'development'),
(21, 47, 'СЕО-оптимизация', 'seo'),
(22, 47, 'контекст', 'context'),
(23, 47, 'реклама', 'ads'),
(24, 47, 'поддержка', 'support'),
(30, 63, 'Поставлена', 'add'),
(31, 63, 'В процессе', 'execute'),
(32, 63, 'Выполнена', 'make'),
(33, 63, 'Проверена', 'apply'),
(34, 63, 'Приостановлено', 'pause'),
(35, 63, 'Отменена', 'stop'),
(36, 63, 'Просмотрена', 'view'),
(37, 68, 'Низкий', '1'),
(38, 68, 'Нормальный', '2'),
(39, 68, 'Высокий', '3'),
(40, 68, 'Горит!!!', '4'),
(41, 69, 'Верстка', 'layout'),
(42, 69, 'Программирование', 'programming'),
(43, 69, 'Разработка дизайна', 'design'),
(44, 69, 'Просчет', 'calculate'),
(45, 69, 'Запуск рекламы', 'add'),
(46, 69, 'Другое', 'other'),
(47, 68, 'Без приоритета', '-1'),
(48, 72, 'Наличный расчет', 'cash'),
(49, 72, 'безналичный платеж ОИ', 'nonecache1'),
(50, 72, 'безналичный платеж "Студио"', 'nonecache2'),
(51, 72, 'безналичный платеж АН', 'nonecache3'),
(52, 72, 'бартер', 'barter'),
(53, 72, 'Приват24', 'privat24'),
(54, 72, 'электронные деньги', 'webmoney'),
(55, 72, 'Другое', 'other'),
(56, 73, 'продажа сайта', 'sell'),
(57, 73, 'перенос на CMS', 'transfer'),
(58, 73, 'редизайн сайта', 'redesign'),
(59, 73, 'добавление дополнительных модулей', 'moduleAdd'),
(60, 73, 'обслуживание (редактирование сайта)', 'support'),
(61, 73, 'хостинг', 'hosting'),
(62, 73, 'регистрация доменного имени', 'domen'),
(63, 73, 'контекстная реклама', 'context'),
(64, 73, 'баннерная реклама в сети LookMy.info', 'bannerLookMy'),
(65, 73, 'баннерная реклама на других (чужих) площадках', 'bannerExternal'),
(66, 73, 'оптимизация сайта', 'seo'),
(67, 73, 'sms-рассылки', 'sms'),
(68, 73, 'SMO', 'smo'),
(69, 73, 'почта', 'mail'),
(70, 73, 'продажа сайта + обслуживание', 'sellAndSupport'),
(71, 73, 'регистрация в каталогах (1PS)', '1ps'),
(72, 73, 'заполнение сайта', 'editing'),
(73, 73, 'написание статей, рерайтинг', 'rewrite'),
(74, 73, 'контекстная реклама (настройка компании)', 'contextTuneUp'),
(75, 73, 'сервис пресс-релизов LookMy.info', 'pressInternal'),
(76, 73, 'сервис пресс-релизов (чужая услуга)', 'pressExternal'),
(77, 82, 'Информация', 'info'),
(78, 82, 'Успешное действие', 'success'),
(79, 82, 'Важное', 'warning'),
(80, 82, 'Внимание', 'danger');

-- --------------------------------------------------------

--
-- Структура таблицы `detailsparent`
--

CREATE TABLE IF NOT EXISTS `detailsparent` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `module` varchar(155) NOT NULL,
  `detailId` int(10) unsigned NOT NULL,
  `parentId` int(10) unsigned NOT NULL,
  `default` text NOT NULL,
  `display` int(11) NOT NULL,
  `multiple` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=88 ;

--
-- Дамп данных таблицы `detailsparent`
--

INSERT INTO `detailsparent` (`id`, `module`, `detailId`, `parentId`, `default`, `display`, `multiple`) VALUES
(3, 'User', 1, 0, '', 1, NULL),
(5, 'User', 3, 1, '', 1, 1),
(15, 'User', 4, 0, '', 1, NULL),
(16, 'User', 5, 0, '', 1, NULL),
(17, 'User', 6, 0, '', 1, NULL),
(18, 'User', 2, 0, '', 1, NULL),
(20, 'Company', 8, 0, '', 1, 0),
(21, 'Company', 9, 0, '', 1, 0),
(22, 'Company', 10, 0, '', 1, 0),
(24, 'Company', 12, 0, '', 1, 0),
(26, 'Company', 11, 0, '', 1, 0),
(27, 'Company', 14, 0, '', 1, 1),
(28, 'User', 15, 0, '', 1, 0),
(29, 'Contact', 16, 0, '', 1, 0),
(30, 'Contact', 1, 0, '', 1, 0),
(31, 'Contact', 3, 0, '', 1, 1),
(32, 'Contact', 4, 0, '', 1, 0),
(33, 'Company', 2, 0, '', 1, 0),
(34, 'Contact', 2, 0, '', 1, 0),
(35, 'Contact', 15, 0, '', 1, 0),
(37, 'Contact', 6, 0, '', 1, 0),
(38, 'Company', 3, 0, '', 1, 1),
(39, 'Company', 17, 0, '', 1, 0),
(40, 'Appeal', 18, 1, '', 1, 0),
(41, 'Appeal', 19, 1, '', 1, 0),
(42, 'Appeal', 20, 1, '', 1, 0),
(43, 'Appeal', 21, 1, '', 1, 0),
(44, 'Appeal', 22, 1, '', 1, 0),
(45, 'Contact', 23, 1, '', 1, 0),
(46, 'User', 23, 1, '', 1, 0),
(47, 'Project', 24, 1, '', 1, 0),
(48, 'Project', 25, 1, '', 1, 0),
(49, 'Project', 26, 1, '', 1, 0),
(50, 'Project', 27, 1, '', 1, 0),
(51, 'Project', 28, 1, '', 1, 0),
(52, 'Project', 29, 1, '', 1, 0),
(53, 'Project', 30, 1, '', 1, 0),
(55, 'Project', 31, 1, '', 1, 0),
(56, 'File', 32, 1, '', 1, 0),
(58, 'Task', 25, 1, '', 1, 0),
(59, 'Task', 26, 1, '', 1, 0),
(60, 'Task', 27, 1, '', 1, 0),
(63, 'Task', 34, 1, '', 1, 0),
(67, 'Task', 18, 1, '', 1, 0),
(68, 'Task', 35, 1, '', 1, 0),
(69, 'Task', 36, 1, '', 1, 0),
(70, 'Comments', 32, 1, '', 1, 0),
(71, 'Payment', 32, 1, '', 1, 0),
(72, 'Payment', 37, 1, '', 1, 0),
(73, 'Payment', 38, 1, '', 1, 0),
(74, 'Payment', 20, 1, '', 1, 0),
(75, 'Payment', 39, 1, '', 1, 0),
(77, 'Payment', 41, 1, '', 1, 0),
(79, 'Payment', 42, 1, '', 1, 0),
(80, 'Task', 43, 1, '', 1, 0),
(81, 'Task', 44, 1, '', 1, 0),
(82, 'Activity', 45, 1, '', 1, 0),
(84, 'ActivityRelations', 6, 1, '', 1, 0),
(85, 'Task', 46, 1, '', 1, 1),
(86, 'User', 47, 1, '', 1, 1),
(87, 'Messages', 48, 1, '', 1, 0);

-- --------------------------------------------------------

--
-- Структура таблицы `detailstypes`
--

CREATE TABLE IF NOT EXISTS `detailstypes` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `fieldName` varchar(100) NOT NULL,
  `type` varchar(155) NOT NULL,
  `validator` varchar(155) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=49 ;

--
-- Дамп данных таблицы `detailstypes`
--

INSERT INTO `detailstypes` (`id`, `name`, `fieldName`, `type`, `validator`) VALUES
(1, 'ФИО', 'name', 'activeTextField', 'required, length'),
(2, 'E-mail', 'mail', 'activeTextField', 'email, length'),
(3, 'Контактный телефон', 'phone', 'activePhoneInputWithButton', 'phone'),
(4, 'Skype', 'skype', 'activeTextField', 'length'),
(5, 'Группа', 'group', 'activeDropDownList', 'required'),
(6, 'Статус', 'status', 'activeDropDownList', 'required'),
(8, 'Сайт', 'site', 'activeTextField', 'length'),
(9, 'Город', 'city', 'activeTextField', 'length'),
(10, 'Адрес', 'address', 'activeTextField', 'length'),
(11, 'Юрлицо для счетов', 'bills', 'activeDropDownList', 'length'),
(12, 'Статус', 'status', 'activeDropDownList', 'length'),
(14, 'Контакт', 'contact', 'activeHiddenField', 'numerical'),
(15, 'Организация', 'company', 'activeHiddenField', 'length'),
(16, 'Должность', 'group', 'activeTextField', 'length'),
(17, 'Фото', 'photo', 'activeHiddenField', 'file'),
(18, 'Текст', 'text', 'activeEditField', 'length'),
(19, 'Дата создания', 'date', 'activeDateField', 'required, date'),
(20, 'Напомнить', 'remind', 'activeDateField', 'date'),
(21, 'С кем общались', 'contactPerson', 'activeDropDownList', 'length'),
(22, 'Тип обращения', 'type', 'activeRadioButtonList', 'required'),
(23, 'Аватар', 'avatar', 'activeFileField', 'file'),
(24, 'Тип проекта', 'type', 'activeDropDownList', 'length'),
(25, 'Дата начала', 'dateStart', 'activeDateField', 'date'),
(26, 'Дата окончания', 'dateEnd', 'activeDateField', 'date'),
(27, 'Бюджет', 'budget', 'activeTextField', 'numerical'),
(28, 'Оплата часа работы програмиста', 'pay', 'activeTextField', 'numerical'),
(29, 'Описание', 'description', 'activeStandardEditField', 'length'),
(30, 'Курирующий менеджер', 'manager', 'activeDropDownList', 'numerical'),
(31, 'Исполнитель по умолчанию', 'defaultWorker', 'activeDropDownList', 'numerical'),
(32, 'Время добавления', 'time', 'activeHiddenField', 'numerical'),
(34, 'Статус задачи', 'status', 'activeHiddenField', 'length'),
(35, 'Приоритет', 'priority', 'activeDropDownList', 'length'),
(36, 'Тип задачи', 'type', 'activeDropDownList', 'length'),
(37, 'Тип оплаты', 'type', 'activeDropDownList', 'length'),
(38, 'Назначение платежа', 'apointment', 'activeDropDownList', 'length'),
(39, 'Комментарий', 'comment', 'activeEditField', 'length'),
(41, 'Долг', 'debt', 'activeCheckBox', 'boolean'),
(42, 'Сформировать счет и отправить клиенту', 'sendBill', 'activeDropDownList', 'boolean'),
(43, 'Затраченое время', 'time', 'activeTextField', 'numerical'),
(44, 'Фактическая дата завершения', 'applyTime', 'activeHiddenField', 'date'),
(45, 'Тип', 'type', 'activeEditField', 'length'),
(46, 'Просмотры', 'usersThatView', 'activeTextField', 'numerical'),
(47, 'Сегодняшние задачи', 'todayTasks', 'activeTextField', 'length'),
(48, 'Тема', 'theme', 'activeTextField', 'length');

-- --------------------------------------------------------

--
-- Структура таблицы `file`
--

CREATE TABLE IF NOT EXISTS `file` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `targetId` int(10) unsigned NOT NULL,
  `moduleName` varchar(155) NOT NULL,
  `name` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `targetId` (`targetId`,`moduleName`)
) ENGINE=MyISAM  DEFAULT CHARSET=cp1251 AUTO_INCREMENT=29 ;

--
-- Дамп данных таблицы `file`
--

INSERT INTO `file` (`id`, `targetId`, `moduleName`, `name`) VALUES
(7, 24, 'Task', 'Знімок екрана з 2014-11-18 10:44:08.png'),
(5, 4, 'Task', 'About the system.pdf'),
(8, 24, 'Task', 'user_save.html'),
(6, 6, 'Task', 'About the system.pdf'),
(9, 21, 'Task', 'Дорбый день.doc'),
(12, 30, 'Task', 'Закат.jpg'),
(14, 0, 'Task', '1427752592_02 - Услуги - готово.png'),
(15, 0, 'Task', '1427752798_02 - Услуги - готово.png'),
(16, 0, 'Task', '1427752960_02 - Услуги - готово.png'),
(17, 0, 'Task', '1427752979_03 - Услуга - задумка страницы.jpg'),
(18, 0, 'Task', '1427753007_02 - Услуги - готово.png'),
(19, 0, 'Task', '1427753317_02 - Услуги - готово.png'),
(20, 0, 'Task', '1427753405_02 - Услуги - готово.png'),
(21, 0, 'Task', '1427753472_03 - Услуга - задумка страницы.jpg'),
(22, 0, 'Task', '1427753483_03 - Услуга - задумка страницы.jpg'),
(23, 0, 'Task', '1427753483_04 - Вариант представления телеканала на странице Услуга.jpg'),
(24, 0, 'Task', '1427753483_05 - Задумка портфолио.png'),
(25, 0, 'Task', '1427753483_ТЗ_краткое.txt'),
(26, 0, 'Task', '1427754200_02 - Услуги - готово.png'),
(27, 0, 'Task', '1427754570_design.psd'),
(28, 0, 'Task', '1427754599_design.psd');

-- --------------------------------------------------------

--
-- Структура таблицы `filter`
--

CREATE TABLE IF NOT EXISTS `filter` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `module` varchar(155) NOT NULL,
  `action` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=cp1251 AUTO_INCREMENT=3 ;

--
-- Дамп данных таблицы `filter`
--

INSERT INTO `filter` (`id`, `name`, `module`, `action`) VALUES
(1, 'компании', 'company', 'actionIndex'),
(2, 'Время добавления комментария', 'comments', 'actionList');

-- --------------------------------------------------------

--
-- Структура таблицы `filterfields`
--

CREATE TABLE IF NOT EXISTS `filterfields` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `filterId` int(10) unsigned NOT NULL,
  `model` varchar(155) NOT NULL,
  `attribute` varchar(255) NOT NULL,
  `type` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `filterId` (`filterId`)
) ENGINE=MyISAM  DEFAULT CHARSET=cp1251 AUTO_INCREMENT=11 ;

-- --------------------------------------------------------

--
-- Структура таблицы `items`
--

CREATE TABLE IF NOT EXISTS `items` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `userId` int(11) NOT NULL,
  `brandId` int(8) NOT NULL,
  `tittle` text NOT NULL,
  PRIMARY KEY (`id`),
  KEY `brandId` (`brandId`),
  KEY `userId` (`userId`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

--
-- Дамп данных таблицы `items`
--

INSERT INTO `items` (`id`, `userId`, `brandId`, `tittle`) VALUES
(1, 8, 0, '1'),
(2, 8, 0, '1'),
(3, 8, 0, '3'),
(4, 4, 0, 'Тестовый тест');

-- --------------------------------------------------------

--
-- Структура таблицы `like`
--

CREATE TABLE IF NOT EXISTS `like` (
  `id` int(8) NOT NULL,
  `itemId` int(8) NOT NULL,
  `userId` int(8) NOT NULL,
  `postId` int(8) NOT NULL DEFAULT '0',
  `ip` varchar(15) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `itemId` (`itemId`),
  KEY `userId` (`userId`),
  KEY `postId` (`postId`)
) ENGINE=MyISAM DEFAULT CHARSET=cp1251;

-- --------------------------------------------------------

--
-- Структура таблицы `orders`
--

CREATE TABLE IF NOT EXISTS `orders` (
  `id` int(11) NOT NULL,
  `userId` int(11) DEFAULT NULL,
  `status` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `userId` (`userId`)
) ENGINE=MyISAM DEFAULT CHARSET=cp1251;

-- --------------------------------------------------------

--
-- Структура таблицы `pages`
--

CREATE TABLE IF NOT EXISTS `pages` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `parent_id` int(10) NOT NULL,
  `title` varchar(250) NOT NULL,
  `description` text NOT NULL,
  PRIMARY KEY (`id`),
  KEY `title` (`title`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура таблицы `payment`
--

CREATE TABLE IF NOT EXISTS `payment` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `userId` int(10) unsigned NOT NULL,
  `companyId` int(10) unsigned NOT NULL,
  `projectId` int(10) unsigned NOT NULL,
  `amount` float unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `userId` (`userId`,`companyId`,`projectId`)
) ENGINE=MyISAM DEFAULT CHARSET=cp1251 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура таблицы `post`
--

CREATE TABLE IF NOT EXISTS `post` (
  `id` int(8) NOT NULL,
  `userId` int(8) NOT NULL,
  `itemId` int(8) NOT NULL,
  `ip` varchar(15) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `userId` (`userId`),
  KEY `itemId` (`itemId`)
) ENGINE=MyISAM DEFAULT CHARSET=cp1251;

-- --------------------------------------------------------

--
-- Структура таблицы `project`
--

CREATE TABLE IF NOT EXISTS `project` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `companyId` int(10) unsigned NOT NULL,
  `title` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `companyId` (`companyId`)
) ENGINE=MyISAM  DEFAULT CHARSET=cp1251 AUTO_INCREMENT=7 ;

--
-- Дамп данных таблицы `project`
--

INSERT INTO `project` (`id`, `companyId`, `title`) VALUES
(2, 1, 'newkernel'),
(3, 1, 'nCore'),
(4, 6, 'Интернет-магазин'),
(5, 7, 'разработка Соц. Сети'),
(6, 8, 'Коллекции аудиовизуального наследия');

-- --------------------------------------------------------

--
-- Структура таблицы `subscription`
--

CREATE TABLE IF NOT EXISTS `subscription` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура таблицы `task`
--

CREATE TABLE IF NOT EXISTS `task` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `companyId` int(10) unsigned NOT NULL,
  `projectId` int(10) unsigned NOT NULL DEFAULT '0',
  `authorId` int(10) unsigned NOT NULL,
  `responsibleId` int(10) unsigned NOT NULL,
  `parentId` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `projectId` (`projectId`),
  KEY `authorId` (`authorId`),
  KEY `responsibleId` (`responsibleId`),
  KEY `companyId` (`companyId`),
  KEY `parentId` (`parentId`)
) ENGINE=MyISAM  DEFAULT CHARSET=cp1251 AUTO_INCREMENT=42 ;

--
-- Дамп данных таблицы `task`
--

INSERT INTO `task` (`id`, `title`, `companyId`, `projectId`, `authorId`, `responsibleId`, `parentId`) VALUES
(2, 'Тестирование ERP', 0, 0, 23, 26, 0),
(3, 'Проверка вывода задач', 0, 0, 1, 23, 0),
(4, 'Тестируем', 0, 0, 1, 23, 0),
(5, 'тестирую Петр', 0, 0, 1, 1, 0),
(6, 'Сделать вкладку', 0, 0, 1, 1, 0),
(7, 'dsdsd', 0, 0, 1, 1, 0),
(8, 'dsadsdas', 0, 0, 1, 1, 0),
(9, 'mine task', 0, 0, 1, 1, 0),
(10, 'rerte', 0, 0, 1, 1, 0),
(11, 'new task', 0, 0, 1, 1, 0),
(12, 'new task', 0, 0, 1, 1, 0),
(13, 'new task', 0, 0, 1, 1, 0),
(14, 'new task', 0, 0, 1, 1, 0),
(15, 'new task', 0, 0, 1, 1, 0),
(16, 'new task', 0, 0, 1, 1, 0),
(17, 'new task', 0, 0, 1, 1, 0),
(18, 'new task', 0, 0, 1, 1, 0),
(19, 'new task', 0, 0, 1, 1, 0),
(20, 'new task', 0, 0, 1, 1, 0),
(21, 'new task', 0, 0, 1, 1, 0),
(22, '1', 0, 0, 1, 1, 0),
(23, '1', 0, 0, 1, 1, 0),
(24, 'new task', 0, 0, 1, 1, 0),
(25, 'mine task', 0, 0, 1, 1, 0),
(26, 'mine task', 0, 0, 1, 1, 0),
(27, 'уццу', 0, 0, 1, 1, 0),
(28, 'ntcn', 0, 0, 1, 1, 0),
(29, 'news', 0, 0, 1, 1, 0),
(30, 'dsfdsf', 0, 0, 1, 1, 0),
(31, 'yyy', 0, 0, 1, 19, 0),
(32, 'таск', 0, 0, 1, 1, 0),
(33, 'dsda', 0, 0, 1, 20, 0),
(34, 'bffggf', 0, 0, 1, 24, 0),
(35, '21312', 0, 0, 1, 1, 0),
(36, '21312', 0, 0, 1, 1, 0),
(37, '21312', 0, 0, 1, 1, 0),
(38, '121', 0, 0, 1, 1, 0),
(39, '121', 0, 0, 1, 1, 0),
(40, '121', 0, 0, 1, 1, 0),
(41, '121', 0, 0, 1, 1, 0);

-- --------------------------------------------------------

--
-- Структура таблицы `timesections`
--

CREATE TABLE IF NOT EXISTS `timesections` (
  `id` int(8) unsigned NOT NULL AUTO_INCREMENT,
  `userId` int(8) unsigned NOT NULL,
  `taskId` int(8) unsigned NOT NULL,
  `timeStart` int(11) DEFAULT NULL,
  `timeEnd` int(11) DEFAULT NULL,
  `time` varchar(5) DEFAULT NULL,
  `comment` text,
  PRIMARY KEY (`id`),
  KEY `userId` (`userId`,`taskId`)
) ENGINE=MyISAM  DEFAULT CHARSET=cp1251 AUTO_INCREMENT=25 ;

--
-- Дамп данных таблицы `timesections`
--

INSERT INTO `timesections` (`id`, `userId`, `taskId`, `timeStart`, `timeEnd`, `time`, `comment`) VALUES
(24, 23, 3, 1424509811, 1424509811, '1:00', NULL),
(23, 23, 3, 1424509808, 1424509808, '1:00', NULL),
(22, 23, 3, 1424509780, 1424509780, '1', NULL);

-- --------------------------------------------------------

--
-- Структура таблицы `user`
--

CREATE TABLE IF NOT EXISTS `user` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `login` varchar(155) NOT NULL,
  `password` varchar(155) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=28 ;

--
-- Дамп данных таблицы `user`
--

INSERT INTO `user` (`id`, `login`, `password`) VALUES
(1, 'admin', '$2a$13$rd0hbcruebkwvu.YSIB/r.nfKFqX0/3icdBoz2CNNvo.Sx87xNz5y'),
(19, 'mitich', '$2a$13$doEb5R0pjEKm2rQOLwotXeeTjcwp2eZiIxQUfNHf.2YqOlnLHSLwm'),
(20, 'anton', '$2a$13$/3xV.GGvN6DeCg4TI2tmOewSlmerPoO9XOvWm2KaJxWUr4D/S8/yK'),
(22, 'aleksandr', '$2a$13$6btL9IwvE6s6B2yRPS.cJ.a1kOQRngCjJvv9KY.xpEUKneqAE/zAO'),
(23, 'vadim', '$2a$13$Q5u30LlZKjsVdXxnjYXRIuhH0tj1M0i.DAqzOw1O2spozBtZZklAO'),
(24, 'ryahin', '$2a$13$dj2YiedBoewLGMbb72DAi.KL33N1A72Y1LBJtpy7pKTKUsQrZ7nme'),
(25, 'yuriy', '$2a$13$HO35hP2OR.IFrTzA77Y7heDl.CPFsJhKzDAfNalkzahk.uZ.cFDNS'),
(26, 'sergey', '$2a$13$JJ7m.X82epuBYGxwgSt6i.VpGFN6OnoPXFA2LtO068QrMdBAjl0Jy'),
(27, 'ivan', '$2a$13$q5rZox6Q2wxgmpJTLTVaL.ilkMeAcEdUzJa62hDNmgV/AsCepNoi.');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
